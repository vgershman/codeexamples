package pro.apptomato.miamir.ui.fragments;

import java.util.List;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.data.RestApi;
import pro.apptomato.miamir.data.partners.PartnersDto;
import pro.apptomato.miamir.ui.adapters.PartnersAdapter;
import pro.apptomato.module.rest.BaseAlertCallback;
import pro.apptomato.module.rest.Res;
import pro.apptomato.module.ui.base.BaseFragment;

/**
 * Created by user on 28.04.17.
 */

public class PartnersFragment extends BaseFragment {


    @Override
    protected void initViews() {
        loadData();
    }


    private void loadData() {
        RestApi.rest().partners().enqueue(new BaseAlertCallback<Res<List<PartnersDto>>>(getContext()) {
            @Override
            protected void onSuccess(Res<List<PartnersDto>> response) {
                if (response.getData() != null && response.getData().size() > 0) {
                    getAQ().id(R.id.gv).adapter(new PartnersAdapter(getContext(), response.getData()));
                }
            }
        });
    }
}

