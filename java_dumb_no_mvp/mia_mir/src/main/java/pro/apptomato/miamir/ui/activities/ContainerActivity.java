package pro.apptomato.miamir.ui.activities;


import pro.apptomato.miamir.app.Screens;
import pro.apptomato.miamir.ui.fragments.MainNewsFragment;
import pro.apptomato.module.ui.base.BaseFragmentActivity;
import pro.apptomato.module.ui.util.Screen;


/**
 * Created by user on 30.11.16.
 */

public class ContainerActivity extends BaseFragmentActivity {


    @Override
    public void init() {
        String id = getIntent().getStringExtra("id");
        try {
            long id_ = Long.parseLong(id);
            MainNewsFragment.sFromPush = id_;
        } catch (Exception ex) {
        }
    }

    @Override
    protected Screen defaultScreen() {
        return Screens.SPLASH;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            if (getCurrentFragment().onFragmentBackPressed()) {
                finish();
            }
        } else {
            super.onBackPressed();
        }
    }
}
