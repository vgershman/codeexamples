package pro.apptomato.miamir.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import pro.apptomato.miamir.app.Config;
import pro.apptomato.miamir.data.auth.AuthDto;
import pro.apptomato.miamir.data.auth.AuthManager;
import pro.apptomato.miamir.data.category.CategoryDto;
import pro.apptomato.miamir.data.news.NewsDto;
import pro.apptomato.miamir.data.partners.PartnersDto;
import pro.apptomato.miamir.data.region.RegionDto;
import pro.apptomato.miamir.data.upload.UploadDto;
import pro.apptomato.module.rest.Res;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by user on 24.10.16.
 */

public class RestApi {

    private static Api sApi;

    public static Api rest() {
        if (sApi == null) {
            sApi = provideApi();
        }
        return sApi;
    }


    private static Api provideApi() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        Interceptor tokenInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                if (!AuthManager.isAuth()) {
                    return chain.proceed(chain.request());
                }
                Request original = chain.request();
                Request request = original.newBuilder()
                        .header("Authorize", AuthManager.getAuthDto().getTo())
                        .header("UID", AuthManager.getAuthDto().getId())
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        };
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(tokenInterceptor).addInterceptor(interceptor).build();
        return new Retrofit.Builder()
                .baseUrl(Config.URL)
                .client(client)
                .addConverterFactory(buildGsonConverter())
                .build().create(Api.class);
    }


    private static GsonConverterFactory buildGsonConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson myGson = gsonBuilder.setLenient().create();
        return GsonConverterFactory.create(myGson);
    }


    public interface Api {

        @Headers("Authorize: " + Config.SECRET_KEY)
        @POST(Config.API + "login")
        Call<Res<AuthDto>> login(@Body HashMap<String, Object> params);

        @GET(Config.API + "?regions")
        Call<Res<List<RegionDto>>> regions();

        @GET(Config.API + "?categories")
        Call<Res<List<CategoryDto>>> categories();

        @GET(Config.API + "getNews?video=1")
        Call<Res<List<NewsDto>>> videos(@Query("limit") int limit, @Query("offset") int offset);

        @GET(Config.API + "getNews?hotmain=1")
        Call<Res<List<NewsDto>>> news(@Query("limit") int limit, @Query("offset") int offset);

        @GET(Config.API + "getNews")
        Call<Res<List<NewsDto>>> newsByCategory(@Query("catabbr") String category, @Query("limit") int limit, @Query("offset") int offset);

        @GET(Config.API + "getNews")
        Call<Res<List<NewsDto>>> newsByRegion(@Query("regabbr") String region, @Query("limit") int limit, @Query("offset") int offset);

        @GET(Config.API + "getNews")
        Call<Res<List<NewsDto>>> newsItem(@Query("id") long id);

        @GET(Config.API + "getNews?recomnews&limit=4")
        Call<Res<List<NewsDto>>> recommend(@Query("id") long id);

        @GET(Config.API + "login")
        Call<Res<String>> favor(@Query("addfav") boolean fav, @Query("id") long id);

        @GET(Config.API + "login?showfavs=true")
        Call<Res<List<NewsDto>>> getFav(@Query("limit") int limit, @Query("offset") int offset);

        @GET(Config.API + "login?subscribe=1")
        Call<Res<Object>> subscribe(@Query("email") String email);

        @GET(Config.API + "login?subscribe=0")
        Call<Res<Object>> unsubscribe();

        @GET(Config.API + "?displaypartners")
        Call<Res<List<PartnersDto>>> partners();

        @POST(Config.API + "push_token2db")
        Call<Res<Object>> pushToken(@Body HashMap<String, String> params);


        @POST(Config.API + "news")
        Call<Res<Object>> sendNews(@Body HashMap<String, Object> params);


        @POST(Config.API + "geocode")
        Call<Res<RegionDto>> geocode(@Body HashMap<String, Object> params);

        @Multipart
        @POST(Config.API + "upload")
        Call<Res<UploadDto>> uploadImages(@Part List<MultipartBody.Part> files);
    }
}
