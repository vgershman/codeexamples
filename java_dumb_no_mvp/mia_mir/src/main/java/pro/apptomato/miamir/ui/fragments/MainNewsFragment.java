package pro.apptomato.miamir.ui.fragments;

import java.util.List;

import pro.apptomato.miamir.app.Screens;
import pro.apptomato.miamir.data.RestApi;
import pro.apptomato.miamir.data.news.NewsDto;
import pro.apptomato.miamir.ui.activities.SlidrActivity;
import pro.apptomato.module.rest.BaseToastCallback;
import pro.apptomato.module.rest.Res;
import retrofit2.Call;

/**
 * Created by user on 29.04.17.
 */

public class MainNewsFragment extends NewsListFragment {


    public static long sFromPush = -1;

    @Override
    protected void initProperties() {
        super.initProperties();
        if (sFromPush != -1) {
            loadById(sFromPush);
        }
    }

    private void loadById(final long id_) {
        sFromPush = -1;
        RestApi.rest().newsItem(id_).enqueue(new BaseToastCallback<Res<List<NewsDto>>>(getContext()) {
            @Override
            protected void onSuccess(Res<List<NewsDto>> response) {
                try {
                    openScreen(Screens.NEWS_ITEM).argSer("item", response.getData().get(0)).result(02).activity(SlidrActivity.class).apply();
                } catch (Exception ex) {
                }
            }
        });
    }

    @Override
    protected Call loadCall() {
        return RestApi.rest().news(LIMIT, getAdapter().getCount());
    }
}
