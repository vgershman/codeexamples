package pro.apptomato.miamir.data.news;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.List;

import pro.apptomato.miamir.app.Config;

/**
 * Created by user on 29.04.17.
 */

public class NewsDto implements Serializable {

    private long id;
    private String title;
    private String pic1, pic2, preview;
    private String date;
    private String regname;
    private String catabbr;
    private String text;
    private List<String> album;
    private boolean faved;
    private String youtube;

    public String getYoutube() {
        return youtube;
    }

    public boolean isVideo() {
        return !TextUtils.isEmpty(youtube);
    }

    public boolean isFaved() {
        return faved;
    }

    public void setFaved(boolean faved) {
        this.faved = faved;
    }

    public String getCatabbr() {
        return catabbr;
    }

    public String getText() {
        return text;
    }

    public String getLink() {
        return "http://миамир.рф/" + catabbr + "/" + id;
    }


    public String getDate() {
        return date;
    }

    public String getRegname() {
        return regname;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPic1() {
        return pic1;
    }

    public String getPic2() {
        return pic2;
    }

    public String getPreview() {
        return preview;
    }

    public List<String> getAlbum() {
        return album;
    }

    public String getBestPic() {
        if (!TextUtils.isEmpty(pic2)) {
            return pic2;
        }
        if (!TextUtils.isEmpty(pic1)) {
            return pic1;
        }
        return "empty";
    }
}
