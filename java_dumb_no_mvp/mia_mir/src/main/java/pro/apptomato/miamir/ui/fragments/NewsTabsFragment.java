package pro.apptomato.miamir.ui.fragments;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.app.Screens;
import pro.apptomato.miamir.data.category.CategoryManager;
import pro.apptomato.module.ui.fragments.base.BaseTabsFragment;
import pro.apptomato.module.ui.util.Screen;

/**
 * Created by user on 28.04.17.
 */

public class NewsTabsFragment extends BaseTabsFragment {

    @Override
    protected void initViews() {
    }


    @Override
    protected String[] listTitles() {
        try {
            String[] titles = new String[CategoryManager.get().size() + 2];
            for (int i = 2; i < CategoryManager.get().size() + 2; i++) {
                titles[i] = CategoryManager.get().get(i - 2).getName();
            }
            titles[0] = "В регионе";
            titles[1] = "Главные";
            return titles;
        } catch (Exception ex) {
            getActivity().finish();
            return new String[]{"Error"};
        }
    }

    @Override
    protected Screen[] listScreens() {
        try {
            List<Screen> screenList = new ArrayList<>();
            screenList.add(Screens.REGION_NEWS);
            screenList.add(Screens.MAIN_NEWS);
            for (int i = 0; i < CategoryManager.get().size(); i++) {
                Screen screen = Screen.create(CategoryNewsFragment.class);
                Bundle args = new Bundle();
                args.putSerializable("category", CategoryManager.get().get(i));
                screen.setBundle(args);
                screenList.add(screen);
            }
            return screenList.toArray(new Screen[screenList.size()]);
        } catch (Exception ex) {
            getActivity().finish();
            return new Screen[]{Screens.MAIN_NEWS};
        }
    }

    @Override
    protected void initProperties() {
        super.initProperties();
        int tab = getArguments().getInt("tab", 1);
        if (tab != -1) {
            openTab(tab);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            getTabsAdapter().getItem(getViewPager().getCurrentItem()).onRequestPermissionsResult(requestCode, permissions, grantResults);
        } catch (Exception ex) {
            String exm = ex.getMessage() + "";
        }
    }

    public void openTab(int tab) {
        getViewPager().setCurrentItem(tab, true);
    }
}
