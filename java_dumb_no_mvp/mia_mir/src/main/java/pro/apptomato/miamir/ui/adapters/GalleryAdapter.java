package pro.apptomato.miamir.ui.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import pro.apptomato.miamir.R;
import pro.apptomato.module.utils.Utils;


public class GalleryAdapter extends PagerAdapter {

    private Context context;
    private List<String> mImages;


    public GalleryAdapter(Context context, List<String> images) {
        this.context = context;
        mImages = images;
    }

    @Override
    public Object instantiateItem(View collection, final int position) {
        final View v;
        v = LayoutInflater.from(context).inflate(R.layout.item_gallery, null, false);
        String image = mImages.get(position);
        if (TextUtils.isEmpty(image)) {
            image = "empty";
        }
        bindPaging(image, v);
        ((ViewPager) collection).addView(v, 0);
        return v;
    }

    private void bindPaging(String url_, final View v) {
        int width = (int) Utils.getScreenWidth(context);
        int height = (int) (Utils.getScreenWidth(context) * 490 * 1.f / 850 * 1.f);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) v.findViewById(R.id.iv).getLayoutParams();
        lp.height = height;
        lp.width = width;
        v.findViewById(R.id.iv).setLayoutParams(lp);
        Picasso.with(context).load(url_).resize(width, height).into((ImageView) v.findViewById(R.id.iv));
    }


    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }


    @Override
    public int getCount() {
        return mImages.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
//    public int getMinHeight() {
//        float screenWidth = Utils.getScreenWidth(context);
//        int minHeight = Integer.MAX_VALUE;
//        for (GiftDto.Image image : mImages) {
//            float itemHeight;
//            if (image.getHeight() > image.getWidth()) {
//                float itemWidth = (screenWidth * 0.94f) / 2;
//                itemHeight = itemWidth / image.getWidth() * image.getHeight();
//            } else {
//                float itemWidth = (screenWidth * 0.96f);
//                itemHeight = itemWidth / image.getWidth() * image.getHeight();
//            }
//            if (itemHeight < minHeight) {
//                minHeight = (int) itemHeight;
//            }
//        }
//        return minHeight;
//    }
}