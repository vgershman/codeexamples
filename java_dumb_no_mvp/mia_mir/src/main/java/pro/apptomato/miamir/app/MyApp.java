package pro.apptomato.miamir.app;


import android.app.Application;

import com.yandex.metrica.YandexMetrica;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by user on 20.12.16.
 */

public class MyApp extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(getApplicationContext());
        YandexMetrica.activate(getApplicationContext(), Config.YANDEX);
        YandexMetrica.enableActivityAutoTracking(this);
        YandexMetrica.setReportCrashesEnabled(true);
        YandexMetrica.setReportNativeCrashesEnabled(true);
    }
}
