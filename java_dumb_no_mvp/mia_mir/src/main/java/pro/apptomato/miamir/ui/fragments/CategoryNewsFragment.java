package pro.apptomato.miamir.ui.fragments;

import pro.apptomato.miamir.data.RestApi;
import pro.apptomato.miamir.data.category.CategoryDto;
import retrofit2.Call;

/**
 * Created by user on 29.04.17.
 */

public class CategoryNewsFragment extends NewsListFragment {

    CategoryDto mCategory;

    @Override
    protected Call loadCall() {
        return RestApi.rest().newsByCategory(mCategory.getCatabbr(), LIMIT, getAdapter().getCount());
    }

    @Override
    protected void initProperties() {
        mCategory = (CategoryDto) getArguments().getSerializable("category");
        if (mCategory != null) {
            super.initProperties();
        }
    }
}
