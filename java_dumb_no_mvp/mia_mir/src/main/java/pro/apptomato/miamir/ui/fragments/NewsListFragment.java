package pro.apptomato.miamir.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.gson.internal.LinkedTreeMap;
import com.r0adkll.slidr.Slidr;

import java.util.List;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.app.Screens;
import pro.apptomato.miamir.data.RestApi;
import pro.apptomato.miamir.data.news.NewsDto;
import pro.apptomato.miamir.data.news.NewsWrapper;
import pro.apptomato.miamir.ui.activities.ContainerActivity;
import pro.apptomato.miamir.ui.activities.SlidrActivity;
import pro.apptomato.miamir.ui.adapters.NewsAdapter;
import pro.apptomato.module.rest.BaseAlertCallback;
import pro.apptomato.module.rest.Res;
import pro.apptomato.module.ui.base.BaseFragment;
import retrofit2.Call;

/**
 * Created by user on 28.04.17.
 */

public abstract class NewsListFragment extends BaseFragment implements NewsAdapter.FavListener {


    protected static final int LIMIT = 30;
    private NewsAdapter mAdapter;
    private SwipeRefreshLayout mSWR;
    private TextView mFooter;
    private boolean mAllDataLoaded = false;


    public NewsAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    protected void initViews() {
        View footer = LayoutInflater.from(getContext()).inflate(R.layout.footer_more, null, false);
        mFooter = (TextView) footer.findViewById(R.id.tv_more);
        mFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadData();
            }
        });
        mSWR = (SwipeRefreshLayout) getAQ().id(R.id.swr).getView();
        mSWR.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAllDataLoaded = false;
                mAdapter.clear();
                loadData();
            }
        });
        if (getMockId() != -1) {
            View mock = LayoutInflater.from(getContext()).inflate(getMockId(), null, false);
            ((FrameLayout) getAQ().id(R.id.mock_container).getView()).addView(mock);
            initMock(mock);
        }
        mAdapter = new NewsAdapter(getContext(), this);
        View listPadding = LayoutInflater.from(getContext()).inflate(R.layout.list_padding, null, false);
        getAQ().id(R.id.lv).getListView().addHeaderView(listPadding);
        getAQ().id(R.id.lv).getListView().addFooterView(footer);
        getAQ().id(R.id.lv).getListView().addFooterView(listPadding);
        getAQ().id(R.id.lv).adapter(mAdapter).itemClicked(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    if (i > 0) {
                        openScreen(Screens.NEWS_ITEM).argSer("item", mAdapter.getItem(i - 1)).result(02).activity(SlidrActivity.class).apply();
                    }
                } catch (Exception ex) {
                }
            }
        });
    }

    @Override
    protected void initProperties() {
        super.initProperties();
        initCallback();
        loadData();
    }

    private void loadData() {
        mFooter.setVisibility(View.GONE);
        if (!mAllDataLoaded) {
            mSWR.setRefreshing(true);
            loadCall().enqueue(mLoadCallback);
        }
    }

    protected int getMockId() {
        return -1;
    }

    protected void initMock(View mock) {
    }

    protected abstract Call loadCall();

    private BaseAlertCallback<Res<List<NewsDto>>> mLoadCallback = null;

    private void initCallback() {
        mLoadCallback = new BaseAlertCallback<Res<List<NewsDto>>>(getActivity()) {
            @Override
            protected void onSuccess(Res<List<NewsDto>> response) {
                if (response.getData().size() > 0) {
                    getAQ().id(R.id.swr).visible();
                    getAQ().id(R.id.mock_container).gone();
                    mAdapter.addAll(response.getData());
                } else {
                    getAQ().id(R.id.swr).invisible();
                    getAQ().id(R.id.mock_container).visible();
                }
                if (response.getData().size() < LIMIT) {
                    mAllDataLoaded = true;
                    mFooter.setVisibility(View.GONE);
                } else {
                    mAllDataLoaded = false;
                    mFooter.setVisibility(View.VISIBLE);
                }
                mSWR.setRefreshing(false);
            }

            @Override
            protected void onError() {
                super.onError();
                getAQ().id(R.id.swr).visible();
                mSWR.setRefreshing(false);
            }

            @Override
            protected void onConnectionFailure() {
                super.onConnectionFailure();
                getAQ().id(R.id.swr).visible();
                mSWR.setRefreshing(false);
            }
        };
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 02 && NewsItemFragment.sWrapper != null) {
            mAdapter.update();
        }
    }

    @Override
    protected boolean hasToolbar() {
        return false;
    }

    @Override
    protected int getContainerView() {
        return R.layout.fragment_news_list;
    }

    @Override
    public void onToggle(final NewsDto item) {
        RestApi.rest().favor(!item.isFaved(), item.getId()).enqueue(new BaseAlertCallback<Res<String>>(getContext()) {
            @Override
            protected void onSuccess(Res<String> response) {
                if (response.isSuccess()) {
                    item.setFaved(!item.isFaved());
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }
}
