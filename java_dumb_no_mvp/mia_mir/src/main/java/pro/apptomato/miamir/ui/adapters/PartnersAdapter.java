package pro.apptomato.miamir.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.data.partners.PartnersDto;
import pro.apptomato.module.utils.Utils;


/**
 * A sample Adapter that demonstrates one use of the SectionableAdapter class,
 * using a hard-coded two-dimensional array for the data source.
 *
 * @author Velos Mobile
 */
/*
 * Copyright � 2012 Velos Mobile
 */
public class PartnersAdapter extends SectionableAdapter {

    private List<PartnersDto> mData;

    private Context mContext;

    public PartnersAdapter(Context context, List<PartnersDto> data) {
        super(LayoutInflater.from(context), R.layout.row_partner, R.id.grid_header, R.id.grid_holder, MODE_VARY_WIDTHS);
        mContext = context;
        mData = data;
    }

    @Override
    public String getItem(int position) {
        for (int i = 0; i < mData.size(); ++i) {
            if (position < mData.get(i).getPartners().size()) {
                return mData.get(i).getPartners().get(position);
            }
            position -= mData.get(i).getPartners().size();
        }
        return null;
    }

    @Override
    protected int getDataCount() {
        int total = 0;
        for (int i = 0; i < mData.size(); ++i) {
            total += mData.get(i).getPartners().size();
        }
        return total;
    }

    @Override
    protected int getSectionsCount() {
        try {
            return mData.size();
        } catch (Exception ex) {
            return 0;
        }
    }

    @Override
    protected int getCountInSection(int index) {
        return mData.get(index).getPartners().size();
    }

    @Override
    protected int getTypeFor(int position) {
        int runningTotal = 0;
        int i = 0;
        for (i = 0; i < mData.size(); ++i) {
            int sectionCount = mData.get(i).getPartners().size();
            if (position < runningTotal + sectionCount)
                return i;
            runningTotal += sectionCount;
        }
        // This will never happen.
        return -1;
    }

    @Override
    protected String getHeaderForSection(int section) {
        return mData.get(section).getName();
    }

    @Override
    protected void bindView(View convertView, int position) {
        String url = getItem(position);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) convertView.findViewById(R.id.iv).getLayoutParams();
        int size = (int) (Utils.getScreenWidth(mContext) / 2);
        lp.width = size;
        lp.height = size;
        convertView.findViewById(R.id.iv).setLayoutParams(lp);
        Picasso.with(mContext).load(url).into((ImageView) convertView.findViewById(R.id.iv));
    }


    public void setData(List<PartnersDto> data) {
        mData = data;
        notifyDataSetChanged();
    }
}
