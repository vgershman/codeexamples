package pro.apptomato.miamir.ui.fragments;

import pro.apptomato.miamir.data.RestApi;
import retrofit2.Call;

/**
 * Created by user on 28.04.17.
 */

public class VideoFragment extends NewsListFragment {


    @Override
    protected Call loadCall() {
        return RestApi.rest().videos(LIMIT, getAdapter().getCount());
    }
}
