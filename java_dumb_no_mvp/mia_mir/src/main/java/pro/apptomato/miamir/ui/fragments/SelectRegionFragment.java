package pro.apptomato.miamir.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.data.RestApi;
import pro.apptomato.miamir.data.region.RegionDto;
import pro.apptomato.miamir.data.region.RegionManager;
import pro.apptomato.miamir.ui.adapters.RegionAdapter;
import pro.apptomato.module.rest.BaseAlertCallback;
import pro.apptomato.module.rest.BaseToastCallback;
import pro.apptomato.module.rest.Res;
import pro.apptomato.module.ui.base.BaseFragment;
import pro.apptomato.module.utils.LocationUtils;
import pro.apptomato.module.utils.PermissionUtils;

/**
 * Created by user on 28.04.17.
 */

public class SelectRegionFragment extends BaseFragment {

    private RegionAdapter mAdapter;

    private RegionDto mRegion;

    private boolean mExact;

    @Override
    protected void initProperties() {
        super.initProperties();
        mRegion = (RegionDto) getArguments().getSerializable("region");
        mExact = getArguments().getBoolean("exact", false);
        proceed();
    }

    private void proceed() {
        mAdapter = new RegionAdapter(getContext(), mExact, mRegion);
        View header = LayoutInflater.from(getContext()).inflate(R.layout.header_select_region, null, false);
        header.findViewById(R.id.btn_find_me).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findMe();
            }
        });
        getAQ().id(R.id.lv).getListView().addHeaderView(header);
        getAQ().id(R.id.lv).adapter(mAdapter);
        loadData();
    }

    @Override
    protected void initViews() {
    }

    private void loadData() {
        RestApi.rest().regions().enqueue(new BaseAlertCallback<Res<List<RegionDto>>>(getContext()) {
            @Override
            protected void onSuccess(Res<List<RegionDto>> response) {
                mAdapter.clear();
                mAdapter.add(RegionDto.FAKE);
                mAdapter.addAll(response.getData());
            }
        });
    }

    private void findMe() {
        if (PermissionUtils.hasGeoPermission(getActivity())) {
            LocationUtils.getLocation(getActivity(), new LocationUtils.LocationListener() {
                @Override
                public void onLocationReceived(Location location) {
                    geocode(location);
                }

                @Override
                public void onError() {
                    Toast.makeText(getContext(), "Что-то пошло не так!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNeedEnable() {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 07);
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 07) {
            findMe();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        findMe();
    }

    private void geocode(Location location) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("lat", location.getLatitude());
        params.put("lng", location.getLongitude());
        RestApi.rest().geocode(params).enqueue(new BaseToastCallback<Res<RegionDto>>(getContext()) {
            @Override
            protected void onSuccess(Res<RegionDto> response) {
                Toast.makeText(getContext(), "Ваш регион " + response.getData().getRegname(), Toast.LENGTH_SHORT).show();
                if (mExact) {
                    Intent data = new Intent();
                    data.putExtra("region", response.getData());
                    ((Activity) (getContext())).setResult(Activity.RESULT_OK, data);
                    ((Activity) (getContext())).finish();
                } else {
                    RegionManager.setCurrent(getContext(), response.getData());
                    getActivity().onBackPressed();
                }
            }

            @Override
            protected void onError() {
                Toast.makeText(getContext(), "Не удалось определить регион", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected boolean hasToolbar() {
        return true;
    }

    @Override
    protected int getTitleId() {
        return R.string.title_select_region;
    }
}
