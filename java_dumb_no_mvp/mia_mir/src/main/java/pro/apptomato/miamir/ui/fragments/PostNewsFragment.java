package pro.apptomato.miamir.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pro.apptomato.miamir.R;
import pro.apptomato.miamir.app.Screens;
import pro.apptomato.miamir.data.RestApi;
import pro.apptomato.miamir.data.auth.AuthManager;
import pro.apptomato.miamir.data.category.CategoryDto;
import pro.apptomato.miamir.data.category.CategoryManager;
import pro.apptomato.miamir.data.region.RegionDto;
import pro.apptomato.miamir.data.region.RegionManager;
import pro.apptomato.miamir.data.settings.SettingsManager;
import pro.apptomato.miamir.data.upload.UploadDto;
import pro.apptomato.miamir.ui.activities.ContainerActivity;
import pro.apptomato.module.rest.BaseAlertCallback;
import pro.apptomato.module.rest.Res;
import pro.apptomato.module.ui.base.BaseFragment;
import pro.apptomato.module.utils.FileUtils;
import pro.apptomato.module.utils.PermissionUtils;
import pro.apptomato.module.utils.Utils;

/**
 * Created by user on 28.04.17.
 */

public class PostNewsFragment extends BaseFragment {

    private CategoryDto mCategory;

    private RegionDto mRegion;

    private List<Uri> mFiles = new ArrayList();

    @Override
    protected void initViews() {
        bindInfo();
        if (RegionManager.isSelected()) {
            mRegion = RegionManager.getCurrent();
        }
        bindCategory();
        getAQ().id(R.id.btn_category).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectCategory();
            }
        });
        bindRegion();
        getAQ().id(R.id.btn_select_region).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openScreen(Screens.REGION).argBool("exact", true).argSer("region", mRegion).activity(ContainerActivity.class).result(01).apply();
            }
        });
        getActivity().findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send();
            }
        });
        getActivity().findViewById(R.id.btn_attach).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAttach();
            }
        });
    }


    private void checkAttach() {
        if (PermissionUtils.hasImagePermission(getActivity())) {
            attach();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        checkAttach();
    }

    private void attach() {
        try {
            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getIntent.setType("image/*");
            Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.setType("image/*");
            Intent chooserIntent = Intent.createChooser(getIntent, "Выбрать фото");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});
            startActivityForResult(chooserIntent, 04);
        } catch (Exception ex) {
        }
    }

    private void bindInfo() {
        if (AuthManager.isAuth() && AuthManager.getAuthDto().isSubscribed()) {
            getAQ().id(R.id.et_email).text(AuthManager.getAuthDto().getEmail());
        }
        if (!TextUtils.isEmpty(SettingsManager.getName())) {
            getAQ().id(R.id.et_name).text(SettingsManager.getName());
        }
        if (!TextUtils.isEmpty(SettingsManager.getPhone())) {
            getAQ().id(R.id.et_phone).text(SettingsManager.getPhone());
        }
    }

    private void send() {
        hideKeyboard();
        String name = getAQ().id(R.id.et_name).getEditable().toString();
        if (name.length() < 3) {
            Toast.makeText(getContext(), "Имя должно быть длиннее", Toast.LENGTH_SHORT).show();
            return;
        }
        String email = getAQ().id(R.id.et_email).getEditable().toString();
        if (!Utils.validEmail(email)) {
            Toast.makeText(getContext(), "E-mail введен неверно", Toast.LENGTH_SHORT).show();
            return;
        }
        String phone = getAQ().id(R.id.et_phone).getEditable().toString();
//        if (phone.length() < "89213456789".length()) {
//            Toast.makeText(getContext(), "Телефон введен неверно", Toast.LENGTH_SHORT).show();
//            return;
//        }
        SettingsManager.setNamePhone(getActivity(), name, phone);
        if (mRegion == null) {
            Toast.makeText(getContext(), "Нужно выбрать регион", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mCategory == null) {
            Toast.makeText(getContext(), "Нужно выбрать рубрику", Toast.LENGTH_SHORT).show();
            return;
        }
        String newsItem = getAQ().id(R.id.et_news).getEditable().toString();
        if (newsItem.length() < 10) {
            Toast.makeText(getContext(), "Новость слишком короткая!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (mFiles.size() > 0) {
            uploadFiles(name, email, phone, newsItem);
        } else {
            createNewsItem(name, email, phone, newsItem, Collections.EMPTY_LIST);
        }
    }

    private void uploadFiles(final String name, final String email, final String phone, final String newsItem) {
        final ProgressDialog progress = ProgressDialog.show(getContext(), "", "Загрузка изображений...");
        List<MultipartBody.Part> parts = new ArrayList<>();
        for (Uri file : mFiles) {
            try {
                parts.add(prepareFilePart("files[" + (mFiles.indexOf(file) + 1) + "]", file));
            } catch (Exception ex) {
            }
        }
        RestApi.rest().uploadImages(parts).enqueue(new BaseAlertCallback<Res<UploadDto>>(getContext()) {
            @Override
            protected void onSuccess(Res<UploadDto> response) {
                progress.cancel();
                createNewsItem(name, email, phone, newsItem, response.getData().getImages());
            }

            @Override
            protected void onError() {
                super.onError();
                progress.cancel();
            }

            @Override
            protected void onConnectionFailure() {
                super.onConnectionFailure();
                progress.cancel();
            }
        });
    }


    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        File file = FileUtils.getFile(getContext(), fileUri);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse(getActivity().getContentResolver().getType(fileUri)), file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    private void createNewsItem(String name, String email, String phone, String newsItem, List<String> files) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("newsauthorinfo", name + ", email: " + email + ", phone: " + phone);
        params.put("newreg", mRegion.getId() + "");
        params.put("newcateg", mCategory.getId() + "");
        params.put("newtitle", "title");
        params.put("newtext", newsItem);
        if (mFiles != null && mFiles.size() > 0) {
            params.put("newimages", files);
        }
        RestApi.rest().sendNews(params).enqueue(new BaseAlertCallback<Res<Object>>(getContext()) {
            @Override
            protected void onSuccess(Res<Object> response) {
                Toast.makeText(getContext(), "Новость успешно отправлена!", Toast.LENGTH_LONG).show();
                clear();
                mFiles.clear();
                bindImages();
                hideKeyboard();
            }
        });
    }

    private void clear() {
        getAQ().id(R.id.et_news).text("");
    }

    private void hideKeyboard() {
        getAQ().id(R.id.et_fake).getEditText().requestFocus();
        Utils.hideSoftKeyboard(getAQ().id(R.id.et_email).getView(), getContext());
    }

    private void bindRegion() {
        getAQ().id(R.id.btn_select_region).text(mRegion != null ? mRegion.getRegname() : "Не выбран");
    }

    private void bindCategory() {
        if (mCategory != null) {
            getAQ().id(R.id.btn_category).text(getNormalName(mCategory.getName()));
        } else {
            getAQ().id(R.id.btn_category).text("Не выбрана");
        }
    }


    private void selectCategory() {
        CharSequence[] items = new CharSequence[CategoryManager.get().size()];
        int i = 0;
        for (CategoryDto cat : CategoryManager.get()) {
            items[i] = getNormalName(cat.getName());
            i++;
        }
        new AlertDialog.Builder(getContext()).setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mCategory = CategoryManager.get().get(i);
                bindCategory();
                dialogInterface.cancel();
            }
        }).create().show();
    }


    public static String getNormalName(String allCaps) {
        try {
            String firstLetter = allCaps.substring(0, 1);
            String body = allCaps.substring(1, allCaps.length()).toLowerCase();
            return firstLetter + body;
        } catch (Exception ex) {
            return "";
        }
    }

    private void bindImages() {
        LinearLayout ll = (LinearLayout) getAQ().id(R.id.container_images).getView();
        ll.removeAllViews();
        for (final Uri uri : mFiles) {
            View item = LayoutInflater.from(getContext()).inflate(R.layout.item_image, null, false);
            item.findViewById(R.id.btn_delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mFiles.remove(uri);
                    bindImages();
                }
            });
            Picasso.with(getContext()).load(uri).into((ImageView) item.findViewById(R.id.iv_image));
            ll.addView(item);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 01 && resultCode == Activity.RESULT_OK) {
            try {
                mRegion = (RegionDto) data.getSerializableExtra("region");
            } catch (Exception ex) {
            }
        }
        if (requestCode == 04 && resultCode == Activity.RESULT_OK) {
            try {
                mFiles.add(data.getData());
                bindImages();
            } catch (Exception ex) {
            }
        }
        bindRegion();
    }
}
