package pro.apptomato.miamir.data.auth;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by user on 03.05.17.
 */

public class AuthDto implements Serializable {

    private String to;
    private String id;

    private String email;

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTo() {
        return to;
    }

    public boolean isSubscribed() {
        return !TextUtils.isEmpty(email);
    }


    public String getEmail() {
        return email;
    }

    public String getId() {
        return id;
    }
}
