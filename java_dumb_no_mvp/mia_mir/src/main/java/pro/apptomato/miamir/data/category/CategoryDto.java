package pro.apptomato.miamir.data.category;

import java.io.Serializable;

/**
 * Created by user on 28.04.17.
 */

public class CategoryDto implements Serializable{

    private int id;
    private String catabbr;
    private String name;

    public int getId() {
        return id;
    }


    public String getCatabbr() {
        return catabbr;
    }

    public String getName() {
        return name;
    }
}
