package pro.apptomato.miamir.ui.adapters;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import java.util.ArrayList;
import java.util.List;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.data.news.NewsDto;
import pro.apptomato.miamir.data.settings.SettingsManager;
import pro.apptomato.miamir.ui.fragments.NewsItemFragment;

/**
 * Created by user on 02.05.17.
 */

public class RecommendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;

    private NewsDto mCurrent;

    private Listener mListener;

    private NewsAdapter.FavListener mFavListener;

    private boolean mVideoIsSet = false;


    private NewsItemFragment mFragment;

    public RecommendAdapter(NewsItemFragment fragment, NewsDto current, Listener listener, NewsAdapter.FavListener favListener) {
        mFragment = fragment;
        mContext = fragment.getContext();
        mCurrent = current;
        mListener = listener;
        mFavListener = favListener;
    }

    private List<NewsDto> mData = new ArrayList<>();

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            return new ViewHolderBody(LayoutInflater.from(mContext).inflate(R.layout.item_news, parent, false));
        } else {
            return new ViewHolderHeader(LayoutInflater.from(mContext).inflate(R.layout.item_details, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderBody) {
            ViewHolderBody vh = (ViewHolderBody) holder;
            final NewsDto item = mData.get(position);
            try {
                vh.tv_title.setText(Html.fromHtml(item.getTitle()));
            } catch (Exception ex) {
                vh.tv_title.setText("");
            }
            vh.tv_meta.setText(NewsAdapter.getDate(item.getDate()) + "  |  " + item.getRegname());
            vh.tv_time.setText(NewsAdapter.getTime(item.getDate()));
            vh.root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onItemClicked(item);
                    }
                }
            });
        }
        if (holder instanceof ViewHolderHeader) {
            ViewHolderHeader vh = (ViewHolderHeader) holder;
            try {
                vh.tv_title.setText(Html.fromHtml(mCurrent.getTitle()));
            } catch (Exception ex) {
                vh.tv_title.setText("");
            }
            vh.tv_meta.setText(NewsAdapter.getDate(mCurrent.getDate()) + "  |  " + mCurrent.getRegname());
            vh.tv_time.setText(NewsAdapter.getTime(mCurrent.getDate()));
            vh.btn_favor.setSelected(mCurrent.isFaved());
            vh.btn_favor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mFavListener.onToggle(mCurrent);
                }
            });
            if (TextUtils.isEmpty(mCurrent.getText())) {
                vh.tv_content.setText("");
            } else {
                vh.tv_content.setTextSize(TypedValue.COMPLEX_UNIT_SP, SettingsManager.geActualTextSize());
                try {
                    vh.tv_content.setText(Html.fromHtml(mCurrent.getText().replaceAll("<img.+/(img)*>", "")));
                } catch (Exception ex) {
                    vh.tv_content.setText(" ");
                }
            }
            if (mCurrent.isVideo()) {
                initVideo(vh);
            }
        }
    }

    private void initVideo(ViewHolderHeader vh) {
        if (!mVideoIsSet) {
            mVideoIsSet = true;
            vh.youtube.setVisibility(View.VISIBLE);
            YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
            FragmentTransaction transaction = mFragment.getChildFragmentManager().beginTransaction();
            youTubePlayerFragment.setRetainInstance(true);
            transaction.replace(R.id.youtube_layout, youTubePlayerFragment).commit();
            youTubePlayerFragment.initialize(mContext.getString(R.string.youtube_key), new YouTubePlayer.OnInitializedListener() {

                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
                    try {
                        if (!wasRestored) {
                            player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                            player.loadVideo(mCurrent.getYoutube());
                            player.play();
                        }
                    } catch (Exception ex) {
                    }
                }

                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult error) {
                    try {
                        String errorMessage = error.toString();
                        Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();
                    } catch (Exception ex) {
                    }
                }
            });
        }
    }

    public void clear() {
        mData.clear();
    }

    public void addAll(List<NewsDto> data) {
        mData.addAll(data);
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return 1;
        }
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setCurrent(NewsDto current) {
        mCurrent = current;
        notifyDataSetChanged();
    }

    public static class ViewHolderBody extends RecyclerView.ViewHolder {

        TextView tv_title, tv_meta, tv_time;
        View root;

        public ViewHolderBody(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.tv_text);
            tv_meta = (TextView) view.findViewById(R.id.tv_meta);
            tv_time = (TextView) view.findViewById(R.id.tv_time);
            view.findViewById(R.id.btn_favor).setVisibility(View.GONE);
            view.findViewById(R.id.iv_preview).setVisibility(View.GONE);
            root = view;
        }
    }

    public static class ViewHolderHeader extends RecyclerView.ViewHolder {

        TextView tv_title, tv_content, tv_meta, tv_time;
        ImageView btn_favor;
        View youtube;

        public ViewHolderHeader(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_content = (TextView) view.findViewById(R.id.tv_content);
            tv_meta = (TextView) view.findViewById(R.id.tv_meta);
            tv_time = (TextView) view.findViewById(R.id.tv_time);
            btn_favor = (ImageView) view.findViewById(R.id.btn_favor);
            youtube = view.findViewById(R.id.youtube_layout);
        }
    }

    public interface Listener {
        void onItemClicked(NewsDto newsDto);
    }
}
