package pro.apptomato.miamir.ui.fragments;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.data.RestApi;
import retrofit2.Call;

/**
 * Created by user on 01.05.17.
 */

public class FavoriteFragment extends NewsListFragment {
    @Override
    protected Call loadCall() {
        return RestApi.rest().getFav(LIMIT, getAdapter().getCount());
    }

    @Override
    protected int getMockId() {
        return R.layout.mock_favorite;
    }
}
