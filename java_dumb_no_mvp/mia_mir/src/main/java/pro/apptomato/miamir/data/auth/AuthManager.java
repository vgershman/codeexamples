package pro.apptomato.miamir.data.auth;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.UUID;

import pro.apptomato.miamir.data.RestApi;
import pro.apptomato.module.rest.BaseToastCallback;
import pro.apptomato.module.rest.Res;

import static android.content.Context.TELEPHONY_SERVICE;

/**
 * Created by user on 03.05.17.
 */

public class AuthManager {

    private static AuthDto sAuthDto = null;

    public static AuthDto getAuthDto() {
        return sAuthDto;
    }

    public static void setAuthDto(Context context, AuthDto authDto) {
        sAuthDto = authDto;
        pushSetup(context);
    }

    static void pushSetup(Context context) {
        String token = null;
        try {
            token = FirebaseInstanceId.getInstance().getToken();
        } catch (Exception ex) {
        }
        if (token != null) {
            sendToken(context, token);
        }
    }

    public static void sendToken(Context context, String token) {
        if (AuthManager.isAuth()) {
            HashMap<String, String> params = new HashMap<>();
            params.put("push_token", token);
            RestApi.rest().pushToken(params).enqueue(new BaseToastCallback<Res<Object>>(context) {
                @Override
                protected void onSuccess(Res<Object> response) {
                }
            });
        }
    }

    public static boolean isAuth() {
        return sAuthDto != null;
    }

    public static String getAppVersion(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (Exception ex) {
            return "unknown";
        }
    }

    public static String getUnique(Context context) {
        String uniqueId = PreferenceManager.getDefaultSharedPreferences(context).getString("unique", null);
        if (uniqueId != null) {
            return uniqueId;
        }
        uniqueId = UUID.randomUUID().toString();
        String androidId = "android_id";
        try {
            androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID) + "";
        } catch (Exception ex) {
        }
//        String imei = "imei";
//        try {
//            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
//            imei = telephonyManager.getDeviceId() + "";
//        } catch (Exception ex) {
//        }
        String serial = Build.SERIAL + "";
        try {
            UUID deviceUuid = UUID.nameUUIDFromBytes((androidId + serial).getBytes());
            uniqueId = deviceUuid.toString();
        } catch (Exception ex) {
        }
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("unique", uniqueId).apply();
        return uniqueId;
    }
}
