package pro.apptomato.miamir.ui.fragments;

import android.os.Build;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import pro.apptomato.miamir.app.Screens;
import pro.apptomato.miamir.data.RestApi;
import pro.apptomato.miamir.data.auth.AuthDto;
import pro.apptomato.miamir.data.auth.AuthManager;
import pro.apptomato.miamir.data.category.CategoryDto;
import pro.apptomato.miamir.data.category.CategoryManager;
import pro.apptomato.miamir.data.region.RegionManager;
import pro.apptomato.miamir.data.settings.SettingsManager;
import pro.apptomato.miamir.ui.activities.DrawerActivity;
import pro.apptomato.module.rest.BaseAlertCallback;
import pro.apptomato.module.rest.Res;
import pro.apptomato.module.ui.base.BaseFragment;

/**
 * Created by user on 28.04.17.
 */

public class SplashFragment extends BaseFragment {

    @Override
    protected void initViews() {
    }

    @Override
    protected void initProperties() {
        super.initProperties();
        RegionManager.init(getContext());
        SettingsManager.init(getContext());
        auth();
    }

    private void loadCategories() {
        RestApi.rest().categories().enqueue(new BaseAlertCallback<Res<List<CategoryDto>>>(getContext(), true) {
            @Override
            protected void onSuccess(Res<List<CategoryDto>> response) {
                if (response.getData() != null && response.getData().size() > 0) {
                    CategoryManager.set(response.getData());
                    openScreen(Screens.NEWS_TABS).activity(DrawerActivity.class).finish().apply();
                } else {
                    Toast.makeText(getContext(), "Не удалось загрузить категории", Toast.LENGTH_LONG).show();
                    getActivity().finish();
                }
            }
        });
    }

    private void auth() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("app_version", AuthManager.getAppVersion(getContext()));
        params.put("manufacturer", Build.MANUFACTURER);
        params.put("model", Build.MODEL);
        params.put("nickname", AuthManager.getUnique(getContext()));
        RestApi.rest().login(params).enqueue(new BaseAlertCallback<Res<AuthDto>>(getContext(), true) {
            @Override
            protected void onSuccess(Res<AuthDto> response) {
                AuthManager.setAuthDto(getActivity(), response.getData());
                loadCategories();
            }
        });
    }
}
