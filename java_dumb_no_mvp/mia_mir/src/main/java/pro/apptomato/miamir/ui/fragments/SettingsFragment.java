package pro.apptomato.miamir.ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.app.Screens;
import pro.apptomato.miamir.data.RestApi;
import pro.apptomato.miamir.data.auth.AuthManager;
import pro.apptomato.miamir.data.region.RegionManager;
import pro.apptomato.miamir.data.settings.SettingsManager;
import pro.apptomato.miamir.ui.activities.ContainerActivity;
import pro.apptomato.module.rest.BaseAlertCallback;
import pro.apptomato.module.rest.Res;
import pro.apptomato.module.ui.base.BaseFragment;
import pro.apptomato.module.utils.Utils;

/**
 * Created by user on 28.04.17.
 */

public class SettingsFragment extends BaseFragment {


    @Override
    protected void initViews() {
        getAQ().id(R.id.container).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
            }
        });
        bindPush();
        bindRegion();
        bindFont();
        ((Switch) getAQ().id(R.id.sw_subscribe).getView()).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (!checked && AuthManager.isAuth() && AuthManager.getAuthDto().isSubscribed()) {
                    showConfirmUnsubscribe();
                }
                if (!checked) {
                    hideKeyboard();
                }
                getAQ().id(R.id.ll_subs).visibility(checked ? View.VISIBLE : View.GONE);
            }
        });
        getAQ().id(R.id.btn_select_region).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openScreen(Screens.REGION).activity(ContainerActivity.class).result(01).apply();
            }
        });
        getAQ().id(R.id.btn_font).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFontSelector();
            }
        });
        getAQ().id(R.id.et_email).getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    subscribe();
                    return true;
                }
                return false;
            }
        });
        bindSubscribe();
    }

    private void bindPush() {
        getAQ().id(R.id.sw_push).checked(SettingsManager.isPushEnabled());
        ((Switch) getAQ().id(R.id.sw_push).getView()).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                SettingsManager.setPushEnabled(getContext(), checked);
            }
        });
    }

    private void hideKeyboard() {
        getAQ().id(R.id.et_fake).getEditText().requestFocus();
        Utils.hideSoftKeyboard(getAQ().id(R.id.et_email).getView(), getContext());
    }

    private void bindFont() {
        getAQ().id(R.id.btn_font).text(SettingsManager.getTextSize() + "%");
    }

    private void showFontSelector() {
        final CharSequence[] items = new CharSequence[]{"70%", "80%", "90%", "100%", "110%", "120%", "130%", "140%", "150%", "200%"};
        new AlertDialog.Builder(getContext()).setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selectFont(items[i].toString());
                dialogInterface.cancel();
            }
        }).create().show();
    }

    private void selectFont(String item) {
        int percent = 100;
        try {
            percent = Integer.parseInt(item.replaceAll("%", ""));
        } catch (Exception ex) {
        }
        SettingsManager.setTextSize(getContext(), percent);
        bindFont();
    }

    private void showConfirmUnsubscribe() {
        new AlertDialog.Builder(getContext()).setMessage("Уверены, что хотите отписаться?").setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getAQ().id(R.id.sw_subscribe).checked(true);
                dialogInterface.cancel();
            }
        }).setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                unsubscribe();
                dialogInterface.cancel();
            }
        }).create().show();
    }

    private void unsubscribe() {
        RestApi.rest().unsubscribe().enqueue(new BaseAlertCallback<Res<Object>>(getContext()) {
            @Override
            protected void onSuccess(Res<Object> response) {
                AuthManager.getAuthDto().setEmail(null);
                bindSubscribe();
            }
        });
    }

    private void subscribe() {
        final String email = getAQ().id(R.id.et_email).getEditable().toString();
        if (Utils.validEmail(email)) {
            RestApi.rest().subscribe(email).enqueue(new BaseAlertCallback<Res<Object>>(getContext()) {
                @Override
                protected void onSuccess(Res<Object> response) {
                    hideKeyboard();
                    AuthManager.getAuthDto().setEmail(email);
                    bindSubscribe();
                }
            });
        } else {
            bindEmailError();
        }
    }

    private void bindEmailError() {
        Toast.makeText(getContext(), "E-mail введен неверно!", Toast.LENGTH_SHORT).show();
    }

    private void bindSubscribe() {
        if (AuthManager.isAuth() && AuthManager.getAuthDto().isSubscribed()) {
            getAQ().id(R.id.sw_subscribe).checked(true);
            getAQ().id(R.id.et_email).text(AuthManager.getAuthDto().getEmail());
            getAQ().id(R.id.et_email).getEditText().setSelection(AuthManager.getAuthDto().getEmail().length());
        } else {
            getAQ().id(R.id.sw_subscribe).checked(false);
            getAQ().id(R.id.et_email).text("");
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 01) {
            bindRegion();
        }
    }

    @Override
    protected int getContainerView() {
        return R.layout.fragment_settings;
    }

    private void bindRegion() {
        getAQ().id(R.id.btn_select_region).text(RegionManager.isSelected() ? RegionManager.getCurrent().getRegname() : "Не выбран");
    }
}
