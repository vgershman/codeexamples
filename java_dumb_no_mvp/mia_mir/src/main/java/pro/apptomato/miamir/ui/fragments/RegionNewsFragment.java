package pro.apptomato.miamir.ui.fragments;

import android.content.Intent;
import android.hardware.Camera;
import android.location.Location;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.app.Screens;
import pro.apptomato.miamir.data.RestApi;
import pro.apptomato.miamir.data.region.RegionDto;
import pro.apptomato.miamir.data.region.RegionManager;
import pro.apptomato.miamir.ui.activities.ContainerActivity;
import pro.apptomato.module.rest.BaseToastCallback;
import pro.apptomato.module.rest.Res;
import pro.apptomato.module.utils.LocationUtils;
import pro.apptomato.module.utils.PermissionUtils;
import retrofit2.Call;

/**
 * Created by user on 29.04.17.
 */

public class RegionNewsFragment extends NewsListFragment {

    @Override
    protected Call loadCall() {
        return RestApi.rest().newsByRegion(RegionManager.getCurrent().getRegabbr(), LIMIT, getAdapter().getCount());
    }

    @Override
    protected int getMockId() {
        return R.layout.mock_region;
    }

    @Override
    protected void initProperties() {
        checkRegion();
    }

    private void checkRegion() {
        if (RegionManager.isSelected()) {
            getAQ().id(R.id.mock_container).gone();
            super.initProperties();
        } else {
            getAQ().id(R.id.mock_container).visible();
        }
    }

    @Override
    protected void initMock(View mock) {
        super.initMock(mock);
        mock.findViewById(R.id.btn_select_region).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openScreen(Screens.REGION).activity(ContainerActivity.class).result(01).apply();
            }
        });
        mock.findViewById(R.id.btn_find_me).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findMe();
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 01) {
            checkRegion();
        }
        if (requestCode == 07) {
            findMe();
        }
    }


    private void findMe() {
        if (PermissionUtils.hasGeoPermission(getActivity())) {
            LocationUtils.getLocation(getActivity(), new LocationUtils.LocationListener() {
                @Override
                public void onLocationReceived(Location location) {
                    geocode(location);
                }

                @Override
                public void onError() {
                    Toast.makeText(getContext(), "Что-то пошло не так!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNeedEnable() {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 07);
                }
            });
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        findMe();
    }

    private void geocode(Location location) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("lat", location.getLatitude());
        params.put("lng", location.getLongitude());
        RestApi.rest().geocode(params).enqueue(new BaseToastCallback<Res<RegionDto>>(getContext()) {
            @Override
            protected void onSuccess(Res<RegionDto> response) {
                Toast.makeText(getContext(), "Ваш регион " + response.getData().getRegname(), Toast.LENGTH_SHORT).show();
                RegionManager.setCurrent(getContext(), response.getData());
                checkRegion();
            }

            @Override
            protected void onError() {
                Toast.makeText(getContext(), "Не удалось определить регион", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
