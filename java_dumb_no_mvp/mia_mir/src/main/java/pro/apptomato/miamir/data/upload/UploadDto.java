package pro.apptomato.miamir.data.upload;

import java.io.Serializable;
import java.util.List;

/**
 * Created by user on 11.05.17.
 */

public class UploadDto implements Serializable {
    private List<String> images;

    public List<String> getImages() {
        return images;
    }
}
