package pro.apptomato.miamir.data.settings;

import android.content.Context;
import android.preference.PreferenceManager;

import static pro.apptomato.module.utils.PrefUtils.store;

/**
 * Created by user on 04.05.17.
 */

public class SettingsManager {

    private static int sTextSize;
    private static boolean sPushEnabled;

    private static String sName, sPhone;


    public static void init(Context context) {
        sTextSize = PreferenceManager.getDefaultSharedPreferences(context).getInt("font", 100);
        sPushEnabled = PreferenceManager.getDefaultSharedPreferences(context).getInt("push", 1) == 1;
        sName = PreferenceManager.getDefaultSharedPreferences(context).getString("name", "");
        sPhone = PreferenceManager.getDefaultSharedPreferences(context).getString("phone", "");
    }

    public static String getName() {
        return sName;
    }

    public static String getPhone() {
        return sPhone;
    }

    public static void setNamePhone(Context context, String name, String phone) {
        sName = name;
        sPhone = phone;
        store(context, "name", name);
        store(context, "phone", phone);
    }

    public static int getTextSize() {
        return sTextSize;
    }

    public static float geActualTextSize() {
        return 14 * sTextSize * 1.f / 100;
    }

    public static void setTextSize(final Context context, int textSize) {
        sTextSize = textSize;
        store(context, "font", textSize);
    }

    public static boolean isPushEnabled() {
        return sPushEnabled;
    }

    public static void setPushEnabled(Context context, boolean enabled) {
        sPushEnabled = enabled;
        store(context, "push", enabled ? 1 : 0);
    }
}
