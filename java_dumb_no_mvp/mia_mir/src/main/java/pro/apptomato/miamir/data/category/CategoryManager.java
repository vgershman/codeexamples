package pro.apptomato.miamir.data.category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 28.04.17.
 */

public class CategoryManager {

    private static List<CategoryDto> sCategories = new ArrayList<>();


    public static List<CategoryDto> get() {
        return sCategories;
    }


    public static void set(List<CategoryDto> categories) {
        sCategories = categories;
    }
}
