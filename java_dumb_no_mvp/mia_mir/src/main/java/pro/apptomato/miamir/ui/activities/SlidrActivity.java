package pro.apptomato.miamir.ui.activities;


import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;

import pro.apptomato.miamir.R;


/**
 * Created by user on 30.11.16.
 */

public class SlidrActivity extends ContainerActivity {


    @Override
    public void init() {
        SlidrConfig config = new SlidrConfig.Builder()
                .edge(true)
                .build();
        Slidr.attach(this, config);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_container;
    }
}
