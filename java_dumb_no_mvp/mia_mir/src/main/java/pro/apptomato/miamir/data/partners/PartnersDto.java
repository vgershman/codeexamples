package pro.apptomato.miamir.data.partners;

import java.io.Serializable;
import java.util.List;

/**
 * Created by user on 01.05.17.
 */

public class PartnersDto implements Serializable {

    private String name;
    private List<String> images;


    public String getName() {
        return name;
    }

    public List<String> getPartners() {
        return images;
    }
}
