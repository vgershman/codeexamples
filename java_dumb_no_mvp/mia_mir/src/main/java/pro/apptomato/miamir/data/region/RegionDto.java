package pro.apptomato.miamir.data.region;

import java.io.Serializable;

/**
 * Created by user on 01.05.17.
 */

public class RegionDto implements Serializable {

    private int id;
    private String regname;
    private String regabbr;

    public RegionDto(int id, String regname, String regabbr) {
        this.id = id;
        this.regname = regname;
        this.regabbr = regabbr;
    }

    public int getId() {
        return id;
    }

    public String getRegname() {
        return regname;
    }

    public String getRegabbr() {
        return regabbr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegionDto regionDto = (RegionDto) o;
        return id == regionDto.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public static RegionDto FAKE = new RegionDto(-1, "Не выбран", "");
}
