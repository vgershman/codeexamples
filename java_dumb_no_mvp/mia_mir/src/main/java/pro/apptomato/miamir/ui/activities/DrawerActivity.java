package pro.apptomato.miamir.ui.activities;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.app.Screens;
import pro.apptomato.miamir.ui.fragments.NewsTabsFragment;
import pro.apptomato.module.ui.base.BaseFragment;
import pro.apptomato.module.ui.base.BaseFragmentActivity;
import pro.apptomato.module.ui.util.OpenScreenQuery;
import pro.apptomato.module.ui.util.Screen;
import pro.apptomato.module.utils.IntentUtils;


/**
 * Created by user on 04.04.17.
 */

public class DrawerActivity extends BaseFragmentActivity {

    private static final int TIME_INTERVAL = 1500;
    private long mBackPressed;

    private final int MENU_PREFIX = 696969;

    private DrawerLayout drawer;
    private Toolbar mToolbar;

    @Override
    public void init() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        initToolbar();
        initMenu();
    }


    @Override
    protected Screen defaultScreen() {
        return Screens.NEWS_TABS;
    }


    private void initToolbar() {
        mToolbar.setNavigationIcon(R.drawable.icon_nb_menu);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        mToolbar.setTitle(R.string.title_feed);
    }


    private void initMenu() {
        ((NavigationView) getAQ().id(R.id.navigation).getView()).setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull android.view.MenuItem item) {
                drawer.closeDrawer(Gravity.START);
                BaseFragment current = getCurrentFragment();
                if (current instanceof NewsTabsFragment && (item.getItemId() == R.id.nav_main || item.getItemId() == R.id.nav_region)) {
                    ((NewsTabsFragment) current).openTab(item.getItemId() == R.id.nav_main ? 1 : 0);
                    mToolbar.setTitle(item.getItemId() == R.id.nav_main ? R.string.menu_main_news : R.string.menu_region);
                    item.setChecked(true);
                    return false;
                }
                OpenScreenQuery osq = null;
                switch (item.getItemId()) {
                    case R.id.nav_main:
                        osq = openScreen(Screens.NEWS_TABS).argInt("tab", 1);
                        mToolbar.setTitle(R.string.title_feed);
                        break;
                    case R.id.nav_video:
                        osq = openScreen(Screens.VIDEO);
                        mToolbar.setTitle(R.string.menu_video);
                        break;
                    case R.id.nav_region:
                        osq = openScreen(Screens.NEWS_TABS).argInt("tab", 0);
                        mToolbar.setTitle(R.string.title_feed);
                        break;
                    case R.id.nav_fav:
                        osq = openScreen(Screens.FAVORITE);
                        mToolbar.setTitle(R.string.menu_fav);
                        break;
                    case R.id.nav_about:
                        osq = openScreen(Screens.ABOUT);
                        mToolbar.setTitle(R.string.menu_about);
                        break;
                    case R.id.nav_settings:
                        osq = openScreen(Screens.SETTINGS);
                        mToolbar.setTitle(R.string.menu_settings);
                        break;
                    case R.id.nav_post:
                        osq = openScreen(Screens.POST);
                        mToolbar.setTitle(R.string.menu_post);
                        break;
                    case R.id.nav_rate:
                        IntentUtils.rate(DrawerActivity.this);
                        return false;
                    case R.id.nav_support:
                        IntentUtils.emailTo(DrawerActivity.this, getString(R.string.config_mail), "");
                        return false;
                    case R.id.nav_partners:
                        osq = openScreen(Screens.PARTNERS);
                        mToolbar.setTitle(R.string.menu_partners);
                        break;
                }
                osq.nostack().apply();
                ///  mToolbar.setTitle(R.string.title_feed);
                item.setChecked(true);
                return false;
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            maybeFinish();
            return;
        } else {
            super.onBackPressed();
        }
    }

    private void maybeFinish() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
            finish();
            return;
        } else {
            Toast.makeText(getBaseContext(), R.string.lbl_exit, Toast.LENGTH_SHORT).show();
        }
        mBackPressed = System.currentTimeMillis();
    }
}
