package pro.apptomato.miamir.app;

import pro.apptomato.miamir.ui.fragments.AboutFragment;
import pro.apptomato.miamir.ui.fragments.FavoriteFragment;
import pro.apptomato.miamir.ui.fragments.MainNewsFragment;
import pro.apptomato.miamir.ui.fragments.NewsItemFragment;
import pro.apptomato.miamir.ui.fragments.NewsListFragment;
import pro.apptomato.miamir.ui.fragments.NewsTabsFragment;
import pro.apptomato.miamir.ui.fragments.PartnersFragment;
import pro.apptomato.miamir.ui.fragments.PostNewsFragment;
import pro.apptomato.miamir.ui.fragments.RegionNewsFragment;
import pro.apptomato.miamir.ui.fragments.SelectRegionFragment;
import pro.apptomato.miamir.ui.fragments.SettingsFragment;
import pro.apptomato.miamir.ui.fragments.SplashFragment;
import pro.apptomato.miamir.ui.fragments.VideoFragment;
import pro.apptomato.module.ui.util.Screen;

public interface Screens {

    String EXTRA = "extra";

    Screen SPLASH = Screen.create(SplashFragment.class);

    Screen NEWS_ITEM = Screen.create(NewsItemFragment.class);
    Screen NEWS_TABS = Screen.create(NewsTabsFragment.class);

    Screen REGION_NEWS = Screen.create(RegionNewsFragment.class);
    Screen MAIN_NEWS = Screen.create(MainNewsFragment.class);
    Screen VIDEO = Screen.create(VideoFragment.class);
    Screen FAVORITE = Screen.create(FavoriteFragment.class);

    Screen ABOUT = Screen.create(AboutFragment.class);
    Screen PARTNERS = Screen.create(PartnersFragment.class);
    Screen POST = Screen.create(PostNewsFragment.class);
    Screen REGION = Screen.create(SelectRegionFragment.class);
    Screen SETTINGS = Screen.create(SettingsFragment.class);

}


