package pro.apptomato.miamir.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.data.region.RegionDto;
import pro.apptomato.miamir.data.region.RegionManager;

/**
 * Created by user on 28.04.17.
 */

public class RegionAdapter extends ArrayAdapter<RegionDto> {


    private RegionDto mRegion;

    private boolean mExact;

    public RegionAdapter(@NonNull Context context, boolean exact, RegionDto regionDto) {
        super(context, 0, new ArrayList<RegionDto>());
        mRegion = regionDto;
        mExact = exact;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_region, null, false);
            convertView.setTag(new ViewHolder(convertView));
        }
        final RegionDto item = getItem(position);
        ViewHolder vh = (ViewHolder) convertView.getTag();
        vh.tvName.setText(item.getRegname() + "");
        if (mExact) {
            if ((position == 0 && mRegion == null || item.equals(mRegion))) {
                vh.tvName.setTextColor(getContext().getResources().getColor(R.color.menu_selected));
                vh.check.setChecked(true);
            } else {
                vh.tvName.setTextColor(getContext().getResources().getColor(R.color.text_dark));
                vh.check.setChecked(false);
            }
        } else {
            if ((position == 0 && !RegionManager.isSelected() || item.equals(RegionManager.getCurrent()))) {
                vh.tvName.setTextColor(getContext().getResources().getColor(R.color.menu_selected));
                vh.check.setChecked(true);
            } else {
                vh.tvName.setTextColor(getContext().getResources().getColor(R.color.text_dark));
                vh.check.setChecked(false);
            }
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mExact) {
                    Intent data = new Intent();
                    data.putExtra("region", position == 0 ? null : item);
                    ((Activity) (getContext())).setResult(Activity.RESULT_OK, data);
                    ((Activity) (getContext())).finish();
                } else {
                    if (position == 0) {
                        RegionManager.setCurrent(getContext(), null);
                    } else {
                        RegionManager.setCurrent(getContext(), item);
                    }
                }
                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    private static class ViewHolder {
        TextView tvName;
        RadioButton check;

        public ViewHolder(View root) {
            tvName = (TextView) root.findViewById(R.id.tv_name);
            check = (RadioButton) root.findViewById(R.id.chk);
        }
    }
}
