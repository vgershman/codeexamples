package pro.apptomato.miamir.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.data.news.NewsDto;
import pro.apptomato.miamir.ui.fragments.NewsItemFragment;

/**
 * Created by user on 28.04.17.
 */

public class NewsAdapter extends ArrayAdapter<NewsDto> {


    public static SimpleDateFormat sTimeFormat = new SimpleDateFormat("HH:mm");

    public static SimpleDateFormat sDateFormat = new SimpleDateFormat("dd MMMM", Locale.getDefault());

    private FavListener mListener;

    public NewsAdapter(Context context, FavListener listener) {
        super(context, 0, new ArrayList());
        mListener = listener;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_news, null, false);
            convertView.setTag(new ViewHolder(convertView));
        }
        ViewHolder vh = (ViewHolder) convertView.getTag();
        final NewsDto item = getItem(position);
        try {
            vh.tv_title.setText(Html.fromHtml(item.getTitle()));
        }catch (Exception ex){
            vh.tv_title.setText("");
        }
        vh.tv_meta.setText(getDate(item.getDate()) + "  |  " + item.getRegname());
        vh.tv_time.setText(getTime(item.getDate()));
        boolean hasImage = !TextUtils.isEmpty(item.getPic1()) || !TextUtils.isEmpty(item.getPic2());
        if (hasImage) {
            vh.preview_container.setVisibility(View.VISIBLE);
            Picasso.with(getContext()).load(item.getBestPic()).into(vh.iv_preview);
        } else {
            vh.preview_container.setVisibility(View.GONE);
        }
        if (item.isVideo()) {
            vh.iv_video.setVisibility(View.VISIBLE);
        } else {
            vh.iv_video.setVisibility(View.GONE);
        }
        vh.btn_fav.setSelected(item.isFaved());
        vh.btn_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onToggle(item);
                }
            }
        });
        return convertView;
    }

    public static String getTime(String date) {
        long time = 0;
        try {
            time = Long.parseLong(date);
        } catch (Exception ex) {
        }
        if (time == 0) {
            return "Неизвестно";
        }
        return sTimeFormat.format(new Date(time * 1000L));
    }

    public static String getDate(String date) {
        long time = 0;
        try {
            time = Long.parseLong(date);
        } catch (Exception ex) {
        }
        if (time == 0) {
            return "Неизвестно";
        }
        int compare = LocalDate.now().compareTo(new LocalDate(time * 1000L));
        if (compare == 0) {
            return "Сегодня";
        } else {
            return sDateFormat.format(new Date(time * 1000L));
        }
    }

    public void update() {
        for (Long id : NewsItemFragment.sWrapper.getNewsFavStatus().keySet()) {
            for (int i = 0; i < getCount(); i++) {
                NewsDto item = getItem(i);
                if (item.getId() == id) {
                    item.setFaved(NewsItemFragment.sWrapper.getNewsFavStatus().get(id));
                }
            }
        }
        NewsItemFragment.sWrapper = null;
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        TextView tv_title, tv_meta, tv_time;
        ImageView iv_preview, btn_fav, iv_video;
        View preview_container;

        public ViewHolder(View view) {
            tv_title = (TextView) view.findViewById(R.id.tv_text);
            iv_preview = (ImageView) view.findViewById(R.id.iv_preview);
            tv_meta = (TextView) view.findViewById(R.id.tv_meta);
            tv_time = (TextView) view.findViewById(R.id.tv_time);
            btn_fav = (ImageView) view.findViewById(R.id.btn_favor);
            iv_video = (ImageView) view.findViewById(R.id.marker_video);
            preview_container = view.findViewById(R.id.container_image);
        }
    }

    public interface FavListener {
        void onToggle(NewsDto item);
    }
}
