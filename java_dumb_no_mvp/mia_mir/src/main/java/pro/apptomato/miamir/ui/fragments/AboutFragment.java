package pro.apptomato.miamir.ui.fragments;

import android.view.View;

import pro.apptomato.miamir.R;
import pro.apptomato.module.ui.base.BaseFragment;
import pro.apptomato.module.utils.IntentUtils;

/**
 * Created by user on 28.04.17.
 */

public class AboutFragment extends BaseFragment {
    @Override
    protected void initViews() {
        getAQ().id(R.id.btn_phone).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentUtils.phoneTo(getContext(), getResources().getString(R.string.config_phone).replaceAll("-", ""));
            }
        });
        getAQ().id(R.id.btn_email).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentUtils.emailTo(getContext(), getString(R.string.config_mail), "");
            }
        });
    }
}
