package pro.apptomato.miamir.ui.fragments;

import android.content.DialogInterface;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.app.Screens;
import pro.apptomato.miamir.data.RestApi;
import pro.apptomato.miamir.data.news.NewsDto;
import pro.apptomato.miamir.data.news.NewsWrapper;
import pro.apptomato.miamir.ui.adapters.GalleryAdapter;
import pro.apptomato.miamir.ui.adapters.NewsAdapter;
import pro.apptomato.miamir.ui.adapters.RecommendAdapter;
import pro.apptomato.module.rest.BaseAlertCallback;
import pro.apptomato.module.rest.BaseToastCallback;
import pro.apptomato.module.rest.Res;
import pro.apptomato.module.ui.base.BaseFragment;
import pro.apptomato.module.utils.IntentUtils;
import pro.apptomato.module.utils.Utils;

/**
 * Created by user on 28.04.17.
 */

public class NewsItemFragment extends BaseFragment implements RecommendAdapter.Listener, NewsAdapter.FavListener {

    private NewsDto mItem;
    private RecommendAdapter mAdapter;

    private RecyclerView mList;

    public static NewsWrapper sWrapper;

    @Override
    protected void initViews() {
        mList = (RecyclerView) getAQ().id(R.id.lv).getView();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mList.setLayoutManager(linearLayoutManager);
        getAQ().id(R.id.btn_share).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                share();
            }
        });
        getAQ().id(R.id.btn_open).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                browse();
            }
        });
    }

    private void share() {
        IntentUtils.share(getContext(), mItem.getLink());
    }

    private void browse() {
        IntentUtils.browser(getContext(), mItem.getLink());
    }

    private void initPreviewHeight() {
        CollapsingToolbarLayout ctl = (CollapsingToolbarLayout) getAQ().id(R.id.collapsingToolbar).getView();
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) ctl.getLayoutParams();
        if (mItem.isVideo() || ("empty".equals(mItem.getPic2()) && (mItem.getAlbum() == null || mItem.getAlbum().isEmpty()))) {
            int actionBarHeight = 0;
            TypedValue tv = new TypedValue();
            if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            }
            lp.height = actionBarHeight + (Build.VERSION.SDK_INT > 19 ? getStatusBarHeight() : (int) (getStatusBarHeight() * 0.5f));
        } else {
            lp.height = (int) (Utils.getScreenWidth(getContext()) * 490 * 1.f / 850 * 1.f);
        }
        ctl.setLayoutParams(lp);
    }


    @Override
    protected void initProperties() {
        super.initProperties();
        mItem = (NewsDto) getArguments().getSerializable("item");
        mAdapter = new RecommendAdapter(this, mItem, this, this);
        mList.setAdapter(mAdapter);
        bindPreview();
        loadContent();
        loadRecommended();
    }

    private void bindPreview() {
        initPreviewHeight();
        ViewPager vp = (ViewPager) getAQ().id(R.id.header).getView();
        if (mItem.isVideo()) {
            return;
        }
        CirclePageIndicator cpi = (CirclePageIndicator) getAQ().id(R.id.cpi).getView();
        List<String> images = new ArrayList<>();
        images.add(mItem.getBestPic());
        try {
            for (String image : mItem.getAlbum()) {
                images.add(image);
            }
        } catch (Exception ex) {
        }
        vp.setAdapter(new GalleryAdapter(getContext(), images));
        cpi.setViewPager(vp);
        if (images.size() > 1) {
            cpi.setVisibility(View.VISIBLE);
        } else {
            cpi.setVisibility(View.GONE);
        }
    }

    private void loadRecommended() {
        RestApi.rest().recommend(mItem.getId()).enqueue(new BaseToastCallback<Res<List<NewsDto>>>(getContext()) {
            @Override
            protected void onSuccess(Res<List<NewsDto>> response) {
                try {
                    mAdapter.clear();
                    int size = response.getData().size();
                    if (size > 4) {
                        size = 4;
                    }
                    mAdapter.addAll(response.getData().subList(0, size));
                } catch (Exception ex) {
                }
            }
        });
    }

    private void loadContent() {
        RestApi.rest().newsItem(mItem.getId()).enqueue(new BaseToastCallback<Res<List<NewsDto>>>(getContext()) {
            @Override
            protected void onSuccess(Res<List<NewsDto>> response) {
                try {
                    mAdapter.setCurrent(response.getData().get(0));
                    mItem = response.getData().get(0);
                    bindPreview();
                } catch (Exception ex) {
                    showError();
                }
            }
        });
    }

    private void showError() {
        new AlertDialog.Builder(getContext()).setMessage("Что-то пошло не так!")
                .setPositiveButton("Повторить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        loadContent();
                    }
                })
                .setNegativeButton("Закрыть", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        getActivity().onBackPressed();
                    }
                }).create().show();
    }


    @Override
    protected boolean hasToolbar() {
        return true;
    }

    @Override
    protected boolean isCollapsing() {
        return true;
    }

    @Override
    public void onItemClicked(NewsDto newsDto) {
        openScreen(Screens.NEWS_ITEM).argSer("item", newsDto).force().apply();
    }

    @Override
    public void onToggle(final NewsDto item) {
        RestApi.rest().favor(!item.isFaved(), item.getId()).enqueue(new BaseAlertCallback<Res<String>>(getContext()) {
            @Override
            protected void onSuccess(Res<String> response) {
                if (response.isSuccess()) {
                    item.setFaved(!item.isFaved());
                    mAdapter.notifyDataSetChanged();
                    putToWrapper(item);
                }
            }
        });
    }

    private void putToWrapper(NewsDto item) {
        if (sWrapper == null) {
            sWrapper = new NewsWrapper();
        }
        sWrapper.put(item);
    }

    public enum State {
        EXPANDED,
        COLLAPSED,
    }


    private State mCurrentState;
    private boolean toolbarIsTransparent = true;

    @Override
    protected void initCollapse() {
        mCurrentState = State.EXPANDED;
        TypedValue tv = new TypedValue();
        int actionBarHeight = 0;
        if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        AppBarLayout appBarLayout = (AppBarLayout) getAQ().id(R.id.appbar).getView();
        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) getAQ().id(R.id.collapsingToolbar).getView();
        if (appBarLayout != null) {
            final int finalActionBarHeight = (int) (actionBarHeight * 1.1f) + ((Build.VERSION.SDK_INT > 19 ? getStatusBarHeight() : (int) (getStatusBarHeight() * 0.3f)));
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                    try {
                        boolean force = "empty".equals(mItem.getBestPic()) && (mItem.getAlbum() == null || mItem.getAlbum().isEmpty());
                        if (i == 0) {
                            mCurrentState = State.EXPANDED;
                        } else if (Math.abs(i) >= appBarLayout.getTotalScrollRange()) {
                            mCurrentState = State.COLLAPSED;
                        }
                        if (((collapsingToolbarLayout.getHeight() + i <= finalActionBarHeight) && mCurrentState.equals(State.COLLAPSED)) || force) {
                            getToolbar().setBackgroundColor(ContextCompat.getColor(getContext(), R.color.primary));
                            toolbarIsTransparent = false;
                        } else if (!toolbarIsTransparent) {
                            mCurrentState = State.EXPANDED;
                            getToolbar().setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.transparent));
                            toolbarIsTransparent = true;
                        }
                    } catch (Exception ex) {
                    }
                }
            });
        }
    }
}
