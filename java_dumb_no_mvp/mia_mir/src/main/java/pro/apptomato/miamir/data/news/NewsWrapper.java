package pro.apptomato.miamir.data.news;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by user on 04.05.17.
 */

public class NewsWrapper implements Serializable {

    HashMap<Long, Boolean> newsFavStatus = new HashMap<>();

    public HashMap<Long, Boolean> getNewsFavStatus() {
        return newsFavStatus;
    }

    public void put(NewsDto item) {
        if (newsFavStatus.containsKey(item.getId())) {
            newsFavStatus.remove(item.getId());
        }
        newsFavStatus.put(item.getId(), item.isFaved());
    }
}
