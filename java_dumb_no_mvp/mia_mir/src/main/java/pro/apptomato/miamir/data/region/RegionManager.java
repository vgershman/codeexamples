package pro.apptomato.miamir.data.region;

import android.content.Context;
import android.preference.PreferenceManager;

import pro.apptomato.module.utils.JSONUtil;

import static pro.apptomato.module.utils.PrefUtils.store;

/**
 * Created by user on 01.05.17.
 */

public class RegionManager {

    public static void init(Context context) {
        try {
            sCurrent = JSONUtil.parse(PreferenceManager.getDefaultSharedPreferences(context).getString("region", null), RegionDto.class);
        } catch (Exception ex) {
            sCurrent = null;
        }
    }

    private static RegionDto sCurrent;

    public static RegionDto getCurrent() {
        return sCurrent;
    }

    public static void setCurrent(Context context, RegionDto current) {
        sCurrent = current;
        if (current == null) {
            store(context, "region", null);
        } else {
            store(context, "region", JSONUtil.toJson(sCurrent));
        }
    }

    public static boolean isSelected() {
        return sCurrent != null;
    }
}
