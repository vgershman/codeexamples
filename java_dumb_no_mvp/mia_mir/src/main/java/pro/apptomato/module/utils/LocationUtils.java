package pro.apptomato.module.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;

/**
 * Created by user on 18.05.17.
 */

public class LocationUtils {


    public static void getLocation(Activity context, final LocationListener locationListener) throws SecurityException {
        boolean informed = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("informed", false);
        if (!informed) {
            showInform(context, locationListener);
            return;
        }
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        String enabled;
        enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ? LocationManager.NETWORK_PROVIDER : null;
        if (enabled == null) {
            enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ? LocationManager.GPS_PROVIDER : null;
        }
        if (enabled == null) {
            showAlert(context, locationListener);
            return;
        }
        Location lastKnown = locationManager.getLastKnownLocation(enabled);
        if (lastKnown != null) {
            locationListener.onLocationReceived(lastKnown);
        } else {
            locationManager.requestLocationUpdates(enabled, 0, 0, new android.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        locationManager.removeUpdates(this);
                        locationListener.onLocationReceived(location);
                    }
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                }

                @Override
                public void onProviderEnabled(String s) {
                }

                @Override
                public void onProviderDisabled(String s) {
                    locationListener.onError();
                }
            });
        }
    }

    private static void showInform(final Activity context, final LocationListener locationListener) {
        new AlertDialog.Builder(context)
                .setCancelable(false)
                .setMessage("Ваша геопозиция будет отправлена на наши сервера для определения региона")
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("informed", true).apply();
                        getLocation(context, locationListener);
                    }
                }).create().show();
    }

    private static void showAlert(final Context context, final LocationListener locationListener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Передача геоданных отключена.\nВключить для продолжения?")
                .setCancelable(false)
                .setPositiveButton("Ок", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        locationListener.onNeedEnable();
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public interface LocationListener {
        void onLocationReceived(Location location);

        void onError();

        void onNeedEnable();
    }
}
