package pro.apptomato.module.ui.util;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import java.io.Serializable;
import java.util.Map;

import pro.apptomato.miamir.app.Screens;
import pro.apptomato.module.ui.base.BaseFragment;
import pro.apptomato.module.ui.base.BaseFragmentActivity;
import pro.apptomato.module.utils.Utils;


/**
 * Created by user on 29.04.16.
 */
public class OpenScreenQuery {

    private Screen mScreen;
    private boolean remove = false;
    private boolean add = false;
    private boolean force = false;
    private Bundle args = new Bundle();
    private boolean stack = true;
    private BaseFragmentActivity mActivity;
    private boolean finish = false;
    private Class<BaseFragmentActivity> mNewActivity = null;
    private boolean clearStack = false;
    private int requestCode;

    private BaseFragment mFragment, mFromFragment;

    public OpenScreenQuery(BaseFragmentActivity activity, Screen screen, BaseFragment fromFragment) {
        mScreen = screen;
        mActivity = activity;
        mFromFragment = fromFragment;
    }

    public OpenScreenQuery fragment(BaseFragment fragment) {
        mFragment = fragment;
        return this;
    }


    public OpenScreenQuery add() {
        this.add = true;
        return this;
    }

    public OpenScreenQuery drawer() {
        //nostack();
        argBool("drawer", true);
        return this;
    }

    public OpenScreenQuery force() {
        this.force = true;
        return this;
    }

    public OpenScreenQuery args(Bundle args) {
        this.args = args;
        return this;
    }


    public OpenScreenQuery argInt(String key, int value) {
        if (args == null) {
            args = new Bundle();
        }
        args.putInt(key, value);
        return this;
    }

    public OpenScreenQuery argBool(String key, boolean value) {
        if (args == null) {
            args = new Bundle();
        }
        args.putBoolean(key, value);
        return this;
    }

    public OpenScreenQuery argSer(String key, Serializable value) {
        if (args == null) {
            args = new Bundle();
        }
        args.putSerializable(key, value);
        return this;
    }

    public OpenScreenQuery argString(Map.Entry<String, String> param) {
        if (args == null) {
            args = new Bundle();
        }
        args.putString(param.getKey(), param.getValue());
        return this;
    }

    public OpenScreenQuery argString(String key, String value) {
        if (args == null) {
            args = new Bundle();
        }
        args.putString(key, value);
        return this;
    }

    public OpenScreenQuery nostack() {
        this.stack = false;
        return this;
    }


    public OpenScreenQuery activity(Class activityClass) {
        mNewActivity = activityClass;
        return this;
    }

    public OpenScreenQuery finish() {
        finish = true;
        return this;
    }

    public boolean apply() {
        try {
            Utils.hideSoftKeyboard(mActivity.findViewById(mActivity.getFragmentContainer()), mActivity);
            if (mActivity == null) {
                return false;
            }
            if (mFragment != null) {
                return openExactFragment();
            }
            if (mScreen == null) {
                return false;
            }
            if (mNewActivity == null) {
                return openFragment();
            } else {
                return openActivity();
            }
        } catch (Exception ex) {
            Log.e("osq", ex.getMessage());
            return false;
        }
    }

    private boolean openExactFragment() {
        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        transaction = transaction.add(mActivity.getFragmentContainer(), mFragment);
        if (stack) {
            transaction = transaction.addToBackStack(mFragment.getClass().toString());
        }
        transaction.commit();
        mActivity.setCurrent(mFragment);
        return true;
    }

    private boolean openActivity() throws Exception {
        Intent intent = new Intent(mActivity, mNewActivity);
        intent.putExtra(Screens.EXTRA, mScreen);
        intent.putExtras(args);
        if (requestCode != 0 && mFromFragment != null) {
            mFromFragment.startActivityForResult(intent, requestCode);
        } else {
            mActivity.startActivity(intent);
        }
        if (finish) {
            mActivity.finish();
        }
        return true;
    }

    private boolean openFragment() throws Exception {
        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        if (mActivity.getCurrentFragment() != null && mActivity.getCurrentFragment().getClass().equals(mScreen.getClazz()) && !force) {
            return false;
        }
        if (remove) {
            try {
                mActivity.getSupportFragmentManager().popBackStackImmediate();
            } catch (Exception ex) {
                Log.e(OpenScreenQuery.class.getName(), ex.getMessage());
                try {
                    mActivity.getSupportFragmentManager().popBackStack();
                } catch (Exception ex1) {
                    Log.e(OpenScreenQuery.class.getName(), ex1.getMessage());
                }
            }
        }
        //Class clazz = Class.forName(mActivity.getPackageName() + ".ui." + mScreen.getClazz());
        BaseFragment newFragment = (BaseFragment) mScreen.getClazz().newInstance();
        newFragment.setArguments(args);
        if (add) {
            transaction = transaction.add(mActivity.getFragmentContainer(), newFragment);
        } else {
            transaction = transaction.replace(mActivity.getFragmentContainer(), newFragment);
        }
        if (stack) {
            transaction = transaction.addToBackStack(mScreen.getClazz().getName());
        }
        transaction.commit();
        if (clearStack) {
            mActivity.getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        mActivity.setCurrent(newFragment);
        return true;
    }

    public OpenScreenQuery clear() {
        clearStack = true;
        return this;
    }

    public OpenScreenQuery result(int i) {
        requestCode = i;
        return this;
    }

    public OpenScreenQuery remove() {
        remove = true;
        return this;
    }
}
