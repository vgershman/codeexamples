package pro.apptomato.module.ui.util;

import android.text.SpannableStringBuilder;
import android.text.style.StrikethroughSpan;
import android.text.style.TypefaceSpan;

/**
 * Created by user on 12.05.16.
 */
public class SpanString extends SpannableStringBuilder {

    public SpanString appendLight(CharSequence text) {
        int start = length();
        append(text);
        setSpan(new TypefaceSpan("sans-serif-light"), start, length(), SPAN_INCLUSIVE_INCLUSIVE);
        return this;
    }

    public SpanString appendRegular(CharSequence text) {
        int start = length();
        append(text);
        setSpan(new TypefaceSpan("sans-serif-regular"), start, length(), SPAN_INCLUSIVE_INCLUSIVE);
        return this;
    }

    public SpanString appendMedium(CharSequence text) {
        int start = length();
        append(text);
        setSpan(new TypefaceSpan("sans-serif-medium"), start, length(), SPAN_INCLUSIVE_INCLUSIVE);
        return this;
    }

    public SpanString appendStriked(CharSequence text) {
        int start = length();
        append(text);
        setSpan(new StrikethroughSpan(), start, length(), SPAN_INCLUSIVE_INCLUSIVE);
        return this;
    }
}
