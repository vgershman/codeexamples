package pro.apptomato.module.ui.util;

import android.content.Context;

/**
 * Created by user on 29.04.16.
 */
public class ResUtil {

    public static int getLayoutIdByName(Context context, String name){
        return context.getResources().getIdentifier(name, "layout", context.getPackageName());
    }

}
