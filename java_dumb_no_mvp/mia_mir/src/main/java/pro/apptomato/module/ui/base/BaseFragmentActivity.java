package pro.apptomato.module.ui.base;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.androidquery.AQuery;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.app.Screens;
import pro.apptomato.module.ui.util.OpenScreenQuery;
import pro.apptomato.module.ui.util.ResUtil;
import pro.apptomato.module.ui.util.Screen;


/**
 * Created by user on 29.04.16.
 */
public abstract class BaseFragmentActivity extends AppCompatActivity {

    private AQuery aQuery;

    public int getFragmentContainer() {
        return R.id.fragment_container;
    }

    private BaseFragment mCurrentFragment;


    public BaseFragment getCurrentFragment() {
        return mCurrentFragment;
    }

    public OpenScreenQuery openScreen(Screen screen) {
        return new OpenScreenQuery(this, screen, null);
    }

    public OpenScreenQuery openScreen(Screen screen, BaseFragment mFromFragment) {
        return new OpenScreenQuery(this, screen, mFromFragment);
    }

    public void setScreenTitle(int title) {
    }

    protected Screen defaultScreen() {
        return null;
    }

    @Override
    public void onBackPressed() {
        if (mCurrentFragment == null) {
            super.onBackPressed();
            return;
        }
        if (mCurrentFragment.onFragmentBackPressed()) {
            super.onBackPressed();
        }
    }

    public AQuery getAQ() {
        return aQuery;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                mCurrentFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(getFragmentContainer());
                if (mCurrentFragment != null) {
                    mCurrentFragment.onFragmentResume();
                }
            }
        });
        setContentView(getContentViewId());
        aQuery = new AQuery(this);
        init();
        openFirstScreen();
    }

    protected void openFirstScreen() {
        Screen firstScreen = (Screen) getIntent().getSerializableExtra(Screens.EXTRA);
        if (firstScreen == null) {
            firstScreen = defaultScreen();
        }
        if (firstScreen != null) {
            openScreen(firstScreen).args(getIntent().getExtras()).apply();
        }
    }

    public abstract void init();


    protected int getContentViewId() {
        String[] name = getClass().getSimpleName().toLowerCase().split("activity");
        String resName = "activity_" + name[0];
        int resId = ResUtil.getLayoutIdByName(this, resName);
        if (resId == -1) {
            throw new IllegalArgumentException("wrong activity name");
        }
        return resId;
    }


    protected void safeRunOnUiThread(int delay, Runnable runnable) {
        new Handler().postDelayed(runnable, delay);
    }

    protected void safeRunOnUiThread(Runnable runnable) {
        new Handler().postDelayed(runnable, 0);
    }

    public void setCurrent(BaseFragment current) {
        mCurrentFragment = current;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } catch (Exception ex) {
        }
    }
}
