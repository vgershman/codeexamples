package pro.apptomato.module.ui.base;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.androidquery.AQuery;

import pro.apptomato.miamir.R;
import pro.apptomato.miamir.ui.activities.DrawerActivity;
import pro.apptomato.miamir.ui.fragments.PostNewsFragment;
import pro.apptomato.module.ui.util.OpenScreenQuery;
import pro.apptomato.module.ui.util.ResUtil;
import pro.apptomato.module.ui.util.Screen;
import pro.apptomato.module.utils.Utils;


/**
 * Created by vgershman on 02.05.14.
 */
public abstract class BaseFragment extends Fragment {

    private AQuery mAQuery;
    protected BaseFragmentActivity mActivity;
    private Toolbar mToolbar;
    private boolean mDrawer;

    protected int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    protected BaseFragmentActivity getMyActivity() {
        return mActivity;
    }


    public Toolbar getToolbar() {
        return mToolbar;
    }

    public OpenScreenQuery openScreen(Screen screen) {
        return mActivity.openScreen(screen, this);
    }

    protected void onBackPressed() {
        Utils.hideSoftKeyboard(getAQ().getView(), getActivity());
        mActivity.onBackPressed();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActivity = (BaseFragmentActivity) getActivity();
        View main = inflater.inflate(getContainerView(), container, false);
        if (!hasToolbar()) {
            return main;
        } else {
            View wrapper;
            if (isCollapsing()) {
                wrapper = inflater.inflate(R.layout.fragment_collapse_toolbar_base, container, false);
            } else {
                wrapper = inflater.inflate(R.layout.fragment_toolbar_base, container, false);
            }
            ((FrameLayout) wrapper.findViewById(R.id.mainContainer)).addView(main);
            return wrapper;
        }
    }

    protected boolean isCollapsing() {
        return false;
    }

    protected void initCollapse() {
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAQuery = new AQuery(view);
        initToolbar();
        setTitle();
        checkBtns();
        initViews();
        initCollapse();
//        try {
//            getAQ().id(R.id.container).clicked(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Utils.hideSoftKeyboard(v, getActivity());
//                }
//            });
//        } catch (Exception ex) {
//        }
//        getAQ().getView().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Utils.hideSoftKeyboard(v, getActivity());
//            }
//        });
    }


    protected void initToolbarNavigation() {
        if (hasToolbar()) {
            if (mDrawer) {
                getToolbar().setNavigationIcon(null);
            } else {
                getToolbar().setNavigationIcon(R.drawable.icon_nb_back);
                getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }
        }
    }

    protected void setTitle() {
        if (hasToolbar() && getTitleId() != -1) {
            ///   getAQ().id(R.id.toolbar_title).text(getTitleId());
            //mToolbar.setTitle(getTitleId());
        }
    }

    protected void setTitle(String title) {
        if (hasToolbar()) {
            mToolbar.setTitle(title);
            //    getAQ().id(R.id.toolbar_title).text(title);
        }
    }


    private void initToolbar() {
        mToolbar = (Toolbar) getAQ().id(R.id.toolbar).getView();
        if (hasToolbar() && Build.VERSION.SDK_INT > 19) {
            ViewGroup.LayoutParams lp = mToolbar.getLayoutParams();
            lp.height = lp.height + getStatusBarHeight();
            mToolbar.setPadding(0, getStatusBarHeight(), 0, 0);
            mToolbar.setLayoutParams(lp);
        }
        if (getTitleId() != -1) {
            mToolbar.setTitle(getTitleId());
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initProperties();
    }

    protected abstract void initViews();

    protected void initProperties() {
        if (getArguments() != null) {
            mDrawer = getArguments().getBoolean("drawer");
        }
        initToolbarNavigation();
    }


    protected int getTitleId() {
        return -1;
    }

    protected int getContainerView() {
        String[] name = getClass().getSimpleName().toLowerCase().split("fragment");
        String resName = "fragment_" + name[0];
        int resId = ResUtil.getLayoutIdByName(getContext(), resName);
        if (resId == -1) {
            throw new IllegalArgumentException("wrong fragment name");
        }
        return resId;
    }

    protected boolean hasToolbar() {
        return false;
    }


    public void onFragmentResume() {
        checkBtns();
    }

    private void checkBtns() {
        try {
            if (getActivity() instanceof DrawerActivity && this instanceof PostNewsFragment) {
                getActivity().findViewById(R.id.ll_tool).setVisibility(View.VISIBLE);
            } else {
                getActivity().findViewById(R.id.ll_tool).setVisibility(View.GONE);
            }
        } catch (Exception ex) {
        }
    }


    public AQuery getAQ() {
        return mAQuery;
    }

    public boolean onFragmentBackPressed() {
        return true;
    }
}

