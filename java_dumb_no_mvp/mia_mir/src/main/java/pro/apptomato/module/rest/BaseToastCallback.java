package pro.apptomato.module.rest;

import android.content.Context;
import android.widget.Toast;

import pro.apptomato.miamir.R;


/**
 * Created by user on 26.04.17.
 */

public abstract class BaseToastCallback<T> extends BaseCallback<T> {

    public BaseToastCallback(Context context) {
        super(context);
    }

    protected void onError() {
        Toast.makeText(mContext, R.string.server_error, Toast.LENGTH_LONG).show();
    }

    protected void onConnectionFailure() {
        Toast.makeText(mContext, R.string.no_connection, Toast.LENGTH_LONG).show();
    }
}
