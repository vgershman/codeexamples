package pro.apptomato.module.ui.util;

import android.os.Bundle;

import java.io.Serializable;

/**
 * Created by user on 29.04.16.
 */
public class Screen implements Serializable {

    private Class clazz;

    private Bundle args;

    public Bundle getArgs() {
        return args;
    }

    public static Screen create(Class clazz) {
        return new Screen(clazz);
    }

    private Screen(Class clazz) {
        this.clazz = clazz;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setBundle(Bundle args){
        this.args = args;
    }
}
