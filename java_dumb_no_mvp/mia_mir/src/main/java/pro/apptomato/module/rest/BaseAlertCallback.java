package pro.apptomato.module.rest;

import android.content.Context;
import android.content.DialogInterface;

import pro.apptomato.miamir.R;
import pro.apptomato.module.ui.util.AlertUtils;


/**
 * Created by user on 26.04.17.
 */

public abstract class BaseAlertCallback<T> extends BaseCallback<T> {

    private boolean mForce;

    public BaseAlertCallback(Context context) {
        super(context);
        mForce = false;
    }

    public BaseAlertCallback(Context context, boolean force) {
        super(context);
        mForce = force;
    }

    protected void onError() {
        try {
            AlertUtils.show(mContext, mForce, R.string.server_error, R.string.btn_retry, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                    retry();
                }
            });
        } catch (Exception ex) {
        }
    }

    protected void onConnectionFailure() {
        AlertUtils.show(mContext, mForce, R.string.no_connection, R.string.btn_retry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
                retry();
            }
        });
    }
}
