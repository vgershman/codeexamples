package pro.apptomato.module.rest;

/**
 * Created by user on 29.04.17.
 */

public class Res<T> {

    private int success;
    private T data;
    private Object error;

    public boolean isSuccess() {
        return success == 1;
    }

    public T getData() {
        return data;
    }

    public Object getError() {
        return error;
    }
}
