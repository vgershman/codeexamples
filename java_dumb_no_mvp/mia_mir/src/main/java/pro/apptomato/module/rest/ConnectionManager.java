package pro.apptomato.module.rest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by user on 26.04.17.
 */

public class ConnectionManager {

    public static boolean isConnected(Context context) {
        try {
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnected();
            int state = isConnected ? 2 : 1;
            return state == 2;
        } catch (Exception ex) {
            return true;
        }
    }
}
