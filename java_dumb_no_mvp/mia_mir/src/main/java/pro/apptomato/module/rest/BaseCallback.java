package pro.apptomato.module.rest;


import android.content.Context;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 26.04.17.
 */

public abstract class BaseCallback<T> implements Callback<T> {

    protected Context mContext;

    private Call<T> mCall;


    public BaseCallback(Context context) {
        mContext = context;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        mCall = call;
        try {
            if (response.isSuccessful() && response.body() != null) {
                if (response.body() instanceof Res) {
                    Res result = ((Res) response.body());
                    if (result.isSuccess()) {
                        onSuccess(response.body());
                    } else {
                        error();
                    }
                } else {
                    error();
                }
            } else {
                error();
            }
        } catch (Exception ex) {
        }
    }

    protected void error() {
        if (ConnectionManager.isConnected(mContext)) {
            onError();
        } else {
            onConnectionFailure();
        }
    }


    @Override
    public void onFailure(Call<T> call, Throwable t) {
        mCall = call;
        error();
    }

    protected void retry() {
        mCall.clone().enqueue(this);
    }

    protected abstract void onSuccess(T response);

    protected abstract void onError();

    protected abstract void onConnectionFailure();
}

