package pro.apptomato.module.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

/**
 * Created by user on 05.05.17.
 */

public class PrefUtils {
    public static void store(final Context context, final String key, final String value) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).apply();
                return null;
            }
        }.execute();
    }

    public static void store(final Context context, final String key, final int value) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(key, value).apply();
                return null;
            }
        }.execute();
    }
}
