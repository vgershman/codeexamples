package pro.apptomato.module.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import pro.apptomato.miamir.R;

/**
 * Created by user on 01.05.17.
 */

public class IntentUtils {
    public static void phoneTo(Context context, String phone) {
        try {
            String uri = "tel:" + phone.trim();
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            context.startActivity(intent);
        } catch (Exception ex) {
        }
    }

    public static void emailTo(Context context, String address, String theme) {
        try {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", address, null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, theme);
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");
            context.startActivity(Intent.createChooser(emailIntent, "Написать в..."));
        } catch (Exception ex) {
        }
    }

    public static void rate(Context context) {
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName()));
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
        }
    }

    public static void browser(Context context, String url) {
        Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        browser.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(browser);
        } catch (ActivityNotFoundException e) {
        }
    }

    public static void share(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, url);
        try {
            context.startActivity(Intent.createChooser(intent, context.getString(R.string.lbl_share)));
        } catch (ActivityNotFoundException e) {
        }
    }
}
