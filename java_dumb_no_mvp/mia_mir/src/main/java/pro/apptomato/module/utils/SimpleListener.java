package pro.apptomato.module.utils;

/**
 * Created by user on 04.04.17.
 */

public interface SimpleListener {
    void onDone(Object... data);
}
