package pro.apptomato.module.ui.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by user on 26.04.17.
 */

public class AlertUtils {

    private static boolean isShown;

    public static void show(Context context, boolean force, int message, int btn, DialogInterface.OnClickListener listener) {
        if (!isShown) {
            new AlertDialog.Builder(context).setCancelable(!force).setMessage(message).setPositiveButton(btn, listener).setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    isShown = false;
                }
            }).create().show();
            isShown = true;
        }
    }
}
