package pro.apptomato.module.ui.fragments.base;

import android.content.Context;
import android.os.Bundle;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;

import pro.apptomato.miamir.R;
import pro.apptomato.module.ui.base.BaseFragment;
import pro.apptomato.module.ui.base.BaseFragmentActivity;
import pro.apptomato.module.ui.util.Screen;


/**
 * Created by user on 12.05.16.
 */
public abstract class BaseTabsFragment extends BaseFragment {


    private TabLayout mTabs;
    private ViewPager mViewPager;
    private TabsAdapter mTabsAdapter;

    public TabLayout getTabs() {
        return mTabs;
    }

    public TabsAdapter getTabsAdapter() {
        return mTabsAdapter;
    }

    public ViewPager getViewPager() {
        return mViewPager;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        initTabs();
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onFragmentResume() {
        super.onFragmentResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActivity = (BaseFragmentActivity) getActivity();
        return inflater.inflate(getContainerView(), container, false);
    }

    protected void initTabs() {
        setTabs();
    }

    protected void setTabs() {
        mTabs = (TabLayout) getView().findViewById(R.id.tabs);
        mTabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        mViewPager = (ViewPager) getView().findViewById(R.id.vp_fragments);
        mTabsAdapter = new TabsAdapter(getActivity(), getChildFragmentManager(), listTitles(), listScreens());
        mViewPager.setAdapter(mTabsAdapter);
        mTabs.setupWithViewPager(mViewPager);
    }

    protected abstract String[] listTitles();

    protected abstract Screen[] listScreens();


    protected abstract void initViews();


    @Override
    protected int getContainerView() {
        return hasToolbar() ? R.layout.fragment_toolbar_tabs_base : R.layout.fragment_tabs_base;
    }

    public static class TabsAdapter extends FragmentPagerAdapter {


        private Context mContext;
        private String[] titles;
        private Screen[] screens;
        private List<BaseFragment> fragments = new ArrayList<>();



        public TabsAdapter(Context context, FragmentManager fm, String[] titles, Screen[] screens) {
            super(fm);
            this.titles = titles;
            this.screens = screens;
            mContext = context;
            for (int i = 0; i < screens.length; i++) {
                fragments.add(null);
            }
        }

        @Override
        public Fragment getItem(int position) {
            BaseFragment fragment = fragments.get(position);
            if (fragment == null) {
                try {
                    fragment = (BaseFragment) screens[position].getClazz().newInstance();
                    fragment.setArguments(screens[position].getArgs());
                } catch (Exception ex) {
                }
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return screens.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }
}
