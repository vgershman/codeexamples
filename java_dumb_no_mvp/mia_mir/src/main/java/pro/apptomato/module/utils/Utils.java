package pro.apptomato.module.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Random;


/**
 * Created by user on 31.12.2015.
 */
public class Utils {

    private static final String ALPHABET = "qwertyuiop[]\'lkjhgfdsazxcvbnm,./1234567890";


    public static long randomTime(long start) {
        return start + randomInt(60 * 24 * 365) * 1000L;
    }

    public static int randomInt(int count) {
        return new Random().nextInt(count);
    }

    public static String randomString(int count) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < count; i++) {
            builder.append(ALPHABET.charAt(new Random().nextInt(ALPHABET.length() - 1)) + "");
        }
        return builder.toString();
    }

    private static Context context;

    public static void init(Context context) {
        Utils.context = context;
    }

    public static void hideSoftKeyboard(View input, Context context) {
        // input.setInputType(0);
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
        } catch (Exception ex) {
        }
    }

    public static void showDebugTextToast() {
        Toast.makeText(context, "В разработке", Toast.LENGTH_SHORT).show();
    }

    public static void showErrorToast() {
        Toast.makeText(context, "Ошибка  сервера", Toast.LENGTH_SHORT).show();
    }

    public static void showTextToast(String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static String getAgeByDate(String date) {
        if (date != null && !date.isEmpty()) {
            Calendar dob = Calendar.getInstance();
            Calendar today = Calendar.getInstance();
            dob.set(Integer.parseInt(date.substring(0, 4)), Integer.parseInt(date.substring(5, 7)),
                    Integer.parseInt(date.substring(8, 10)));
            int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
            if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
                age--;
            }
            Integer ageInt = age;
            String ageS = ageInt.toString();
            return ageS;
        }
        return "";
    }

    public static void setEditTextText(final EditText editText, final String text) {
        editText.post(new Runnable() {

            @Override
            public void run() {
                editText.setText(text);
            }
        });
    }

    public static void loadImage(ImageView view, String url) {
    }

    public static float getDisplayHeight(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.heightPixels;
    }

    public static float pxFromDp(Context context1, int dp) {
        DisplayMetrics displayMetrics = context1.getResources().getDisplayMetrics();
        return displayMetrics.density * dp;
    }

    public static float getScreenWidth(Context context1) {
        DisplayMetrics displayMetrics = context1.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    public static long day(long time) {
        int day = (int) (time / 1000L * 1.f / 60 / 60 / 24);
        return day * 24 * 60 * 60;
    }

    public static String readInputStream(InputStream is) {
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        String line;
        try {
            while ((line = r.readLine()) != null) {
                total.append(line).append('\n');
            }
        } catch (Exception ex) {
            return "";
        }
        return total.toString();
    }


    public static boolean validEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
