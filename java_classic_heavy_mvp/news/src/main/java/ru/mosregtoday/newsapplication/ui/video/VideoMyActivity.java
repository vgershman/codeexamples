package ru.mosregtoday.newsapplication.ui.video;


import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;

import ru.mosregtoday.newsapplication.R;
import ru.mosregtoday.newsapplication.app.ExtrasConfig;


public class VideoMyActivity extends Activity {

    String videoId;

    private VideoView mVideoView;

    private boolean mute = false;

    private boolean stopped = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (getIntent().getExtras() != null)
            videoId = getIntent().getStringExtra(ExtrasConfig.YOUTUBE);
        // videoId = "https://video.img.ria.ru/Volume43/Flv/2018/06/09/2018_06_09_PutinObracsheniye16x9RIACLEAN_rirgt0l2.e4x.mp4";
        setContentView(R.layout.fragment_video_my);
        mVideoView = findViewById(R.id.ivVideo);
        bindVideo();
    }

    private void bindVideo() {
        if (videoId != null && mVideoView != null) {
            mVideoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mute) {
                        mVideoView.setVolume(0f);
                    } else {
                        mVideoView.setVolume(0.6f);
                    }
                    mute = !mute;
                }
            });
            mVideoView.setVolume(0.6f);
            mVideoView.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared() {
                    mVideoView.start();
                }
            });
            mVideoView.setVideoURI(Uri.parse(videoId));
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            mVideoView.stopPlayback();
            mVideoView.release();
        } catch (Exception ex) {
        }
    }
}
