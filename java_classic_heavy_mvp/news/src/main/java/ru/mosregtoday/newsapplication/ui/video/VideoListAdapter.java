package ru.mosregtoday.newsapplication.ui.video;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.mosregtoday.newsapplication.R;
import ru.mosregtoday.newsapplication.app.ExtrasConfig;
import ru.mosregtoday.newsapplication.repository.model.Video;
import ru.mosregtoday.newsapplication.repository.model.Videos;
import ru.mosregtoday.newsapplication.utils.ColoredHeaderFormatter;


public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {

    private Context mContext;
    private static LayoutInflater inflater = null;
    Videos videos;
    List<Video> list;
    private boolean mTwoPane;
    private FragmentManager fragmentManager;


    public VideoListAdapter(Context context, FragmentManager fragmentManager, boolean mTwoPane) {
        mContext = context;
        this.mTwoPane = mTwoPane;
        this.fragmentManager = fragmentManager;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(Videos videos) {
        if (this.videos == null) {
            this.videos = videos;
            this.list = new ArrayList<>(videos.getItems());
            notifyDataSetChanged();
            loadFirstVideoForTablet();
        } else {
            int position = list.size();
            this.list.addAll(videos.getItems());
            videos.setItems(this.list);
            notifyItemRangeInserted(position, list.size());
        }
    }

    private void loadFirstVideoForTablet() {
        if (mTwoPane) {
            openVideo(0);
        }
    }

    @Override
    public VideoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vi = inflater.inflate(R.layout.item_video, null);
        return new ViewHolder(vi);
    }


    @Override
    public void onBindViewHolder(VideoListAdapter.ViewHolder holder, int position) {
        holder.headerText.setText(ColoredHeaderFormatter.createHeaderText(list.get(position).getTimestamp(), false));
        holder.subHeaderText.setText(list.get(position).getTitle());
        holder.anonceText.setText(list.get(position).getAnnounce());
        Glide.with(mContext)
                .load(list.get(position).getFull_image())
                .into(holder.logo);
        holder.play.setTag(position);
        holder.play.setOnClickListener(onItemClickListener);
        holder.favouritesButton.setTag(position);
        holder.favouritesButton.setOnClickListener(onFavouritesClickListener);
    }


    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();
            openVideo(position);
        }
    };


    private void openVideo(int position) {
        Video video = list.get(position);
        boolean isYouTube = (video.getVideo_stream() == null || video.getVideo_stream().isEmpty());
        Bundle arguments = new Bundle();
        if (isYouTube) {
            arguments.putString(ExtrasConfig.YOUTUBE, String.valueOf((video.getYoutube()).getCode()));
        } else {
            arguments.putString(ExtrasConfig.YOUTUBE, video.getVideo_stream());
        }
        if (mTwoPane) {
            Fragment fragment = isYouTube ? new YoutubeDetailFragment() : new VideoDetailFragment();
            fragment.setArguments(arguments);
            fragmentManager.beginTransaction()
                    .replace(R.id.item_youtube_container, fragment)
                    .commit();
        } else {
            Intent intent = new Intent(mContext, isYouTube ? YouTubeMyActivity.class : VideoMyActivity.class);
            intent.putExtras(arguments);
            mContext.startActivity(intent);
        }
    }


    private View.OnClickListener onFavouritesClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();
        }
    };

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public Videos getVideos() {
        return this.videos;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.logoImage)
        ImageView logo;
        @BindView(R.id.playImage)
        ImageView play;

        @BindView(R.id.newsHeaderText)
        TextView headerText;
        @BindView(R.id.newsSubHeaderText)
        TextView subHeaderText;
        @BindView(R.id.newsAnonceText)
        TextView anonceText;

        @BindView(R.id.favouritesButton)
        ImageButton favouritesButton;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
