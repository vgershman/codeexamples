package ru.mosregtoday.newsapplication.ui.video;

import ru.mosregtoday.newsapplication.repository.Repository;


public class VideoListInteractor implements IVideoListInteractor {
    @Override
    public void getVideos(int nextPage, onVideosGetListener listener) {
        Repository.getVideos(nextPage, listener);
    }
}
