package ru.mosregtoday.newsapplication.ui.video;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import ru.mosregtoday.newsapplication.repository.model.Videos;


public class VideoListPresenter extends MvpBasePresenter<IVideoListView> implements IVideoListPresenter, IVideoListInteractor.onVideosGetListener {

    private IVideoListInteractor interactor;

    public VideoListPresenter() {
        interactor = new VideoListInteractor();
    }

    @Override
    public void loadVideos(boolean pullToRefresh, int nextPage) {
        if (isViewAttached()) {
            if (!pullToRefresh) getView().showLoader();
            interactor.getVideos(nextPage, this);
        }
    }

    @Override
    public void refreshList() {
        if (isViewAttached()) {
            interactor.getVideos(0, this);
        }
    }

    @Override
    public void onSuccess(Videos list) {
        if (isViewAttached()) {
            getView().setData(list);
            getView().showContent();
        }
    }

    @Override
    public void onError(String error) {
        if (isViewAttached()) {
            getView().hideLoader();
            getView().showError(error);
        }
    }

    @Override
    public void onError(int errorStringID) {
        if (isViewAttached()) {
            getView().hideLoader();
            getView().showError(errorStringID);
        }
    }


}
