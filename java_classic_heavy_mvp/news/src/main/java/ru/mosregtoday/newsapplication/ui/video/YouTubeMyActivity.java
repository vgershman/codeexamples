package ru.mosregtoday.newsapplication.ui.video;


import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;


import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

import ru.mosregtoday.newsapplication.R;
import ru.mosregtoday.newsapplication.app.ConstConfig;
import ru.mosregtoday.newsapplication.app.ExtrasConfig;
import ru.mosregtoday.newsapplication.ui.YouTubeFailureRecoveryActivity;


public class YouTubeMyActivity extends YouTubeFailureRecoveryActivity {

    String videoId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (getIntent().getExtras() != null)
            videoId = getIntent().getStringExtra(ExtrasConfig.YOUTUBE);

        setContentView(R.layout.fragment_youtube);

        YouTubePlayerFragment youTubePlayerFragment =
                (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtubeFragment);
        youTubePlayerFragment.initialize(ConstConfig.DEVELOPER_KEY, this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player,
                                        boolean wasRestored) {
        if (!wasRestored) {
            player.loadVideo(videoId);
        }
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtubeFragment);
    }

}
