package ru.mosregtoday.newsapplication.ui.video;


public interface IVideoListPresenter {

    void loadVideos(boolean pullToRefresh, int currentPage);

    void refreshList();
}
