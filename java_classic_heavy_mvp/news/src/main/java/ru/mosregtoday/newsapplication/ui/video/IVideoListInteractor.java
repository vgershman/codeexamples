package ru.mosregtoday.newsapplication.ui.video;

import ru.mosregtoday.newsapplication.repository.model.Publications;
import ru.mosregtoday.newsapplication.repository.model.Videos;
import ru.mosregtoday.newsapplication.ui.IGeneralListener;


public interface IVideoListInteractor {

    interface onVideosGetListener extends IGeneralListener {
        void onSuccess(Videos list);
    }

    void getVideos(int nextPage, onVideosGetListener listener);
}
