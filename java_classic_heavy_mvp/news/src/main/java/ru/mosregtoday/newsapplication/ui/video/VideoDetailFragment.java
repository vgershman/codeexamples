package ru.mosregtoday.newsapplication.ui.video;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;

import ru.mosregtoday.newsapplication.R;
import ru.mosregtoday.newsapplication.app.ExtrasConfig;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link VideoListActivity}
 * in two-pane mode (on tablets) or a {@link YouTubeMyActivity}
 * on handsets.
 */
public class VideoDetailFragment extends Fragment {
    String videoId;
    private boolean mute = false;

    private VideoView mVideoView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public VideoDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ExtrasConfig.YOUTUBE)) {
            videoId = getArguments().getString(ExtrasConfig.YOUTUBE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_my, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mVideoView = getView().findViewById(R.id.ivVideo);
        bindVideo();
    }

    private void bindVideo() {
        if (videoId != null && mVideoView != null) {
            mVideoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mute) {
                        mVideoView.setVolume(0f);
                    } else {
                        mVideoView.setVolume(0.6f);
                    }
                    mute = !mute;
                }
            });
            mVideoView.setVolume(0.6f);
            mVideoView.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared() {
                    mVideoView.start();
                }
            });
            mVideoView.setVideoURI(Uri.parse(videoId));
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            mVideoView.stopPlayback();
            mVideoView.release();
        } catch (Exception ex) {
        }
    }
}
