package ru.mosregtoday.newsapplication.ui.video;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.hannesdorfmann.mosby.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.MvpLceViewStateActivity;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.data.RetainingLceViewState;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.mosregtoday.newsapplication.R;

import ru.mosregtoday.newsapplication.repository.model.Videos;
import ru.mosregtoday.newsapplication.ui.favourites.FavouritesActivity;
import ru.mosregtoday.newsapplication.ui.newsList.NewsListActivity;
import ru.mosregtoday.newsapplication.utils.EndlessRecyclerOnScrollListener;
import ru.mosregtoday.newsapplication.utils.ErrorUtils;


public class VideoListActivity extends MvpLceViewStateActivity<CoordinatorLayout, Videos, IVideoListView, VideoListPresenter>
        implements IVideoListView, SwipeRefreshLayout.OnRefreshListener, NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.videoList)
    RecyclerView recyclerView;
    VideoListAdapter adapter;

    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;

    @BindView(R.id.drawerLayout)
    DrawerLayout drawer;
    @BindView(R.id.navigationView)
    NavigationView navigationView;

    int nextPage = 0;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);
        setRetainInstance(true);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        setupSideMenu();

        if (findViewById(R.id.item_youtube_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w600dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        refreshLayout.setOnRefreshListener(this);
        adapter = new VideoListAdapter(this, getSupportFragmentManager(), mTwoPane);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                nextPage = current_page;
                loadData(false);
            }
        });
    }

    @NonNull
    @Override
    public VideoListPresenter createPresenter() {
        return new VideoListPresenter();
    }


    @Override
    public void onRefresh() {
        presenter.refreshList();
    }

    @NonNull
    @Override
    public LceViewState<Videos, IVideoListView> createViewState() {
        return new RetainingLceViewState<Videos, IVideoListView>();

    }

    @Override
    public Videos getData() {
        return adapter == null ? null : adapter.getVideos();
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
        refreshLayout.setRefreshing(pullToRefresh);
    }

    @Override
    public void showError(String error) {
        ErrorUtils.showError(error, this);
    }

    @Override
    public void showError(int errorStringID) {
        ErrorUtils.showError(errorStringID, this);
    }

    @Override
    public void showLoader() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        refreshLayout.setRefreshing(false);
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void setData(Videos data) {
        adapter.setData(data);
    }

    @Override
    public void showContent() {
        super.showContent();
        hideLoader();
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.loadVideos(pullToRefresh, nextPage);
    }

    private void setupSideMenu() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_news:
                startActivity(new Intent(this, NewsListActivity.class));
                break;
            case R.id.nav_video:
                break;
            case R.id.nav_favourites:
                startActivity(new Intent(this, FavouritesActivity.class));
                break;
        }

        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }
}
