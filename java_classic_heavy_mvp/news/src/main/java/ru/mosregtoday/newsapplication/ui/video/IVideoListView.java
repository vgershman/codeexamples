package ru.mosregtoday.newsapplication.ui.video;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import ru.mosregtoday.newsapplication.repository.model.Publications;
import ru.mosregtoday.newsapplication.repository.model.Videos;


public interface IVideoListView extends MvpLceView<Videos> {
    void showError(String error);

    void showError(int errorStringID);

    void showLoader();

    void hideLoader();
}
