package fitness.online.app.activity.main.fragment.handbook;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.model.pojo.realm.handbook.HandbookFilter;
import fitness.online.app.mvp.contract.fragment.HandbookFilterFragmentContract;
import fitness.online.app.recycler.data.HandbookFilterData;
import fitness.online.app.recycler.item.HandbookFilterItem;
import io.reactivex.Observable;

/**
 * Created by user on 16.10.17.
 */

public class HandbookFilterFragmentPresenter extends HandbookFilterFragmentContract.Presenter implements HandbookFilterData.Listener {

    public void loadFilter() {
        Observable.just(RealmHandbookDataSource.getInstance().getFilter()).subscribe(this::loadFilterSuccess);
    }


    @Override
    public void loadFilterSuccess(List<HandbookFilter> filter) {
        executeBounded(view -> view.bindData(prepareData(filter)));
    }

    @Override
    public void onFilterClicked() {
        RealmHandbookDataSource.getInstance().enableFilter(true);
        executeBounded(view -> view.filterAction());
    }

    @Override
    public void onClearFilterClicked() {
        RealmHandbookDataSource.getInstance().enableFilter(false);
        Observable.just(RealmHandbookDataSource.getInstance().defaultFilter()).subscribe(this::loadFilterSuccess);
        executeBounded(view -> view.filterAction());
    }

    private List<BaseItem> prepareData(List<HandbookFilter> data) {
        ArrayList<BaseItem> result = new ArrayList<>();
        for (HandbookFilter item : data) {
            result.add(new HandbookFilterItem(new HandbookFilterData(item, this)));
        }
        return result;
    }

    @Override
    public void onRangeChanged(HandbookFilter filter, int min, int max) {
        RealmHandbookDataSource.getInstance().changeFilter(filter, min, max);
        //executeBounded(view -> view.updateData());
    }

    @Override
    public void onFilterEnabled(HandbookFilter filter, boolean enabled) {
        RealmHandbookDataSource.getInstance().enableFilter(filter, enabled);
        executeBounded(view -> view.updateData());
    }
}
