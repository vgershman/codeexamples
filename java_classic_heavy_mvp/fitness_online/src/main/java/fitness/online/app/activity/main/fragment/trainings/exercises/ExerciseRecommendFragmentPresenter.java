package fitness.online.app.activity.main.fragment.trainings.exercises;

import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.contract.fragment.ExerciseRecommendFragmentContract;

/**
 * Created by user on 28.11.17.
 */

public class ExerciseRecommendFragmentPresenter extends ExerciseRecommendFragmentContract.Presenter {


    @Override
    public void onPlayClicked(int exerciseId) {
        HandbookExercise exercise = RealmHandbookDataSource.getInstance().getExerciseById(exerciseId + "");
        HandbookNavigation navigation = new HandbookNavigation();
        navigation.setId(exercise.getId());
        executeBounded(view -> view.openExerciseInfo(navigation));
    }
}
