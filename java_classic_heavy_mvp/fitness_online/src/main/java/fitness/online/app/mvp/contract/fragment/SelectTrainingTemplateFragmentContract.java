package fitness.online.app.mvp.contract.fragment;

import com.trimf.recycler.item.BaseItem;

import java.util.List;

import fitness.online.app.model.pojo.realm.common.trainings.TrainingTemplate;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface SelectTrainingTemplateFragmentContract {

    interface View extends FragmentView {

        void dataReplace(List<BaseItem> data);

        void onTemplateClicked(TrainingTemplate template);

        void onCreateCustom();

        void onPricesLoaded();
    }

    abstract class Presenter extends BaseFragmentPresenter<SelectTrainingTemplateFragmentContract.View> {

        public abstract void loadTemplates();
    }
}
