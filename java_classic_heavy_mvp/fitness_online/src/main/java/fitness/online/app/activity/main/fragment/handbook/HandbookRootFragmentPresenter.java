package fitness.online.app.activity.main.fragment.handbook;

import android.support.v4.app.Fragment;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.model.Api;
import fitness.online.app.model.api.HandbookApi;
import fitness.online.app.model.pojo.realm.handbook.Handbook;
import fitness.online.app.model.pojo.realm.handbook.HandbookEntity;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.model.pojo.realm.handbook.HandbookResponse;
import fitness.online.app.mvp.contract.fragment.HandbookRootFragmentContract;
import fitness.online.app.recycler.data.HandbookNavigationData;
import fitness.online.app.recycler.item.HandbookNavigationItem;
import fitness.online.app.recycler.item.HandbookProductItem;
import fitness.online.app.util.rx.ExceptionUtils;
import fitness.online.app.util.scheduler.SchedulerTransformer;


/**
 * Created by user on 04.10.17.
 */

public class HandbookRootFragmentPresenter extends HandbookRootFragmentContract.Presenter implements HandbookNavigationData.Listener {

    private String categoryId;

    private boolean fromDiet, fromTrainer;

    public HandbookRootFragmentPresenter(String categoryId, boolean fromDiet, boolean fromTrainer) {
        this.categoryId = categoryId;
        this.fromDiet = fromDiet;
        this.fromTrainer = fromTrainer;
    }

    public String getCategoryId() {
        return categoryId;
    }

    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        if (firstBind) {
            loadData();
        }
    }

    @Override
    public void loadData() {
        RealmHandbookDataSource.getInstance().listData(categoryId).subscribe(this::loadDataLocalSuccess);
    }

    @Override
    public void sync() {
        RealmHandbookDataSource realmDao = RealmHandbookDataSource.getInstance();
        Handbook handbook = realmDao.getCurrentHandbook();
        Integer version = null;
        if (handbook != null) {
            version = handbook.getVersion();
        }
        HandbookApi handbookApi = Api.getService(HandbookApi.class);
        handbookApi.getApiV1Handbook(version).compose(SchedulerTransformer.ioToMain()).subscribe(this::handbookLoaded, this::handbookFailed);
    }

    public void handbookFailed(Throwable t) {
        ExceptionUtils.sendThrowable(t);
        executeBounded(view -> view.setRefreshing(false));
    }

    public void handbookLoaded(HandbookResponse response) {
        RealmHandbookDataSource.getInstance().store(response);
        executeBounded(view -> view.setRefreshing(false));
        executeBounded(view -> view.setupCacher());
        loadData();
    }

    @Override
    public void bindView(HandbookRootFragmentContract.View mvpView) {
        super.bindView(mvpView);
    }

    @Override
    public void loadDataLocalSuccess(List<HandbookNavigation> data) {
        executeBounded(view -> view.dataReplace(prepareData(data)));
    }


    private List<BaseItem> prepareData(List<HandbookNavigation> data) {
        ArrayList<BaseItem> result = new ArrayList<>();
        for (HandbookNavigation item : data) {
            boolean isLast = data.indexOf(item) == data.size() - 1;
            if (item.getType().equals(HandbookEntity.PRODUCT)) {
                result.add(new HandbookProductItem(item, this, isLast));
            } else {
                result.add(new HandbookNavigationItem(item, this, false, isLast));
            }
        }
        return result;
    }

    @Override
    public void onClick(HandbookNavigation handbookNavigation) {
        executeBounded(view -> view.onNavigationSelect(handbookNavigation));
    }

    @Override
    public void onSecondaryClick(HandbookNavigation handbookNavigation) {
    }

    protected Fragment createNextFragment(HandbookNavigation navigation) {
        if (navigation.getType().equals(HandbookEntity.FAQ) || navigation.getType().equals(HandbookEntity.SPORT_FOOD) || navigation.getType().equals(HandbookEntity.PHARMACY)) {
            return HandbookPostFragment.newInstance(navigation);
        }
        if (navigation.getType().equals(HandbookEntity.EXERCISE)) {
            return HandbookExerciseFragment.newInstance(navigation, false);
        }
        if (navigation.getType().equals(HandbookEntity.PRODUCT)) {
            return HandbookProductFragment.newInstance(navigation, fromDiet, fromTrainer);
        }
        List<HandbookNavigation> catItems = RealmHandbookDataSource.getInstance().listDataSimple(navigation.getId());
        if (catItems.size() == 1) {
            return createNextFragment(catItems.get(0));
        }
        return HandbookRootFragment.newInstance(navigation, fromDiet, fromTrainer);
    }
}
