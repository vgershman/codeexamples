package fitness.online.app.activity.main.fragment.trainings.exercises;

import android.support.annotation.NonNull;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import fitness.online.app.R;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.ResponseListener;
import fitness.online.app.data.remote.RetrofitTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.model.pojo.realm.common.trainings.ExercisePyramid;
import fitness.online.app.model.pojo.realm.common.trainings.ExercisePyramidDto;
import fitness.online.app.model.pojo.realm.common.trainings.HistoryRecord;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingDay;
import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.contract.fragment.RecommendFragmentContract;
import fitness.online.app.recycler.data.trainings.ExerciseHistoryData;
import fitness.online.app.recycler.data.trainings.ExerciseRecommendData;
import fitness.online.app.recycler.item.ClickListener;
import fitness.online.app.recycler.item.HeaderItem_;
import fitness.online.app.recycler.item.trainings.ExerciseHistoryItem;
import fitness.online.app.recycler.item.trainings.RecommendCardioItem;
import fitness.online.app.recycler.item.trainings.RecommendLinearItem;
import fitness.online.app.recycler.item.trainings.RecommendPyramidItem;

/**
 * Created by user on 28.11.17.
 */

public class RecommendFragmentPresenter extends RecommendFragmentContract.Presenter implements ClickListener<HistoryRecord>, ExerciseRecommendData.Listener {

    private DayExerciseDto mExercise;

    private RecommendLinearItem mRecommendItem;

    private RecommendPyramidItem mPyramidItem;

    private RecommendCardioItem mCardioItem;

    private String mType;

    private TrainingCourse mCourse;

    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        if (firstBind) {
            sync();
            loadData();
        }
    }

    public RecommendFragmentPresenter(DayExerciseDto mExercise, String mType, TrainingCourse mCourse) {
        this.mExercise = mExercise;
        this.mType = mType;
        this.mCourse = mCourse;
    }

    @Override
    public void onPlayClicked(int exerciseId) {
        HandbookExercise exercise = RealmHandbookDataSource.getInstance().getExerciseById(exerciseId + "");
        HandbookNavigation navigation = new HandbookNavigation();
        navigation.setId(exercise.getId());
        executeBounded(view -> view.openExerciseInfo(navigation));
    }

    @Override
    public void loadData() {
        RealmTrainingsDataSource.getInstance().getHistoryByExerciseId(mExercise.getId()).subscribe(this::bindData);
    }

    @Override
    public void sync() {
        RetrofitTrainingsDataSource.getInstance().syncHistory(mCourse.getId(), mExercise.getId(), new BasicResponseListener() {
            @Override
            public void success(Object response) {
                loadData();
            }
        });
    }

    @Override
    public void upsync() {
        showProgressBar();
        RetrofitTrainingsDataSource.getInstance().updateTrainingDay(mCourse.getId(), mExercise.getTraining_day_id(), new ResponseListener<List<TrainingDay>>() {
            @Override
            public void success(List<TrainingDay> response) {
                executeBounded(view -> view.onSaved(true));
                hideProgressBar();
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                hideProgressBar();
            }
        });
    }

    private void bindData(List<HistoryRecord> data) {
        executeBounded(view -> view.dataReplace(prepareData(data)));
    }

    private List<BaseItem> prepareData(List<HistoryRecord> completed_sets) {
        List<BaseItem> items = new ArrayList<>();
        if (DayExerciseDto.PYRAMID.equals(mType)) {
            if (mPyramidItem == null) {
                mPyramidItem = new RecommendPyramidItem(new ExerciseRecommendData(mExercise, this));
            }
            items.add(mPyramidItem);
        }
        if (DayExerciseDto.LINEAR.equals(mType)) {
            if (mRecommendItem == null) {
                mRecommendItem = new RecommendLinearItem(new ExerciseRecommendData(mExercise, this));
            }
            items.add(mRecommendItem);
        }
        if (DayExerciseDto.CARDIO.equals(mType)) {
            if (mCardioItem == null) {
                mCardioItem = new RecommendCardioItem(new ExerciseRecommendData(mExercise, this));
            }
            items.add(mCardioItem);
        }
        if (completed_sets != null && completed_sets.size() != 0) {
            items.add(new HeaderItem_(R.string.header_exercise_history));
        }
        String prevHeader = "";
        int number = 1;
        for (HistoryRecord completed_set : completed_sets) {
            boolean showHeader;
            String curHeader = completed_set.getExecutedDate();
            if (curHeader.equals(prevHeader)) {
                showHeader = false;
                number--;
            } else {
                showHeader = true;
                number = countByDate(completed_sets, curHeader);
            }
            prevHeader = curHeader;
            items.add(new ExerciseHistoryItem(new ExerciseHistoryData(completed_set, showHeader, curHeader, number, this, null)));
        }
        return items;
    }

    private int countByDate(List<HistoryRecord> completed_sets, String curHeader) {
        int result = 0;
        for (HistoryRecord record : completed_sets) {
            if (record.getExecutedDate().equals(curHeader)) {
                result++;
            }
        }
        return result;
    }


    @Override
    public void onClick(HistoryRecord data) {
    }


    @Override
    public void onPyramidSave(boolean percents, List<ExercisePyramid> pyramid, String comment) {
        mExercise.setSet_type(DayExerciseDto.PYRAMID);
        mExercise.setComment(comment);
        List<ExercisePyramidDto> pyramidDtos = new ArrayList<>();
        for (ExercisePyramid exercisePyramid : pyramid) {
            pyramidDtos.add(new ExercisePyramidDto(exercisePyramid));
        }
        mExercise.setRecommended_pyramid(pyramidDtos);
        mExercise.setWeight_type(percents ? DayExerciseDto.PERCENT : DayExerciseDto.KG);
        executeBounded(view -> view.onSaved(false));
    }

    @Override
    public void onLinearSave(int sets, int repeats, String weight, boolean percents, String comment) {
        mExercise.setSet_type(DayExerciseDto.LINEAR);
        mExercise.setComment(comment);
        mExercise.setRecommended_sets(sets);
        mExercise.setRecommended_repeats(repeats);
        if (percents) {
            mExercise.setRecommended_max_weight_percent(Double.valueOf(weight));
            mExercise.setRecommended_weight_value(null);
            mExercise.setWeight_type(DayExerciseDto.PERCENT);
        } else {
            mExercise.setWeight_type(DayExerciseDto.KG);
            mExercise.setRecommended_max_weight_percent(null);
            mExercise.setRecommended_weight_value(Double.valueOf(weight));
        }
        executeBounded(view -> view.onSaved(false));
    }

    @Override
    public void onCardioSave(int minutes, int minPulse, int maxPulse, String comment) {
        mExercise.setSet_type(DayExerciseDto.CARDIO);
        mExercise.setComment(comment);
        mExercise.setRecommended_sets(minutes);
        mExercise.setRecommended_weight_value((maxPulse * 1000 + minPulse) * 1.d);
        executeBounded(view -> view.onSaved(false));
    }

    @Override
    public void onError(int error) {
        executeBounded(view -> view.error(error));
    }
}
