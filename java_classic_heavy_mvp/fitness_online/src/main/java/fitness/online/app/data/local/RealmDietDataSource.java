package fitness.online.app.data.local;

import fitness.online.app.model.pojo.realm.chat.Chat;
import fitness.online.app.model.pojo.realm.common.diet.Diet;
import fitness.online.app.model.pojo.realm.common.diet.Meal;
import fitness.online.app.model.pojo.realm.common.diet.Product;
import fitness.online.app.util.realm.RealmHelper;
import io.realm.Realm;
import timber.log.Timber;

/**
 * Created by user on 15.12.17.
 */

public class RealmDietDataSource {

    private Realm mRealm = RealmHelper.getCommonRealmInstance();

    private String editComment = null;

    public void deleteProduct(int productId) {
        try {
            mRealm.beginTransaction();
            mRealm.where(Product.class).equalTo("id", productId).findAll().deleteAllFromRealm();
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }

    public void deleteMeal(Integer mealId) {
        try {
            mRealm.beginTransaction();
            mRealm.where(Meal.class).equalTo("id", mealId).findAll().deleteAllFromRealm();
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }

    public String getEditComment() {
        return editComment;
    }

    public void setComment(String s) {
        editComment = s;
    }

    public void updateMeal(Meal meal, String time) {
        if (meal.isManaged()) {
            try {
                mRealm.beginTransaction();
                meal.setTime(time);
            } catch (Throwable throwable) {
                Timber.e(throwable);
            } finally {
                if (mRealm.isInTransaction()) {
                    mRealm.commitTransaction();
                }
            }
        }
    }

    public void updateProduct(Product product, Integer portion, String comment) {
        try {
            mRealm.beginTransaction();
            product.setComment(comment);
            product.setProduct_portion(portion);
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }

    public static class INSTANCE_HOLDER {
        public static final RealmDietDataSource INSTANCE = new RealmDietDataSource();
    }

    public static RealmDietDataSource getInstance() {
        return RealmDietDataSource.INSTANCE_HOLDER.INSTANCE;
    }

    public Diet getDietByCourseId(int courseId) {
        return mRealm.where(Diet.class).equalTo("course_id", courseId).findFirst();
    }

    public void storeDiet(Diet diet) {
        try {
            mRealm.beginTransaction();
            mRealm.copyToRealmOrUpdate(diet);
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }
}
