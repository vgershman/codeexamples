package fitness.online.app.activity.main.fragment.trainings.courses;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingTemplate;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.SelectTrainingTemplateFragmentContract;
import fitness.online.app.recycler.adapter.UniversalAdapter;

/**
 * Created by user on 26.10.17.
 */

public class SelectTemplateFragment extends BaseFragment<SelectTemplateFragmentPresenter> implements SelectTrainingTemplateFragmentContract.View {

    @BindView(R.id.recycler_view)
    public RecyclerView mRecyclerView;

    private ArrayList<BaseItem> mList = new ArrayList();

    private UniversalAdapter mAdapter;


    public static SelectTemplateFragment newInstance() {
        return new SelectTemplateFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new SelectTemplateFragmentPresenter();
    }

    @Nullable
    @Override
    public String getTitle() {
        return getString(R.string.ttl_select_template);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mAdapter = new UniversalAdapter(mList, 0);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void dataReplace(List<BaseItem> data) {
        mAdapter.replaceList(data);
    }

    @Override
    public void onTemplateClicked(TrainingTemplate template) {
        getFragNavController().pushFragment(TrainingTemplateDetailsFragment.newInstance(template));
    }

    @Override
    public void onCreateCustom() {
        getFragNavController().pushFragment(CreateTrainingFragment.newInstance());
    }

    @Override
    public void onPricesLoaded() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_select_template;
    }
}
