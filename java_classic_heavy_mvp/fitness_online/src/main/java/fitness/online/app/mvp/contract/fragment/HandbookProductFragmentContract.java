package fitness.online.app.mvp.contract.fragment;

import android.support.annotation.Nullable;

import fitness.online.app.model.pojo.realm.common.diet.Product;
import fitness.online.app.model.pojo.realm.handbook.HandbookProduct;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface HandbookProductFragmentContract {

    interface View extends FragmentView {
        void bindData(HandbookProduct product);

        void onSelected(boolean edit);

        void bindDiet(Product product);
    }

    abstract class Presenter extends BaseFragmentPresenter<HandbookProductFragmentContract.View> {

        public abstract void loadData();

        public abstract void loadDataLocalSuccess(@Nullable HandbookProduct product);

        public abstract void confirm(Integer portion, String comment);
    }
}
