package fitness.online.app.mvp.contract.fragment;

import com.trimf.recycler.item.BaseItem;

import java.util.List;

import fitness.online.app.model.pojo.realm.handbook.HandbookCategory;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 22.12.17.
 */

public interface SelectNutritionFragmentContract {

    interface View extends FragmentView {

        void bind(HandbookCategory category);

        void dataReplace(List<BaseItem> items);

        void viewDetails(HandbookNavigation handbookNavigation);

        void updateMenu();

        void onSelected(int depth);

        String getComment();

        void updateList(int newIndex, int oldIndex);
    }

    abstract class Presenter extends BaseFragmentPresenter<SelectNutritionFragmentContract.View> {

        public abstract void loadData();

        public abstract void confirm();
    }
}
