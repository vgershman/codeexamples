package fitness.online.app.activity.main.fragment.handbook;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.model.pojo.realm.handbook.HandbookEntity;
import fitness.online.app.mvp.contract.fragment.HandbookPostFragmentContract;
import io.reactivex.Observable;


/**
 * Created by user on 04.10.17.
 */

public class HandbookPostFragmentPresenter extends HandbookPostFragmentContract.Presenter {

    @Override
    public void loadData(@NonNull String id, @NonNull String type) {
        HandbookEntity entity = RealmHandbookDataSource.getInstance().getPostByIdAndType(id, type);
        Observable<HandbookEntity> observable = Observable.empty();
        if (entity != null) {
            observable = observable.just(entity);
        }
        observable.subscribe(this::loadDataLocalSuccess);
    }

    @Override
    public void loadDataLocalSuccess(@Nullable HandbookEntity handbookEntity) {
        executeBounded(view -> view.bindData(handbookEntity));
    }
}
