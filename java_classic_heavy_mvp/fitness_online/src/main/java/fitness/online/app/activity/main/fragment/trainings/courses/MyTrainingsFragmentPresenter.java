package fitness.online.app.activity.main.fragment.trainings.courses;

import android.support.annotation.NonNull;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;

import fitness.online.app.App;
import fitness.online.app.R;
import fitness.online.app.data.local.RealmOrdersDataSource;
import fitness.online.app.data.local.RealmSessionDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.data.local.RealmUsersDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitTrainingsDataSource;
import fitness.online.app.model.Api;
import fitness.online.app.model.api.FinancesApi;
import fitness.online.app.model.api.UsersApi;
import fitness.online.app.model.pojo.realm.common.order.Order;
import fitness.online.app.model.pojo.realm.common.order.OrderPayStatusEnum;
import fitness.online.app.model.pojo.realm.common.trainings.CoursesResponse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingTemplate;
import fitness.online.app.model.pojo.realm.common.user.User;
import fitness.online.app.mvp.contract.fragment.MyTrainingsFragmentContract;
import fitness.online.app.recycler.data.ButtonData_;
import fitness.online.app.recycler.data.EmptyData;
import fitness.online.app.recycler.data.EmptyDataWithButton;
import fitness.online.app.recycler.data.trainings.MyTrainingData;
import fitness.online.app.recycler.item.ClickListener;
import fitness.online.app.recycler.item.EmptyItemWithButton;
import fitness.online.app.recycler.item.trainings.MyTrainingItem;
import fitness.online.app.util.DateUtils;
import fitness.online.app.util.scheduler.SchedulerTransformer;
import fitness.online.app.view.progressBar.ProgressBarEntry;

/**
 * Created by user on 24.10.17.
 */

public class MyTrainingsFragmentPresenter extends MyTrainingsFragmentContract.Presenter implements ClickListener {

    private ProgressBarEntry entry;
    private static final String SUCCESS_RESULT = "success";

    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        //if (firstBind) {
        sync(() -> load());
        //}
        load();
    }

    @Override
    public void load() {
        RealmTrainingsDataSource.getInstance().listMyTrainings().subscribe(this::processData);
    }


    @Override
    public void sync(MyTrainingsFragmentContract.SyncListener syncListener) {
        executeBounded(view -> {
            entry = view.showProgressBar(false);
        });
        RetrofitTrainingsDataSource.getInstance().syncMyTrainings(new BasicResponseListener() {
            @Override
            public void success(Object response) {
                syncListener.onSynced();
                if (entry != null) {
                    executeBounded(view -> view.hideProgressBar(entry));
                }
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                if (entry != null) {
                    executeBounded(view -> view.hideProgressBar(entry));
                }
            }
        }, null);
    }

    private void empty() {
        List<BaseItem> items = new ArrayList<>();
        items.add(new EmptyItemWithButton(new EmptyDataWithButton(new EmptyData(R.string.empty_my_trainings, R.drawable.ic_bg_trainings),
                new ButtonData_((R.string.btn_create_training), this))));
        executeBounded(view -> view.dataReplace(items));
    }

    private void processData(CoursesResponse response) {
        executeBounded(view -> view.setRefreshing(false));
        //     empty();
        if (response.getCourses() == null || response.getCourses().size() == 0) {
            empty();
        } else {
            bindData(response);
        }
    }

    private void bindData(CoursesResponse response) {
        executeBounded(view -> view.dataReplace(prepareData(response)));
    }

    private List<BaseItem> prepareData(CoursesResponse response) {
        ArrayList<BaseItem> result = new ArrayList<>();
        for (TrainingCourse course : response.getCourses()) {
            TrainingTemplate template = null;
            User user = null;
            Order order = null;
            if (course.getInvoice_id() != null) {
                order = RealmTrainingsDataSource.getInstance().getOrderById(course.getInvoice_id());
                if (order != null) {
                    user = new User(RealmUsersDataSource.getInstance().getUserFull(order.getTrainerId()));
                }
            } else {
                if (course.getTemplate_id() != null) {
                    template = getTemplate(course.getTemplate_id(), response.getTemplates());
                } else {
                    user = new User(RealmSessionDataSource.getInstance().getCurrentUser());
                }
            }
            result.add(new MyTrainingItem(new MyTrainingData(course, template, user, order, this, deleteListener, prolongListener)));
        }
        return result;
    }

    private ClickListener<MyTrainingData> deleteListener = data -> executeBounded(view -> view.askToDelete(data.getCourse()));

    private ClickListener<TrainingCourse> prolongListener = data -> {
        sync(() -> {
            load();
            check(data);
        });
    };

    private void check(TrainingCourse course) {
        TrainingCourse course1 = RealmTrainingsDataSource.getInstance().getCourseById(course.getId());
        Date activeTo = DateUtils.parseUtcDate(course1.getActiveTo());
        Date curDate = new Date();
        int days = DateUtils.getDays(curDate, activeTo);
        if (days < 6) {
            Order order = RealmTrainingsDataSource.getInstance().getOrderById(course1.getInvoice_id());
            checkOrder(order, course);
            //payUser(order);
        }
    }

    private void checkOrder(@Nonnull Order order, TrainingCourse course) {
        if (order.getStatus().equals(OrderPayStatusEnum.NOT_PAID)) {
            payUser(order);
        } else {
            createNewOrder(course);
        }
    }

    private void createNewOrder(TrainingCourse course) {
        showProgressBar();
        FinancesApi financesApi = Api.getService(FinancesApi.class);
        financesApi.postApiV1Finances(
                null,
                1,
                RealmSessionDataSource.getInstance().getCurrentUser().getId(),
                course.getId(),
                null,
                null,
                null,
                null,
                null
        )
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(response -> {
                            RealmOrdersDataSource.getInstance()
                                    .addOrder(response).subscribe(() ->
                                    payUser(response.getOrder()));
                        },
                        throwable -> {
                            hideProgressBar();
                            executeBounded(errorView -> errorView.showError(throwable));
                        });
    }


    private void payUser(@NonNull Order order) {
        showProgressBar();
        UsersApi usersApi = Api.getService(UsersApi.class);
        usersApi.getApiV1UsersMeCard()
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(response -> {
                    if (response.code() == 204) {
                        RealmUsersDataSource.getInstance().removeCardsResponse();
                        payNoCard(order);
                    } else {
                        payWithCard(order);
                    }
                }, throwable -> {
                    hideProgressBar();
                    executeBounded(errorView -> errorView.showError(throwable));
                });
    }

    private void payWithCard(@NonNull Order order) {
        FinancesApi financesApi = Api.getService(FinancesApi.class);
        financesApi.postApiV1FinancesIdPay(order.getId())
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(
                        response -> {
                            hideProgressBar();
                            sync(() -> load());
                        }, throwable -> hideProgressBar()
                );
    }

    private void payNoCard(@NonNull Order order) {
        FinancesApi financesApi = Api.getService(FinancesApi.class);
        financesApi.getApiV1FinancesIdRequestPaymentData(order.getId())
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(
                        response -> {
                            hideProgressBar();
                            executeBounded(view -> view.openPayFragment(response.getPaymentData().getRequestString()));
                        }, throwable -> hideProgressBar()
                );
    }


    public void deleteCourse(TrainingCourse course) {
        showProgressBar();
        RetrofitTrainingsDataSource.getInstance().deleteTraining(course.getId(), new BasicResponseListener() {
            @Override
            public void success(Object response) {
                hideProgressBar();
                load();
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                hideProgressBar();
            }
        });
    }

    private TrainingTemplate getTemplate(Integer template_id, List<TrainingTemplate> templates) {
        for (TrainingTemplate template : templates) {
            if (template_id.equals(template.getId())) {
                return template;
            }
        }
        return null;
    }

    @Override
    public void onWebViewResultUrl(@Nonnull String resultUrl) {
        if (resultUrl.contains(SUCCESS_RESULT)) {
            sync(() -> load());
        } else {
            executeBounded(view -> {
                view.showDialog(
                        App.getContext().getString(R.string.error),
                        App.getContext().getString(R.string.error_paying_try_later)
                );
            });
        }
    }

    @Override
    public void onClick(Object clicked) {
        if (clicked instanceof TrainingCourse) {
            executeBounded(view -> view.onTrainingSelect((TrainingCourse) clicked));
        }
        if (clicked instanceof ButtonData_) {
            executeBounded(view -> view.openNewTraining());
        }
    }
}
