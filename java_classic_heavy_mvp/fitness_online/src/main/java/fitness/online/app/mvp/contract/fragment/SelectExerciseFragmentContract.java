package fitness.online.app.mvp.contract.fragment;

import com.trimf.recycler.item.BaseItem;

import java.util.List;

import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface SelectExerciseFragmentContract {

    interface View extends FragmentView {

        void dataReplace(List<BaseItem> data);

        void onCategorySelect(String categoryId);

        void openExercise(HandbookNavigation item);
    }

    abstract class Presenter extends BaseFragmentPresenter<SelectExerciseFragmentContract.View> {

        public abstract void loadData();

        public abstract void loadDataLocalSuccess(List<HandbookNavigation> data);

        public abstract void confirmSelected();
    }
}
