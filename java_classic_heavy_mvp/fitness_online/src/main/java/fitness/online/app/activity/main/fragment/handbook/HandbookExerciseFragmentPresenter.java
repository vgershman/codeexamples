package fitness.online.app.activity.main.fragment.handbook;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.mvp.contract.fragment.HandbookExerciseFragmentContract;
import io.reactivex.Observable;


/**
 * Created by user on 04.10.17.
 */

public class HandbookExerciseFragmentPresenter extends HandbookExerciseFragmentContract.Presenter {

    @Override
    public void loadData(@NonNull String id) {
        HandbookExercise exercise = RealmHandbookDataSource.getInstance().getExerciseById(id);
        Observable<HandbookExercise> observable = Observable.empty();
        if (exercise != null) {
            observable = observable.just(exercise);
        }
        observable.subscribe(this::loadDataLocalSuccess);
    }

    @Override
    public void loadDataLocalSuccess(@Nullable HandbookExercise exercise) {
        executeBounded(view -> view.bindData(exercise));
    }

    @Override
    public void toggle(HandbookExercise exercise) {
        RealmTrainingsDataSource.getInstance().toggleSelected(exercise.getId());
    }

    @Override
    public void onPlayClicked(HandbookExercise exercise) {
        executeBounded(view -> view.openVideoPlayer(exercise));
    }
}
