package fitness.online.app.activity.main.fragment.trainings.exercises;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.activity.main.fragment.handbook.HandbookExerciseFragment;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.local.RealmSessionDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.RecommendFragmentContract;
import fitness.online.app.recycler.adapter.UniversalAdapter;
import fitness.online.app.util.rx.ExceptionUtils;

/**
 * Created by user on 28.11.17.
 */

public class RecommendFragment extends BaseFragment<RecommendFragmentPresenter> implements RecommendFragmentContract.View {


    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private UniversalAdapter mAdapter;

    private List<BaseItem> mList = new ArrayList<>();

    private DayExerciseDto mExerciseDto;

    private TrainingCourse mCourse;

    private String mType;

    private HandbookExercise mHandbookExercise;

    @Override
    public int getMenuId() {
        return R.menu.exercise_history;
    }


    @Nullable
    @Override
    public String getTitle() {
        return mHandbookExercise.getTitle();
    }

    public static RecommendFragment newInstance(DayExerciseDto exercise, TrainingCourse course, String type) {
        RecommendFragment fragment = new RecommendFragment();
        Bundle args = new Bundle();
        args.putInt("exer", exercise.getId());
        args.putInt("course", course.getId());
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mExerciseDto = RealmTrainingsDataSource.getInstance().getEditExerciseById(getArguments().getInt("exer"));
        mHandbookExercise = RealmHandbookDataSource.getInstance().getExerciseById(mExerciseDto.getPost_exercise_id() + "");
        mCourse = RealmTrainingsDataSource.getInstance().getCourseById(getArguments().getInt("course"));
        mType = getArguments().getString("type");
        mPresenter = new RecommendFragmentPresenter(mExerciseDto, mType, mCourse);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exercise_play:
                mPresenter.onPlayClicked(mExerciseDto.getPost_exercise_id());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void openExerciseInfo(HandbookNavigation navigation) {
        getFragNavController().pushFragment(HandbookExerciseFragment.newInstance(navigation, false));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mAdapter = new UniversalAdapter(mList, 0);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }


    @Override
    public int getLayout() {
        return R.layout.fragment_exercise_history;
    }


    @Override
    public void updateList() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void error(int error) {
        ExceptionUtils.sendThrowable(new Throwable(getString(error)));
    }

    @Override
    public void onSaved(boolean flag) {
        boolean isTrainer = RealmSessionDataSource.getInstance().isTrainer();
        if (flag) {
            RealmTrainingsDataSource.getInstance().setNeedReaload(true);
        }
        if (isTrainer && !flag) {
            mPresenter.upsync();
        } else {
            getFragNavController().popFragment();
        }
    }


    @Override
    public void dataReplace(List<BaseItem> data) {
        mAdapter.replaceList(data);
    }
}
