package fitness.online.app.activity.main.fragment.trainings.courses;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.MyTrainingsFragmentContract;
import fitness.online.app.recycler.adapter.UniversalAdapter;
import fitness.online.app.activity.webView.WebViewActivity;

import static android.app.Activity.RESULT_OK;

/**
 * Created by user on 24.10.17.
 */

public class MyTrainingsFragment extends BaseFragment<MyTrainingsFragmentPresenter> implements MyTrainingsFragmentContract.View {

    private static final int WEB_VIEW_ACTIVITY = 1010;

    @BindView(R.id.recycler_view)
    public RecyclerView mRecyclerView;

    @BindView(R.id.swipe_refresh_layout)
    public SwipeRefreshLayout mRefresher;

    private ArrayList<BaseItem> mList = new ArrayList();

    private UniversalAdapter mAdapter;


    @NonNull
    public static MyTrainingsFragment newInstance() {
        MyTrainingsFragment fragment = new MyTrainingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new MyTrainingsFragmentPresenter();
    }

    @Nullable
    @Override
    public String getTitle() {
        return getString(R.string.ttl_my_trainings);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mAdapter = new UniversalAdapter(mList, 0);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mRefresher.setOnRefreshListener(() -> mPresenter.sync(() -> mPresenter.load()));
        return view;
    }

    @Override
    public int getMenuId() {
        return R.menu.my_trainings;
    }

    @Override
    public void askToDelete(TrainingCourse course) {
        new AlertDialog.Builder(getContext())
                .setMessage(R.string.alert_delete_course)
                .setPositiveButton(R.string.alert_yes, (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    mPresenter.deleteCourse(course);
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel())
                .create().show();
    }

    @Override
    public void openPayFragment(@Nonnull String url) {
        Intent intent = WebViewActivity.getOpenIntent(
                getActivity(),
                url,
                getString(R.string.pay),
                true,
                false,
                new String[]{"fit://", "fitnessonlineapp.com"}
        );
        startActivityForResult(intent, WEB_VIEW_ACTIVITY);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_training:
                openNewTraining();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == WEB_VIEW_ACTIVITY) {
            if (resultCode == RESULT_OK) {
                String resultUrl = data.getStringExtra(WebViewActivity.EXTRA_RESULT_URL);
                if (resultUrl == null) {
                    resultUrl = "";
                }
                mPresenter.onWebViewResultUrl(resultUrl);
            }
        }
    }

    @Override
    public void dataReplace(List<BaseItem> data) {
        mAdapter.replaceList(data);
    }

    @Override
    public void onTrainingSelect(TrainingCourse course) {
        getFragNavController().pushFragment(TrainingFragment.newInstance(course, false));
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        mRefresher.setRefreshing(refreshing);
    }

    @Override
    public void openNewTraining() {
        getFragNavController().pushFragment(SelectTemplateFragment.newInstance());
    }


    @Override
    public int getLayout() {
        return R.layout.fragment_my_trainings;
    }
}
