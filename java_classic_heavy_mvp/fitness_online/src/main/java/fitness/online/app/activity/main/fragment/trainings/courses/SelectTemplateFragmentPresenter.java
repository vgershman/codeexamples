package fitness.online.app.activity.main.fragment.trainings.courses;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.SkuDetails;
import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import fitness.online.app.App;
import fitness.online.app.R;
import fitness.online.app.billing.BillingManager;
import fitness.online.app.data.local.RealmBillingDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.billing.SkuData;
import fitness.online.app.model.pojo.realm.common.trainings.CoursesResponse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingTemplate;
import fitness.online.app.mvp.contract.fragment.SelectTrainingTemplateFragmentContract;
import fitness.online.app.recycler.data.ButtonData_;
import fitness.online.app.recycler.data.trainings.TrainingTemplateData;
import fitness.online.app.recycler.item.ClickListener;
import fitness.online.app.recycler.item.HeaderItem_;
import fitness.online.app.recycler.item.RedButtonItem;
import fitness.online.app.recycler.item.trainings.TrainingTemplateItem;
import fitness.online.app.view.progressBar.ProgressBarEntry;

/**
 * Created by user on 26.10.17.
 */

public class SelectTemplateFragmentPresenter extends SelectTrainingTemplateFragmentContract.Presenter implements ClickListener {

    private ProgressBarEntry entry;

    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        if (firstBind) {
            loadTemplates();
        }
    }

    @Override
    public void loadTemplates() {
        executeBounded(view -> entry = view.showProgressBar(false));
        RetrofitTrainingsDataSource.getInstance().listTemplates(new BasicResponseListener<CoursesResponse>() {
            @Override
            public void success(CoursesResponse response) {
                if (entry != null) {
                    executeBounded(view -> view.hideProgressBar(entry));
                }
                executeBounded(view -> view.dataReplace(prepareData(response)));
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                if (entry != null) {
                    executeBounded(view -> view.hideProgressBar(entry));
                }
            }
        });
    }

    private void syncPrices(List<String> skuList) {
        BillingManager.getInstance().connect(new BillingManager.ConnectListener() {
            @Override
            public void connected(@NonNull BillingManager billingManager) {
                billingManager.querySkuDetailsAsync(BillingClient.SkuType.INAPP, skuList, (responseCode, skuDetailsList) -> {
                    if (responseCode == BillingClient.BillingResponse.OK) {
                        if (skuDetailsList != null) {
                            for (SkuDetails skuDetail : skuDetailsList) {
                                SkuData skuData = new SkuData(
                                        skuDetail.getSku(),
                                        skuDetail.getDescription(),
                                        skuDetail.getPrice(),
                                        skuDetail.getPriceCurrencyCode(),
                                        skuDetail.getTitle(),
                                        skuDetail.getType()
                                );
                                RealmBillingDataSource.getInstance().saveSkuData(skuData);
                                executeBounded(view -> view.onPricesLoaded());
                            }
                        }
                    } else {
                        executeBounded(errorView -> errorView.showError(new Throwable(App.getContext().getString(R.string.error_loading_inap_prices))));
                    }
                });
            }

            @Override
            public void disconnected(@NonNull BillingManager billingManager) {
                //do nothing
            }
        });
    }

    private List<BaseItem> prepareData(CoursesResponse response) {
        List<String> skuList = new ArrayList<>();
        List<BaseItem> results = new ArrayList<>();
        List<TrainingTemplate> authorTemplates = new ArrayList<>();
        List<TrainingTemplate> freeTemplates = new ArrayList<>();
        for (TrainingTemplate template : response.getTemplates()) {
            if (template.getType().equals("author")) {
                authorTemplates.add(template);
            }
            if (template.getType().equals("free")) {
                freeTemplates.add(template);
            }
            if (!TextUtils.isEmpty(template.getAndroidInapId())) {
                skuList.add(template.getAndroidInapId());
            }
        }
        if (authorTemplates.size() > 0) {
            results.add(new HeaderItem_(R.string.header_author_templates));
            for (TrainingTemplate template : authorTemplates) {
                results.add(new TrainingTemplateItem(new TrainingTemplateData(template, authorTemplates.indexOf(template) == authorTemplates.size() - 1, this)));
            }
        }
        if (freeTemplates.size() > 0) {
            results.add(new HeaderItem_(R.string.header_free_templates));
            for (TrainingTemplate template : freeTemplates) {
                results.add(new TrainingTemplateItem(new TrainingTemplateData(template, freeTemplates.indexOf(template) == freeTemplates.size() - 1, this)));
            }
        }
        results.add(new RedButtonItem(new ButtonData_(R.string.btn_create_custom_course, this)));
        syncPrices(skuList);
        return results;
    }

    @Override
    public void onClick(Object data) {
        if (data instanceof ButtonData_) {
            executeBounded(view -> view.onCreateCustom());
        }
        if (data instanceof TrainingTemplate) {
            executeBounded(view -> view.onTemplateClicked((TrainingTemplate) data));
        }
    }
}
