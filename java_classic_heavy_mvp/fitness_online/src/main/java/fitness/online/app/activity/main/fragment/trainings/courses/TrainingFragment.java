package fitness.online.app.activity.main.fragment.trainings.courses;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;

import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.activity.main.fragment.trainings.diet.TrainingDietFragment;
import fitness.online.app.activity.main.fragment.trainings.exercises.TrainingDaysFragment;
import fitness.online.app.activity.main.fragment.trainings.nutrition.TrainingNutritionFragment;
import fitness.online.app.data.local.RealmOrdersDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.data.local.RealmUsersDataSource;
import fitness.online.app.model.pojo.realm.common.order.Order;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.common.user.UserFull;
import fitness.online.app.model.pojo.realm.handbook.HandbookCategory;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.TrainingFragmentContract;
import fitness.online.app.util.ImageHelper;

/**
 * Created by user on 25.10.17.
 */

public class TrainingFragment extends BaseFragment<TrainingFragmentPresenter> implements TrainingFragmentContract.View {

    @BindView(R.id.btnWorkout)
    LinearLayout btnWorkout;

    @BindView(R.id.btnDiet)
    LinearLayout btnDiet;

    @BindView(R.id.btnNutrition)
    LinearLayout btnNutrition;

    @BindView(R.id.btnPharm)
    LinearLayout btnPharm;

    @BindView(R.id.ivWorkout)
    SimpleDraweeView ivWorkout;

    @BindView(R.id.ivDiet)
    SimpleDraweeView ivDiet;

    @BindView(R.id.ivNutrition)
    SimpleDraweeView ivNutrition;

    @BindView(R.id.ivPharm)
    SimpleDraweeView ivPharm;

    private TrainingCourse mTraining;
    private boolean mTrainer;

    public static TrainingFragment newInstance(TrainingCourse trainingCourse, boolean trainer) {
        TrainingFragment trainingFragment = new TrainingFragment();
        Bundle args = new Bundle();
        args.putBoolean("trainer", trainer);
        args.putInt("training", trainingCourse.getId());
        trainingFragment.setArguments(args);
        return trainingFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTraining = RealmTrainingsDataSource.getInstance().getCourseById(getArguments().getInt("training"));
        mTrainer = getArguments().getBoolean("trainer");
        mPresenter = new TrainingFragmentPresenter(mTraining, mTrainer);
    }

    @Override
    public int getMenuId() {
        if (mTrainer) {
            return R.menu.trainer_training_main;
        } else {
            return super.getMenuId();
        }
    }


    @Nullable
    @Override
    public String getTitle() {
        if (mTrainer) {
            if (mTraining.getInvoice_id() != null) {
                Order order = RealmOrdersDataSource.getInstance().getById(mTraining.getInvoice_id());
                if (order != null) {
                    UserFull userFull = RealmUsersDataSource.getInstance().getUserFull(order.getClientId());
                    if (userFull != null) {
                        return userFull.getFullName();
                    }
                }
            }
        } else {
            if (mTraining.getInvoice_id() != null) {
                Order order = RealmOrdersDataSource.getInstance().getById(mTraining.getInvoice_id());
                if (order != null) {
                    UserFull userFull = RealmUsersDataSource.getInstance().getUserFull(order.getTrainerId());
                    if (userFull != null) {
                        return String.format(getString(R.string.from_trainer), userFull.getFullName());
                    }
                }
            }
        }
        return mTraining.getName();
    }

    @Override
    public void bindTrainings(HandbookCategory category) {
        btnWorkout.setVisibility(View.VISIBLE);
        btnWorkout.setOnClickListener(view -> openTrainings());
        ImageHelper.loadHandbookIcon(ivWorkout, category.getPhoto_url(), category.getPhoto_ext());
    }

    private void openTrainings() {
        getFragNavController().pushFragment(TrainingDaysFragment.newInstance(mTraining));
    }

    private void openNutrition() {
        getFragNavController().pushFragment(TrainingNutritionFragment.newInstance(mTraining));
    }

    private void openDiet() {
        getFragNavController().pushFragment(TrainingDietFragment.newInstance(mTraining));
    }

    private void openPharm() {
        //getFragNavController().pushFragment(TrainingExercisesFragment.newInstance(""));
    }

    @Override
    public void bindDiet(HandbookCategory category) {
        btnDiet.setVisibility(View.VISIBLE);
        btnDiet.setOnClickListener(view -> openDiet());
        ImageHelper.loadHandbookIcon(ivDiet, category.getPhoto_url(), category.getPhoto_ext());
    }

    @Override
    public void bindNutrition(HandbookCategory category) {
        btnNutrition.setVisibility(View.VISIBLE);
        btnNutrition.setOnClickListener(view -> openNutrition());
        ImageHelper.loadHandbookIcon(ivNutrition, category.getPhoto_url(), category.getPhoto_ext());
    }

    @Override
    public void bindPharm(HandbookCategory category) {
        btnPharm.setVisibility(View.VISIBLE);
        btnPharm.setOnClickListener(view -> openPharm());
        ImageHelper.loadHandbookIcon(ivPharm, category.getPhoto_url(), category.getPhoto_ext());
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_training;
    }
}
