package fitness.online.app.activity.main.fragment.trainings.diet;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fitness.online.app.R;
import fitness.online.app.data.local.RealmDietDataSource;
import fitness.online.app.data.local.RealmSessionDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitDietDataSource;
import fitness.online.app.model.pojo.realm.common.diet.Diet;
import fitness.online.app.model.pojo.realm.common.diet.DietResponse;
import fitness.online.app.model.pojo.realm.common.diet.Meal;
import fitness.online.app.model.pojo.realm.common.diet.MealResponse;
import fitness.online.app.model.pojo.realm.common.diet.Product;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.mvp.contract.fragment.TrainingFragmentDietContract;
import fitness.online.app.recycler.data.ButtonData_;
import fitness.online.app.recycler.data.EmptyData;
import fitness.online.app.recycler.data.EmptyDataWithButton;
import fitness.online.app.recycler.data.diet.DietData;
import fitness.online.app.recycler.data.diet.MealHeaderData;
import fitness.online.app.recycler.data.diet.MealProductData;
import fitness.online.app.recycler.item.ClickListener;
import fitness.online.app.recycler.item.EmptyItem;
import fitness.online.app.recycler.item.EmptyItemWithButton;
import fitness.online.app.recycler.item.SimpleButtonItem;
import fitness.online.app.recycler.item.diet.DietCommentItem;
import fitness.online.app.recycler.item.diet.DietSummaryItem;
import fitness.online.app.recycler.item.diet.MealHeaderItem;
import fitness.online.app.recycler.item.diet.MealProductItem;

/**
 * Created by user on 25.10.17.
 */

public class TrainingDietFragmentPresenter extends TrainingFragmentDietContract.Presenter implements ClickListener<MealProductData> {

    private TrainingCourse mCourse;

    private Diet mDiet;

    private boolean editMode = false;

    private boolean needSync = false;

    public boolean isEditMode() {
        return editMode;
    }


    public TrainingDietFragmentPresenter(TrainingCourse course) {
        this.mCourse = course;
    }

    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        if (firstBind || needSync) {
            needSync = false;
            sync();
        }
        loadData();
    }

    private void processData(Diet diet) {
        mDiet = diet;
        if (!editMode && (mDiet == null || mDiet.getMeals() == null || mDiet.getMeals().size() == 0)) {
            empty();
        } else {
            bindData();
        }
    }


    public void empty() {
        List<BaseItem> items = new ArrayList<>();
        if (mCourse.getInvoice_id() == null || RealmSessionDataSource.getInstance().isTrainer()) {
            items.add(new EmptyItemWithButton(new EmptyDataWithButton(new EmptyData(R.string.empty_diet, R.drawable.add_diet),
                    new ButtonData_(R.string.btn_add_new_meal, data -> {
                        editMode = true;
                        addNewMeal();
                        executeBounded(view -> view.bindState(editMode));
                    }))));
        } else {
            items.add(new EmptyItem(new EmptyData(R.string.empty_diet, R.drawable.add_diet)));
        }
        executeBounded(view -> view.dataReplace(items));
    }

    private void addNewMeal() {
        Meal meal = new Meal("07:00");
        List<Product> products = new ArrayList<>();
        meal.setProducts(products);
        showProgressBar(true);
        RetrofitDietDataSource.getInstance().createNewMeal(mDiet.getId(), meal, new BasicResponseListener<MealResponse>() {
            @Override
            public void success(MealResponse response) {
                askTime(response.getMeal());
                hideProgressBar();
                sync();
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                hideProgressBar();
            }
        });
    }

    public void sync() {
        showProgressBar(false);
        RetrofitDietDataSource.getInstance().getDietByCourseId(mCourse.getId(), new BasicResponseListener<DietResponse>() {
            @Override
            public void success(DietResponse response) {
                hideProgressBar();
                loadData();
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                hideProgressBar();
            }
        });
    }

    @Override
    public void loadData() {
        processData(RealmDietDataSource.getInstance().getDietByCourseId(mCourse.getId()));
    }

    @Override
    public void edit() {
        editMode = true;
        executeBounded(view -> view.bindState(editMode));
        loadData();
    }

    @Override
    public void confirm() {
        editMode = false;
        executeBounded(view -> view.bindState(editMode));
        if (RealmDietDataSource.getInstance().getEditComment() != null) {
            showProgressBar(false);
            RetrofitDietDataSource.getInstance().updateDiet(RealmDietDataSource.getInstance().getEditComment(), new BasicResponseListener<DietResponse>() {
                @Override
                public void success(DietResponse response) {
                    hideProgressBar();
                    mDiet = response.getDiet();
                    loadData();
                }

                @Override
                public void error(@NonNull Throwable throwable) {
                    super.error(throwable);
                    hideProgressBar();
                }
            });
        }
    }

    @Override
    public void deleteProduct(MealProductData data) {
        int productId = data.getProduct().getId();
        RealmDietDataSource.getInstance().deleteProduct(productId);
        loadData();
        showProgressBar(true);
        RetrofitDietDataSource.getInstance().deleteProduct(mDiet.getId(), data.getMeal().getId(), productId, new BasicResponseListener() {
            @Override
            public void success(Object response) {
                hideProgressBar();
                sync();
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                hideProgressBar();
            }
        });
    }

    @Override
    public void setMealTime(Meal meal, String time) {
        showProgressBar(false);
        RealmDietDataSource.getInstance().updateMeal(meal, time);
        loadData();
        RetrofitDietDataSource.getInstance().updateMeal(meal.getId(), time, new BasicResponseListener<MealResponse>() {
            @Override
            public void success(MealResponse response) {
                hideProgressBar();
                sync();
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                hideProgressBar();
            }
        });
    }

    private void bindData() {
        executeBounded(view -> view.dataReplace(prepareData()));
    }

    private List<BaseItem> prepareData() {
        List<BaseItem> result = new ArrayList<>();
        List<Meal> meals = new ArrayList<>();
        meals.addAll(mDiet.getMeals());
        Collections.sort(meals, (meal, t1) -> {
            int time1 = 0;
            try {
                time1 = Integer.parseInt(meal.getTime().replace(":", ""));
            } catch (Exception ex) {
            }
            int time2 = 0;
            try {
                time2 = Integer.parseInt(t1.getTime().replace(":", ""));
            } catch (Exception ex) {
            }
            return time1 - time2;
        });
        for (int i = 0; i < meals.size(); i++) {
            Meal meal = meals.get(i);
            boolean showPane = (meal.getProducts() != null && meal.getProducts().size() > 0);
            result.add(new MealHeaderItem(new MealHeaderData(i + 1, meal, editMode, showPane,
                    data -> addProductTo(data),
                    data -> askToDeleteMeal(data),
                    data -> askTime(data))));
            List<Product> sortedProducts = new ArrayList<>();
            sortedProducts.addAll(meal.getProducts());
            Collections.sort(sortedProducts, (product, t1) -> product.getId() - t1.getId());
            for (Product product : sortedProducts) {
                boolean last = meal.getProducts().indexOf(product) == meal.getProducts().size() - 1;
                result.add(new MealProductItem(new MealProductData(meal, product, last, editMode, this)));
            }
        }
        if (editMode) {
            result.add(new SimpleButtonItem(new ButtonData_(R.string.btn_add_new_meal, data -> addNewMeal())));
        } else {
            result.add(new DietSummaryItem(new DietData(mDiet)));
        }
        if (editMode || !TextUtils.isEmpty(mDiet.getCommon_advices())) {
            result.add(new DietCommentItem(new DietData(mDiet), editMode));
        }
        return result;
    }

    private void askTime(Meal data) {
        executeBounded(view -> view.selectTime(data));
    }

    private void askToDeleteMeal(Meal data) {
        executeBounded(view -> view.askToDelete(data));
    }

    private void askToDeleteProduct(MealProductData data) {
        executeBounded(view -> view.askToDeleteProduct(data));
    }

    public void deleteMeal(Meal data) {
        showProgressBar(true);
        int mealId = data.getId();
        RealmDietDataSource.getInstance().deleteMeal(mealId);
        loadData();
        RetrofitDietDataSource.getInstance().deleteMeal(mDiet.getId(), mealId, new BasicResponseListener() {
            @Override
            public void success(Object response) {
                hideProgressBar();
                sync();
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                hideProgressBar();
            }
        });
    }

    private void addProductTo(Meal data) {
        needSync = true;
        RetrofitDietDataSource.getInstance().setEditMeal(data);
        executeBounded(view -> view.selectProduct());
    }

    @Override
    public void onClick(MealProductData data) {
        if (isEditMode()) {
            askToDeleteProduct(data);
        } else {
            needSync = true;
            RetrofitDietDataSource.getInstance().setEditMeal(data.getMeal());
            executeBounded(view -> view.openProduct(data));
        }
    }
}
