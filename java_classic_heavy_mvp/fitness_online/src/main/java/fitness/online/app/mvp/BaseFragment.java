package fitness.online.app.mvp;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ncapdevi.fragnav.FragNavController;
import com.trello.navi2.component.support.NaviFragment;
import com.trello.rxlifecycle2.LifecycleProvider;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.trello.rxlifecycle2.navi.NaviLifecycle;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import javax.annotation.Nonnull;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import fitness.online.app.App;
import fitness.online.app.R;
import fitness.online.app.util.DialogHelper;
import fitness.online.app.util.rx.ExceptionUtils;
import fitness.online.app.view.OptionMenuIcon;
import fitness.online.app.view.progressBar.ProgressBarEntry;
import io.reactivex.Observable;

public abstract class BaseFragment<T extends BaseFragmentPresenter> extends NaviFragment implements FragmentView {
    protected T mPresenter;
    private Unbinder mUnbinder;
    Unregistrar mUnregistrar;

    public final LifecycleProvider<FragmentEvent> fragentLifecycleProvider =
            NaviLifecycle.createFragmentLifecycleProvider(this);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mPresenter != null) {
            mPresenter.bindView(this);
        }
        // keyboard open listener
        final boolean[] ignore = {KeyboardVisibilityEvent.isKeyboardVisible(getActivity())};

        mUnregistrar = KeyboardVisibilityEvent.registerEventListener(
                getActivity(), b -> {
                    if (ignore[0]) {
                        ignore[0] = false;
                    } else {
                        keyboardOpen(b);
                    }
                }
        );
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mPresenter != null) {
            mPresenter.unbindView();
        }
        if (mUnregistrar != null) {
            mUnregistrar.unregister();
            mUnregistrar = null;
        }
    }

    protected void keyboardOpen(boolean open) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        int layout = getLayout();
        if (mPresenter != null) {
            mPresenter.onCreateView();
        }
        if (layout != View.NO_ID) {
            View view = inflater.inflate(getLayout(), container, false);
            mUnbinder = ButterKnife.bind(this, view);
            return view;
        }
        return null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) {
            mPresenter.onDestroyView();
        }
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.onDestroy();
        }
    }

    @Nullable
    public String getTitle() {
        return null;
    }

    public View getTitleView(Toolbar toolbar) {
        return null;
    }

    public int getMenuId() {
        return R.menu.empty_menu;
    }

    public void updateMenuBadge(OptionMenuIcon optionMenuIcon, int menuId) {
        //do nothing
    }

    public void searchView(@Nonnull android.view.MenuItem searchMenuItem, @NonNull SearchView searchView) {
        //do nothing
    }

    public int getHomeIconId() {
        return View.NO_ID;
    }

    public int getFabIconId() {
        return View.NO_ID;
    }

    public boolean isTransparentToolBar() {
        return false;
    }

    public int getToolBarColor() {
        return ContextCompat.getColor(App.getContext(), R.color.primary);
    }

    public boolean showToolBarShadow() {
        return true;
    }

    public abstract int getLayout();

    public Observable<FragmentEvent> getLifecycle() {
        return fragentLifecycleProvider.lifecycle();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void showDialog(String title, String text) {
        DialogHelper.showDialog(getActivity(), title, text, null, null);
    }

    @Override
    public void showDialog(String title, String text, DialogInterface.OnClickListener ok, DialogInterface.OnClickListener cancel) {
        DialogHelper.showDialog(getActivity(), title, text, ok, cancel);
    }

    @Override
    public void showEditTextDialog(@NonNull String title,
                                   @Nullable String text,
                                   @Nullable String hint,
                                   int maxLines,
                                   int inputType,
                                   @NonNull DialogHelper.EditTextDialogListener listener) {
        DialogHelper.showEditTextDialog(
                getActivity(),
                title,
                text,
                hint,
                maxLines,
                inputType,
                listener
        );
    }

    @Override
    public
    @NonNull
    ProgressBarEntry showProgressBar(boolean blockTouch) {
        Activity activity = getActivity();
        if (activity instanceof ActionBarActivity) {
            return ((ActionBarActivity) activity).showProgressBar(blockTouch);
        }

        return new ProgressBarEntry(blockTouch);
    }

    @Override
    public void hideProgressBar(ProgressBarEntry entry) {
        Activity activity = getActivity();
        if (activity instanceof ActionBarActivity) {
            ((ActionBarActivity) activity).hideProgressBar(entry);
        }
    }

    @Override
    public void updateFab() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof ActionBarActivity) {
            ((ActionBarActivity) activity).updateFab();
        }
    }

    public FragNavController getFragNavController() {
        if (getActivity() instanceof BaseActivity) {
            return ((BaseActivity) getActivity()).getFragNavController();
        }
        throw new IllegalStateException("FragNavController not supported");
    }

    public void invalidateOptionsMenu() {
        getActivity().invalidateOptionsMenu();
    }

    protected void addChildFragment(@NonNull BaseFragment fragment) {
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content, fragment);
        transaction.commit();
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).hideSoftKeyboard();
        } else {
            throw new IllegalStateException("hideSoftKeyboard not supported");
        }
    }

    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(@NonNull View view) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showSoftKeyboard(view);
        } else {
            throw new IllegalStateException("showSoftKeyboard not supported");
        }
    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        return false;
    }

    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void fabClicked() {
        //do nothing
    }

    //todo: think make other way
    public void newObject(@NonNull Object object) {
        //do nothing
    }

    @Override
    public void showError(Throwable throwable) {
        ExceptionUtils.sendThrowable(throwable);
    }
}
