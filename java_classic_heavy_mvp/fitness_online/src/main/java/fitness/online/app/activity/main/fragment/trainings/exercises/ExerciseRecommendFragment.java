package fitness.online.app.activity.main.fragment.trainings.exercises;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;

import com.trimf.viewpager.SimpleFragmentPagerAdapter;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.activity.main.fragment.handbook.HandbookExerciseFragment;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.ExerciseRecommendFragmentContract;

/**
 * Created by user on 28.11.17.
 */

public class ExerciseRecommendFragment extends BaseFragment<ExerciseRecommendFragmentPresenter> implements ExerciseRecommendFragmentContract.View {

    @BindView(R.id.vp_fragments)
    ViewPager vpFragments;

    @BindView(R.id.tl_fragments)
    TabLayout tlFragments;

    SimpleFragmentPagerAdapter mFragmentPagerAdapter;

    private DayExerciseDto mExerciseDto;

    private HandbookExercise mHandbookExercise;

    private TrainingCourse mCourse;

    public static ExerciseRecommendFragment newInstance(DayExerciseDto exercise, TrainingCourse course) {
        ExerciseRecommendFragment fragment = new ExerciseRecommendFragment();
        Bundle args = new Bundle();
        args.putInt("exer", exercise.getId());
        args.putInt("course", course.getId());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ExerciseRecommendFragmentPresenter();
        mExerciseDto = RealmTrainingsDataSource.getInstance().getEditExerciseById(getArguments().getInt("exer"));
        mHandbookExercise = RealmHandbookDataSource.getInstance().getExerciseById(mExerciseDto.getPost_exercise_id() + "");
        mCourse = RealmTrainingsDataSource.getInstance().getCourseById(getArguments().getInt("course"));
    }

    @Override
    public int getMenuId() {
        return R.menu.exercise_history;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exercise_play:
                if (mExerciseDto != null) {
                    mPresenter.onPlayClicked(mExerciseDto.getPost_exercise_id());
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void openExerciseInfo(HandbookNavigation navigation) {
        getFragNavController().pushFragment(HandbookExerciseFragment.newInstance(navigation, false));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpViewPager();
    }

    @Nullable
    @Override
    public String getTitle() {
        return mHandbookExercise.getTitle();
    }

    private void setUpViewPager() {
        mFragmentPagerAdapter = new SimpleFragmentPagerAdapter(getChildFragmentManager());
        mFragmentPagerAdapter.addFragment(RecommendFragment.newInstance(mExerciseDto, mCourse, DayExerciseDto.LINEAR), getString(R.string.page_recommend_linear));
        mFragmentPagerAdapter.addFragment(RecommendFragment.newInstance(mExerciseDto, mCourse, DayExerciseDto.PYRAMID), getString(R.string.page_recommend_pyramid));
        vpFragments.setAdapter(mFragmentPagerAdapter);
        tlFragments.setupWithViewPager(vpFragments);
        if (DayExerciseDto.PYRAMID.equals(mExerciseDto.getSet_type())) {
            vpFragments.setCurrentItem(1);
        }
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_exercise_recommend;
    }
}
