package fitness.online.app.data.remote;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.android.billingclient.api.Purchase;

import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import fitness.online.app.model.pojo.realm.common.MediaData;
import fitness.online.app.model.pojo.realm.common.comment.NewCommentResponse;
import fitness.online.app.model.pojo.realm.common.post.NewPostResponse;
import fitness.online.app.model.pojo.realm.common.sending.NewSendingComment;
import fitness.online.app.model.pojo.realm.common.sending.NewSendingPhoto;
import fitness.online.app.model.pojo.realm.common.sending.NewSendingPost;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingTemplate;
import fitness.online.app.model.pojo.realm.common.user.UserFullResponse;

public interface DataSource {
    void createPost(@NonNull NewSendingPost newSendingPost, @NonNull CreateListener<NewPostResponse> listener);

    void updatePost(int id, @Nullable String body, @NonNull List<MediaData> media, @NonNull CreateListener<NewPostResponse> listener);

    void createComment(@NonNull NewSendingComment newSendingComment, int postId, @NonNull CreateListener<NewCommentResponse> listener);

    void createPhoto(@NonNull NewSendingPhoto newSendingPhoto, @NonNull CreateListener<UserFullResponse> listener);

    void createAvatar(@NonNull Uri uri, @NonNull CreateListener<UserFullResponse> listener);

    void updateUser(@NonNull Map<String, Object> params, @NonNull CreateListener<UserFullResponse> listener);

    void createCourse(@Nonnull Purchase purchase, @Nonnull TrainingTemplate trainingTemplate, @NonNull CreateListener listener);

    interface CreateListener<T> {
        void success(T response);

        void error(@NonNull Throwable throwable);
    }
}
