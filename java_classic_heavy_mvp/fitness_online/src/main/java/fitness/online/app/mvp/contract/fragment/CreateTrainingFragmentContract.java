package fitness.online.app.mvp.contract.fragment;

import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface CreateTrainingFragmentContract {

    interface View extends FragmentView {

        void onSuccess();
    }

    abstract class Presenter extends BaseFragmentPresenter<CreateTrainingFragmentContract.View> {

        public abstract void createTraining(String name, String goal);
    }
}
