package fitness.online.app.data.remote;

import android.support.annotation.NonNull;

/**
 * Created by user on 26.10.17.
 */

public interface ResponseListener<T> {
    void success(T response);

    void error(@NonNull Throwable throwable);
}
