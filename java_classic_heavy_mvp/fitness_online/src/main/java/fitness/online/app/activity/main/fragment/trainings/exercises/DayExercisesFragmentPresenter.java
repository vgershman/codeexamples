package fitness.online.app.activity.main.fragment.trainings.exercises;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Pair;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import fitness.online.app.R;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.local.RealmSessionDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.model.pojo.realm.common.trainings.EditTrainingDay;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingDay;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingDayResponse;
import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.mvp.contract.fragment.DayExercisesFragmentContract;
import fitness.online.app.recycler.data.ButtonData_;
import fitness.online.app.recycler.data.trainings.DayExerciseData;
import fitness.online.app.recycler.data.trainings.TrainingDayData;
import fitness.online.app.recycler.item.ClickListener;
import fitness.online.app.recycler.item.RedButtonItem;
import fitness.online.app.recycler.item.SimpleButtonItem;
import fitness.online.app.recycler.item.trainings.DayExerciseItem;
import fitness.online.app.recycler.item.trainings.DayHeaderItem;
import fitness.online.app.view.progressBar.ProgressBarEntry;

/**
 * Created by user on 30.10.17.
 */

public class DayExercisesFragmentPresenter extends DayExercisesFragmentContract.Presenter implements ClickListener {

    private boolean mEditState = false;

    private TrainingDay mDay;

    private TrainingCourse mCourse;

    private ProgressBarEntry entry;


    public DayExercisesFragmentPresenter(TrainingDay day, TrainingCourse course, boolean editState) {
        mDay = day;
        mCourse = course;
        mEditState = editState;
        if (mEditState) {
            RealmTrainingsDataSource.getInstance().editTrainingDay(mDay);
            RealmTrainingsDataSource.getInstance().initSelectedExercises();
        }
    }

    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        if (firstBind || RealmTrainingsDataSource.getInstance().needReload()) {
            RealmTrainingsDataSource.getInstance().setNeedReaload(false);
            loadData();
        }
    }

    @Override
    public void loadData() {
        bindState();
        if (mDay == null) {
            bindData(new ArrayList<>());
        } else {
            bindData(RealmTrainingsDataSource.getInstance().listExercisesByDay_(mDay.getId()));
        }
    }

    private void bindState() {
        executeBounded(view -> view.bindState(mEditState));
    }

    private void bindData(List<DayExerciseDto> exercises) {
        executeBounded(view -> view.dataReplace(prepareData(exercises)));
    }

    @Override
    public void edit() {
        editMode();
        mEditState = true;
        loadData();
    }

    public void editMode() {
        RealmTrainingsDataSource.getInstance().editTrainingDay(mDay);
        RealmTrainingsDataSource.getInstance().initSelectedExercises();
    }

    @Override
    public void deleteDay() {
        executeBounded(view -> entry = view.showProgressBar(true));
        RetrofitTrainingsDataSource.getInstance().deleteDay(mCourse.getId(), mDay.getId(), new BasicResponseListener<Object>() {
            @Override
            public void success(Object response) {
                executeBounded(view -> {
                    view.hideProgressBar(entry);
                    view.onDayEditSuccess();
                });
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                executeBounded(view -> view.hideProgressBar(entry));
            }
        });
    }

    public boolean isEditState() {
        return mEditState;
    }

    private List<BaseItem> prepareData(List<DayExerciseDto> exercises) {
        List<BaseItem> items = new ArrayList<>();
        TrainingDay headerDay = mDay;
        int headerNumber = 0;
        if (headerDay == null) {
            EditTrainingDay newDay = RealmTrainingsDataSource.getInstance().getEditDay();
            String title = "";
            if (newDay != null && newDay.getTitle() != null) {
                title = newDay.getTitle();
            }
            headerDay = new TrainingDay(-1, title);
            headerNumber = RealmTrainingsDataSource.getInstance().getNextDayNumberByCourse(mCourse);
        } else {
            headerNumber = RealmTrainingsDataSource.getInstance().getDayIndex(headerDay);
        }
        DayHeaderItem headerItem = new DayHeaderItem(new TrainingDayData(mEditState, headerDay, headerNumber, null, this, mDay == null));
        headerItem.getData().setTextListener(newText -> {
            if (RealmTrainingsDataSource.getInstance().getEditDay() != null) {
                RealmTrainingsDataSource.getInstance().getEditDay().setTitle(newText);
            }
        });
        items.add(headerItem);
        List<DayExerciseDto> exerciseDtos;
        if (!mEditState) {
            exerciseDtos = exercises;
        } else {
            exerciseDtos = RealmTrainingsDataSource.getInstance().listEditTrainings();
        }
        for (DayExerciseDto dayExercise : exerciseDtos) {
            items.add(new DayExerciseItem(new DayExerciseData(mEditState, dayExercise, this, superSetListener, dragListener, deleteListener)));
        }
        if (mCourse.getInvoice_id() == null || RealmSessionDataSource.getInstance().isTrainer()) {
            if (mEditState) {
                items.add(new SimpleButtonItem(new ButtonData_(R.string.btn_add_new_exercise, this)));
                if (mDay != null) {
                    items.add(new RedButtonItem(new ButtonData_(R.string.btn_delete_whole_day, data -> askToDeleteDay())));
                }
            }
        }
        return items;
    }

    private void askToDeleteDay() {
        executeBounded(view -> view.askToDeleteDay());
    }

    private ClickListener<DayExerciseDto> deleteListener = data -> executeBounded(view -> view.showDeleteAlert(data));

    private ClickListener<RecyclerView.ViewHolder> dragListener = data -> executeBounded(view -> view.startDrag(data));

    private ClickListener<Pair<DayExerciseDto, Boolean>> superSetListener = data -> {
        RealmTrainingsDataSource.getInstance().toggleSuperset(data.first.getId(), data.second);
        executeBounded(view -> view.updateList());
    };

    @Override
    public void onClick(Object data) {
        if (data instanceof DayExerciseDto) {
            HandbookExercise handbookExercise = RealmHandbookDataSource.getInstance().getExerciseById(((DayExerciseDto) data).getPost_exercise_id() + "");
            executeBounded(view -> view.onExerciseClick((DayExerciseDto) data, handbookExercise));
        }
        if (data instanceof ButtonData_) {
            RealmTrainingsDataSource.getInstance().setNeedReaload(true);
            executeBounded(view -> view.onSelectExercisesClick());
        }
    }

    @Override
    public void confirm() {
        if (mDay == null) {
            createNewDay();
        } else {
            updateDay();
        }
    }

    private void updateDay() {
        EditTrainingDay editTrainingDay = RealmTrainingsDataSource.getInstance().getEditDay();
        if (TextUtils.isEmpty(editTrainingDay.getTitle()) || TextUtils.isEmpty(editTrainingDay.getTitle().replaceAll(" ", ""))) {
            executeBounded(view -> view.noTitle());
            return;
        }
        if (editTrainingDay.getExercises() == null || editTrainingDay.getExercises().size() == 0) {
            executeBounded(view -> view.noExercises());
            return;
        }
        executeBounded(view -> entry = view.showProgressBar(true));
        RetrofitTrainingsDataSource.getInstance().updateTrainingDay(mCourse.getId(), mDay.getId(), new BasicResponseListener<List<TrainingDay>>() {
            @Override
            public void success(List<TrainingDay> response) {
                mEditState = false;
                loadData();
                executeBounded(view -> view.hideProgressBar(entry));
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                executeBounded(view -> view.hideProgressBar(entry));
            }
        });
    }

    private void createNewDay() {
        EditTrainingDay editTrainingDay = RealmTrainingsDataSource.getInstance().getEditDay();
        if (TextUtils.isEmpty(editTrainingDay.getTitle()) || TextUtils.isEmpty(editTrainingDay.getTitle().replaceAll(" ", ""))) {
            executeBounded(view -> view.noTitle());
            return;
        }
        if (editTrainingDay.getExercises() == null || editTrainingDay.getExercises().size() == 0) {
            executeBounded(view -> view.noExercises());
            return;
        }
        executeBounded(view -> entry = view.showProgressBar(true));
        RetrofitTrainingsDataSource.getInstance().createNewTrainingDay(mCourse.getId(), new BasicResponseListener<TrainingDayResponse>() {
            @Override
            public void success(TrainingDayResponse response) {
                executeBounded(view -> view.onDayEditSuccess());
                executeBounded(view -> view.hideProgressBar(entry));
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                executeBounded(view -> view.hideProgressBar(entry));
            }
        });
    }

    public void updatePositions(List<BaseItem> items) {
        for (int i = 0; i < items.size(); i++) {
            BaseItem item = items.get(i);
            if (item instanceof DayExerciseItem) {
                String exerciseId = ((DayExerciseItem) item).getData().getDayExercise().getId().toString();
                RealmTrainingsDataSource.getInstance().setSelectedPosition(exerciseId, i);
            }
        }
        RealmTrainingsDataSource.getInstance().checkSuperset();
    }

    public void deleteExcercise(DayExerciseDto data) {
        RealmTrainingsDataSource.getInstance().getEditDay().getExercises().remove(data);
        RealmTrainingsDataSource.getInstance().checkSuperset();
        loadData();
    }
}
