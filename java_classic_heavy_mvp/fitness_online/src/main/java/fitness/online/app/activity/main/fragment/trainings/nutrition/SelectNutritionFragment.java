package fitness.online.app.activity.main.fragment.trainings.nutrition;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.activity.main.fragment.handbook.HandbookPostFragment;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.local.RealmNutritionDataSource;
import fitness.online.app.model.pojo.realm.handbook.HandbookCategory;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.SelectNutritionFragmentContract;
import fitness.online.app.recycler.adapter.UniversalAdapter;

/**
 * Created by user on 22.12.17.
 */

public class SelectNutritionFragment extends BaseFragment<SelectNutritionFragmentPresenter> implements SelectNutritionFragmentContract.View {

    private String mCategoryId;
    private boolean mEditable;
    private Integer mItemId;
    private String mComment;

    @BindView(R.id.llComment)
    LinearLayout llComment;

    @BindView(R.id.etComment)
    EditText etComment;


    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private UniversalAdapter mAdapter;

    private List<BaseItem> items = new ArrayList<>();

    private String mTitle = "";

    @Override
    public void bind(HandbookCategory category) {
        mTitle = category.getTitle();
    }

    @Override
    public void dataReplace(List<BaseItem> items) {
        mAdapter.replaceList(items);
    }

    @Override
    public int getMenuId() {
        if (mEditable) {
            if (RealmNutritionDataSource.getInstance().getSelectedId() == null) {
                return R.menu.confirm;
            } else {
                return R.menu.confirm_active;
            }
        } else {
            return R.menu.empty_menu;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.confirm) {
            mPresenter.confirm();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void viewDetails(HandbookNavigation handbookNavigation) {
        getFragNavController().pushFragment(HandbookPostFragment.newInstance(handbookNavigation));
    }

    @Override
    public void updateMenu() {
        invalidateOptionsMenu();
    }

    @Override
    public void onSelected(int depth) {
        getFragNavController().popFragments(depth);
    }

    @Override
    public String getComment() {
        return etComment.getText().toString();
    }

    @Override
    public void updateList(int newIndex, int oldIndex) {
        mAdapter.notifyItemChanged(newIndex);
        mAdapter.notifyItemChanged(oldIndex);
        invalidateOptionsMenu();
    }

    @Nullable
    @Override
    public String getTitle() {
        return mTitle;
    }

    public static SelectNutritionFragment newInstance(Integer courseId, String categoryId, boolean editable, Integer selectedId, String comment) {
        SelectNutritionFragment fragment = new SelectNutritionFragment();
        Bundle args = new Bundle();
        args.putInt("course", courseId);
        args.putBoolean("editable", editable);
        args.putString("category", categoryId);
        if (selectedId != null) {
            args.putInt("item", selectedId);
        }
        args.putString("comment", comment);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mAdapter = new UniversalAdapter(items, 0);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
        etComment.setEnabled(mEditable);
        etComment.setText(mComment);
        etComment.setSelection(etComment.getText().toString().length());
        if (!mEditable && TextUtils.isEmpty(mComment)) {
            llComment.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCategoryId = getArguments().getString("category");
        mTitle = RealmHandbookDataSource.getInstance().getCategoryById(mCategoryId).getTitle();
        mEditable = getArguments().getBoolean("editable");
        mItemId = getArguments().getInt("item");
        mComment = getArguments().getString("comment", "");
        int courseId = getArguments().getInt("course");
        mPresenter = new SelectNutritionFragmentPresenter(courseId, mCategoryId, mItemId, mEditable);
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_select_nutrition;
    }
}
