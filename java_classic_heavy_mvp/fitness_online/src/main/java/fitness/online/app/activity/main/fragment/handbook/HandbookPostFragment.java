package fitness.online.app.activity.main.fragment.handbook;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.parceler.Parcels;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.model.pojo.realm.handbook.HandbookCategory;
import fitness.online.app.model.pojo.realm.handbook.HandbookEntity;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.HandbookPostFragmentContract;
import fitness.online.app.util.ImageHelper;

/**
 * Created by user on 09.10.17.
 */

public class HandbookPostFragment extends BaseFragment<HandbookPostFragmentPresenter> implements HandbookPostFragmentContract.View {

    private static final String EXTRA_INPUT = "navigation";

    @BindView(R.id.tv_title)
    TextView title;

    @BindView(R.id.tv_description)
    TextView description;

    @BindView(R.id.iv_avatar)
    SimpleDraweeView avatar;

    private HandbookCategory mCategory;


    @Override
    public void bindData(HandbookEntity handbookEntity) {
        title.setText(handbookEntity.getTitle());
        description.setText(handbookEntity.getDescription());
        ImageHelper.loadImage(avatar, handbookEntity.getPhoto_url(), handbookEntity.getPhoto_ext());
        mCategory = RealmHandbookDataSource.getInstance().getCategoryById(handbookEntity.getCategory_id());
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_handbook_post;
    }

    @NonNull
    public static HandbookPostFragment newInstance(@Nullable HandbookNavigation navigation) {
        HandbookPostFragment fragment = new HandbookPostFragment();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_INPUT, Parcels.wrap(navigation));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        HandbookNavigation navigation = Parcels.unwrap(getArguments().getParcelable(EXTRA_INPUT));
        mPresenter = new HandbookPostFragmentPresenter();
        mPresenter.loadData(navigation.getId(), navigation.getType());
    }

    @Nullable
    @Override
    public String getTitle() {
        return mCategory == null ? "" : mCategory.getTitle();
    }
}
