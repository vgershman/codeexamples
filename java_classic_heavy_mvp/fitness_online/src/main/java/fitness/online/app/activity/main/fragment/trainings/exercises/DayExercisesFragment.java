package fitness.online.app.activity.main.fragment.trainings.exercises;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.MenuItem;
import android.view.View;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.data.local.RealmSessionDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingDay;
import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.DayExercisesFragmentContract;
import fitness.online.app.recycler.SwipeTouchCallback;
import fitness.online.app.recycler.adapter.UniversalAdapter;
import fitness.online.app.util.rx.ExceptionUtils;


/**
 * Created by user on 30.10.17.
 */

public class DayExercisesFragment extends BaseFragment<DayExercisesFragmentPresenter> implements DayExercisesFragmentContract.View {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private UniversalAdapter mAdapter;

    private ItemTouchHelper mTouchHelper;

    private List<BaseItem> mList = new ArrayList<>();

    private TrainingDay mDay;

    private TrainingCourse mCourse;

    private boolean mEditMode = false;


    public static DayExercisesFragment newInstance(TrainingDay day, TrainingCourse course) {
        DayExercisesFragment fragment = new DayExercisesFragment();
        Bundle args = new Bundle();
        if (day != null) {
            args.putInt("day", day.getId());
        }
        args.putInt("course", course.getId());
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void dataReplace(List<BaseItem> items) {
        mAdapter.replaceList(items);
    }

    @Override
    public void onExerciseClick(DayExerciseDto exercise, HandbookExercise handbookExercise) {
        boolean recommend = mEditMode || RealmSessionDataSource.getInstance().isTrainer();
        if (!recommend) {
            getFragNavController().pushFragment(ExerciseHistoryFragment.newInstance(exercise, mCourse, true));
        } else {
            if (RealmSessionDataSource.getInstance().isTrainer()) {
                mPresenter.editMode();
            }
            if (HandbookExercise.CARDI0.equals(handbookExercise.getExercise_type())) {
                getFragNavController().pushFragment(RecommendFragment.newInstance(exercise, mCourse, DayExerciseDto.CARDIO));
            } else {
                getFragNavController().pushFragment(ExerciseRecommendFragment.newInstance(exercise, mCourse));
            }
        }
    }

    @Nullable
    @Override
    public String getTitle() {
        if (mDay == null) {
            return getString(R.string.ttl_new_day);
        }
        return getString(R.string.ttl_existing_day);
    }


    @Override
    public int getMenuId() {
        if (mCourse.getInvoice_id() == null || RealmSessionDataSource.getInstance().isTrainer()) {
            if (mPresenter.isEditState()) {
                return R.menu.training_day_edit;
            } else {
                return R.menu.training_day_view;
            }
        } else {
            return R.menu.empty_menu;
        }
    }

    @Override
    public int getHomeIconId() {
        if (mDay == null) {
            return R.drawable.ic_actionbar_close;
        } else {
            return super.getHomeIconId();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_edit) {
            mPresenter.edit();
        }
        if (item.getItemId() == R.id.action_confirm) {
            mPresenter.confirm();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void bindState(boolean isEdit) {
        mEditMode = isEdit;
        if (isEdit) {
            mTouchHelper = new ItemTouchHelper(new SwipeTouchCallback(mAdapter));
            mTouchHelper.attachToRecyclerView(mRecyclerView);
        } else {
            if (mTouchHelper != null) {
                mTouchHelper.attachToRecyclerView(null);
            }
        }
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onSelectExercisesClick() {
        getFragNavController().pushFragment(SelectExerciseFragment.newInstance(null));
    }

    @Override
    public void onDayEditSuccess() {
        getFragNavController().popFragment();
    }

    @Override
    public void noExercises() {
        ExceptionUtils.sendThrowable(new Throwable(getString(R.string.error_add_excercises)));
    }

    @Override
    public void noTitle() {
        ExceptionUtils.sendThrowable(new Throwable(getString(R.string.error_day_no_title)));
    }

    @Override
    public void updateList() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void startDrag(RecyclerView.ViewHolder viewHolder) {
        mTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void showDeleteAlert(DayExerciseDto data) {
        new AlertDialog.Builder(getContext())
                .setMessage(R.string.alert_delete_excercise)
                .setPositiveButton(R.string.alert_yes, (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    mPresenter.deleteExcercise(data);
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel())
                .create().show();
    }

    @Override
    public void askToDeleteDay() {
        new AlertDialog.Builder(getContext())
                .setMessage(R.string.alert_delete_day)
                .setPositiveButton(R.string.alert_yes, (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    mPresenter.deleteDay();
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel())
                .create().show();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int day = getArguments().getInt("day", -1);
        if (day != -1) {
            mDay = RealmTrainingsDataSource.getInstance().getDayById(day);
        }
        mCourse = RealmTrainingsDataSource.getInstance().getCourseById(getArguments().getInt("course"));
        mPresenter = new DayExercisesFragmentPresenter(mDay, mCourse, mDay == null);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new UniversalAdapter(mList, 0);
        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
                super.onItemRangeMoved(fromPosition, toPosition, itemCount);
                mPresenter.updatePositions(mList);
                mAdapter.notifyItemChanged(fromPosition);
                mAdapter.notifyItemChanged(toPosition);
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_day_exercises;
    }
}
