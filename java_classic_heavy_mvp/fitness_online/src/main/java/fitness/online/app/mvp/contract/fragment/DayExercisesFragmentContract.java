package fitness.online.app.mvp.contract.fragment;

import android.support.v7.widget.RecyclerView;

import com.trimf.recycler.item.BaseItem;

import java.util.List;

import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface DayExercisesFragmentContract {

    interface View extends FragmentView {

        void dataReplace(List<BaseItem> items);

        void onExerciseClick(DayExerciseDto exercise, HandbookExercise handbookExercise);

        void bindState(boolean isEdit);

        void onSelectExercisesClick();

        void onDayEditSuccess();

        void noExercises();

        void noTitle();

        void updateList();

        void startDrag(RecyclerView.ViewHolder viewHolder);

        void showDeleteAlert(DayExerciseDto data);

        void askToDeleteDay();
    }

    abstract class Presenter extends BaseFragmentPresenter<DayExercisesFragmentContract.View> {


        public abstract void loadData();

        public abstract void edit();

        public abstract void confirm();

        public abstract void deleteDay();

        public abstract void editMode();
    }
}
