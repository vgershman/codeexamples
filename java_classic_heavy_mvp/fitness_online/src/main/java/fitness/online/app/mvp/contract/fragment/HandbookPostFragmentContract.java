package fitness.online.app.mvp.contract.fragment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import fitness.online.app.model.pojo.realm.handbook.HandbookEntity;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface HandbookPostFragmentContract {

    interface View extends FragmentView {
        void bindData(HandbookEntity handbookEntity);
    }

    abstract class Presenter extends BaseFragmentPresenter<HandbookPostFragmentContract.View> {

        public abstract void loadData(@NonNull String id, @NonNull String type);

        public abstract void loadDataLocalSuccess(@Nullable HandbookEntity handbookEntity);
    }
}
