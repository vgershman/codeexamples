package fitness.online.app.mvp.contract.fragment;

import com.trimf.recycler.item.BaseItem;

import java.util.List;

import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface RecommendFragmentContract {

    interface View extends FragmentView {

        void dataReplace(List<BaseItem> data);

        void updateList();

        void error(int error);

        void onSaved(boolean flag);

        void openExerciseInfo(HandbookNavigation navigation);
    }

    abstract class Presenter extends BaseFragmentPresenter<RecommendFragmentContract.View> {

        public abstract void onPlayClicked(int excerciseId);

        public abstract void loadData();

        public abstract void sync();

        public abstract void upsync();
    }
}
