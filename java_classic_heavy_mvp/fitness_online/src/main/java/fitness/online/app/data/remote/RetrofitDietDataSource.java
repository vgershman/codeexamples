package fitness.online.app.data.remote;

import fitness.online.app.data.local.RealmDietDataSource;
import fitness.online.app.model.Api;
import fitness.online.app.model.api.DietsApi;
import fitness.online.app.model.pojo.realm.common.diet.Diet;
import fitness.online.app.model.pojo.realm.common.diet.Meal;
import fitness.online.app.model.pojo.realm.common.diet.MealRequest;
import fitness.online.app.model.pojo.realm.common.diet.Product;
import fitness.online.app.util.rx.ExceptionUtils;
import fitness.online.app.util.scheduler.SchedulerTransformer;

/**
 * Created by user on 13.12.17.
 */

public class RetrofitDietDataSource {

    private Meal editMeal = null;

    private Diet diet = null;

    public void setEditMeal(Meal editMeal) {
        this.editMeal = editMeal;
    }

    public Meal getEditMeal() {
        return editMeal;
    }

    public void updateMeal(int mealId, String time, BasicResponseListener listener) {
        Api.getService(DietsApi.class).updateMeal(diet.getId(), mealId, time)
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> listener.success(data),
                        exception -> ExceptionUtils.sendThrowable(exception));
    }

    public void updateDiet(String comment, BasicResponseListener listener) {
        Api.getService(DietsApi.class).updateDiet(diet.getId(), comment)
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> listener.success(data),
                        exception -> ExceptionUtils.sendThrowable(exception));
    }

    public void deleteProduct(int dietId, int mealId, int productId, BasicResponseListener listener) {
        Api.getService(DietsApi.class).deleteProduct(dietId, mealId, productId).compose(SchedulerTransformer.ioToMain())
                .subscribe(() -> {
                    //RealmTrainingsDataSource.getInstance().storeMyTrainings(data);
                    listener.success(null);
                }, exception -> ExceptionUtils.sendThrowable(exception));
    }

    public void updateProduct(Product product, BasicResponseListener listener) {
        Api.getService(DietsApi.class).updateProduct(diet.getId(), editMeal.getId(), product.getId(), product.getPost_product_id(), product.getProduct_portion(), product.getComment())
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> listener.success(data), exception -> ExceptionUtils.sendThrowable(exception));
    }


    public void addProductToMeal(Product product, BasicResponseListener listener) {
        Api.getService(DietsApi.class).addProductToMeal(diet.getId(), editMeal.getId(), product.getPost_product_id(), product.getProduct_portion(), product.getComment())
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> listener.success(data), exception -> ExceptionUtils.sendThrowable(exception));
    }

    public void getDietByCourseId(int courseId, BasicResponseListener listener) {
        Api.getService(DietsApi.class).getDietByCourse(courseId).compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> {
                    diet = data.getDiet();
                    diet.setCourse_id(courseId);
                    RealmDietDataSource.getInstance().storeDiet(diet);
                    listener.success(data);
                }, exception -> ExceptionUtils.sendThrowable(exception));
    }

    public void createNewMeal(int dietId, Meal meal, BasicResponseListener listener) {
        Api.getService(DietsApi.class).createMeal(dietId, new MealRequest(meal)).compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> {
                    listener.success(data);
                }, exception -> ExceptionUtils.sendThrowable(exception));
    }

    public void deleteMeal(int dietId, int mealId, BasicResponseListener listener) {
        Api.getService(DietsApi.class).deleteMealById(dietId, mealId).compose(SchedulerTransformer.ioToMain())
                .subscribe(() -> {
                    listener.success(null);
                }, exception -> ExceptionUtils.sendThrowable(exception));
    }

    public static class INSTANCE_HOLDER {
        public static final RetrofitDietDataSource INSTANCE = new RetrofitDietDataSource();
    }

    public static RetrofitDietDataSource getInstance() {
        return RetrofitDietDataSource.INSTANCE_HOLDER.INSTANCE;
    }
}
