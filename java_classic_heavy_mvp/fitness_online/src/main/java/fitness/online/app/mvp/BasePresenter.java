package fitness.online.app.mvp;

import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.Queue;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenter<T extends MvpView> implements Presenter<T> {

    protected final String TAG = this.getClass().getSimpleName();

    private final CompositeDisposable mDisposables = new CompositeDisposable();

    private Queue<ViewAction> mActionQueue = new LinkedList<>();

    private WeakReference<T> mViewWeakReference;

    @Override
    public void bindView(T mvpView) {
        mViewWeakReference = new WeakReference<>(mvpView);
        ViewAction action;
        while ((action = mActionQueue.poll()) != null) {
            action.call(mvpView);
        }
    }

    @Override
    public void unbindView() {
        mViewWeakReference = null;
    }

    private
    @Nullable
    T getView() {
        if (mViewWeakReference != null) {
            return mViewWeakReference.get();
        }
        return null;
    }

    protected boolean isViewBounded() {
        return getView() != null;
    }


    public void addSubscription(Disposable... subs) {
        for (Disposable sub : subs) {
            mDisposables.add(sub);
        }
    }

    public void unsubsribeAll() {
        if (!mDisposables.isDisposed()) {
            mDisposables.clear();
        }
    }

    public interface ViewAction<V extends MvpView> {
        void call(V v);
    }

    protected void executeBounded(final ViewAction<T> action) {
        final T view = getView();
        if (view != null) {
            action.call(view);
        } else {
            mActionQueue.add(action);
        }
    }


    private static class MvpViewNotAttachedException extends RuntimeException {
        MvpViewNotAttachedException() {
            super("Please call Presenter.bindView(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }
}

