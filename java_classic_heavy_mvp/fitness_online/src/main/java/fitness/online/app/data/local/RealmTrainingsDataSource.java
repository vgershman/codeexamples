package fitness.online.app.data.local;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import fitness.online.app.model.pojo.realm.common.diet.Diet;
import fitness.online.app.model.pojo.realm.common.order.Order;
import fitness.online.app.model.pojo.realm.common.social.SocialToken;
import fitness.online.app.model.pojo.realm.common.trainings.CourseHistoryItem;
import fitness.online.app.model.pojo.realm.common.trainings.CoursesResponse;
import fitness.online.app.model.pojo.realm.common.trainings.DayExercise;
import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.model.pojo.realm.common.trainings.EditTrainingDay;
import fitness.online.app.model.pojo.realm.common.trainings.HistoryRecord;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingDay;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingDayResponse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingTemplate;
import fitness.online.app.model.pojo.realm.common.user.User;
import fitness.online.app.util.realm.RealmHelper;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;
import timber.log.Timber;

/**
 * Created by user on 20.11.17.
 */

public class RealmTrainingsDataSource {

    private List<DayExerciseDto> mSelectedExercises = new ArrayList<>();
    private boolean mNeedReload = false;


    public void subscribeHistory(RealmChangeListener<RealmResults<HistoryRecord>> changeListener) {
        mRealm.where(HistoryRecord.class).findAll().addChangeListener(changeListener);
    }

    public void storeMyTrainings(CoursesResponse courses) {
        List<User> users = courses.getUsers();
        if (users != null) {
            for (User user : users) {
                RealmUsersDataSource.getInstance().updateUserFullByUser(user);
            }
        }

        try {
            mRealm.beginTransaction();

            if (courses.getCourse() != null) {
                mRealm.copyToRealmOrUpdate(courses.getCourse());
            }
            mRealm.copyToRealmOrUpdate(courses.getTemplates());
            mRealm.copyToRealmOrUpdate(courses.getCourses());
            mRealm.copyToRealmOrUpdate(courses.getOrders());
            if (courses.getCourses() != null) {
                for (TrainingCourse trainingCourse : mRealm.where(TrainingCourse.class).findAll()) {
                    boolean exist = false;
                    for (TrainingCourse course : courses.getCourses()) {
                        if (course.getId() == trainingCourse.getId()) {
                            exist = true;
                            break;
                        }
                    }
                    if (!exist) {
                        // mRealm.where(TrainingCourse.class).equalTo("id", trainingCourse.getId()).findAll().deleteAllFromRealm();
                    }
                }
            }
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }

    public void initSelectedExercises() {
        mSelectedExercises = new ArrayList<>();
    }

    public void setSelectedPosition(String exerciseId, int position) {
        for (DayExerciseDto selectedExercise : mEditDay.getExercises()) {
            if (exerciseId.equals(selectedExercise.getId() + "")) {
                selectedExercise.setPosition(position);
            }
        }
    }


    public List<HistoryRecord> getLocalHistory() {
        return mRealm.copyFromRealm(mRealm.where(HistoryRecord.class).lessThan("id", 0).findAll());
    }

    public List<DayExerciseDto> listEditTrainings() {
        Collections.sort(mEditDay.getExercises(), (dayExercise, t1) -> {
            if (dayExercise.getPosition() == null || t1.getPosition() == null) {
                return 0;
            }
            return dayExercise.getPosition() - t1.getPosition();
        });
        return mEditDay.getExercises();
    }


    private EditTrainingDay mEditDay;


    public void editTrainingDay(TrainingDay day) {
        mEditDay = new EditTrainingDay();
        if (day != null) {
            mEditDay.setId(day.getId());
            mEditDay.setTitle(day.getTitle());
            mEditDay.setExercises(RealmTrainingsDataSource.getInstance().listExercisesByDay_(day.getId()));
        }
    }

    public boolean isSelected(String exerciseId) {
        for (DayExerciseDto selectedExercise : mSelectedExercises) {
            if (exerciseId.equals(selectedExercise.getPost_exercise_id() + "")) {
                return true;
            }
        }
        return false;
    }

    public void toggleSelected(String exerciseId) {
        for (DayExerciseDto selectedExercise : mSelectedExercises) {
            if (exerciseId.equals(selectedExercise.getPost_exercise_id() + "")) {
                mSelectedExercises.remove(selectedExercise);
                return;
            }
        }
        try {
            DayExerciseDto exercise = new DayExerciseDto(generateNewExcerciseId(), Integer.valueOf(exerciseId));
            exercise.setPosition(getLastSelectedInEditMode() + 1);
            mSelectedExercises.add(exercise);
        } catch (Exception ex) {
        }
    }

    public EditTrainingDay getEditDay() {
        return mEditDay;
    }

    private int generateNewExcerciseId() {
        int minId = 0;
        for (DayExerciseDto exerciseDto : mEditDay.getExercises()) {
            if (exerciseDto.getId() < minId) {
                minId = exerciseDto.getId();
            }
        }
        for (DayExerciseDto exerciseDto : mSelectedExercises) {
            if (exerciseDto.getId() < minId) {
                minId = exerciseDto.getId();
            }
        }
        minId--;
        return minId;
    }

    public void confirmExercises() {
        mEditDay.setExercises(mSelectedExercises);
        mSelectedExercises.clear();
    }


    public DayExercise getExerciseById(Integer exerId) {
        return mRealm.where(DayExercise.class).equalTo("id", exerId).findFirst();
    }


    public DayExerciseDto getEditExerciseById(Integer exerId) {
        for (DayExerciseDto exerciseDto : mEditDay.getExercises()) {
            if (exerciseDto.getId().equals(exerId)) {
                return exerciseDto;
            }
        }
        return null;
    }


    public List<DayExercise> listExercisesByDay(Integer dayId) {
        return (mRealm.where(DayExercise.class).equalTo("training_day_id", dayId).equalTo("status", "published").findAllSorted("position"));
    }


    public List<DayExerciseDto> listExercisesByDay_(Integer dayId) {
        List<DayExerciseDto> results = new ArrayList<>();
        for (DayExercise dayExercise : listExercisesByDay(dayId)) {
            results.add(new DayExerciseDto(dayExercise));
        }
        return results;
    }

    public int getNextDayNumberByCourse(TrainingCourse course) {
        return (int) mRealm.where(TrainingDay.class).notEqualTo("status", "deleted").equalTo("course_id", course.getId()).count() + 1;
    }

    public TrainingTemplate getTemplateById(int templateId) {
        return mRealm.where(TrainingTemplate.class).equalTo(TrainingTemplate.FIELD_ID, templateId).findFirst();
    }

    public TrainingCourse getCourseById(int courseId) {
        return mRealm.where(TrainingCourse.class).equalTo("id", courseId).findFirst();
    }

    public TrainingDay getDayById(int dayId) {
        return mRealm.where(TrainingDay.class).equalTo("id", dayId).findFirst();
    }

    public Observable<List<TrainingDay>> listDaysByCourseId(int courseId) {
        return Observable.just(mRealm.where(TrainingDay.class).equalTo("course_id", courseId).notEqualTo("status", "deleted").findAllSorted("id"));
    }

    public Observable<List<TrainingTemplate>> listTemplates() {
        return Observable.just(mRealm.where(TrainingTemplate.class).findAll());
    }

    public Observable<List<TrainingCourse>> listCourses() {
        return Observable.just(mRealm.where(TrainingCourse.class).findAllSorted("created_at", Sort.DESCENDING));
    }

    public Observable<CoursesResponse> listMyTrainings() {
        return Observable.zip(listTemplates(), listCourses(),
                (trainingTemplates, courses) -> new CoursesResponse(trainingTemplates, courses));
    }

    public Observable<List<HistoryRecord>> getHistoryByExerciseId(int exerciseId) {
        return Observable.just(mRealm.copyFromRealm(mRealm.where(HistoryRecord.class).equalTo("exercise_id", exerciseId).findAllSorted("executed_at", Sort.DESCENDING)));
    }

    public Observable<List<HistoryRecord>> getHistoryByPostExerciseId(int courseId, int exerciseId) {
        return Observable.just(mRealm.copyFromRealm(mRealm.where(HistoryRecord.class).equalTo("course_id", courseId).equalTo("post_exercise_id", exerciseId).findAllSorted("executed_at", Sort.DESCENDING)));
    }

    public Observable<List<CourseHistoryItem>> getHistoryByCourseId(int courseId) {
//        List<Integer> exerciseIds = new ArrayList<>();
//        for (TrainingDay day : mRealm.where(TrainingDay.class).equalTo("course_id", courseId).findAll()) {
//            for (DayExercise dayExercise : mRealm.where(DayExercise.class).equalTo("training_day_id", day.getId()).findAll()) {
//                exerciseIds.add(dayExercise.getId());
//            }
//        }
//        if (exerciseIds.size() == 0) {
//            return Observable.empty();
//        }
        List<HistoryRecord> records = new ArrayList<>();
        records.addAll(mRealm.where(HistoryRecord.class).equalTo("course_id", courseId).findAllSorted("executed_at", Sort.DESCENDING));
        for (HistoryRecord record : records) {
            for (TrainingDay day : mRealm.where(TrainingDay.class).equalTo("course_id", courseId).findAll()) {
                for (DayExercise dayExercise : mRealm.where(DayExercise.class).equalTo("training_day_id", day.getId()).findAll()) {
                    if (dayExercise.getId() == record.getExercise_id()) {
                        record.setInfo(day, dayExercise);
                    }
                }
            }
        }
        List<CourseHistoryItem> mCourseHistoryItems = new ArrayList<>();
        for (HistoryRecord record : records) {
            CourseHistoryItem item = new CourseHistoryItem(record.getExecutedDate(), record.getTrainingDay());
            if (!mCourseHistoryItems.contains(item)) {
                mCourseHistoryItems.add(item);
            } else {
                item = mCourseHistoryItems.get(mCourseHistoryItems.indexOf(item));
            }
            if (!item.getRecords().contains(record.getDayExercise())) {
                item.getRecords().add(record.getDayExercise());
            }
        }
        return Observable.just(mCourseHistoryItems);
    }


    public int getLastSelected(Integer dayId, boolean isEditMode) {
        int lastSelected = 0;
        List<DayExerciseDto> exerciseList;
        if (isEditMode || dayId == null) {
            exerciseList = listEditTrainings();
        } else {
            exerciseList = listExercisesByDay_(dayId);
        }
        for (DayExerciseDto selected : exerciseList) {
            if (selected.getPosition() > lastSelected) {
                lastSelected = selected.getPosition();
            }
        }
        return lastSelected;
    }

    public int getLastSelectedInEditMode() {
        int lastSelected = 0;
        for (DayExerciseDto selected : getEditDay().getExercises()) {
            if (selected.getPosition() > lastSelected) {
                lastSelected = selected.getPosition();
            }
        }
        for (DayExerciseDto selected : mSelectedExercises) {
            if (selected.getPosition() > lastSelected) {
                lastSelected = selected.getPosition();
            }
        }
        return lastSelected;
    }

    public int getFirstSelected(Integer dayId, boolean isEditMode) {
        int firstSelected = Integer.MAX_VALUE;
        List<DayExerciseDto> exerciseList;
        if (isEditMode || dayId == null) {
            exerciseList = listEditTrainings();
        } else {
            exerciseList = listExercisesByDay_(dayId);
        }
        for (DayExerciseDto selected : exerciseList) {
            if (selected.getPosition() < firstSelected) {
                firstSelected = selected.getPosition();
            }
        }
        return firstSelected;
    }

    private Realm mRealm = RealmHelper.getCommonRealmInstance();

    public void storeDays(TrainingDayResponse data, Integer courseId, HashMap<Integer, Integer> idsMap) {

        try {
            mRealm.beginTransaction();

            if (data.getTraining_day() != null) {
                mRealm.copyToRealmOrUpdate(data.getTraining_day());
            }
            mRealm.copyToRealmOrUpdate(data.getTraining_days());

            mRealm.copyToRealmOrUpdate(data.getExercises());

            if (data.getCompleted_sets() != null) {
                for (HistoryRecord record : data.getCompleted_sets()) {
                    if (data.getExercises() != null) {
                        for (DayExercise exercise : data.getExercises()) {
                            if (exercise.getId() == record.getExercise_id()) {
                                record.setPost_exercise_id(exercise.getPost_exercise_id());
                                Integer cid = null;
                                if (courseId != null) {
                                    cid = courseId;
                                }
                                if (idsMap != null) {
                                    cid = idsMap.get(exercise.getId());
                                }
                                record.setCourse_id(cid);
                            }
                        }
                    }
                }
            }
            if (data.getTraining_days() != null && data.getTraining_days().size() > 0 && courseId != null) {
                List<TrainingDay> days = mRealm.where(TrainingDay.class).equalTo("course_id", courseId).findAll();
                for (TrainingDay day : days) {
                    day.setStatus("deleted");
                    for (TrainingDay day_ : data.getTraining_days()) {
                        if (day_.getId() == day.getId()) {

                            day.setStatus(day_.getStatus());
                        }
                    }
                }
            }
            mRealm.copyToRealmOrUpdate(data.getCompleted_sets());
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }

    public void storeHistory(TrainingDayResponse data, Integer courseId, HashMap<Integer, Integer> idsMap) {

        try {
            mRealm.beginTransaction();
            mRealm.copyToRealmOrUpdate(data.getTraining_days());
            mRealm.copyToRealmOrUpdate(data.getExercises());

            if (data.getCompleted_sets() != null) {
                for (HistoryRecord record : data.getCompleted_sets()) {
                    if (data.getExercises() != null) {
                        for (DayExercise exercise : data.getExercises()) {
                            if (exercise.getId() == record.getExercise_id()) {
                                record.setPost_exercise_id(exercise.getPost_exercise_id());
                                Integer cid = null;
                                if (courseId != null) {
                                    cid = courseId;
                                }
                                if (idsMap != null) {
                                    cid = idsMap.get(exercise.getId());
                                }
                                record.setCourse_id(cid);
                            }
                        }
                    }
                }
            }
            mRealm.copyToRealmOrUpdate(data.getCompleted_sets());
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }

    public void deleteDay(Integer dayId) {
        try {
            mRealm.beginTransaction();

            TrainingDay day = mRealm.where(TrainingDay.class).equalTo("id", dayId).findFirst();
            if (day != null) {
                day.setStatus("deleted");
            }
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }

    public void deleteHistoryRecord(Integer recordId) {
        try {
            mRealm.beginTransaction();

            mRealm.where(HistoryRecord.class).equalTo("id", recordId).findAll().deleteAllFromRealm();
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }

    public void deleteCourse(Integer courseId) {
        try {
            mRealm.beginTransaction();
            mRealm.where(TrainingCourse.class).equalTo("id", courseId).findAll().deleteAllFromRealm();
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }


    public Integer getDayIndex(TrainingDay headerDay) {
        List<TrainingDay> courseDays = mRealm.where(TrainingDay.class).notEqualTo("status", "deleted").equalTo("course_id", headerDay.getCourse_id()).findAll();
        for (int i = 0; i < courseDays.size(); i++) {
            if (headerDay.getId() == courseDays.get(i).getId()) {
                return i + 1;
            }
        }
        return 0;
    }

    private int generateHistoryId() {
        int minId = 0;
        try {
            minId = mRealm.where(HistoryRecord.class).min("id").intValue();
        } catch (Exception ex) {
        }
        if (minId > 0) {
            minId = 0;
        }
        return minId - 1;
    }

    public Completable createHistoryRecord(Integer courseId, Integer postExerciseId, Integer exerciseId, int repeats, String value, String comment, int type) {
        return Completable.fromAction(() -> {
            try {
                mRealm.beginTransaction();
                HistoryRecord record = new HistoryRecord(generateHistoryId(), exerciseId, repeats, value, type, comment, postExerciseId, courseId);
                record.setGuid(UUID.randomUUID().toString());
                mRealm.copyToRealm(record);
            } catch (Throwable throwable) {
                Timber.e(throwable);
            } finally {
                if (mRealm.isInTransaction()) {
                    mRealm.commitTransaction();
                }
            }
        });
    }

    public void clearLocalHistory() {
        try {
            mRealm.beginTransaction();
            mRealm.where(HistoryRecord.class).lessThan("id", 0).findAll().deleteAllFromRealm();
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }

    public List<DayExerciseDto> fromHistoryItem(CourseHistoryItem historyItem) {
        List<DayExerciseDto> results = new ArrayList<>();
        for (DayExercise record : historyItem.getRecords()) {
            results.add(new DayExerciseDto(record));
        }
        Collections.sort(results, ((dayExerciseDto, t1) -> dayExerciseDto.getPosition() - t1.getPosition()));
        return results;
    }


    public boolean isSuperTop(Integer id, Integer dayId, CourseHistoryItem historyItem) {
        List<DayExerciseDto> editExercises;
        if (historyItem != null) {
            editExercises = fromHistoryItem(historyItem);
        } else {
            editExercises = dayId == null ? listEditTrainings() : listExercisesByDay_(dayId);
        }
        for (int i = 0; i < editExercises.size(); i++) {
            DayExerciseDto exercise = editExercises.get(i);
            if (exercise.getId().equals(id)) {
                int superSet = exercise.getSuperset();
                if (superSet > 0) {
                    if (i == 0) return true;
                    DayExerciseDto prev = editExercises.get(i - 1);
                    if (prev.getSuperset() == superSet) return true;
                }
            }
        }
        return false;
    }

    public boolean isSuperBottom(Integer id, Integer dayId, CourseHistoryItem historyItem) {
        List<DayExerciseDto> editExercises;
        if (historyItem != null) {
            editExercises = fromHistoryItem(historyItem);
        } else {
            editExercises = dayId == null ? listEditTrainings() : listExercisesByDay_(dayId);
        }
        for (int i = 0; i < editExercises.size(); i++) {
            DayExerciseDto exercise = editExercises.get(i);
            if (exercise.getId().equals(id)) {
                int superSet = exercise.getSuperset();
                if (superSet > 0) {
                    if (i == editExercises.size() - 1) return true;
                    DayExerciseDto next = editExercises.get(i + 1);
                    if (next.getSuperset() == superSet) return true;
                }
            }
        }
        return false;
    }

    private int getLastSupersetIndex() {
        int maxSuperset = 0;
        for (DayExerciseDto exerciseDto : listEditTrainings()) {
            if (exerciseDto.getSuperset() > maxSuperset) {
                maxSuperset = exerciseDto.getSuperset();
            }
        }
        return maxSuperset;
    }

    public void toggleSuperset(Integer id, boolean top) {
        List<DayExerciseDto> editExercises = listEditTrainings();
        for (int i = 0; i < editExercises.size(); i++) {
            DayExerciseDto selectedExercise = editExercises.get(i);
            if (selectedExercise.getId().equals(id)) {
                int oldSuperset = selectedExercise.getSuperset();
                if (oldSuperset == 0) {
                    if (top && i > 0) {
                        DayExerciseDto prev = editExercises.get(i - 1);
                        if (prev.getSuperset() != 0) {
                            selectedExercise.setSuperset(prev.getSuperset());
                        } else {
                            int newSupersetIndex = getLastSupersetIndex() + 1;
                            selectedExercise.setSuperset(newSupersetIndex);
                            prev.setSuperset(newSupersetIndex);
                        }
                    }
                    if (!top && i < editExercises.size() - 1) {
                        DayExerciseDto next = editExercises.get(i + 1);
                        if (next.getSuperset() != 0) {
                            selectedExercise.setSuperset(next.getSuperset());
                        } else {
                            int newSupersetIndex = getLastSupersetIndex() + 1;
                            selectedExercise.setSuperset(newSupersetIndex);
                            next.setSuperset(newSupersetIndex);
                        }
                    }
                } else {
                    if (top && i > 0) {
                        DayExerciseDto prev = editExercises.get(i - 1);
                        if (prev.getSuperset() == oldSuperset) {
                            selectedExercise.setSuperset(0);
                            if (i > 1) {
                                DayExerciseDto prevPrev = editExercises.get(i - 2);
                                if (prevPrev.getSuperset() != prev.getSuperset()) {
                                    prev.setSuperset(0);
                                }
                            }
                            checkSuperset();
                        }
                    }
                    if (!top && i < editExercises.size() - 1) {
                        DayExerciseDto next = editExercises.get(i + 1);
                        if (next.getSuperset() == oldSuperset) {
                            selectedExercise.setSuperset(0);
                            if (i < editExercises.size() - 2) {
                                DayExerciseDto nextNext = editExercises.get(i + 2);
                                if (nextNext.getSuperset() != next.getSuperset()) {
                                    next.setSuperset(0);
                                }
                            }
                            checkSuperset();
                        }
                    }
                }
            }
        }
    }

    public void checkSuperset() {
        List<DayExerciseDto> exercises = listEditTrainings();
        for (int i = 0; i < listEditTrainings().size(); i++) {
            DayExerciseDto exercise = exercises.get(i);
            int prev = 0;
            int next = 0;
            if (i > 0) {
                prev = exercises.get(i - 1).getSuperset();
            }
            if (i < exercises.size() - 1) {
                next = exercises.get(i + 1).getSuperset();
            }
            if (exercise.getSuperset() != 0) {
                if (exercise.getSuperset() != prev && exercise.getSuperset() != next) {
                    exercise.setSuperset(0);
                }
            }
        }
    }

    public boolean isCourseFilled(int courseId) {
        boolean exercisesFilled =
                mRealm.where(TrainingDay.class).equalTo("course_id", courseId).findAll().size() > 0;
        boolean dietFilled = mRealm.where(Diet.class).equalTo("course_id", courseId).findAll().size() > 0;
        return exercisesFilled || dietFilled;
    }

    public void logout() {
        try {
            mRealm.beginTransaction();
            mRealm.where(TrainingCourse.class).findAll().deleteAllFromRealm();
            mRealm.where(TrainingTemplate.class).findAll().deleteAllFromRealm();
            mRealm.where(TrainingDay.class).findAll().deleteAllFromRealm();
            mRealm.where(DayExercise.class).findAll().deleteAllFromRealm();
            mRealm.where(HistoryRecord.class).findAll().deleteAllFromRealm();
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }

    public Order getOrderById(Integer invoiceId) {
        return mRealm.where(Order.class).equalTo("id", invoiceId).findFirst();
    }

    public boolean needReload() {
        return mNeedReload;
    }

    public void setNeedReaload(boolean needReload) {
        mNeedReload = needReload;
    }

    public static class INSTANCE_HOLDER {
        public static final RealmTrainingsDataSource INSTANCE = new RealmTrainingsDataSource();
    }

    public static RealmTrainingsDataSource getInstance() {
        return RealmTrainingsDataSource.INSTANCE_HOLDER.INSTANCE;
    }
}
