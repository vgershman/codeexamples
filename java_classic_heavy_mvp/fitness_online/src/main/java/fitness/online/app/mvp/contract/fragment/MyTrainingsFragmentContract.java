package fitness.online.app.mvp.contract.fragment;

import com.trimf.recycler.item.BaseItem;

import java.util.List;

import javax.annotation.Nonnull;

import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface MyTrainingsFragmentContract {

    interface View extends FragmentView {

        void dataReplace(List<BaseItem> data);

        void onTrainingSelect(TrainingCourse course);

        void setRefreshing(boolean refreshing);

        void openNewTraining();

        void askToDelete(TrainingCourse course);

        void openPayFragment(@Nonnull String url);
    }

    abstract class Presenter extends BaseFragmentPresenter<MyTrainingsFragmentContract.View> {

        public abstract void load();

        public abstract void sync(SyncListener syncListener);

        public abstract void onWebViewResultUrl(String resultUrl);
    }

    interface SyncListener {
        void onSynced();
    }
}
