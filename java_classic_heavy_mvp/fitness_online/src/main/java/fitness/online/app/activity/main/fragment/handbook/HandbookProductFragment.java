package fitness.online.app.activity.main.fragment.handbook;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.parceler.Parcels;

import java.text.DecimalFormat;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.model.pojo.realm.common.diet.Product;
import fitness.online.app.model.pojo.realm.handbook.HandbookCategory;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.model.pojo.realm.handbook.HandbookProduct;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.HandbookProductFragmentContract;

/**
 * Created by user on 09.10.17.
 */

public class HandbookProductFragment extends BaseFragment<HandbookProductFragmentPresenter> implements HandbookProductFragmentContract.View {


    private static final String EXTRA_INPUT = "navigation";


    private HandbookCategory mCategory;
    private boolean fromDiet;
    private boolean fromTrainer;
    private Double mCalories;
    private Integer mPortion;
    private DecimalFormat format = new DecimalFormat("#.#");


    @Override
    public void bindData(HandbookProduct product) {
        mCalories = product.getCalories();
        title.setText(product.getTitle());
        calories.setText(product.getFormatted(product.getCalories()));
        proteins.setText(product.getFormatted(product.getProteins()));
        carbohydrates.setText(product.getFormatted(product.getCarbohydrates()));
        fats.setText(product.getFormatted(product.getFats()));
        fatsFilled.setText(product.getFormatted(product.getSaturated_fats()));
        fatsPoly.setText(product.getFormatted(product.getPolyunsaturated_fats()));
        fatsMono.setText(product.getFormatted(product.getMonounsaturated_fats()));
        fatsTrans.setText(product.getFormatted(product.getTrans_fatty_acids()));
        carotine.setText(product.getFormatted(product.getBeta_carotene()));
        vitaminA.setText(product.getFormatted(product.getVitamin_a()));
        vitaminB12.setText(product.getFormatted(product.getVitamin_b12()));
        vitaminB6.setText(product.getFormatted(product.getVitamin_b6()));
        vitaminD.setText(product.getFormatted(product.getVitamin_d()));
        vitaminE.setText(product.getFormatted(product.getVitamin_e()));
        vitaminC.setText(product.getFormatted(product.getVitamin_c()));
        ribo.setText(product.getFormatted(product.getRiboflavin()));
        ferrum.setText(product.getFormatted(product.getIron()));
        kal.setText(product.getFormatted(product.getPotassium()));
        calcium.setText(product.getFormatted(product.getCalcium()));
        magnium.setText(product.getFormatted(product.getMagnesium()));
        cuprum.setText(product.getFormatted(product.getCuprum()));
        natrium.setText(product.getFormatted(product.getNatrium()));
        phosphor.setText(product.getFormatted(product.getPhosphorus()));
        zinc.setText(product.getFormatted(product.getZinc()));
        cletchatka.setText(product.getFormatted(product.getCellulose()));
        cholysterol.setText(product.getCholesterol());
        bindGlycemia(product);
        bindCaloriesDensity(product);
        bindAntyOx(product);
        bindFactor(product);
        mCategory = RealmHandbookDataSource.getInstance().getCategoryById(product.getCategory_id());
        if (fromDiet) {
            initInput();
        }
    }

    @Override
    public void onSelected(boolean edit) {
        getFragNavController().popFragments(edit ? 1 : 3);
    }

    @Override
    public void bindDiet(Product product) {
        if (fromTrainer && TextUtils.isEmpty(product.getComment())) {
            llComment.setVisibility(View.GONE);
        }
        etComment.setText(product.getComment());
        etComment.setSelection(etComment.getText().toString().length());
        try {
            etPortion.setText(Double.valueOf(product.getProduct_portion()).intValue() + "");
            etPortion.setSelection(etPortion.getText().length());
        } catch (NumberFormatException ex) {
            etPortion.setText("");
        }
    }

    private void initInput() {
        containerInput.setVisibility(View.VISIBLE);
        showSoftKeyboard(etPortion);
        etComment.setEnabled(!fromTrainer);
        etPortion.setEnabled(!fromTrainer);
        etPortion.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    mPortion = Integer.parseInt(charSequence.toString());
                    tvCalorage.setText(format.format(mPortion * mCalories / 100) + " Ккал");
                } catch (NumberFormatException ex) {
                    tvCalorage.setText(0 + " Ккал");
                    mPortion = 0;
                }
                invalidateOptionsMenu();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void bindGlycemia(HandbookProduct product) {
        StringBuilder value = new StringBuilder(product.getFormatted(product.getGlycemia_index()));
        if (product.getGlycemia_index() != null) {
            Integer glycemia = null;
            try {
                glycemia = Math.round(product.getGlycemia_index().floatValue());
            } catch (NumberFormatException ex) {
            }
            int maxGlycemia = getResources().getInteger(R.integer.glycemia_max);
            if (glycemia > maxGlycemia) {
                glycemia = maxGlycemia;
            }
            if (glycemia != null) {
                String textMetrics = getGlycemiaMetrics(glycemia);
                value.append(" (");
                value.append(textMetrics.toLowerCase());
                value.append(")");
                sbGlycemia.setVisibility(View.VISIBLE);
                sbGlycemia.setOnTouchListener((view, motionEvent) -> true);
                sbGlycemia.setProgress(glycemia);
            } else {
                sbGlycemia.setVisibility(View.INVISIBLE);
            }
        }
        tvGlycemia.setText(value.toString());
    }

    private String getGlycemiaMetrics(Integer glycemia) {
        int low = getResources().getInteger(R.integer.glycemia_low);
        int mid = getResources().getInteger(R.integer.glycemia_mid);
        if (glycemia <= low) return getResources().getString(R.string.value_low);
        if (glycemia <= mid) return getResources().getString(R.string.value_mid);
        return getResources().getString(R.string.value_high);
    }

    private String getDensityMetrics(Integer density) {
        int superLow = getResources().getInteger(R.integer.density_very_low);
        int low = getResources().getInteger(R.integer.density_low);
        int mid = getResources().getInteger(R.integer.density_mid);
        int high = getResources().getInteger(R.integer.density_high);
        if (density <= superLow) return getResources().getString(R.string.value_super_low);
        if (density <= low) return getResources().getString(R.string.value_low);
        if (density <= mid) return getResources().getString(R.string.value_mid);
        if (density <= high) return getResources().getString(R.string.value_high);
        return getResources().getString(R.string.value_super_high);
    }

    private String getAntioxMetrics(Integer antiox) {
        int low = getResources().getInteger(R.integer.antyox_low);
        int mid = getResources().getInteger(R.integer.antyox_mid);
        if (antiox <= low) return getResources().getString(R.string.value_low);
        if (antiox <= mid) return getResources().getString(R.string.value_mid);
        return getResources().getString(R.string.value_high);
    }

    private String getInflammMetrics(Integer inflamm) {
        int low = getResources().getInteger(R.integer.inflammatory_low);
        int mid = getResources().getInteger(R.integer.inflammatory_mid);
        if (inflamm <= low) return getResources().getString(R.string.value_low);
        if (inflamm <= mid) return getResources().getString(R.string.value_mid);
        return getResources().getString(R.string.value_high);
    }


    private void bindFactor(HandbookProduct product) {
        StringBuilder value = new StringBuilder(product.getFormatted(product.getInflammatory_factor()));
        if (product.getInflammatory_factor() != null) {
            Integer inflamm = null;
            try {
                inflamm = Math.round(Float.valueOf(product.getInflammatory_factor()));
            } catch (NumberFormatException ex) {
            }
            if (inflamm != null) {
                String textMetrics = getInflammMetrics(inflamm);
                value.append(" (");
                value.append(textMetrics.toLowerCase());
                value.append(")");
                sbFactor.setVisibility(View.VISIBLE);
                sbFactor.setOnTouchListener((view, motionEvent) -> true);
                sbFactor.setProgress(getInflammScale(inflamm));
            } else {
                sbFactor.setVisibility(View.INVISIBLE);
            }
        }
        tvFactor.setText(value.toString());
    }

    private Integer getInflammScale(Integer inflamm) {
        return Math.round(Math.abs(inflamm / 20f * ((inflamm < 0) ? 0.5f : 1.5f)));
    }

    private void bindAntyOx(HandbookProduct product) {
        StringBuilder value = new StringBuilder(product.getFormatted(product.getAntioxidant()));
        if (product.getAntioxidant() != null) {
            Integer antiox = null;
            try {
                antiox = Math.round(Float.valueOf(product.getAntioxidant()));
            } catch (NumberFormatException ex) {
            }
            int maxAntiox = getResources().getInteger(R.integer.density_max);
            if (antiox > maxAntiox) {
                antiox = maxAntiox;
            }
            if (antiox != null) {
                String textMetrics = getAntioxMetrics(antiox);
                value.append(" (");
                value.append(textMetrics.toLowerCase());
                value.append(")");
                sbAntyOx.setVisibility(View.VISIBLE);
                sbAntyOx.setOnTouchListener((view, motionEvent) -> true);
                sbAntyOx.setProgress(antiox);
            } else {
                sbAntyOx.setVisibility(View.INVISIBLE);
            }
        }
        tvAntyOx.setText(value.toString());
    }

    private void bindCaloriesDensity(HandbookProduct product) {
        Integer multiplier = getResources().getInteger(R.integer.density_multiplier);
        StringBuilder value = new StringBuilder(product.getFormatted(product.getDensity_of_calories_index()));
        if (product.getDensity_of_calories_index() != null) {
            Integer density = null;
            try {
                density = Math.round(Float.valueOf(product.getDensity_of_calories_index()) * multiplier);
            } catch (NumberFormatException ex) {
            }
            int maxCaloriesDensity = getResources().getInteger(R.integer.density_max);
            if (density > maxCaloriesDensity) {
                density = maxCaloriesDensity;
            }
            if (density != null) {
                String textMetrics = getDensityMetrics(density);
                value.append(" (");
                value.append(textMetrics.toLowerCase());
                value.append(")");
                sbCaloriesDensity.setVisibility(View.VISIBLE);
                sbCaloriesDensity.setOnTouchListener((view, motionEvent) -> true);
                sbCaloriesDensity.setProgress(density);
            } else {
                sbCaloriesDensity.setVisibility(View.INVISIBLE);
            }
        }
        tvCaloriesDensity.setText(value.toString());
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_handbook_product;
    }

    @NonNull
    public static HandbookProductFragment newInstance(@Nullable HandbookNavigation navigation, boolean fromDiet, boolean fromTrainer) {
        HandbookProductFragment fragment = new HandbookProductFragment();
        Bundle args = new Bundle();
        args.putBoolean("diet", fromDiet);
        args.putBoolean("trainer", fromTrainer);
        args.putParcelable(EXTRA_INPUT, Parcels.wrap(navigation));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        HandbookNavigation navigation = Parcels.unwrap(getArguments().getParcelable(EXTRA_INPUT));
        fromDiet = getArguments().getBoolean("diet", false);
        fromTrainer = getArguments().getBoolean("trainer", false);
        mPresenter = new HandbookProductFragmentPresenter(navigation.getId(), fromDiet, fromTrainer);
        mPresenter.loadData();
    }

    @Override
    public int getMenuId() {
        if (fromDiet && !fromTrainer) {
            if (mPortion != null && mPortion > 0) {
                return R.menu.confirm_active;
            } else {
                return R.menu.confirm;
            }
        } else {
            return super.getMenuId();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.confirm:
                confirmPortion();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void confirmPortion() {
        String comment = etComment.getText().toString();
        mPresenter.confirm(mPortion, comment);
    }

    @Nullable
    @Override
    public String getTitle() {
        return mCategory == null ? "" : mCategory.getTitle();
    }


    @BindView(R.id.containerInput)
    View containerInput;

    @BindView(R.id.etWeight)
    EditText etPortion;

    @BindView(R.id.llComment)
    LinearLayout llComment;

    @BindView(R.id.etComment)
    EditText etComment;

    @BindView(R.id.tv_cal)
    TextView tvCalorage;

    @BindView(R.id.tv_title)
    TextView title;

    @BindView(R.id.tv_calories)
    TextView calories;

    @BindView(R.id.tv_proteins)
    TextView proteins;

    @BindView(R.id.tv_carbohydrates)
    TextView carbohydrates;

    @BindView(R.id.tv_fats)
    TextView fats;

    @BindView(R.id.tv_fats_filled)
    TextView fatsFilled;

    @BindView(R.id.tv_fats_poly)
    TextView fatsPoly;

    @BindView(R.id.tv_fats_mono)
    TextView fatsMono;

    @BindView(R.id.tv_fats_trans)
    TextView fatsTrans;

    @BindView(R.id.tv_carotine)
    TextView carotine;

    @BindView(R.id.tv_vitamin_a)
    TextView vitaminA;

    @BindView(R.id.tv_vitamin_b12)
    TextView vitaminB12;

    @BindView(R.id.tv_vitamin_b6)
    TextView vitaminB6;

    @BindView(R.id.tv_vitamin_d)
    TextView vitaminD;

    @BindView(R.id.tv_vitamin_e)
    TextView vitaminE;

    @BindView(R.id.tv_vitamin_c)
    TextView vitaminC;

    @BindView(R.id.tv_ribo)
    TextView ribo;

    @BindView(R.id.tv_ferrum)
    TextView ferrum;

    @BindView(R.id.tv_kal)
    TextView kal;

    @BindView(R.id.tv_calcium)
    TextView calcium;

    @BindView(R.id.tv_magn)
    TextView magnium;

    @BindView(R.id.tv_cuprum)
    TextView cuprum;

    @BindView(R.id.tv_natrium)
    TextView natrium;

    @BindView(R.id.tv_phos)
    TextView phosphor;

    @BindView(R.id.tv_zinc)
    TextView zinc;

    @BindView(R.id.tv_clet)
    TextView cletchatka;

    @BindView(R.id.tv_chol)
    TextView cholysterol;

    @BindView(R.id.tv_glycemia)
    TextView tvGlycemia;

    @BindView(R.id.sb_glycemia)
    SeekBar sbGlycemia;

    @BindView(R.id.tv_cal_density)
    TextView tvCaloriesDensity;

    @BindView(R.id.sb_calories_density)
    SeekBar sbCaloriesDensity;

    @BindView(R.id.tv_factor)
    TextView tvFactor;

    @BindView(R.id.sb_factor)
    SeekBar sbFactor;

    @BindView(R.id.tv_antyox)
    TextView tvAntyOx;

    @BindView(R.id.sb_antyox)
    SeekBar sbAntyOx;
}
