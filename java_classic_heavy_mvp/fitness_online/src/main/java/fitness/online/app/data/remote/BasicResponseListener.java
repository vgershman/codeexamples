package fitness.online.app.data.remote;

import android.support.annotation.NonNull;

import fitness.online.app.util.rx.ExceptionUtils;

/**
 * Created by user on 26.10.17.
 */

public abstract class BasicResponseListener<T> implements ResponseListener<T> {
    @Override
    public void error(@NonNull Throwable throwable) {
        ExceptionUtils.sendThrowable(throwable);
    }
}
