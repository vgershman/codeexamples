package fitness.online.app.mvp.contract.fragment;

import com.trimf.recycler.item.BaseItem;

import java.util.List;

import fitness.online.app.model.pojo.realm.handbook.HandbookFilter;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface HandbookFilterFragmentContract {

    interface View extends FragmentView {
        void bindData(List<BaseItem> filter);

        void updateData();

        void filterAction();
    }

    abstract class Presenter extends BaseFragmentPresenter<HandbookFilterFragmentContract.View> {

        public abstract void loadFilter();

        public abstract void loadFilterSuccess(List<HandbookFilter> filter);

        public abstract void onFilterClicked();

        public abstract void onClearFilterClicked();
    }
}
