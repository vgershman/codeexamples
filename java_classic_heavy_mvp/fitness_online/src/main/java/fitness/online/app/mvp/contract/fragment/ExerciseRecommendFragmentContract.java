package fitness.online.app.mvp.contract.fragment;

import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface ExerciseRecommendFragmentContract {


    interface View extends FragmentView {
        void openExerciseInfo(HandbookNavigation navigation);
    }

    abstract class Presenter extends BaseFragmentPresenter<ExerciseRecommendFragmentContract.View> {


        public abstract void onPlayClicked(int post_exercise_id);
    }
}
