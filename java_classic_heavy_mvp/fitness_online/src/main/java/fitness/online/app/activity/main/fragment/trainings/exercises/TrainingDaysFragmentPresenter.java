package fitness.online.app.activity.main.fragment.trainings.exercises;

import android.support.annotation.NonNull;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import fitness.online.app.R;
import fitness.online.app.data.local.RealmSessionDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingDay;
import fitness.online.app.mvp.contract.fragment.TrainingDaysFragmentContract;
import fitness.online.app.recycler.data.ButtonData_;
import fitness.online.app.recycler.data.EmptyData;
import fitness.online.app.recycler.data.EmptyDataWithButton;
import fitness.online.app.recycler.data.trainings.TrainingDayData;
import fitness.online.app.recycler.item.ClickListener;
import fitness.online.app.recycler.item.EmptyItem;
import fitness.online.app.recycler.item.EmptyItemWithButton;
import fitness.online.app.recycler.item.SimpleButtonItem;
import fitness.online.app.recycler.item.trainings.TrainingDayItem;
import fitness.online.app.view.progressBar.ProgressBarEntry;

/**
 * Created by user on 28.10.17.
 */

public class TrainingDaysFragmentPresenter extends TrainingDaysFragmentContract.Presenter implements ClickListener {


    private TrainingCourse course;

    private ProgressBarEntry entry;

    private boolean fromTrainer = false;

    public TrainingDaysFragmentPresenter(TrainingCourse course) {
        this.course = course;
    }

    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        if (firstBind) {
            sync();
        }
        load();
    }

    @Override
    public void load() {
        fromTrainer = course.getInvoice_id() != null && !RealmSessionDataSource.getInstance().isTrainer();
        RealmTrainingsDataSource.getInstance().listDaysByCourseId(course.getId()).subscribe(this::processData);
    }

    private void processData(List<TrainingDay> trainingDays) {
        executeBounded(view -> view.setRefreshing(false));
        if (trainingDays == null || trainingDays.size() == 0) {
            empty();
        } else {
            bindData(trainingDays);
        }
    }

    private void bindData(List<TrainingDay> trainingDays) {
        executeBounded(view -> view.dataReplace(prepareData(trainingDays)));
    }

    @Override
    public void sync() {
        executeBounded(view -> entry = view.showProgressBar(false));
        RetrofitTrainingsDataSource.getInstance().syncDaysByCourse(course.getId(), new BasicResponseListener() {
            @Override
            public void success(Object response) {
                load();
                if (entry != null) {
                    executeBounded(view -> view.hideProgressBar(entry));
                }
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                if (entry != null) {
                    executeBounded(view -> view.hideProgressBar(entry));
                }
            }
        });
    }


    private List<BaseItem> prepareData(List<TrainingDay> days) {
        List<BaseItem> items = new ArrayList<>();
        for (TrainingDay day : days) {
            int number = (days.indexOf(day) + 1);
            items.add(new TrainingDayItem(new TrainingDayData(false, day, number, null, this, number == days.size())));
        }
        if (!fromTrainer) {
            items.add(new SimpleButtonItem(new ButtonData_(R.string.btn_add_new_day, this)));
        }
        return items;
    }

    @Override
    public void empty() {
        List<BaseItem> items = new ArrayList<>();
        if (!fromTrainer) {
            items.add(new EmptyItemWithButton(new EmptyDataWithButton(new EmptyData(R.string.empty_training_days, R.drawable.ic_bg_trainings),
                    new ButtonData_(R.string.btn_add_new_day, this))));
        } else {
            items.add(new EmptyItem(new EmptyData(R.string.empty_training_days, R.drawable.ic_bg_trainings)));
        }
        executeBounded(view -> view.dataReplace(items));
    }

    @Override
    public void onClick(Object data) {
        if (data instanceof TrainingDay) {
            executeBounded(view -> view.onDaySelected((TrainingDay) data));
        }
        if (data instanceof ButtonData_) {
            executeBounded(view -> view.onAddNewDay());
        }
    }
}
