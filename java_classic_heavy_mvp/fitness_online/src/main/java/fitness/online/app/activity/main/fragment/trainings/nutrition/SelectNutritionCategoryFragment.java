package fitness.online.app.activity.main.fragment.trainings.nutrition;

import android.os.Bundle;
import android.support.annotation.NonNull;

import org.parceler.Parcels;

import fitness.online.app.activity.main.fragment.handbook.HandbookRootFragment;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.model.pojo.realm.handbook.HandbookEntity;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;

/**
 * Created by user on 22.12.17.
 */

public class SelectNutritionCategoryFragment extends HandbookRootFragment {


    @NonNull
    public static SelectNutritionCategoryFragment newInstance(Integer courseId) {
        SelectNutritionCategoryFragment fragment = new SelectNutritionCategoryFragment();
        Bundle args = new Bundle();
        args.putInt("courseId", courseId);
        HandbookNavigation navigation = new HandbookNavigation();
        navigation.setId(RealmHandbookDataSource.getInstance().getNutritionParentCategoryId());
        navigation.setType(HandbookEntity.CATEGORY);
        args.putParcelable(EXTRA_INPUT, Parcels.wrap(navigation));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onNavigationSelect(HandbookNavigation handbookNavigation) {
        int courseId = getArguments().getInt("courseId");
        getFragNavController().pushFragment(SelectNutritionFragment.newInstance(courseId, handbookNavigation.getId(), true, null, null));
    }
}
