package fitness.online.app.activity.main.fragment.trainings.exercises;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.CourseHistoryFragmentContract;
import fitness.online.app.recycler.adapter.UniversalAdapter;

/**
 * Created by user on 13.11.17.
 */

public class CourseHistoryFragment extends BaseFragment<CourseHistoryFragmentPresenter> implements CourseHistoryFragmentContract.View {


    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;


    @BindView(R.id.swipe_refresh_layout)
    public SwipeRefreshLayout mRefresher;

    private UniversalAdapter mAdapter;

    private List<BaseItem> mList = new ArrayList<>();


    private TrainingCourse mCourse;

    public static CourseHistoryFragment newInstance(TrainingCourse course) {
        CourseHistoryFragment fragment = new CourseHistoryFragment();
        Bundle args = new Bundle();
        args.putInt("course", course.getId());
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCourse = RealmTrainingsDataSource.getInstance().getCourseById(getArguments().getInt("course"));
        mPresenter = new CourseHistoryFragmentPresenter(mCourse);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mAdapter = new UniversalAdapter(mList, 0);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
        mRefresher.setOnRefreshListener(() -> mPresenter.sync());
        return view;
    }


    @Nullable
    @Override
    public String getTitle() {
        return getString(R.string.ttl_course_history);
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        mRefresher.setRefreshing(refreshing);
    }


    @Override
    public void dataReplace(List<BaseItem> data) {
        mAdapter.replaceList(data);
    }

    @Override
    public void openExerciseHistory(DayExerciseDto data) {
        if (data.getId() != null) {
            getFragNavController().pushFragment(ExerciseHistoryFragment.newInstance(data, mCourse, false));
        }
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_course_history;
    }
}
