package fitness.online.app.activity.main.fragment.trainings.nutrition;

import android.support.annotation.NonNull;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.local.RealmNutritionDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitNutritionDataSource;
import fitness.online.app.model.pojo.realm.common.nutrition.NutritionDto;
import fitness.online.app.model.pojo.realm.handbook.HandbookCategory;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.contract.fragment.SelectNutritionFragmentContract;
import fitness.online.app.recycler.data.HandbookNavigationData;
import fitness.online.app.recycler.item.trainings.SelectExerciseItem;

/**
 * Created by user on 22.12.17.
 */

public class SelectNutritionFragmentPresenter extends SelectNutritionFragmentContract.Presenter implements HandbookNavigationData.Listener {

    private Integer courseId;
    private String categoryId;
    private Integer itemId;
    private boolean editable;
    private String comment;

    private int indexSelected = -1;

    private List<BaseItem> items = new ArrayList<>();

    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        if (firstBind) {
            HandbookCategory category = RealmHandbookDataSource.getInstance().getCategoryById(categoryId);
            executeBounded(view -> view.bind(category));
            loadData();
        }
    }

    public SelectNutritionFragmentPresenter(Integer courseId, String categoryId, Integer itemId, boolean editable) {
        this.categoryId = categoryId;
        this.itemId = itemId;
        this.editable = editable;
        this.courseId = courseId;
        if (itemId != null) {
            NutritionDto selectedNutrition = RealmNutritionDataSource.getInstance().getNutritionById(itemId);
            if (selectedNutrition != null) {
                RealmNutritionDataSource.getInstance().setSelectedId(selectedNutrition.getPost_sport_food_id());
            }
        }
    }

    @Override
    public void loadData() {
        executeBounded(view -> view.updateMenu());
        RealmHandbookDataSource.getInstance().listData(categoryId).subscribe(this::bindData);
    }

    @Override
    public void confirm() {
        int selectedId = RealmNutritionDataSource.getInstance().getSelectedId();
        executeBounded(view -> comment = view.getComment());
        showProgressBar();
        if (itemId == 0) {
            RetrofitNutritionDataSource.getInstance().createNutrition(courseId, selectedId, comment, new BasicResponseListener() {
                @Override
                public void success(Object response) {
                    hideProgressBar();
                    RealmNutritionDataSource.getInstance().setSelectedId(null);
                    executeBounded(view -> view.onSelected(2));
                }

                @Override
                public void error(@NonNull Throwable throwable) {
                    super.error(throwable);
                    hideProgressBar();
                }
            });
        } else {
            RetrofitNutritionDataSource.getInstance().updateNutrition(courseId, itemId, selectedId, comment, new BasicResponseListener() {
                @Override
                public void success(Object response) {
                    hideProgressBar();
                    RealmNutritionDataSource.getInstance().setSelectedId(null);
                    executeBounded(view -> view.onSelected(1));
                }

                @Override
                public void error(@NonNull Throwable throwable) {
                    super.error(throwable);
                    hideProgressBar();
                }
            });
        }
    }

    private void bindData(List<HandbookNavigation> data) {
        executeBounded(view -> view.dataReplace(prepareData(data)));
    }

    private List<BaseItem> prepareData(List<HandbookNavigation> data) {
        ArrayList<BaseItem> result = new ArrayList<>();
        for (HandbookNavigation item : data) {
            int index = data.indexOf(item);
            item.setIndex(index);
            boolean isLast = index == data.size() - 1;
            boolean isSelected = item.getId().equals(RealmNutritionDataSource.getInstance().getSelectedId() + "");
            if (isSelected) {
                indexSelected = index;
            }
            if (editable || isSelected) {
                result.add(new SelectExerciseItem(item, this, isSelected, isLast || !editable));
            }
        }
        items = result;
        return items;
    }

    @Override
    public void onClick(HandbookNavigation handbookNavigation) {
        try {
            RealmNutritionDataSource.getInstance().setSelectedId(Integer.valueOf(handbookNavigation.getId()));
            int oldIndex = indexSelected;
            int newIndex = handbookNavigation.getIndex();
            indexSelected = newIndex;
            if (oldIndex != -1) {
                ((SelectExerciseItem) items.get(oldIndex)).getData().setSelected(false);
            }
            ((SelectExerciseItem) items.get(newIndex)).getData().setSelected(true);
            executeBounded(view -> view.updateList(newIndex, oldIndex));
        } catch (Exception ex) {
        }
        //loadData();
    }

    @Override
    public void onSecondaryClick(HandbookNavigation handbookNavigation) {
        executeBounded(view -> view.viewDetails(handbookNavigation));
    }
}
