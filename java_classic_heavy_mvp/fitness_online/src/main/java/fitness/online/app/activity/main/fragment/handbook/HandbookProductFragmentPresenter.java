package fitness.online.app.activity.main.fragment.handbook;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import fitness.online.app.data.local.RealmDietDataSource;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitDietDataSource;
import fitness.online.app.model.pojo.realm.common.diet.Product;
import fitness.online.app.model.pojo.realm.common.diet.ProductResponse;
import fitness.online.app.model.pojo.realm.handbook.HandbookProduct;
import fitness.online.app.mvp.contract.fragment.HandbookProductFragmentContract;
import io.reactivex.Observable;


/**
 * Created by user on 04.10.17.
 */

public class HandbookProductFragmentPresenter extends HandbookProductFragmentContract.Presenter {

    private boolean fromDiet, fromTrainer;

    private String id;

    private Product mProduct;

    public HandbookProductFragmentPresenter(String id, boolean fromDiet, boolean fromTrainer) {
        this.fromDiet = fromDiet;
        this.id = id;
        this.fromTrainer = fromTrainer;
    }

    @Override
    public void loadData() {
        HandbookProduct product = RealmHandbookDataSource.getInstance().getProductById(id);
        Observable<HandbookProduct> observable = Observable.empty();
        if (product != null) {
            observable = observable.just(product);
        }
        observable.subscribe(this::loadDataLocalSuccess);
    }

    @Override
    public void loadDataLocalSuccess(@Nullable HandbookProduct product) {
        executeBounded(view -> view.bindData(product));
        if (fromDiet && RetrofitDietDataSource.getInstance().getEditMeal() != null) {
            for (Product product_ : RetrofitDietDataSource.getInstance().getEditMeal().getProducts()) {
                if ((product_.getPost_product_id() + "").equals(id)) {
                    mProduct = product_;
                    executeBounded(view -> view.bindDiet(mProduct));
                    break;
                }
            }
        }
    }

    @Override
    public void confirm(Integer portion, String comment) {
        boolean edit = false;
        showProgressBar(true);
        if (mProduct == null) {
            mProduct = new Product(Integer.valueOf(id), portion);
            mProduct.setComment(comment);
        } else {
            edit = true;
            RealmDietDataSource.getInstance().updateProduct(mProduct, portion, comment);
        }
        if (!edit) {
            boolean finalEdit = edit;
            RetrofitDietDataSource.getInstance().addProductToMeal(mProduct, new BasicResponseListener<ProductResponse>() {
                @Override
                public void success(ProductResponse response) {
                    hideProgressBar();
                    executeBounded(view -> view.onSelected(finalEdit));
                }

                @Override
                public void error(@NonNull Throwable throwable) {
                    super.error(throwable);
                    hideProgressBar();
                }
            });
        } else {
            boolean finalEdit1 = edit;
            RetrofitDietDataSource.getInstance().updateProduct(mProduct, new BasicResponseListener<ProductResponse>() {
                @Override
                public void success(ProductResponse response) {
                    hideProgressBar();
                    executeBounded(view -> view.onSelected(finalEdit1));
                }

                @Override
                public void error(@NonNull Throwable throwable) {
                    super.error(throwable);
                    hideProgressBar();
                }
            });
        }
    }
}
