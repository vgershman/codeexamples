package fitness.online.app.mvp.contract.fragment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface HandbookExerciseFragmentContract {

    interface View extends FragmentView {

        void bindData(HandbookExercise exercise);

        void openVideoPlayer(HandbookExercise exercise);
    }

    abstract class Presenter extends BaseFragmentPresenter<HandbookExerciseFragmentContract.View> {

        public abstract void onPlayClicked(HandbookExercise exercise);

        public abstract void loadData(@NonNull String id);

        public abstract void loadDataLocalSuccess(@Nullable HandbookExercise exercise);

        public abstract void toggle(HandbookExercise exercise);
    }
}
