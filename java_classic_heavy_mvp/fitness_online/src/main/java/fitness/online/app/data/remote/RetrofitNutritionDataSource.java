package fitness.online.app.data.remote;

import fitness.online.app.data.local.RealmNutritionDataSource;
import fitness.online.app.model.Api;
import fitness.online.app.model.api.CoursesApi;
import fitness.online.app.util.rx.ExceptionUtils;
import fitness.online.app.util.scheduler.SchedulerTransformer;

/**
 * Created by user on 13.12.17.
 */

public class RetrofitNutritionDataSource {

    public void getNutritionByCourse(int courseId, BasicResponseListener listener) {
        Api.getService(CoursesApi.class).getNutrition(courseId).compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> {
                    RealmNutritionDataSource.getInstance().storeNutrition(data, courseId);
                    listener.success(data);
                }, exception -> ExceptionUtils.sendThrowable(exception));
    }


    public void createNutrition(int courseId, int id, String comment, BasicResponseListener listener) {
        Api.getService(CoursesApi.class).createNutrition(courseId, id, comment).compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> {
                    RealmNutritionDataSource.getInstance().storeNutrition(data, courseId);
                    listener.success(data);
                }, exception -> ExceptionUtils.sendThrowable(exception));
    }

    public void updateNutrition(int courseId, int nid, int id, String comment, BasicResponseListener listener) {
        Api.getService(CoursesApi.class).updateNutrition(courseId, nid, id, comment).compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> {
                    RealmNutritionDataSource.getInstance().storeNutrition(data, courseId);
                    listener.success(data);
                }, exception -> ExceptionUtils.sendThrowable(exception));
    }

    public void deleteNutrition(int courseId, int nid, BasicResponseListener listener) {
        Api.getService(CoursesApi.class).deleteNutrition(courseId, nid).compose(SchedulerTransformer.ioToMain())
                .subscribe(() -> {
                    listener.success(null);
                }, exception -> ExceptionUtils.sendThrowable(exception));
    }

    public static class INSTANCE_HOLDER {
        public static final RetrofitNutritionDataSource INSTANCE = new RetrofitNutritionDataSource();
    }

    public static RetrofitNutritionDataSource getInstance() {
        return RetrofitNutritionDataSource.INSTANCE_HOLDER.INSTANCE;
    }
}
