package fitness.online.app.activity.main.fragment.trainings.exercises;

import android.support.annotation.NonNull;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.CourseHistoryItem;
import fitness.online.app.model.pojo.realm.common.trainings.DayExercise;
import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.mvp.contract.fragment.CourseHistoryFragmentContract;
import fitness.online.app.recycler.data.trainings.DayExerciseData;
import fitness.online.app.recycler.data.trainings.TrainingDayData;
import fitness.online.app.recycler.item.ClickListener;
import fitness.online.app.recycler.item.trainings.DayExerciseItem;
import fitness.online.app.recycler.item.trainings.TrainingDayItem;

/**
 * Created by user on 31.10.17.
 */

public class CourseHistoryFragmentPresenter extends CourseHistoryFragmentContract.Presenter implements ClickListener<DayExerciseDto> {


    private TrainingCourse mCourse;

    public CourseHistoryFragmentPresenter(TrainingCourse course) {
        mCourse = course;
    }

    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        if (firstBind) {
            sync();
        }
        loadData();
    }

    @Override
    public void loadData() {
        RealmTrainingsDataSource.getInstance().getHistoryByCourseId(mCourse.getId()).subscribe(this::bindData);
    }

    @Override
    public void sync() {
        executeBounded(view -> view.setRefreshing(false));
        showProgressBar(false);
        RetrofitTrainingsDataSource.getInstance().syncHistoryByCourse(mCourse.getId(), new BasicResponseListener() {
            @Override
            public void success(Object response) {
                loadData();
                hideProgressBar();
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                hideProgressBar();
            }
        }, 1);
    }

    private void bindData(List<CourseHistoryItem> records) {
        executeBounded(view -> view.dataReplace(prepareData(records)));
    }

    private List<BaseItem> prepareData(List<CourseHistoryItem> courseHistoryItems) {
        List<BaseItem> items = new ArrayList<>();
        for (CourseHistoryItem historyItem : courseHistoryItems) {
            items.add(new TrainingDayItem(new TrainingDayData(false, historyItem.getDay(), 0, historyItem.getDate(), null, false)));
            for (DayExercise dayExercise : historyItem.getRecords()) {
                DayExerciseData data = new DayExerciseData(false, new DayExerciseDto(dayExercise), this, null, null, null);
                data.setHistoryItem(historyItem);
                items.add(new DayExerciseItem(data));
            }
            ((DayExerciseItem) items.get(items.size() - 1)).getData().setNeedBottomLine();
        }
        return items;
    }

    @Override
    public void onClick(DayExerciseDto data) {
        executeBounded(view -> view.openExerciseHistory(data));
    }
}
