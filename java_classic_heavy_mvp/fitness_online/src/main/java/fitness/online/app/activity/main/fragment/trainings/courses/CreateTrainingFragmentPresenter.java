package fitness.online.app.activity.main.fragment.trainings.courses;

import android.support.annotation.NonNull;

import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.CoursesResponse;
import fitness.online.app.mvp.contract.fragment.CreateTrainingFragmentContract;

/**
 * Created by user on 24.10.17.
 */

public class CreateTrainingFragmentPresenter extends CreateTrainingFragmentContract.Presenter {


    @Override
    public void createTraining(String name, String goal) {
        showProgressBar(true);
        RetrofitTrainingsDataSource.getInstance().createTraining(name, goal, new BasicResponseListener<CoursesResponse>() {
            @Override
            public void success(CoursesResponse response) {
                RealmTrainingsDataSource.getInstance().storeMyTrainings(response);
                hideProgressBar();
                executeBounded(view -> view.onSuccess());
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                hideProgressBar();
            }
        });
    }
}
