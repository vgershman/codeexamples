package fitness.online.app.activity.main.fragment.trainings.exercises;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingDay;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.TrainingDaysFragmentContract;
import fitness.online.app.recycler.adapter.UniversalAdapter;
import fitness.online.app.util.LinearManagerWrapper;

/**
 * Created by user on 28.10.17.
 */

public class TrainingDaysFragment extends BaseFragment<TrainingDaysFragmentPresenter> implements TrainingDaysFragmentContract.View {


    @BindView(R.id.recycler_view)
    public RecyclerView mRecyclerView;

    @BindView(R.id.swipe_refresh_layout)
    public SwipeRefreshLayout mRefresher;

    private ArrayList<BaseItem> mList = new ArrayList();

    private UniversalAdapter mAdapter;

    private TrainingCourse mCourse;

    public static TrainingDaysFragment newInstance(TrainingCourse course) {
        TrainingDaysFragment fragment = new TrainingDaysFragment();
        Bundle args = new Bundle();
        args.putInt("course", course.getId());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCourse = RealmTrainingsDataSource.getInstance().getCourseById(getArguments().getInt("course"));
        mPresenter = new TrainingDaysFragmentPresenter(mCourse);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mAdapter = new UniversalAdapter(mList, 0);
        mRecyclerView.setLayoutManager(new LinearManagerWrapper(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
        mRefresher.setOnRefreshListener(() -> mPresenter.sync());
        return view;
    }

    @Override
    public int getMenuId() {
        return R.menu.history;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.btn_history) {
            getFragNavController().pushFragment(CourseHistoryFragment.newInstance(mCourse));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public String getTitle() {
        return getString(R.string.ttl_training_days);
    }

    @Override
    public void dataReplace(List<BaseItem> data) {
        mAdapter.replaceList(data);
    }

    @Override
    public void onDaySelected(TrainingDay day) {
        getFragNavController().pushFragment(DayExercisesFragment.newInstance(day, mCourse));
    }

    @Override
    public void onAddNewDay() {
        getFragNavController().pushFragment(DayExercisesFragment.newInstance(null, mCourse));
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        mRefresher.setRefreshing(refreshing);
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_training_days;
    }
}
