package fitness.online.app.data.remote;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;

import java.util.List;

import javax.annotation.Nonnull;

import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.Api;
import fitness.online.app.model.api.CoursesApi;
import fitness.online.app.model.pojo.realm.common.social.SocialToken;
import fitness.online.app.model.pojo.realm.common.trainings.CoursesResponse;
import fitness.online.app.model.pojo.realm.common.trainings.DayExercise;
import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseWrapper;
import fitness.online.app.model.pojo.realm.common.trainings.EditTrainingDayBody;
import fitness.online.app.model.pojo.realm.common.trainings.HistoryRecord;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingDay;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingDayResponse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingHistoryBody;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingTemplate;
import fitness.online.app.util.rx.ExceptionUtils;
import fitness.online.app.util.scheduler.SchedulerTransformer;

/**
 * Created by user on 26.10.17.
 */

public class RetrofitTrainingsDataSource {


    public void publishCourse(Integer courseId, BasicResponseListener listener) {
        Api.getService(CoursesApi.class).publishCourse(courseId)
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(listener::success, listener::error);
    }

    public void deleteHistoryRecord(Integer courseId, Integer recordId, BasicResponseListener listener) {
        Api.getService(CoursesApi.class).deleteApiV1CoursesWorkoutIdCompletedSetsId(courseId, recordId)
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(() -> {
                    RealmTrainingsDataSource.getInstance().deleteHistoryRecord(recordId);
                    listener.success(null);
                }, listener::error);
    }

    public void deleteTraining(Integer courseId, BasicResponseListener listener) {
        Api.getService(CoursesApi.class).deleteApiV1CoursesCourseId(courseId)
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(() -> {
                    RealmTrainingsDataSource.getInstance().deleteCourse(courseId);
                    listener.success(null);
                }, listener::error);
    }


    public void syncMyTrainings(BasicResponseListener listener, Integer beforeId) {
        Api.getService(CoursesApi.class).getApiV1Courses(beforeId).compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> {
                    RealmTrainingsDataSource.getInstance().storeMyTrainings(data);
                    if (data.getCourses().size() > 0) {
                        syncMyTrainings(listener, data.getCourses().get(data.getCourses().size() - 1).getId());
                    } else {
                        listener.success(null);
                    }
                }, exception -> ExceptionUtils.sendThrowable(exception));
    }

    public void syncHistory(Integer courseId, Integer exerciseId, BasicResponseListener listener) {
        //todo wtf limit
        Api.getService(CoursesApi.class).getApiV1CoursesWorkoutIdCompletedSets(courseId, exerciseId + "", 1).compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> {
                    RealmTrainingsDataSource.getInstance().storeHistory(data, courseId, null);
                    listener.success(null);
                }, exception -> ExceptionUtils.sendThrowable(exception));
    }

    public void syncHistoryByCourse(Integer courseId, BasicResponseListener listener, int page) {
        //todo wtf limit
        Api.getService(CoursesApi.class).getApiV1CoursesWorkoutIdCompletedSets(courseId, null, page).compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> {
                    RealmTrainingsDataSource.getInstance().storeHistory(data, courseId, null);
                    if (data.getCompleted_sets().size() > 0) {
                        syncHistoryByCourse(courseId, listener, page + 1);
                    } else {
                        listener.success(null);
                    }
                }, exception -> {
                    ExceptionUtils.sendThrowable(exception);
                    listener.success(null);
                });
    }


    public void syncDaysByCourse(Integer courseId, BasicResponseListener listener) {
        Api.getService(CoursesApi.class).getApiV1CoursesWorkoutIdTrainingDays(courseId)
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(data -> {
                    RealmTrainingsDataSource.getInstance().storeDays(data, courseId, null);
                    listener.success(null);
                }, exception -> ExceptionUtils.sendThrowable(exception));
    }

    public void listTemplates(ResponseListener<CoursesResponse> listener) {
        CoursesApi coursesApi = Api.getService(CoursesApi.class);
        coursesApi.getApiV1CoursesTemplates()
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(listener::success, listener::error);
    }

    public void createCourse(@Nonnull TrainingTemplate template, @Nullable String purchaseJson, ResponseListener<CoursesResponse> listener) {
        CoursesApi.CreateCourse createCourse = new CoursesApi.CreateCourse();
        createCourse.templateId = template.getId();
        if (!TextUtils.isEmpty(purchaseJson)) {
            createCourse.inappReceipt = Base64.encodeToString(purchaseJson.getBytes(), Base64.DEFAULT);
        }
        Api.getService(CoursesApi.class).postApiV1Courses(new CoursesApi.CreateCourseObject(createCourse))
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(listener::success, listener::error);
    }

    public void createCourseRepost(@Nonnull TrainingTemplate template, @NonNull SocialToken token, ResponseListener<CoursesResponse> listener) {
        CoursesApi.CreateCourse createCourse = new CoursesApi.CreateCourse();
        Api.getService(CoursesApi.class).postApiV1Courses(
                template.getId(),
                token.getType(),
                token.getToken()
        )
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(listener::success, listener::error);
    }

    public void deleteDay(Integer courseId, Integer dayId, ResponseListener<Object> listener) {
        Api.getService(CoursesApi.class).deleteApiV1CoursesWorkoutIdTrainingDaysId(courseId, dayId)
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(() -> {
                            RealmTrainingsDataSource.getInstance().deleteDay(dayId);
                            listener.success(null);
                        },
                        listener::error);
    }

    public void createNewTrainingDay(Integer courseId, ResponseListener<TrainingDayResponse> listener) {
        BasicResponseListener<TrainingDayResponse> fakeListener = new BasicResponseListener<TrainingDayResponse>() {
            @Override
            public void success(TrainingDayResponse response) {
                RealmTrainingsDataSource.getInstance().storeDays(response, courseId, null);
                listener.success(response);
            }
        };
        for (DayExerciseDto editExercise : RealmTrainingsDataSource.getInstance().getEditDay().getExercises()) {
            if (editExercise.getId() < 0) {
                editExercise.setId(null);
            }
        }
        Api.getService(CoursesApi.class).postApiV1CoursesWorkoutIdTrainingDays(courseId, new EditTrainingDayBody(RealmTrainingsDataSource.getInstance().getEditDay()))
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(fakeListener::success, fakeListener::error);
    }

    public void createHistoryRecords(List<HistoryRecord> records, BasicResponseListener<TrainingDayResponse> listener) {
        Api.getService(CoursesApi.class).postApiV1CoursesCompletedSets(new TrainingHistoryBody(records))
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(listener::success, listener::error);
    }


    public void createTraining(String name, String goal, BasicResponseListener<CoursesResponse> listener) {
        BasicResponseListener<CoursesResponse> fakeListener = new BasicResponseListener<CoursesResponse>() {
            @Override
            public void success(CoursesResponse response) {
                RealmTrainingsDataSource.getInstance().storeMyTrainings(response);
                listener.success(response);
            }
        };
        Api.getService(CoursesApi.class).postApiV1Courses(name, goal).compose(SchedulerTransformer.ioToMain())
                .subscribe(fakeListener::success, fakeListener::error);
    }

    public void configureExercise(Integer courseId, Integer dayId, Integer exerciseId, DayExerciseDto dayExercise, ResponseListener listener) {
        ResponseListener fakeListener = new ResponseListener() {
            @Override
            public void success(Object response) {
                // RealmTrainingsDataSource.getInstance().storeDays(response, courseId, null);
                listener.success(response);
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                listener.error(throwable);
            }
        };
        Api.getService(CoursesApi.class).configureExercise(courseId, dayId, exerciseId, new DayExerciseWrapper(dayExercise))
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(fakeListener::success, fakeListener::error);
    }

    public void updateTrainingDay(Integer courseId, Integer dayId, ResponseListener<List<TrainingDay>> listener) {
        for (DayExercise exercise : RealmTrainingsDataSource.getInstance().listExercisesByDay(dayId)) {
            boolean toDelete = true;
            for (DayExerciseDto editExercise : RealmTrainingsDataSource.getInstance().getEditDay().getExercises()) {
                if (exercise.getId() == editExercise.getId()) {
                    toDelete = false;
                }
            }
            if (toDelete) {
                DayExerciseDto deletedExercise = new DayExerciseDto(exercise.getId(), exercise.getPost_exercise_id());
                deletedExercise.setDelete(true);
                RealmTrainingsDataSource.getInstance().getEditDay().getExercises().add(deletedExercise);
            }
        }
        for (DayExerciseDto editExercise : RealmTrainingsDataSource.getInstance().getEditDay().getExercises()) {
            if (editExercise.getId() < 0) {
                editExercise.setId(null);
            }
        }
        ResponseListener<TrainingDayResponse> fakeListener = new BasicResponseListener<TrainingDayResponse>() {
            @Override
            public void success(TrainingDayResponse response) {
                RealmTrainingsDataSource.getInstance().storeDays(response, courseId, null);
                listener.success(null);
            }
        };
        Api.getService(CoursesApi.class).putApiV1CoursesWorkoutIdTrainingDaysId(courseId, dayId, new EditTrainingDayBody(RealmTrainingsDataSource.getInstance().getEditDay()))
                .compose(SchedulerTransformer.ioToMain())
                .subscribe(fakeListener::success, fakeListener::error);
    }

    public static class INSTANCE_HOLDER {
        public static final RetrofitTrainingsDataSource INSTANCE = new RetrofitTrainingsDataSource();
    }

    public static RetrofitTrainingsDataSource getInstance() {
        return RetrofitTrainingsDataSource.INSTANCE_HOLDER.INSTANCE;
    }
}
