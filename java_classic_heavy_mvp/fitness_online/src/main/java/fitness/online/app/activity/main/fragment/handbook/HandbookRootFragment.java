package fitness.online.app.activity.main.fragment.handbook;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.trimf.recycler.item.BaseItem;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.model.pojo.realm.handbook.HandbookCategory;
import fitness.online.app.model.pojo.realm.handbook.HandbookEntity;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.model.pojo.realm.handbook.HandbookProduct;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.HandbookRootFragmentContract;
import fitness.online.app.recycler.adapter.UniversalAdapter;
import fitness.online.app.util.handbook.HandbookCacher;

/**
 * Created by user on 04.10.17.
 */

public class HandbookRootFragment extends BaseFragment<HandbookRootFragmentPresenter> implements HandbookRootFragmentContract.View {


    private HandbookCategory mCategory;

    protected static final String EXTRA_INPUT = "navigation_id";

    @BindView(R.id.recycler_view)
    public RecyclerView mRecyclerView;

    private ArrayList<BaseItem> mList = new ArrayList();

    private UniversalAdapter mAdapter;

    @BindView(R.id.swipe_refresh_layout)
    public SwipeRefreshLayout mRefresher;

    private boolean fromDiet;
    private boolean fromTrainer;

    @Override
    public int getLayout() {
        return R.layout.fragment_handbook;
    }

    @NonNull
    public static HandbookRootFragment newInstance(@Nullable HandbookNavigation navigation, boolean fromDiet, boolean fromTrainer) {
        HandbookRootFragment fragment = new HandbookRootFragment();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_INPUT, Parcels.wrap(navigation));
        args.putBoolean("diet", fromDiet);
        args.putBoolean("trainer", fromTrainer);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HandbookNavigation navigation = Parcels.unwrap(getArguments().getParcelable(EXTRA_INPUT));
        fromDiet = getArguments().getBoolean("diet", false);
        fromTrainer = getArguments().getBoolean("trainer", false);
        String catId = null;
        if (navigation != null && navigation.getType().equals(HandbookEntity.CATEGORY)) {
            catId = navigation.getId();
            mCategory = RealmHandbookDataSource.getInstance().getCategoryById(catId);
            if (mCategory.getPost_type().equals(HandbookProduct.EXERCISE) && mCategory.getParent_id() != null) {
                HandbookCacher.setCurrentCategory(mCategory.getCategory_id());
            } else {
                HandbookCacher.setCurrentCategory(null);
            }
        }
        mPresenter = new HandbookRootFragmentPresenter(catId, fromDiet, fromTrainer);
    }

    @Nullable
    @Override
    public String getTitle() {
        return mCategory == null ? getString(R.string.ttl_handbook) : mCategory.getTitle();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mAdapter = new UniversalAdapter(mList, 0);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mRefresher.setOnRefreshListener(() -> mPresenter.sync());
        if (mPresenter.getCategoryId() == null) {
            mRefresher.setEnabled(true);
        } else {
            mRefresher.setEnabled(false);
        }
        return view;
    }


    @Override
    public int getMenuId() {
        if (mCategory == null || !"post_product".equals(mCategory.getPost_type())) {
            return R.menu.empty_menu;
        }
        if (RealmHandbookDataSource.getInstance().isFilterEnabled()) {
            return R.menu.product_filter;
        } else {
            return R.menu.product;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.product_filter:
                getFragNavController().pushFragment(HandbookFilterFragment.newInstance());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void setRefreshing(boolean refreshing) {
        mRefresher.setRefreshing(refreshing);
    }

    @Override
    public void setupCacher() {
        HandbookCacher.setup(getActivity());
    }

    @Override
    public void dataReplace(List<BaseItem> data) {
        mAdapter.replaceList(data);
    }

    @Override
    public void onNavigationSelect(HandbookNavigation handbookNavigation) {
        getFragNavController().pushFragment(mPresenter.createNextFragment(handbookNavigation));
    }
}
