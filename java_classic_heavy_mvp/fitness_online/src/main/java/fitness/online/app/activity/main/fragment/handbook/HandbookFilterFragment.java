package fitness.online.app.activity.main.fragment.handbook;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import fitness.online.app.R;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.HandbookFilterFragmentContract;
import fitness.online.app.recycler.adapter.UniversalAdapter;

/**
 * Created by user on 16.10.17.
 */

public class HandbookFilterFragment extends BaseFragment<HandbookFilterFragmentPresenter> implements HandbookFilterFragmentContract.View {


    @BindView(R.id.recycler_view)
    public RecyclerView mRecyclerView;

    @OnClick(R.id.btnFilter)
    public void onFilterClicked() {
        mPresenter.onFilterClicked();
    }

    @OnClick(R.id.btnClearFilter)
    public void onClearFilterClicked() {
        mPresenter.onClearFilterClicked();
    }


    private UniversalAdapter mAdapter;


    @NonNull
    public static HandbookFilterFragment newInstance() {
        HandbookFilterFragment fragment = new HandbookFilterFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter = new HandbookFilterFragmentPresenter();
        mPresenter.loadFilter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mAdapter = new UniversalAdapter(new ArrayList<>(), 0);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }


    @Override
    public void bindData(List<BaseItem> filterItems) {
        mAdapter.replaceList(filterItems);
    }

    @Override
    public void updateData() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void filterAction() {
        mPresenter.loadFilter();
        getActivity().onBackPressed();
    }

    @Nullable
    @Override
    public String getTitle() {
        return getString(R.string.ttl_handook_filter);
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_handbook_filter;
    }
}
