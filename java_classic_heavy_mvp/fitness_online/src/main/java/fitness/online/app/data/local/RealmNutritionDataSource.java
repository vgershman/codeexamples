package fitness.online.app.data.local;

import java.util.List;

import fitness.online.app.model.pojo.realm.common.nutrition.NutritionDto;
import fitness.online.app.model.pojo.realm.common.nutrition.NutritionResponse;
import fitness.online.app.util.realm.RealmHelper;
import io.realm.Realm;
import timber.log.Timber;

/**
 * Created by user on 15.12.17.
 */

public class RealmNutritionDataSource {

    private Realm mRealm = RealmHelper.getCommonRealmInstance();
    private Integer selectedId = null;


    public void deleteNutrition(Integer id) {
        try {
            mRealm.beginTransaction();
            mRealm.where(NutritionDto.class).equalTo("id", id).findAll().deleteAllFromRealm();
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }


    public void setSelectedId(Integer selectedId) {
        this.selectedId = selectedId;
    }

    public Integer getSelectedId() {
        return selectedId;
    }

    public static class INSTANCE_HOLDER {
        public static final RealmNutritionDataSource INSTANCE = new RealmNutritionDataSource();
    }

    public static RealmNutritionDataSource getInstance() {
        return RealmNutritionDataSource.INSTANCE_HOLDER.INSTANCE;
    }


    public List<NutritionDto> getNutritionByCourse(int courseId) {
        return mRealm.where(NutritionDto.class).equalTo("course_id", courseId).findAll();
    }

    public NutritionDto getNutritionById(int id) {
        return mRealm.where(NutritionDto.class).equalTo("id", id).findFirst();
    }

    public void storeNutrition(NutritionResponse nutritionResponse, int courseId) {
        try {
            mRealm.beginTransaction();
            if (nutritionResponse.getSport_food() != null) {
                nutritionResponse.getSport_food().setCourse_id(courseId);
                mRealm.copyToRealmOrUpdate(nutritionResponse.getSport_food());
            }
            if (nutritionResponse.getSport_foods() != null) {
                for (NutritionDto nutritionDto : nutritionResponse.getSport_foods()) {
                    nutritionDto.setCourse_id(courseId);
                    mRealm.copyToRealmOrUpdate(nutritionDto);
                }
            }
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }
    }
}
