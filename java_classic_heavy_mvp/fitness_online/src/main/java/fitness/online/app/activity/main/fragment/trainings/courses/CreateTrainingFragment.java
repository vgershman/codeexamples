package fitness.online.app.activity.main.fragment.trainings.courses;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.EditText;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.CreateTrainingFragmentContract;
import fitness.online.app.util.rx.ExceptionUtils;

/**
 * Created by user on 24.10.17.
 */

public class CreateTrainingFragment extends BaseFragment<CreateTrainingFragmentPresenter> implements CreateTrainingFragmentContract.View {

    @BindView(R.id.et_name)
    EditText etName;

    @BindView(R.id.et_goal)
    EditText etGoal;


    @NonNull
    public static CreateTrainingFragment newInstance() {
        CreateTrainingFragment fragment = new CreateTrainingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new CreateTrainingFragmentPresenter();
    }


    @Override
    public int getMenuId() {
        return R.menu.create_training;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ok:
                checkAndCreateTraining();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkAndCreateTraining() {
        String name = etName.getText().toString();
        String goal = etGoal.getText().toString();
        if (TextUtils.isEmpty(name.replaceAll(" ", ""))
                || TextUtils.isEmpty(goal.replaceAll(" ", ""))) {
            ExceptionUtils.sendThrowable(new Throwable(getString(R.string.error_new_training)));
            return;
        }
        mPresenter.createTraining(name, goal);
    }


    @Override
    public void onSuccess() {
        getFragNavController().popFragments(2);
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_create_training;
    }

    @Nullable
    @Override
    public String getTitle() {
        return getString(R.string.ttl_create_training);
    }

    @Override
    public int getHomeIconId() {
        return R.drawable.ic_actionbar_close;
    }
}
