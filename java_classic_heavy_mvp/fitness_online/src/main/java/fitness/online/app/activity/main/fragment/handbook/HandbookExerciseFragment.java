package fitness.online.app.activity.main.fragment.handbook;

import android.content.res.Resources;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yqritc.scalablevideoview.ScalableVideoView;

import org.parceler.Parcels;

import java.io.File;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.activity.main.fragment.video.VideoPlayerFragment;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.pojo.realm.RealmString;
import fitness.online.app.model.pojo.realm.handbook.HandbookCategory;
import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.HandbookExerciseFragmentContract;
import fitness.online.app.util.ImageHelper;
import fitness.online.app.util.UrlHelper;
import fitness.online.app.util.handbook.HandbookCacher;

/**
 * Created by user on 09.10.17.
 */

public class HandbookExerciseFragment extends BaseFragment<HandbookExerciseFragmentPresenter> implements HandbookExerciseFragmentContract.View {


    private static final String EXTRA_INPUT = "navigation";


    @BindView(R.id.tv_title)
    TextView title;

    @BindView(R.id.tv_description)
    TextView description;

    @BindView(R.id.tv_warning)
    TextView warning;

    @BindView(R.id.ll_how_to)
    LinearLayout howTo;

    @BindView(R.id.ivVideo)
    ScalableVideoView videoView;


    private HandbookCategory mCategory;
    private HandbookExercise mExercise;
    private boolean mSelectable;


    @Override
    public void bindData(HandbookExercise exercise) {
        mExercise = exercise;
        title.setText(exercise.getTitle());
        description.setText(exercise.getDescription());
        warning.setText(exercise.getWarning());
        howTo.removeAllViews();
        for (RealmString step : exercise.getExecution_order()) {
            View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_execution, howTo, false);
            ((TextView) itemView.findViewById(R.id.tv_content)).setText(step.getS());
            howTo.addView(itemView);
        }
        bindVideo(exercise, false);
        mCategory = RealmHandbookDataSource.getInstance().getCategoryById(exercise.getCategory_id());
    }


    private void bindVideo(HandbookExercise exercise, boolean force) {
        File file = HandbookCacher.getCacheFile(getContext(), exercise.getPreviewPath());
        Uri videoUri = null;
        if (file != null && !force) {
            videoUri = Uri.fromFile(file);
        } else {
            videoUri = Uri.parse(UrlHelper.getUrl(exercise.getPreviewPath(), null, false));
        }
        try {
            videoView.release();
        } catch (Exception ex) {
            String exm = ex.getMessage();
        }
        try {
            videoView.setDataSource(getContext(), videoUri);
        } catch (Exception ex) {
            String exm = ex.getMessage();
        }
        videoView.setLooping(true);
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                if (!force) {
                    bindVideo(exercise, true);
                }
                return true;
            }
        });
        try {
            videoView.prepareAsync(mediaPlayer -> videoView.start());
        } catch (Exception ex) {
            if (!force) {
                bindVideo(exercise, true);
            }
        }
    }


    @Override
    public void openVideoPlayer(HandbookExercise exercise) {
        getFragNavController().pushFragment(VideoPlayerFragment.newInstance(exercise.getVideo_link()));
    }

    @Override
    public int getMenuId() {
        if (mSelectable) {
            if (RealmTrainingsDataSource.getInstance().isSelected(mExercise.getId())) {
                return R.menu.exercise_selected;
            } else {
                return R.menu.exercise_unselected;
            }
        } else {
            return R.menu.exercise;
        }
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_handbook_exercise;
    }

    @NonNull
    public static HandbookExerciseFragment newInstance(@Nullable HandbookNavigation navigation, boolean selectable) {
        HandbookExerciseFragment fragment = new HandbookExerciseFragment();
        Bundle args = new Bundle();
        args.putBoolean("selectable", selectable);
        args.putParcelable(EXTRA_INPUT, Parcels.wrap(navigation));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        HandbookNavigation navigation = Parcels.unwrap(getArguments().getParcelable(EXTRA_INPUT));
        mSelectable = getArguments().getBoolean("selectable");
        mPresenter = new HandbookExerciseFragmentPresenter();
        mPresenter.loadData(navigation.getId());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setVideoSize();
        return view;
    }

    private void setVideoSize() {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) videoView.getLayoutParams();
        int videoWidth = (int) (Resources.getSystem().getDisplayMetrics().widthPixels * 0.6f);
        layoutParams.height = (int) ((videoWidth) * ImageHelper.EXERCISE_VIDEO_RATIO);
        layoutParams.width = videoWidth;
        videoView.setLayoutParams(layoutParams);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exercise_play:
                mPresenter.onPlayClicked(mExercise);
                return true;
            case R.id.ok:
                mPresenter.toggle(mExercise);
                getFragNavController().popFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        try {
            videoView.release(); //Internal NPE possibly :(
        } catch (Exception ex) {
        }
        super.onDestroyView();
    }

    @Nullable
    @Override
    public String getTitle() {
        return mCategory == null ? "" : mCategory.getTitle();
    }
}
