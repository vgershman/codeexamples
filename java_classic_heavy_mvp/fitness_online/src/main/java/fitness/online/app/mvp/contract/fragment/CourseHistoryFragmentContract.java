package fitness.online.app.mvp.contract.fragment;

import com.trimf.recycler.item.BaseItem;

import java.util.List;

import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface CourseHistoryFragmentContract {

    interface View extends FragmentView {

        void dataReplace(List<BaseItem> items);

        void openExerciseHistory(DayExerciseDto data);

        void setRefreshing(boolean refreshing);
    }

    abstract class Presenter extends BaseFragmentPresenter<CourseHistoryFragmentContract.View> {

        public abstract void loadData();

        public abstract void sync();
    }
}
