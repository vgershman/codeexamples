package fitness.online.app.mvp.contract.fragment;

import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface CreateTrainingDayFragmentContract {

    interface View extends FragmentView {

        void onError();

        void onSuccess();
    }

    abstract class Presenter extends BaseFragmentPresenter<CreateTrainingDayFragmentContract.View> {

        public abstract void createTrainingDay(TrainingCourse course, String dayName);

        public abstract void initCreation();
    }
}
