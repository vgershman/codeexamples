package fitness.online.app.activity.main.fragment.trainings.nutrition;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.data.local.RealmSessionDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.TrainingFragmentNutritionContract;
import fitness.online.app.recycler.adapter.UniversalAdapter;
import fitness.online.app.recycler.data.nutrition.NutritionData;

/**
 * Created by user on 25.10.17.
 */

public class TrainingNutritionFragment extends BaseFragment<TrainingNutritionFragmentPresenter> implements TrainingFragmentNutritionContract.View {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mRefresher;

    private UniversalAdapter mAdapter;
    private TrainingCourse mCourse;
    private List<BaseItem> mList = new ArrayList<>();

    @Override
    public void bind() {
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_handbook;
    }

    @Nullable
    @Override
    public String getTitle() {
        return getString(R.string.ttl_nutrition);
    }

    @Override
    public void dataReplace(List<BaseItem> items) {
        mList = items;
        mAdapter.replaceList(mList);
    }

    @Override
    public void selectNutrition() {
        getFragNavController().pushFragment(SelectNutritionCategoryFragment.newInstance(mCourse.getId()));
    }

    @Override
    public void editNutrition(NutritionData data) {
        getFragNavController().pushFragment(SelectNutritionFragment.newInstance(
                mCourse.getId(),
                data.getHandbook().getCategory_id(),
                mCourse.getInvoice_id() == null || RealmSessionDataSource.getInstance().isTrainer(),
                data.getNutrition().getId(),
                data.getNutrition().getComment()));
    }

    @Override
    public void askToDelete(NutritionData data) {
        new AlertDialog.Builder(getContext())
                .setMessage(R.string.alert_delete_nutrition)
                .setPositiveButton(R.string.alert_yes, (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    mPresenter.delete(data);
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel())
                .create().show();
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        mRefresher.setRefreshing(refreshing);
    }

    @Override
    public int getMenuId() {
        if (mCourse.getInvoice_id() == null || RealmSessionDataSource.getInstance().isTrainer()) {
            return R.menu.edit_prices;
        } else {
            return R.menu.empty_menu;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.edit_prices_add) {
            mPresenter.addNutrition();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static TrainingNutritionFragment newInstance(TrainingCourse course) {
        TrainingNutritionFragment trainingFragment = new TrainingNutritionFragment();
        Bundle args = new Bundle();
        args.putInt("course", course.getId());
        trainingFragment.setArguments(args);
        return trainingFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCourse = RealmTrainingsDataSource.getInstance().getCourseById(getArguments().getInt("course"));
        mPresenter = new TrainingNutritionFragmentPresenter(mCourse);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new UniversalAdapter(mList, 0);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
        mRefresher.setOnRefreshListener(() -> mPresenter.sync());
    }
}
