package fitness.online.app.activity.main.fragment.trainings.exercises;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.activity.main.fragment.handbook.HandbookExerciseFragment;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.DayExercise;
import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.model.pojo.realm.common.trainings.HistoryRecord;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.user.ExerciseHistoryFragmentContract;
import fitness.online.app.recycler.adapter.UniversalAdapter;
import fitness.online.app.util.LinearManagerWrapper;
import fitness.online.app.util.rx.ExceptionUtils;
import fitness.online.app.util.trainings.TrainingHistoryService;

/**
 * Created by user on 31.10.17.
 */

public class ExerciseHistoryFragment extends BaseFragment<ExerciseHistoryFragmentPresenter> implements ExerciseHistoryFragmentContract.View {


    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private UniversalAdapter mAdapter;

    private List<BaseItem> mList = new ArrayList<>();

    private DayExercise mExercise;

    private TrainingCourse mCourse;

    private boolean mInput;

    public static ExerciseHistoryFragment newInstance(DayExerciseDto exercise, TrainingCourse course, boolean input) {
        ExerciseHistoryFragment fragment = new ExerciseHistoryFragment();
        Bundle args = new Bundle();
        args.putInt("exer", exercise.getId());
        args.putInt("course", course.getId());
        args.putBoolean("input", input);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mExercise = RealmTrainingsDataSource.getInstance().getExerciseById(getArguments().getInt("exer"));
        mCourse = RealmTrainingsDataSource.getInstance().getCourseById(getArguments().getInt("course"));
        mInput = getArguments().getBoolean("input");
        mPresenter = new ExerciseHistoryFragmentPresenter(mExercise, mCourse, mInput);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mAdapter = new UniversalAdapter(mList, 0);
        mRecyclerView.setLayoutManager(new LinearManagerWrapper(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void askToDelete(HistoryRecord data) {
        new AlertDialog.Builder(getContext())
                .setMessage(R.string.alert_delete_set)
                .setPositiveButton(R.string.alert_yes, (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    mPresenter.deleteRecord(data);
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel())
                .create().show();
    }


    @Override
    public int getMenuId() {
        return R.menu.exercise_history;
    }


    @Nullable
    @Override
    public String getTitle() {
        if (mExercise != null) {
            try {
                return RealmHandbookDataSource.getInstance().getCategoryById(RealmHandbookDataSource.getInstance().getExerciseById(mExercise.getPost_exercise_id() + "").getCategory_id()).getTitle();
            } catch (Exception ex) { //todo wtf
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exercise_play:
                if (mExercise != null) {
                    mPresenter.onPlayClicked(mExercise.getPost_exercise_id());
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void openExerciseInfo(HandbookNavigation navigation) {
        getFragNavController().pushFragment(HandbookExerciseFragment.newInstance(navigation, false));
    }

    @Override
    public void historyUpSync() {
        hideSoftKeyboard();
        getActivity().startService(new Intent(getContext(), TrainingHistoryService.class));
    }

    @Override
    public void updateList() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void error(int errorId) {
        hideSoftKeyboard();
        ExceptionUtils.sendThrowable(new Throwable(getString(errorId)));
    }


    @Override
    public void dataReplace(List<BaseItem> data) {
        mAdapter.replaceList(data);
    }


    @Override
    public int getLayout() {
        return R.layout.fragment_exercise_history;
    }
}
