package fitness.online.app.mvp;

import android.os.Handler;

import fitness.online.app.view.progressBar.ProgressBarEntry;

public class BaseFragmentPresenter<T extends FragmentView> extends BasePresenter<T> {
    private ProgressBarEntry mProgressBarEntry;
    private boolean mFirstBind = true;

    @Override
    public void bindView(T mvpView) {
        super.bindView(mvpView);
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            bindViewAfterTransaction(mFirstBind);
            mFirstBind = false;
        }, 1);
    }

    protected void bindViewAfterTransaction(boolean firstBind) {
    }

    public void onCreateView(){

    }

    public void onDestroyView() {

    }

    public void onDestroy(){

    }

    protected void showProgressBar() {
        showProgressBar(true);
    }

    protected void showProgressBar(boolean blockTouch) {
        executeBounded(view -> {
            if (mProgressBarEntry == null) {
                mProgressBarEntry = view.showProgressBar(blockTouch);
            }
        });
    }

    protected void hideProgressBar() {
        executeBounded(view -> {
            if (mProgressBarEntry != null) {
                view.hideProgressBar(mProgressBarEntry);
                mProgressBarEntry = null;
            }
        });
    }
}
