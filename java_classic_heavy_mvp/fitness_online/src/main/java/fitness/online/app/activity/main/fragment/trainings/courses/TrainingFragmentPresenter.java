package fitness.online.app.activity.main.fragment.trainings.courses;

import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.handbook.HandbookEntity;
import fitness.online.app.mvp.contract.fragment.TrainingFragmentContract;

/**
 * Created by user on 25.10.17.
 */

public class TrainingFragmentPresenter extends TrainingFragmentContract.Presenter {

    private TrainingCourse mCourse;
    private boolean mTrainer;

    public TrainingFragmentPresenter(TrainingCourse course, boolean trainer) {
        mCourse = course;
        mTrainer = trainer;
    }

    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        loadData();
        if(firstBind){
            //syncHistory();
        }
    }

    private void syncHistory() {
        RetrofitTrainingsDataSource.getInstance().syncHistoryByCourse(mCourse.getId(), new BasicResponseListener() {
            @Override
            public void success(Object response) {
            }
        }, 1);
    }

    @Override
    public void loadData() {

        if (mCourse.isTraining_days_available() || mTrainer) {
            executeBounded(view -> view.bindTrainings(RealmHandbookDataSource.getInstance().getParentCategoryFor(HandbookEntity.EXERCISE)));
        }
        if (mCourse.isDiet_available() || mTrainer) {
            executeBounded(view -> view.bindDiet(RealmHandbookDataSource.getInstance().getParentCategoryFor(HandbookEntity.PRODUCT)));
        }
        if (mCourse.isSport_food_available() || mTrainer) {
            executeBounded(view -> view.bindNutrition(RealmHandbookDataSource.getInstance().getParentCategoryFor(HandbookEntity.SPORT_FOOD)));
        }
        if (mCourse.isPharmacy_available() || mTrainer) {
            executeBounded(view -> view.bindPharm(RealmHandbookDataSource.getInstance().getParentCategoryFor(HandbookEntity.PHARMACY)));
        }
    }
}
