package fitness.online.app.activity.main.fragment.trainings.exercises;

import android.support.annotation.NonNull;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import fitness.online.app.R;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.trainings.DayExercise;
import fitness.online.app.model.pojo.realm.common.trainings.DayExerciseDto;
import fitness.online.app.model.pojo.realm.common.trainings.HistoryRecord;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.contract.fragment.user.ExerciseHistoryFragmentContract;
import fitness.online.app.recycler.data.trainings.ExerciseHistoryData;
import fitness.online.app.recycler.data.trainings.ExerciseHistoryEditData;
import fitness.online.app.recycler.item.ClickListener;
import fitness.online.app.recycler.item.HeaderItem_;
import fitness.online.app.recycler.item.trainings.ExerciseHistoryEditCardioItem;
import fitness.online.app.recycler.item.trainings.ExerciseHistoryEditItem;
import fitness.online.app.recycler.item.trainings.ExerciseHistoryItem;
import fitness.online.app.recycler.item.trainings.ExerciseHistoryTitleItem;

/**
 * Created by user on 31.10.17.
 */

public class ExerciseHistoryFragmentPresenter extends ExerciseHistoryFragmentContract.Presenter implements ClickListener<HistoryRecord> {

    private DayExercise mExercise;

    private TrainingCourse mCourse;

    private ExerciseHistoryEditItem mEditItem;
    private ExerciseHistoryEditCardioItem mCardioItem;

    private HandbookExercise mHandbookExercise;

    private boolean mInput;

    public ExerciseHistoryFragmentPresenter(DayExercise exercise, TrainingCourse course, boolean input) {
        this.mExercise = exercise;
        this.mCourse = course;
        mInput = input;
    }

    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        if (firstBind) {
            sync();
            loadData();
            RealmTrainingsDataSource.getInstance().subscribeHistory(historyRecords -> loadData());
        }
    }

    @Override
    public void onPlayClicked(int exerciseId) {
        HandbookNavigation navigation = new HandbookNavigation();
        navigation.setId(mHandbookExercise.getId());
        executeBounded(view -> view.openExerciseInfo(navigation));
    }

    @Override
    public void loadData() {
        mHandbookExercise = RealmHandbookDataSource.getInstance().getExerciseById(mExercise.getPost_exercise_id() + "");
        RealmTrainingsDataSource.getInstance().getHistoryByPostExerciseId(mCourse.getId(), mExercise.getPost_exercise_id()).subscribe(this::bindData);
    }

    @Override
    public void sync() {
        showProgressBar(false);
        RetrofitTrainingsDataSource.getInstance().syncHistory(mCourse.getId(), mExercise.getPost_exercise_id(), new BasicResponseListener() {
            @Override
            public void success(Object response) {
                loadData();
                hideProgressBar();
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                hideProgressBar();
            }
        });
    }

    private void bindData(List<HistoryRecord> data) {
        executeBounded(view -> view.dataReplace(prepareData(data)));
    }

    private ExerciseHistoryEditData.Listener editListener = new ExerciseHistoryEditData.Listener() {
        @Override
        public void onInputRecord(int repeat, String weight, String comment) {
            newRecord(repeat, weight, comment);
        }

        @Override
        public void onError(int errorId) {
            executeBounded(view -> view.error(errorId));
        }
    };

    private List<BaseItem> prepareData(List<HistoryRecord> completed_sets) {
        List<BaseItem> items = new ArrayList<>();
        HandbookExercise handbookExercise = RealmHandbookDataSource.getInstance().getExerciseById(mExercise.getPost_exercise_id() + "");
        items.add(new ExerciseHistoryTitleItem(new ExerciseHistoryEditData(new DayExerciseDto(mExercise), mHandbookExercise, null)));
        if (mInput) {
            if (HandbookExercise.CARDI0.equals(mHandbookExercise.getExercise_type())) {
                if (mCardioItem == null) {
                    mCardioItem = new ExerciseHistoryEditCardioItem(new ExerciseHistoryEditData(new DayExerciseDto(mExercise), handbookExercise, editListener));
                }
                items.add(mCardioItem);
            } else {
                if (mEditItem == null) {
                    mEditItem = new ExerciseHistoryEditItem(new ExerciseHistoryEditData(new DayExerciseDto(mExercise), handbookExercise, editListener));
                }
                items.add(mEditItem);
            }
        }
        if (completed_sets != null && completed_sets.size() != 0) {
            items.add(new HeaderItem_(R.string.header_exercise_history));
        }
        String prevHeader = "";
        int number = 1;
        for (HistoryRecord completed_set : completed_sets) {
            boolean showHeader;
            String curHeader = completed_set.getExecutedDate();
            if (curHeader.equals(prevHeader)) {
                showHeader = false;
                number--;
            } else {
                showHeader = true;
                number = countByDate(completed_sets, curHeader);
            }
            prevHeader = curHeader;
            items.add(new ExerciseHistoryItem(new ExerciseHistoryData(completed_set, showHeader, curHeader, number, this, deleteListener)));
        }
        return items;
    }

    private ClickListener<HistoryRecord> deleteListener = new ClickListener<HistoryRecord>() {
        @Override
        public void onClick(HistoryRecord data) {
            if (data.isToday()) {
                ExerciseHistoryFragmentPresenter.this.executeBounded(view -> view.askToDelete(data));
            }
        }
    };

    private int countByDate(List<HistoryRecord> completed_sets, String curHeader) {
        int result = 0;
        for (HistoryRecord record : completed_sets) {
            if (record.getExecutedDate().equals(curHeader)) {
                result++;
            }
        }
        return result;
    }


    @Override
    public void newRecord(int repeats, String value, String comment) {
        RealmTrainingsDataSource.getInstance().createHistoryRecord(mCourse.getId(), mExercise.getPost_exercise_id(), mExercise.getId(), repeats, value, comment, HandbookExercise.CARDI0.equals(mHandbookExercise.getExercise_type()) ? 2 : 0)
                .subscribe(() -> {
                    loadData();
                    executeBounded(view -> view.historyUpSync());
                });
    }

    @Override
    public void deleteRecord(HistoryRecord data) {
        if (data.getId() < 0) {
            RealmTrainingsDataSource.getInstance().deleteHistoryRecord(data.getId());
            deleteResponseListener.success(null);
        } else {
            showProgressBar(true);
            RetrofitTrainingsDataSource.getInstance().deleteHistoryRecord(mCourse.getId(), data.getId(), deleteResponseListener);
        }
    }

    private BasicResponseListener<Object> deleteResponseListener = new BasicResponseListener<Object>() {
        @Override
        public void success(Object response) {
            loadData();
            hideProgressBar();
        }

        @Override
        public void error(@NonNull Throwable throwable) {
            super.error(throwable);
            hideProgressBar();
        }
    };

    @Override
    public void onClick(HistoryRecord data) {
        if (mEditItem != null) {
            mEditItem.getData().setRecord(data);
        }
        if (mCardioItem != null) {
            mCardioItem.getData().setRecord(data);
        }
        executeBounded(view -> view.updateList());
    }
}
