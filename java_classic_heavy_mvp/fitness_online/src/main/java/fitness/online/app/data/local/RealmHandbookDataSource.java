package fitness.online.app.data.local;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import fitness.online.app.model.pojo.realm.common.NewComplain;
import fitness.online.app.model.pojo.realm.handbook.Handbook;
import fitness.online.app.model.pojo.realm.handbook.HandbookCategory;
import fitness.online.app.model.pojo.realm.handbook.HandbookEntity;
import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.model.pojo.realm.handbook.HandbookFaq;
import fitness.online.app.model.pojo.realm.handbook.HandbookFilter;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.model.pojo.realm.handbook.HandbookPharmacy;
import fitness.online.app.model.pojo.realm.handbook.HandbookProduct;
import fitness.online.app.model.pojo.realm.handbook.HandbookResponse;
import fitness.online.app.model.pojo.realm.handbook.HandbookSportfood;
import fitness.online.app.util.realm.RealmHelper;
import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.Sort;
import timber.log.Timber;

/**
 * Created by user on 05.10.17.
 */


public class RealmHandbookDataSource {
    private Realm mRealm = RealmHelper.getHandbookInstance();


    private boolean mFilterEnabled = false;

    public boolean isFilterEnabled() {
        return mFilterEnabled;
    }

    public void enableFilter(HandbookFilter filter, boolean enabled) {
        mRealm.beginTransaction();
        filter.setEnabled(enabled);
        mRealm.commitTransaction();
    }

    public void changeFilter(HandbookFilter filter, int min, int max) {
        mRealm.beginTransaction();
        filter.setValues(min, max);
        mRealm.commitTransaction();
    }

    public List<HandbookExercise> listExercises() {
        return mRealm.where(HandbookExercise.class).findAll();
    }

    public void enableFilter(boolean enabled) {
        mFilterEnabled = enabled;
    }

    public String getExerciseParentCategoryId() {
        try {
            return mRealm.where(HandbookCategory.class).isNull("parent_id").equalTo("post_type", HandbookEntity.EXERCISE).findFirst().getCategory_id();
        } catch (Exception ex) {
            return null;
        }
    }

    public String getNutritionParentCategoryId() {
        try {
            return mRealm.where(HandbookCategory.class).isNull("parent_id").equalTo("post_type", HandbookEntity.SPORT_FOOD).findFirst().getCategory_id();
        } catch (Exception ex) {
            return null;
        }
    }

    public String getProductParentCategoryId() {
        try {
            return mRealm.where(HandbookCategory.class).isNull("parent_id").equalTo("post_type", HandbookEntity.PRODUCT).findFirst().getCategory_id();
        } catch (Exception ex) {
            return null;
        }
    }

    public HandbookCategory getParentCategoryFor(String type) {
        return mRealm.where(HandbookCategory.class).isNull("parent_id").equalTo("post_type", type).findFirst();
    }


    public static class INSTANCE_HOLDER {
        public static final RealmHandbookDataSource INSTANCE = new RealmHandbookDataSource();
    }

    public static RealmHandbookDataSource getInstance() {
        return RealmHandbookDataSource.INSTANCE_HOLDER.INSTANCE;
    }

    @Nullable
    public Handbook getCurrentHandbook() {
        return mRealm.where(Handbook.class).findFirst();
    }


    private List<HandbookProduct> filter(HandbookCategory category) {
        RealmQuery<HandbookProduct> query = mRealm.where(HandbookProduct.class);
        if (category.getParent_id() != null) {
            query = query.equalTo("category_id", category.getCategory_id());
        }
        for (HandbookFilter filter : getFilter()) {
            if (filter.isEnabled()) {
                query = query.between(filter.getKey(), filter.getMinValue() * 1.d, filter.getMaxValue() * 1.d);
            }
        }
        return query.findAllSorted("title");
    }


    public List<HandbookNavigation> listDataSimple(String categoryId) {
        List<HandbookEntity> entities = new ArrayList<>();
        if (mFilterEnabled) {
            HandbookCategory curCategory = getCategoryById(categoryId);
            if (curCategory != null && curCategory.getPost_type().equals(HandbookEntity.PRODUCT)) {
                entities.addAll(filter(curCategory));
                return fromEntity(entities);
            }
        }
        List<HandbookCategory> categories = mRealm.where(HandbookCategory.class).equalTo("parent_id", categoryId).findAllSorted("weight", Sort.ASCENDING, "title", Sort.ASCENDING);
        if (categories != null && categories.size() > 0) {
            entities.addAll(categories);
            return fromEntity(entities);
        }
        HandbookCategory category = getCategoryById(categoryId);
        if (category != null) {
            if (category.getPost_type().equals(HandbookEntity.EXERCISE)) {
                entities.addAll(mRealm.where(HandbookExercise.class).equalTo("category_id", categoryId).findAllSorted("title"));
            }
            if (category.getPost_type().equals(HandbookEntity.PRODUCT)) {
                entities.addAll(mRealm.where(HandbookProduct.class).equalTo("category_id", categoryId).findAllSorted("title"));
            }
            if (category.getPost_type().equals(HandbookEntity.PHARMACY)) {
                entities.addAll(mRealm.where(HandbookPharmacy.class).equalTo("category_id", categoryId).findAllSorted("title"));
            }
            if (category.getPost_type().equals(HandbookEntity.SPORT_FOOD)) {
                entities.addAll(mRealm.where(HandbookSportfood.class).equalTo("category_id", categoryId).findAllSorted("title"));
            }
            if (category.getPost_type().equals(HandbookEntity.FAQ)) {
                entities.addAll(mRealm.where(HandbookFaq.class).equalTo("category_id", categoryId).findAllSorted("title"));
            }
        }
        return fromEntity(entities);
    }

    public Observable<List<HandbookNavigation>> listData(String categoryId) {
        List<HandbookNavigation> items = listDataSimple(categoryId);
        if (items == null || items.size() == 0) {
            return Observable.empty();
        } else {
            return Observable.just(items);
        }
    }

    private List<HandbookNavigation> fromEntity(List<HandbookEntity> entities) {
        ArrayList<HandbookNavigation> result = new ArrayList<>();
        for (HandbookEntity entity : entities) {
            HandbookNavigation item = new HandbookNavigation();
            item.setTitle(entity.getTitle());
            item.setPhoto_url(entity.getPhoto_url());
            item.setPhoto_ext(entity.getPhoto_ext());
            item.setId(entity.getId());
            item.setType(entity.getType());
            if (entity instanceof HandbookProduct) {
                item.setSub_text(((HandbookProduct) entity).getCaloriesFormatted());
            }
            result.add(item);
        }
        return result;
    }

    @Nullable
    public HandbookCategory getCategoryById(String categoryId) {
        return mRealm.where(HandbookCategory.class).equalTo("id", categoryId).findFirst();
    }

    public HandbookEntity getPostByIdAndType(String id, String type) {
        HandbookEntity entity = null;
        switch (type) {
            case HandbookEntity.FAQ:
                entity = mRealm.where(HandbookFaq.class).equalTo("id", id).findFirst();
                break;
            case HandbookEntity.PHARMACY:
                entity = mRealm.where(HandbookPharmacy.class).equalTo("id", id).findFirst();
                break;
            case HandbookEntity.SPORT_FOOD:
                entity = mRealm.where(HandbookSportfood.class).equalTo("id", id).findFirst();
                break;
        }
        return entity;
    }

    public HandbookExercise getExerciseById(String id) {
        return mRealm.where(HandbookExercise.class).equalTo("id", id).findFirst();
    }

    public HandbookProduct getProductById(String id) {
        return mRealm.where(HandbookProduct.class).equalTo("id", id).findFirst();
    }

    public HandbookSportfood getNutritionById(String id) {
        return mRealm.where(HandbookSportfood.class).equalTo("id", id).findFirst();
    }

    public List<HandbookFilter> getFilter() {
        List<HandbookFilter> filter = mRealm.where(HandbookFilter.class).findAll();
        if (filter == null || filter.size() == 0) {
            filter = defaultFilter();
        }
        return filter;
    }

    public List<HandbookFilter> defaultFilter() {
        try {
            mRealm.beginTransaction();
            mRealm.where(HandbookFilter.class).findAll().deleteAllFromRealm();
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }

        List<HandbookFilter> filterItems = new ArrayList<>();
        filterItems.add(new HandbookFilter(1, "calories", "Калории, ккал", getMaxValue("calories"), true));
        filterItems.add(new HandbookFilter(2, "proteins", "Белки, г", getMaxValue("proteins"), false));
        filterItems.add(new HandbookFilter(3, "carbohydrates", "Углеводы, г", getMaxValue("carbohydrates"), false));
        filterItems.add(new HandbookFilter(4, "fats", "Жиры, г", getMaxValue("fats"), false));
        filterItems.add(new HandbookFilter(5, "saturated_fats", "Насыщенные жиры, г", getMaxValue("saturated_fats"), false));
        filterItems.add(new HandbookFilter(6, "cellulose", "Клетчатка", getMaxValue("cellulose"), false));
        filterItems.add(new HandbookFilter(7, "glycemia_index", "Индекс гликемии", getMaxValue("glycemia_index"), false));

        try {
            mRealm.beginTransaction();
            mRealm.copyToRealmOrUpdate(filterItems);
        } catch (Throwable throwable) {
            Timber.e(throwable);
        } finally {
            if (mRealm.isInTransaction()) {
                mRealm.commitTransaction();
            }
        }

        return filterItems;
    }

    private int getMaxValue(String key) {
        return (mRealm.where(HandbookProduct.class).max(key).intValue());
    }


    public void store(HandbookResponse handbookResponse) {
        if (handbookResponse.getHandbook() != null) {
            Handbook newHandbook = handbookResponse.getHandbook();
            if (getCurrentHandbook() == null) {
                try {
                    mRealm.beginTransaction();
                    mRealm.insertOrUpdate(newHandbook);
                } catch (Throwable throwable) {
                    Timber.e(throwable);
                } finally {
                    if (mRealm.isInTransaction()) {
                        mRealm.commitTransaction();
                    }
                }
            } else {
                if (getCurrentHandbook().getVersion() != handbookResponse.getHandbook().getVersion()) {
                    try {
                        mRealm.beginTransaction();
                        getCurrentHandbook().setVersion(newHandbook.getVersion());
                        mRealm.copyToRealmOrUpdate(newHandbook.getCategories());
                        mRealm.copyToRealmOrUpdate(newHandbook.getPost_exercises());
                        mRealm.copyToRealmOrUpdate(newHandbook.getPost_faqs());
                        mRealm.copyToRealmOrUpdate(newHandbook.getPost_pharmacies());
                        mRealm.copyToRealmOrUpdate(newHandbook.getPost_sport_foods());
                        mRealm.copyToRealmOrUpdate(newHandbook.getPost_products());
                    } catch (Throwable throwable) {
                        Timber.e(throwable);
                    } finally {
                        if (mRealm.isInTransaction()) {
                            mRealm.commitTransaction();
                        }
                    }
                }
            }
        }
    }
}
