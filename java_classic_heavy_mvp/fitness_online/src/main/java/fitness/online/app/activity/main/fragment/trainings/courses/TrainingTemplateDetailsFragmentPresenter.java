package fitness.online.app.activity.main.fragment.trainings;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import fitness.online.app.App;
import fitness.online.app.R;
import fitness.online.app.billing.BillingManager;
import fitness.online.app.billing.PurchaseListener;
import fitness.online.app.data.local.RealmBillingDataSource;
import fitness.online.app.data.local.RealmSessionDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitTrainingsDataSource;
import fitness.online.app.model.RetrofitException;
import fitness.online.app.model.pojo.realm.common.billing.PurchaseData;
import fitness.online.app.model.pojo.realm.common.billing.SkuData;
import fitness.online.app.model.pojo.realm.common.social.SocialToken;
import fitness.online.app.model.pojo.realm.common.trainings.CoursesResponse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingTemplate;
import fitness.online.app.mvp.contract.fragment.TrainingTemplateDetailsFragmentContract;

/**
 * Created by user on 26.10.17.
 */

public class TrainingTemplateDetailsFragmentPresenter extends TrainingTemplateDetailsFragmentContract.Presenter {

    private TrainingTemplate mTemplate;

    private SkuData mSkuData;

    public TrainingTemplateDetailsFragmentPresenter(TrainingTemplate template) {
        mTemplate = template;
    }

    PurchaseListener mPurchaseListener = new PurchaseListener() {

        @Override
        public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
            if (responseCode == BillingClient.BillingResponse.OK && purchases != null) {
                for (Purchase purchase : purchases) {
                    if (purchase.getSku().equals(mTemplate.getAndroidInapId())) {
                        createCourse(mTemplate, purchase.getOriginalJson());
                    }
                }
            } else {
                executeBounded(view -> {
                    hideProgressBar();
                    view.setButtonEnabled(true);
                });
            }
        }

        @Override
        public void error(@Nonnull String skuId, int error) {
            executeBounded(view -> {
                hideProgressBar();
                view.setButtonEnabled(true);
                view.showError(new Throwable(App.getContext().getString(R.string.error_purchase)));
            });
        }

        @Override
        public void alreadyOwned(@Nonnull String skuId) {
            BillingManager.getInstance().connect(new BillingManager.ConnectListener() {
                @Override
                public void connected(@NonNull BillingManager billingManager) {
                    billingManager.queryPurchases(new BillingManager.QueryPurchasesListener() {
                        @Override
                        public void success(@NonNull Purchase.PurchasesResult purchasesResult) {
                            if (purchasesResult.getPurchasesList() != null) {
                                boolean purchased = false;
                                for (Purchase purchase : purchasesResult.getPurchasesList()) {
                                    if (purchase.getSku().equals(mTemplate.getAndroidInapId())) {
                                        createCourse(mTemplate, purchase.getOriginalJson());
                                        purchased = true;
                                        break;
                                    }
                                }
                                if (!purchased) {
                                    executeBounded(view -> {
                                        hideProgressBar();
                                        view.setButtonEnabled(true);
                                        view.showError(new Throwable(App.getContext().getString(R.string.already_owned_purchase)));
                                    });
                                }
                            }
                        }

                        @Override
                        public void error(@NonNull Purchase.PurchasesResult purchasesResult) {
                            executeBounded(view -> {
                                hideProgressBar();
                                view.setButtonEnabled(true);
                                view.showError(new Throwable(App.getContext().getString(R.string.already_owned_purchase)));
                            });
                        }
                    });
                }

                @Override
                public void disconnected(@NonNull BillingManager billingManager) {
                    executeBounded(view -> {
                        hideProgressBar();
                        view.setButtonEnabled(true);
                        view.showError(new Throwable(App.getContext().getString(R.string.already_owned_purchase)));
                    });
                }
            });
        }
    };


    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        if (firstBind) {
            executeBounded(view -> view.bind(mTemplate));
            if (isInap()) {
                loadPriceRemote();
            }
        }
        if (isInap()) {
            loadPriceLocal();
        }
        BillingManager.addPurchaseListener(mPurchaseListener);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        BillingManager.removePurchaseListener(mPurchaseListener);
    }

    private boolean isInap() {
        return mTemplate.isInap();
    }

    private void loadPriceLocal() {
        String inapId = mTemplate.getAndroidInapId();
        if (!TextUtils.isEmpty(inapId)) {
            PurchaseData purchaseData = RealmBillingDataSource.getInstance().getPurchase(inapId);
            if (purchaseData != null) {
                executeBounded(view -> {
                    view.updateButtonText(App.getContext().getString(R.string.create));
                    view.setButtonEnabled(true);
                });
            } else {
                SkuData skuData = RealmBillingDataSource.getInstance()
                        .getSkuData(inapId);
                if (skuData != null) {
                    executeBounded(view -> {
                        view.updateButtonText(skuData.getPrice());
                        view.setButtonEnabled(true);
                    });
                }
            }
        }
    }


    private void loadPriceRemote() {
        BillingManager.getInstance().connect(new BillingManager.ConnectListener() {
            @Override
            public void connected(@NonNull BillingManager billingManager) {
                billingManager.queryPurchases(new BillingManager.QueryPurchasesListener() {
                    @Override
                    public void success(@NonNull Purchase.PurchasesResult purchasesResult) {
                        List<PurchaseData> purchaseDatas = new ArrayList<>();
                        boolean purchased = false;
                        if (purchasesResult.getPurchasesList() != null) {
                            for (Purchase purchase : purchasesResult.getPurchasesList()) {
                                if (purchase.getSku().equals(mTemplate.getAndroidInapId())) {
                                    purchased = true;
                                }
                                purchaseDatas.add(new PurchaseData(
                                        purchase.getSku(),
                                        purchase.getPurchaseToken(),
                                        purchase.getOriginalJson()
                                ));
                            }
                        }
                        RealmBillingDataSource.getInstance().updatePurchases(purchaseDatas);
                        if (purchased) {
                            executeBounded(view -> {
                                view.updateButtonText(App.getContext().getString(R.string.create));
                                view.setButtonEnabled(true);
                            });
                        } else {
                            querySkuDetails(billingManager);
                        }
                    }

                    @Override
                    public void error(@NonNull Purchase.PurchasesResult purchasesResult) {
                        querySkuDetails(billingManager);
                    }
                });
            }

            @Override
            public void disconnected(@NonNull BillingManager billingManager) {
                //do nothing
            }
        });
    }

    private void querySkuDetails(@Nonnull BillingManager billingManager) {
        List<String> skuList = new ArrayList<>();
        skuList.add(mTemplate.getAndroidInapId());
        billingManager.querySkuDetailsAsync(BillingClient.SkuType.INAPP, skuList, new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {
                if (responseCode == BillingClient.BillingResponse.OK) {
                    if (skuDetailsList != null) {
                        for (SkuDetails skuDetail : skuDetailsList) {
                            SkuData skuData = new SkuData(
                                    skuDetail.getSku(),
                                    skuDetail.getDescription(),
                                    skuDetail.getPrice(),
                                    skuDetail.getPriceCurrencyCode(),
                                    skuDetail.getTitle(),
                                    skuDetail.getType()
                            );
                            RealmBillingDataSource.getInstance().saveSkuData(skuData);
                            if (skuData.getSku().equals(mTemplate.getAndroidInapId())) {
                                mSkuData = skuData;
                                executeBounded(view -> {
                                    view.updateButtonText(skuData.getPrice());
                                    view.setButtonEnabled(true);
                                });
                            }
                        }
                    }
                } else {
                    executeBounded(errorView -> errorView.showError(new Throwable(App.getContext().getString(R.string.error_loading_inap_prices))));
                }
            }
        });
    }

    @Override
    public void enroll(TrainingTemplate template) {
        if (isInap()) {
            executeBounded(view -> {
                showProgressBar();
                view.setButtonEnabled(false);
                String inapId = mTemplate.getAndroidInapId();
                if (!TextUtils.isEmpty(inapId)) {
                    PurchaseData purchaseData = RealmBillingDataSource.getInstance().getPurchase(inapId);
                    if (purchaseData != null) {
                        createCourse(mTemplate, purchaseData.getOriginalJson());
                    } else {
                        if (mSkuData != null) {
                            view.purchase(mSkuData);
                        } else {
                            view.showError(new Throwable(App.getContext().getString(R.string.try_later)));
                        }
                    }
                }
            });
        } else if (template.isAvailable_with_repost()) {
            createCourseRepost(template);
        } else {
            createCourse(template, null);
        }
    }

    private void createCourseRepost(@Nonnull TrainingTemplate template) {
        SocialToken socialToken = RealmSessionDataSource.getInstance().getSocialToken();
        if (socialToken != null) {
            createCourseRepost(template, socialToken);
        } else {
            showProgressBar();
            executeBounded(TrainingTemplateDetailsFragmentContract.View::loginVk);
        }
    }

    private void createCourseRepost(@Nonnull TrainingTemplate template, @NonNull SocialToken socialToken) {
        showProgressBar();
        RetrofitTrainingsDataSource.getInstance().createCourseRepost(
                template,
                socialToken,
                new BasicResponseListener<CoursesResponse>() {
                    @Override
                    public void success(CoursesResponse response) {
                        successCoursesResponse(response);
                    }

                    @Override
                    public void error(@NonNull Throwable throwable) {
                        if (throwable instanceof RetrofitException) {
                            RetrofitException retrofitException = (RetrofitException) throwable;
                            if (retrofitException.getResponse().code() == 500) {
                                RealmSessionDataSource.getInstance().deleteSocialToken();
                                executeBounded(TrainingTemplateDetailsFragmentContract.View::loginVk);
                                return;
                            }
                        }
                        errorCoursesResponse(throwable);
                    }
                });
    }

    private void createCourse(@Nonnull TrainingTemplate template, @Nullable String purchaseJson) {
        showProgressBar();
        RetrofitTrainingsDataSource.getInstance().createCourse(
                template,
                purchaseJson,
                new BasicResponseListener<CoursesResponse>() {
                    @Override
                    public void success(CoursesResponse response) {
                        successCoursesResponse(response);
                    }

                    @Override
                    public void error(@NonNull Throwable throwable) {
                        errorCoursesResponse(throwable);
                    }
                });
    }

    private void successCoursesResponse(@NonNull CoursesResponse response) {
        RealmTrainingsDataSource.getInstance().storeMyTrainings(response);
        hideProgressBar();
        executeBounded(TrainingTemplateDetailsFragmentContract.View::onEnrollSuccess);
    }

    private void errorCoursesResponse(@NonNull Throwable throwable) {
        hideProgressBar();
        executeBounded(view -> {
            view.showError(throwable);
            view.setButtonEnabled(true);
        });
    }

    @Override
    public void errorPurchaseConnected() {
        executeBounded(view -> {
            hideProgressBar();
            view.setButtonEnabled(true);
        });
        executeBounded(errorView -> errorView.showError(new Throwable(App.getContext().getString(R.string.error_purchase))));
    }

    @Override
    public void loginVkSuccess() {
        createCourseRepost(mTemplate);
    }

    @Override
    public void loginVkError(@NonNull Throwable throwable) {
        errorCoursesResponse(throwable);
    }
}
