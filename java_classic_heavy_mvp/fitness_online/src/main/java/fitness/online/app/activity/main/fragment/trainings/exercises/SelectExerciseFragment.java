package fitness.online.app.activity.main.fragment.trainings.exercises;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.activity.main.fragment.handbook.HandbookExerciseFragment;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.model.pojo.realm.handbook.HandbookCategory;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.SelectExerciseFragmentContract;
import fitness.online.app.recycler.adapter.UniversalAdapter;

/**
 * Created by user on 04.10.17.
 */

public class SelectExerciseFragment extends BaseFragment<SelectExerciseFragmentPresenter> implements SelectExerciseFragmentContract.View {


    private HandbookCategory mCategory;

    private static final String EXTRA_INPUT = "navigation_id";

    @BindView(R.id.recycler_view)
    public RecyclerView mRecyclerView;

    private ArrayList<BaseItem> mList = new ArrayList();

    private UniversalAdapter mAdapter;

    @Override
    public int getLayout() {
        return R.layout.fragment_handbook;
    }

    @NonNull
    public static SelectExerciseFragment newInstance(@Nullable String category) {
        SelectExerciseFragment fragment = new SelectExerciseFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_INPUT, category);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String catId = getArguments().getString(EXTRA_INPUT);
        mCategory = RealmHandbookDataSource.getInstance().getCategoryById(catId);
        mPresenter = new SelectExerciseFragmentPresenter(catId);
    }

    @Nullable
    @Override
    public String getTitle() {
        return mCategory == null ? getString(R.string.ttl_select_exercise) : mCategory.getTitle();
    }

    @Override
    public int getHomeIconId() {
        if (mCategory == null) {
            return R.drawable.ic_actionbar_close;
        } else {
            return super.getHomeIconId();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mAdapter = new UniversalAdapter(mList, 0);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }


    @Override
    public int getMenuId() {
        if (mCategory == null) {
            return R.menu.empty_menu;
        } else {
            return R.menu.create_training;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ok:
                mPresenter.confirmSelected();
                getFragNavController().popFragments(2);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void openExercise(HandbookNavigation item) {
        getFragNavController().pushFragment(HandbookExerciseFragment.newInstance(item, true));
    }

    @Override
    public void dataReplace(List<BaseItem> data) {
        mAdapter.replaceList(data);
    }

    @Override
    public void onCategorySelect(String categoryId) {
        getFragNavController().pushFragment(SelectExerciseFragment.newInstance(categoryId));
    }
}
