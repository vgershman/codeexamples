package fitness.online.app.mvp;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import fitness.online.app.util.DialogHelper;
import fitness.online.app.view.progressBar.ProgressBarEntry;

public interface FragmentView extends MvpView {
    void showDialog(String title, String text);
    void showDialog(String title, String text, DialogInterface.OnClickListener ok, DialogInterface.OnClickListener cancel);
    void showEditTextDialog(@NonNull String title,
                            @Nullable String text,
                            @Nullable String hint,
                            int maxLines,
                            int inputType,
                            @NonNull DialogHelper.EditTextDialogListener listener);
    ProgressBarEntry showProgressBar(boolean blockTouch);
    void hideProgressBar(ProgressBarEntry entry);
    void updateFab();
    void fabClicked();
    void showError(Throwable throwable);
}
