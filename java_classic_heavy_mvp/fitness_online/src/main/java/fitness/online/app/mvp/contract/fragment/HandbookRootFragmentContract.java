package fitness.online.app.mvp.contract.fragment;

import com.trimf.recycler.item.BaseItem;

import java.util.List;

import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragmentPresenter;
import fitness.online.app.mvp.FragmentView;

/**
 * Created by user on 04.10.17.
 */

public interface HandbookRootFragmentContract {

    interface View extends FragmentView {

        void dataReplace(List<BaseItem> data);

        void onNavigationSelect(HandbookNavigation handbookNavigation);

        void setRefreshing(boolean refreshing);

        void setupCacher();
    }

    abstract class Presenter extends BaseFragmentPresenter<HandbookRootFragmentContract.View> {

        public abstract void loadData();

        public abstract void sync();

        public abstract void loadDataLocalSuccess(List<HandbookNavigation> data);
    }
}
