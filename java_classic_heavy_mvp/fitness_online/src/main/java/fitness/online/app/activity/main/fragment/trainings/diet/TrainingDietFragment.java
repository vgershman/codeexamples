package fitness.online.app.activity.main.fragment.trainings.diet;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.trimf.recycler.item.BaseItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.activity.main.fragment.handbook.HandbookProductFragment;
import fitness.online.app.activity.main.fragment.handbook.HandbookRootFragment;
import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.local.RealmSessionDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.diet.Meal;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.model.pojo.realm.handbook.HandbookEntity;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.TrainingFragmentDietContract;
import fitness.online.app.recycler.adapter.UniversalAdapter;
import fitness.online.app.recycler.data.diet.MealProductData;
import fitness.online.app.view.time.TimePickerDialogFragment;

/**
 * Created by user on 25.10.17.
 */

public class TrainingDietFragment extends BaseFragment<TrainingDietFragmentPresenter> implements TrainingFragmentDietContract.View {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;


    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    private UniversalAdapter mAdapter;

    private List<BaseItem> mList = new ArrayList<>();

    private TrainingCourse mCourse;


    @Override
    public void bind() {
    }

    @Override
    public void dataReplace(List<BaseItem> items) {
        mList = items;
        mAdapter.replaceList(mList);
    }

    @Nullable
    @Override
    public String getTitle() {
        return getString(R.string.ttl_diet);
    }

    @Override
    public int getMenuId() {
        if (mCourse.getInvoice_id() == null || RealmSessionDataSource.getInstance().isTrainer()) {
            if (mPresenter.isEditMode()) {
                return R.menu.training_day_edit;
            } else {
                return R.menu.training_day_view;
            }
        } else {
            return R.menu.empty_menu;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_edit) {
            mPresenter.edit();
        }
        if (item.getItemId() == R.id.action_confirm) {
            mPresenter.confirm();
            hideSoftKeyboard();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void bindState(boolean isEdit) {
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void selectProduct() {
        HandbookNavigation navigation = new HandbookNavigation();
        navigation.setType(HandbookEntity.CATEGORY);
        navigation.setId(RealmHandbookDataSource.getInstance().getProductParentCategoryId());
        getFragNavController().pushFragment(HandbookRootFragment.newInstance(navigation, true, mCourse.getInvoice_id() != null && !RealmSessionDataSource.getInstance().isTrainer()));
    }

    @Override
    public void askToDelete(Meal data) {
        new AlertDialog.Builder(getContext())
                .setMessage(R.string.alert_delete_meal)
                .setPositiveButton(R.string.alert_yes, (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    mPresenter.deleteMeal(data);
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel())
                .create().show();
    }

    @Override
    public void askToDeleteProduct(MealProductData data) {
        new AlertDialog.Builder(getContext())
                .setMessage(R.string.alert_delete_product)
                .setPositiveButton(R.string.alert_yes, (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    mPresenter.deleteProduct(data);
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel())
                .create().show();
    }

    @Override
    public void openProduct(MealProductData data) {
        HandbookNavigation navigation = new HandbookNavigation();
        navigation.setId(data.getProduct().getPost_product_id() + "");
        navigation.setType(HandbookEntity.PRODUCT);
        getFragNavController().pushFragment(HandbookProductFragment.newInstance(navigation, true, mCourse.getInvoice_id() != null && !RealmSessionDataSource.getInstance().isTrainer()));
    }

    @Override
    public void selectTime(Meal data) {
        TimePickerDialogFragment dialog = new TimePickerDialogFragment();
        Bundle args = new Bundle();
        try {
            args.putSerializable("date", timeFormat.parse(data.getTime()));
        } catch (Exception ex) {
        }
        dialog.setArguments(args);
        dialog.setListener(date -> mPresenter.setMealTime(data, timeFormat.format(date)));
        dialog.show(getChildFragmentManager(), "timePicker");
    }


    @Override
    public int getLayout() {
        return R.layout.fragment_diet;
    }

    public static TrainingDietFragment newInstance(TrainingCourse training) {
        TrainingDietFragment trainingFragment = new TrainingDietFragment();
        Bundle args = new Bundle();
        args.putInt("course", training.getId());
        trainingFragment.setArguments(args);
        return trainingFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCourse = RealmTrainingsDataSource.getInstance().getCourseById(getArguments().getInt("course"));
        mPresenter = new TrainingDietFragmentPresenter(mCourse);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new UniversalAdapter(mList, 0);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
    }
}
