package fitness.online.app.activity.main.fragment.trainings.courses;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.facebook.drawee.view.SimpleDraweeView;

import javax.annotation.Nonnull;

import butterknife.BindView;
import fitness.online.app.R;
import fitness.online.app.activity.login.AuthFascade;
import fitness.online.app.activity.main.fragment.trainings.TrainingTemplateDetailsFragmentPresenter;
import fitness.online.app.billing.BillingManager;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.pojo.realm.common.billing.SkuData;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingTemplate;
import fitness.online.app.mvp.BaseFragment;
import fitness.online.app.mvp.contract.fragment.TrainingTemplateDetailsFragmentContract;
import fitness.online.app.util.ImageHelper;

/**
 * Created by user on 26.10.17.
 */

public class TrainingTemplateDetailsFragment extends BaseFragment<TrainingTemplateDetailsFragmentPresenter> implements TrainingTemplateDetailsFragmentContract.View {

    @BindView(R.id.tv_description)
    TextView description;

    @BindView(R.id.tv_title)
    TextView title;

    @BindView(R.id.tv_author)
    TextView author;

    @BindView(R.id.tv_type)
    TextView type;

    @BindView(R.id.iv_avatar)
    SimpleDraweeView image;

    @BindView(R.id.btn_enroll)
    TextView enroll;

    @BindView(R.id.llAuthor)
    View authorContainer;

    @BindView(R.id.contents)
    TextView contents;

    @BindView(R.id.price_progressbar)
    View mPriceProgressBar;

    private TrainingTemplate mTemplate;

    AuthFascade authFascade = new AuthFascade();

    private static final String EXTRA_INPUT = "extra";

    public static TrainingTemplateDetailsFragment newInstance(TrainingTemplate template) {
        TrainingTemplateDetailsFragment fragment = new TrainingTemplateDetailsFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(EXTRA_INPUT, template.getId());
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTemplate = RealmTrainingsDataSource.getInstance().getTemplateById(getArguments().getInt(EXTRA_INPUT));
        mPresenter = new TrainingTemplateDetailsFragmentPresenter(mTemplate);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        enroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.enroll(mTemplate);
            }
        });
    }

    @Nullable
    @Override
    public String getTitle() { //todo not sure
        if (mTemplate.getType().equals("free")) {
            return getString(R.string.free_training);
        }
        if (mTemplate.getType().equals("author")) {
            return getString(R.string.author_training);
        }
        return mTemplate.getTitle();
    }

    @Override
    public void bind(TrainingTemplate template) {
        if (template == null) {
            Toast.makeText(getContext(), R.string.err_unexpected, Toast.LENGTH_SHORT).show();
            getFragNavController().popFragment();
            return; //todo wtf?
        }
        ImageHelper.loadImage(image, template.getFull_photo_url(), template.getFull_photo_ext());
        title.setText(template.getTitle());
        description.setText(template.getDescription());
        if (!TextUtils.isEmpty(template.getAuthor())) {
            authorContainer.setVisibility(View.VISIBLE);
            author.setText(template.getAuthor());
        } else {
            authorContainer.setVisibility(View.GONE);
        }
        type.setText(template.getTypeNice());
        String inapId = template.getAndroidInapId();
        if (!TextUtils.isEmpty(inapId)) {
            enroll.setText("");
            enroll.setEnabled(false);
            mPriceProgressBar.setVisibility(View.VISIBLE);
        } else {
            mPriceProgressBar.setVisibility(View.GONE);
            if (template.isAvailable_with_repost()) {
                enroll.setText(getString(R.string.free_for_repost));
            } else {
                enroll.setText(getString(R.string.free));
            }
        }
        contents.setText(template.getContentsAsString());
    }

    @Override
    public void onEnrollSuccess() {
        getFragNavController().popFragments(2);
    }

    @Override
    public void updateButtonText(@Nonnull String title) {
        enroll.setText(title);
        mPriceProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void purchase(@Nonnull SkuData skuData) {
        BillingManager.getInstance().connect(new BillingManager.ConnectListener() {
            @Override
            public void connected(@NonNull BillingManager billingManager) {
                billingManager.initiatePurchaseFlow(skuData.getSku(), BillingClient.SkuType.INAPP, getActivity());
            }

            @Override
            public void disconnected(@NonNull BillingManager billingManager) {
                mPresenter.errorPurchaseConnected();
            }
        });
    }

    @Override
    public void setButtonEnabled(boolean enabled) {
        enroll.setEnabled(enabled);
    }

    @Override
    public void loginVk() {
        authFascade.loginWithVk(getActivity())
                .subscribe(
                        vkAccessToken -> mPresenter.loginVkSuccess()
                        , throwable -> {
                            mPresenter.loginVkError(throwable);
                        }
                );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        authFascade.getLoginManager().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_training_template_details;
    }
}
