package fitness.online.app.activity.main.fragment.trainings.exercises;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import fitness.online.app.data.local.RealmHandbookDataSource;
import fitness.online.app.data.local.RealmTrainingsDataSource;
import fitness.online.app.model.pojo.realm.handbook.HandbookEntity;
import fitness.online.app.model.pojo.realm.handbook.HandbookExercise;
import fitness.online.app.model.pojo.realm.handbook.HandbookNavigation;
import fitness.online.app.mvp.contract.fragment.SelectExerciseFragmentContract;
import fitness.online.app.recycler.data.HandbookNavigationData;
import fitness.online.app.recycler.item.HandbookNavigationItem;
import fitness.online.app.recycler.item.trainings.SelectExerciseItem;


/**
 * Created by user on 04.10.17.
 */

public class SelectExerciseFragmentPresenter extends SelectExerciseFragmentContract.Presenter implements HandbookNavigationData.Listener {


    private String mCategoryId;

    public SelectExerciseFragmentPresenter(String mCategoryId) {
        this.mCategoryId = mCategoryId;
    }

    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        if (firstBind) {
            loadData();
        }
    }

    @Override
    public void loadData() {
        if (mCategoryId == null) {
            mCategoryId = RealmHandbookDataSource.getInstance().getExerciseParentCategoryId();
        }
        RealmHandbookDataSource.getInstance().listData(mCategoryId).subscribe(this::loadDataLocalSuccess);
    }

    @Override
    public void loadDataLocalSuccess(List<HandbookNavigation> data) {
        executeBounded(view -> view.dataReplace(prepareData(data)));
    }

    @Override
    public void confirmSelected() {
        RealmTrainingsDataSource.getInstance().confirmExercises();
    }


    private List<BaseItem> prepareData(List<HandbookNavigation> data) {
        ArrayList<BaseItem> result = new ArrayList<>();
        for (HandbookNavigation item : data) {
            boolean isLast = data.indexOf(item) == data.size() - 1;
            if (item.getType().equals(HandbookEntity.EXERCISE)) {
                boolean isSelected = RealmTrainingsDataSource.getInstance().isSelected(item.getId());
                result.add(new SelectExerciseItem(item, this, isSelected, isLast));
            } else {
                result.add(new HandbookNavigationItem(item, this, false, isLast));
            }
        }
        return result;
    }

    @Override
    public void onClick(HandbookNavigation handbookNavigation) {
        if (handbookNavigation.getType().equals(HandbookExercise.CATEGORY)) {
            executeBounded(view -> view.onCategorySelect(handbookNavigation.getId()));
        }
        if (handbookNavigation.getType().equals(HandbookExercise.EXERCISE)) {
            RealmTrainingsDataSource.getInstance().toggleSelected(handbookNavigation.getId());
        }
    }

    @Override
    public void onSecondaryClick(HandbookNavigation handbookNavigation) {
        if (handbookNavigation.getType().equals(HandbookExercise.EXERCISE)) {
            executeBounded(view -> view.openExercise(handbookNavigation));
        }
    }
}
