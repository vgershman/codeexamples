package fitness.online.app.activity.main.fragment.trainings.nutrition;

import android.support.annotation.NonNull;

import com.trimf.recycler.item.BaseItem;

import java.util.ArrayList;
import java.util.List;

import fitness.online.app.R;
import fitness.online.app.data.local.RealmNutritionDataSource;
import fitness.online.app.data.local.RealmSessionDataSource;
import fitness.online.app.data.remote.BasicResponseListener;
import fitness.online.app.data.remote.RetrofitNutritionDataSource;
import fitness.online.app.model.pojo.realm.common.nutrition.NutritionDto;
import fitness.online.app.model.pojo.realm.common.nutrition.NutritionResponse;
import fitness.online.app.model.pojo.realm.common.trainings.TrainingCourse;
import fitness.online.app.mvp.contract.fragment.TrainingFragmentNutritionContract;
import fitness.online.app.recycler.data.ButtonData_;
import fitness.online.app.recycler.data.EmptyData;
import fitness.online.app.recycler.data.EmptyDataWithButton;
import fitness.online.app.recycler.data.nutrition.NutritionData;
import fitness.online.app.recycler.item.ClickListener;
import fitness.online.app.recycler.item.EmptyItem;
import fitness.online.app.recycler.item.EmptyItemWithButton;
import fitness.online.app.recycler.item.nutrition.NutritionItem;

/**
 * Created by user on 25.10.17.
 */

public class TrainingNutritionFragmentPresenter extends TrainingFragmentNutritionContract.Presenter implements ClickListener<NutritionData> {

    private TrainingCourse mCourse;

    private List<NutritionDto> mNutritionDtos;


    @Override
    protected void bindViewAfterTransaction(boolean firstBind) {
        super.bindViewAfterTransaction(firstBind);
        if (firstBind) {
            sync();
        } else {
            loadData();
        }
    }

    void sync() {
        showProgressBar();
        RetrofitNutritionDataSource.getInstance().getNutritionByCourse(mCourse.getId(), new BasicResponseListener<NutritionResponse>() {
            @Override
            public void success(NutritionResponse response) {
                hideProgressBar();
                executeBounded(view -> view.setRefreshing(false));
                loadData();
            }

            @Override
            public void error(@NonNull Throwable throwable) {
                super.error(throwable);
                hideProgressBar();
                executeBounded(view -> view.setRefreshing(false));
            }
        });
    }

    public TrainingNutritionFragmentPresenter(TrainingCourse course) {
        mCourse = course;
    }


    @Override
    public void loadData() {
        proceedData(RealmNutritionDataSource.getInstance().getNutritionByCourse(mCourse.getId()));
    }

    @Override
    public void addNutrition() {
        executeBounded(view -> view.selectNutrition());
    }

    @Override
    public void delete(NutritionData data) {
        int nid = data.getNutrition().getId();
        RetrofitNutritionDataSource.getInstance().deleteNutrition(mCourse.getId(), data.getNutrition().getId(), new BasicResponseListener() {
            @Override
            public void success(Object response) {
                RealmNutritionDataSource.getInstance().deleteNutrition(nid);
                loadData();
            }
        });
    }

    private void proceedData(List<NutritionDto> nutritionDtos) {
        mNutritionDtos = nutritionDtos;
        if (nutritionDtos == null || nutritionDtos.size() == 0) {
            empty();
        } else {
            bindData();

        }
    }

    private void bindData() {
        List<BaseItem> items = new ArrayList<>();
        for (NutritionDto nutritionDto : mNutritionDtos) {
            boolean last = mNutritionDtos.indexOf(nutritionDto) == mNutritionDtos.size() - 1;
            items.add(new NutritionItem(new NutritionData(nutritionDto, last, this, data -> executeBounded(view -> view.askToDelete(data)))));
        }
        executeBounded(view -> view.dataReplace(items));
    }


    public void empty() {
        List<BaseItem> items = new ArrayList<>();
        if (mCourse.getInvoice_id() == null || RealmSessionDataSource.getInstance().isTrainer()) {
            items.add(new EmptyItemWithButton(new EmptyDataWithButton(new EmptyData(R.string.empty_nutrition, R.drawable.add_sport_food),
                    new ButtonData_(R.string.btn_add_new_meal, data -> addNutrition()))));
        } else {
            items.add(new EmptyItem(new EmptyData(R.string.empty_nutrition, R.drawable.add_sport_food)));
        }
        executeBounded(view -> view.dataReplace(items));
    }

    @Override
    public void onClick(NutritionData data) {
        executeBounded(view -> view.editNutrition(data));
    }
}
