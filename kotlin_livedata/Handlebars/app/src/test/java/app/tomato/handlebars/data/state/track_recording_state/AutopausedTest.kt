package app.tomato.handlebars.data.state.track_recording_state

import android.location.Location
import app.tomato.handlebars.data.state.HistoryPoint
import app.tomato.handlebars.data.state.RecentPoint
import app.tomato.handlebars.data.state.StateLive
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mockito

/**
 * Created by themylogin on 3/14/18.
 */
class AutopausedTest {
    val locations = mutableListOf<Location>()

    @Before
    fun setUp() {
        val locationsCount = 100

        for (i in 0..locationsCount) {
            this.locations.add(Mockito.mock(Location::class.java))
        }

        for (i in 0..locationsCount) {
            for (j in 0..locationsCount) {
                Mockito.`when`(this.locations[i].distanceTo(this.locations[j])).thenReturn(Math.abs(i - j).toFloat())
            }
        }

        StateLive.currentSpeed = null

        StateLive.recentPoints.clear()
        StateLive.historyPoints.clear()

        StateLive.state = Autopaused(StateLive)
    }

    @Test
    fun notifyLocation_doesNotResumeBecauseStillIdle() {
        StateLive.recentPoints.addAll(mutableListOf(
                RecentPoint(0L, locations[0], null),
                RecentPoint(1000L, locations[1], 1.0f),
                RecentPoint(2000L, locations[2], 1.0f),
                RecentPoint(3000L, locations[2], 0.0f),
                RecentPoint(4000L, locations[2], 0.0f),
                RecentPoint(5000L, locations[2], 0.0f),
                RecentPoint(6000L, locations[2], 0.0f)
        ))

        StateLive.state.notifyLocation(6000L, locations[2])

        assertEquals(StateLive.state::class, Autopaused::class)
    }

    @Test
    fun notifyLocation_doesNotResumeBecauseStartedMovingOnlyNow() {
        StateLive.recentPoints.addAll(mutableListOf(
                RecentPoint(0L, locations[0], null),
                RecentPoint(1000L, locations[2], 2.0f),
                RecentPoint(2000L, locations[4], 2.0f),
                RecentPoint(3000L, locations[4], 0.0f),
                RecentPoint(4000L, locations[4], 0.0f),
                RecentPoint(5000L, locations[4], 0.0f),
                RecentPoint(6000L, locations[6], 2.0f)
        ))

        StateLive.state.notifyLocation(6000L, locations[6])

        assertEquals(StateLive.state::class, Autopaused::class)
    }

    @Test
    fun notifyLocation_resumes() {
        StateLive.recentPoints.addAll(mutableListOf(
                RecentPoint(0L, locations[0], null),
                RecentPoint(1000L, locations[2], 2.0f),
                RecentPoint(2000L, locations[4], 2.0f),
                RecentPoint(3000L, locations[4], 0.0f),
                RecentPoint(4000L, locations[4], 0.0f),
                RecentPoint(5000L, locations[4], 0.0f),
                RecentPoint(6000L, locations[6], 2.0f),
                RecentPoint(20000L, locations[32], 2.0f)
        ))

        StateLive.state.notifyLocation(20000L, locations[34])

        assertEquals(StateLive.state::class, Recording::class)

        assertEquals(
                mutableListOf(
                        HistoryPoint(6000L, locations[6], false),
                        HistoryPoint(20000L, locations[32], false)
                ),
                StateLive.historyPoints
        )
    }
}