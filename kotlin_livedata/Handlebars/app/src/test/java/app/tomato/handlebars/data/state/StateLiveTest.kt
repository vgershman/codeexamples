package app.tomato.handlebars.data.state

import android.location.Location
import app.tomato.handlebars.data.state.track_recording_state.Stopped
import app.tomato.handlebars.data.widget.CurrentSpeedLiveData
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class StateLiveTest {
    val locations = mutableListOf<Location>()

    @Before
    fun setUp() {
        val locationsCount = 100

        for (i in 0..locationsCount) {
            this.locations.add(mock(Location::class.java))
        }

        for (i in 0..locationsCount) {
            for (j in 0..locationsCount) {
                `when`(this.locations[i].distanceTo(this.locations[j])).thenReturn(Math.abs(i - j).toFloat())
            }
        }

        CurrentSpeedLiveData.setSpeed(null)

        StateLive.recentPoints.clear()
        StateLive.historyPoints.clear()

        StateLive.setCurrentState(Stopped())
    }

    @Test
    fun tick_resetsCurrentSpeedIfLastPointIsTooOld() {
        CurrentSpeedLiveData.setSpeed(5.0f)
        //State.currentSpeed = 5.0f
        StateLive.recentPoints.add(RecentPoint(1000L, locations[0], null))

        StateLive.tick(17000L)

        assertEquals(null, CurrentSpeedLiveData.value)
    }

    @Test
    fun tick_doesNotTouchCurrentSpeedIfLastPointIsNotOldEnough() {
        CurrentSpeedLiveData.setSpeed(5.0f)
        StateLive.recentPoints.add(RecentPoint(1000L, locations[0], null))

        StateLive.tick(2000L)

        assertEquals(5.0f, CurrentSpeedLiveData.value)
    }

    @Test
    fun notifyLocation_calculatesCurrentSpeed() {
        StateLive.recentPoints.add(RecentPoint(1000L, locations[0], null))

        StateLive.notifyLocation(locations[5])

        assertEquals(5.0f, CurrentSpeedLiveData.value)
    }

    @Test
    fun notifyLocation_doesNotCalculateCurrentSpeedIfLastPointIsTooOld() {
        StateLive.recentPoints.add(RecentPoint(1000L, locations[0], null))

        StateLive.notifyLocation(locations[80])

        assertEquals(null, CurrentSpeedLiveData.value)

    }

    @Test
    fun notifyLocation_addsToRecentPoints() {
        StateLive.recentPoints.add(RecentPoint(1000L, locations[5], null))

        StateLive.notifyLocation(locations[11])

        assertEquals(
                mutableListOf(
                        RecentPoint(1000L, locations[5], null),
                        RecentPoint(2000L, locations[11], 6.0f)
                ),
                StateLive.recentPoints
        )
    }

    @Test
    fun notifyLocation_cleansRecentPoints() {
        StateLive.recentPoints.add(RecentPoint(1000L, locations[1], null))
        StateLive.recentPoints.add(RecentPoint(20000L, locations[20], null))
        StateLive.recentPoints.add(RecentPoint(21000L, locations[21], 1.0f))

        StateLive.notifyLocation(locations[22])

        assertEquals(
                mutableListOf(
                        RecentPoint(20000L, locations[20], null),
                        RecentPoint(21000L, locations[21], 1.0f),
                        RecentPoint(22000L, locations[22], 1.0f)
                ),
                StateLive.recentPoints
        )
    }

    @Test
    fun calculateAverageSpeed() {
        StateLive.historyPoints.addAll(mutableListOf(
                HistoryPoint(0L, locations[0], false),
                HistoryPoint(1000L, locations[1], false),
                HistoryPoint(2000L, locations[3], false),
                HistoryPoint(3000L, locations[7], true),
                HistoryPoint(4000L, locations[7], false)
        ))

        assertEquals(
                1.75f,
                StateLive.calculateAverageSpeed(null, null, false)
        )
    }

    @Test
    fun calculateAverageSpeed_excludeInactive() {
        StateLive.historyPoints.addAll(mutableListOf(
                HistoryPoint(0L, locations[0], false),
                HistoryPoint(1000L, locations[1], false),
                HistoryPoint(2000L, locations[3], false),
                HistoryPoint(3000L, locations[7], true),
                HistoryPoint(4000L, locations[7], false)
        ))

        assertEquals(
                2.3333333f,
                StateLive.calculateAverageSpeed(null, null, true)
        )
    }

    @Test
    fun calculateTotalDistance() {
        StateLive.historyPoints.addAll(mutableListOf(
                HistoryPoint(0L, locations[0], false),
                HistoryPoint(1000L, locations[1], false),
                HistoryPoint(2000L, locations[3], false),
                HistoryPoint(3000L, locations[7], true),
                HistoryPoint(14000L, locations[70], false),
                HistoryPoint(15000L, locations[71], false)
        ))

        assertEquals(
                8.0f,
                StateLive.calculateTotalDistance()
        )
    }

    @Test
    fun calculateTotalTime() {
        StateLive.historyPoints.addAll(mutableListOf(
                HistoryPoint(0L, locations[0], false),
                HistoryPoint(1000L, locations[1], false),
                HistoryPoint(2000L, locations[3], false),
                HistoryPoint(3000L, locations[7], true),
                HistoryPoint(14000L, locations[70], false),
                HistoryPoint(15000L, locations[71], false)
        ))

        assertEquals(
                20.0f,
                StateLive.calculateTotalTime(20000L, false)
        )
    }
    @Test
    fun calculateTotalTime_excludeInactive() {
        StateLive.historyPoints.addAll(mutableListOf(
                HistoryPoint(0L, locations[0], false),
                HistoryPoint(1000L, locations[1], false),
                HistoryPoint(2000L, locations[3], false),
                HistoryPoint(3000L, locations[7], true),
                HistoryPoint(14000L, locations[70], false),
                HistoryPoint(15000L, locations[71], false)
        ))

        assertEquals(
                9.0f,
                StateLive.calculateTotalTime(20000L, true)
        )
    }
}
