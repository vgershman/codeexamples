package app.tomato.handlebars.data.widget

import org.junit.Test

import org.junit.Assert.*

import com.luckycatlabs.sunrisesunset.dto.Location
import java.util.*

/**
 * Created by themylogin on 3/6/18.
 */
class SunsetSunriseLiveDataCalculatorTest {
    val MOSCOW = Location(55.730993, 37.475703)
    val MURMANSK = Location(68.969582, 33.074558)

    private fun date(y : Int, m : Int, d : Int, h : Int, i : Int, tz : String) : Calendar {
        val c = Calendar.getInstance()
        c.set(y, m - 1, d, h, i)
        c.timeZone = TimeZone.getTimeZone(tz)
        return c
    }

    //

    @Test
    fun assertRegularSunrise() {
        assertEquals(
                Pair<String, String>("Sunrise", "07:10"),
                SunsetSunriseLiveDataCalculator.calculate(MOSCOW, date(2018, 3, 6, 2, 0, "Europe/Moscow"))
        )
    }

    @Test
    fun assertRegularSunset() {
        assertEquals(
                Pair<String, String>("Sunset", "18:14"),
                SunsetSunriseLiveDataCalculator.calculate(MOSCOW, date(2018, 3, 6, 10, 0, "Europe/Moscow"))
        )
    }

    @Test
    fun assertRegularNextDaySunrise() {
        assertEquals(
                Pair<String, String>("Sunrise", "07:07"),
                SunsetSunriseLiveDataCalculator.calculate(MOSCOW, date(2018, 3, 6, 22, 0, "Europe/Moscow"))
        )
    }

    //

    @Test
    fun assertLastSunsetBeforePolarDayStart() {
        assertEquals(
                Pair<String, String>("Sunset", "23:57"),
                SunsetSunriseLiveDataCalculator.calculate(MURMANSK, date(2018, 5, 19, 23, 55, "Europe/Moscow"))
        )
    }

    @Test
    fun assertPolarDayStart() {
        assertEquals(
                Pair<String, String>("Sunrise", "01:25"),
                SunsetSunriseLiveDataCalculator.calculate(MURMANSK, date(2018, 5, 20, 1, 0, "Europe/Moscow"))
        )
    }

    @Test
    fun assertPolarDay() {
        assertEquals(
                Pair<String, String>("Sunset", "--:--"),
                SunsetSunriseLiveDataCalculator.calculate(MURMANSK, date(2018, 5, 20, 1, 30, "Europe/Moscow"))
        )
    }
}
