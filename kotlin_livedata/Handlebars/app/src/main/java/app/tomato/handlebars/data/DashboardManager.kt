package app.tomato.handlebars.data

import android.arch.lifecycle.LiveData
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.ui.MapSettings
import io.realm.Realm
import java.util.*

/**
 * Created by user on 27.02.18.
 */

object DashboardManager : LiveData<List<Dashboard>>() {

    override fun onActive() {
        super.onActive()
        reload()

    }

    fun getWidgetById(id: String): Widget? {
        return Realm.getDefaultInstance().where(Widget::class.java).equalTo("id", id).findFirst()
    }

    fun reload() {
        value = Realm.getDefaultInstance().where(Dashboard::class.java).sort("position").findAll()
    }

    fun fixPositions() {
        reload()
        val dashboards = Realm.getDefaultInstance().where(Dashboard::class.java).sort("position").findAll()
        transaction {
            for (i in 0 until dashboards.count()) {
                dashboards[i]?.position = i + 1
            }
        }

    }

    fun listWidgetsByDashboard(dashboard: Dashboard): List<Widget> {
        val result = ArrayList<Widget>()
        result.addAll(listWidgetByDashAndType(dashboard.id!!, "large").take(dashboard.large * MyApp.Config.LARGE_WIDGET_COUNT))
        result.addAll(listWidgetByDashAndType(dashboard.id!!, "normal").take(dashboard.normal * MyApp.Config.NORMAL_WIDGET_COUNT))
        result.addAll(listWidgetByDashAndType(dashboard.id!!, "medium").take(dashboard.medium * MyApp.Config.MEDIUM_WIDGET_COUNT))
        if (dashboard.maps) {
            result.addAll(listWidgetByDashAndType(dashboard.id!!, "map").take(2))
        }
        return result

//        return Realm.getDefaultInstance().where(Widget::class.java)
//                .equalTo("dashId", dashboard.id).sort("position")
//                .lessThan("position", dashboard.).findAll()
    }

    fun newDashboard(): Dashboard {
        val maxPosition = Realm.getDefaultInstance().where(Dashboard::class.java).max("position")
        val dashboard = Dashboard(UUID.randomUUID().toString(), (maxPosition?.toInt() ?: 0) + 1)
        dashboard.maps = true
        Realm.getDefaultInstance().beginTransaction()
        Realm.getDefaultInstance().copyToRealmOrUpdate(dashboard)
        Realm.getDefaultInstance().commitTransaction()
        return dashboard
    }

    fun newWidget(did: String, type: String, pos: Int, name: String, settings: String?): Widget {
        val widget = Widget(UUID.randomUUID().toString(), did, type, pos, name, settings)
        transaction {
            Realm.getDefaultInstance().copyToRealmOrUpdate(widget)
        }
        return widget
    }

    fun getDashboardById(did: String): Dashboard? {
        return Realm.getDefaultInstance().where(Dashboard::class.java).equalTo("id", did).findFirst()
    }

    fun listWidgetByDashAndType(did: String, type: String): List<Widget> {
        return Realm.getDefaultInstance().where(Widget::class.java)
                .equalTo("dashId", did).equalTo("type", type).sort("position").findAll()
    }

    fun transaction(action: () -> Unit) {
        RealmUtil.transaction(action)
    }

    fun onSort(from: Int, to: Int) {
        transaction {
            val old = value!![from].position
            value!![from].position = value!![to].position
            value!![to].position = old
        }

    }

}