package app.tomato.handlebars.ui.widgets

import app.tomato.handlebars.MyApp
import app.tomato.handlebars.data.widget.*
import kotlinx.android.synthetic.main.widget_gauge.*

/**
 * Created by user on 01.03.18.
 */
class SpeedWidget : GaugeWidget<Float>() {

    override fun format(value: Float): String {
        val speedInKmH = String.format("%.1f", MyApp.metersToLocal(value * 18 / 5 * 1000)).replace(",", ".")
        if (speedInKmH.length > 4) {
            applyValueSize(MyApp.WidgetConfig.BIG_VALUE_SCALER)
        } else {
            applyValueSize(1f)
        }
        return speedInKmH
    }

    override fun getMetric(): String {
        if (MyApp.isImperial()) {
            return "mi\nh"
        }
        return "km\nh"
    }

    override fun getEmptyValue() = "__._"

    override fun setup() {
        CurrentSpeedLiveData.observe(this, observer)
    }

    override fun title() = "Speed"

}

class TimeWidget : GaugeWidget<String>() {

    override fun format(value: String) = value


    override fun setup() {
        TimerLiveData.observe(this, observer)
    }

    override fun title() = "Time"

}

class SunsetWidget : GaugeWidget<String>() {

    override fun format(value: String) = value

    override fun title() = "Sunset"

    override fun getEmptyValue() = "--:--"


    override fun setup() {
        SunsetLiveData.observe(this, observer)
    }
}

class SunriseWidget : GaugeWidget<String>() {

    override fun format(value: String) = value

    override fun getEmptyValue() = "--:--"


    override fun title() = "Sunrise"

    override fun setup() {
        SunriseLiveData.observe(this, observer)
    }
}

class SunsetSunriseWidget : GaugeWidget<Pair<String, String>>() {
    override fun format(value: Pair<String, String>): String {
        tvTtl.text = value?.first
        return value.second
    }

    override fun title() = "Sunset/Sunrise"

    override fun setup() {
        SunsetSunriseLiveData.observe(this, observer)
    }
}

class DistanceWidget : GaugeWidget<Float>() {
    override fun format(value: Float): String {
        val formattedValue = String.format("%.1f", MyApp.metersToLocal(value)).replace(",", ".")
        if (formattedValue.length > 4) {
            applyValueSize(MyApp.WidgetConfig.BIG_VALUE_SCALER)
        } else {
            applyValueSize(1f)
        }
        return formattedValue
    }


    override fun getMetric(): String {
        if (MyApp.isImperial()) {
            return "mi"
        }
        return "km"
    }

    override fun getEmptyValue() = "__._"

    override fun title() = "Distance"

    override fun setup() {
        DistanceLiveData.observe(this, observer)
    }

}

class TotalTimeWidget : GaugeWidget<String>() {

    override fun format(value: String): String {
        if (value.length > 5) {
            applyValueSize(MyApp.WidgetConfig.BIG_VALUE_SCALER)
        } else {
            applyValueSize(1f)
        }
        return value
    }

    override fun getEmptyValue() = "--:--"

    var exclude: Boolean = true


    override fun setup() {
        TrackTimeListeners.getListenerFor(exclude).observe(this, observer)
    }

    override fun initProperties() {
        exclude = arguments!!["setting"] as String? ?: "excluded" == "excluded"
        super.initProperties()
    }

    override fun title() = "Total Time"

}

class BatteryWidget : GaugeWidget<Int>() {

    override fun format(value: Int) = "$value%"

    override fun setup() {
        BatteryLiveData.context = activity
        BatteryLiveData.observe(this, observer)
    }

    override fun title() = "Battery"

}

class AverageSpeedWidget : GaugeWidget<Float>() {

    override fun format(value: Float): String {
        val speedInKmH = String.format("%.1f", MyApp.metersToLocal(value * 1000 * 18 / 5)).replace(",", ".")
        if (speedInKmH.length > 4) {
            applyValueSize(MyApp.WidgetConfig.BIG_VALUE_SCALER)
        } else {
            applyValueSize(1f)
        }
        return speedInKmH
    }

    override fun getMetric(): String {
        if (MyApp.isImperial()) {
            return "mi\nh"
        }
        return "km\nh"
    }

    override fun getEmptyValue() = "__._"

    var settings: String = "ride_excluded"

    override fun title() = "Average Speed"

    override fun initProperties() {
        settings = arguments!!["setting"] as String? ?: settings
        super.initProperties()
    }

    override fun setup() {
        AverageSpeedListeners.getListenerForSetting(settings).observe(this, observer)
    }
}