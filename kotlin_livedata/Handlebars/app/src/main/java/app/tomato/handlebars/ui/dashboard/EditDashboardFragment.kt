package app.tomato.handlebars.ui.dashboard

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import app.tomato.basic.help.dpToPx
import app.tomato.basic.help.screenHeight
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import app.tomato.handlebars.data.Dashboard
import app.tomato.handlebars.data.DashboardManager
import app.tomato.handlebars.data.Widget
import app.tomato.handlebars.data.WidgetOption
import app.tomato.handlebars.ui.dashboard.DashboardUtils.bindTableItem
import app.tomato.handlebars.ui.widgets.MapWidget
import kotlinx.android.synthetic.main.fragment_edit_dashboard.*


/**
 * Created by user on 27.02.18.
 */
class EditDashboardFragment : BasicFragment() {

    override val hasToolbar = true

    var newWidgetPos: Int? = null
    var newWidgetType: String? = null
    var widgetToReplace: Widget? = null
    var dashboard: Dashboard? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        return view
    }

    private fun bindMap() {
        try {
            if (childFragmentManager.findFragmentById(R.id.mainMap) == null) {
                val mapWidget = MapWidget()
                mapWidget.arguments = Bundle()
                mapWidget.arguments!!.putString("did", dashboard?.id)
                mapWidget.arguments!!.putFloat("zoom", dashboard?.zoom ?: 0f)
                mapWidget.arguments!!.putBoolean("enabledZoom", true)
                childFragmentManager.beginTransaction().replace(R.id.mainMap, mapWidget).commit()
            }
        } catch (ex: Exception) {

        }
    }

    override fun initViews() {
        setScaler()
        btnAddLarge.setOnClickListener {
            addLarge()
            bindDashboard()
        }
        btnRemoveLarge.setOnClickListener {
            removeLarge()
            bindDashboard()
        }
        btnAddNormal.setOnClickListener {
            addNormal()
            bindDashboard()
        }
        btnRemoveNormal.setOnClickListener {
            removeNormal()
            bindDashboard()
        }
        btnAddMedium.setOnClickListener {
            addMedium()
            bindDashboard()
        }
        btnRemoveMedium.setOnClickListener {
            removeMedium()
            bindDashboard()
        }
        cbMaps.setOnCheckedChangeListener(mapsCheckListener)
        val width = screenWidth(context!!)
        val newWidth = (width - dpToPx(context!!, 40)).toInt()
        val height = screenHeight(context!!)
        val newHeight = (newWidth / width * height).toInt()
        llTable.layoutParams.height = newHeight
        // = LinearLayout.LayoutParams(newWidth, newHeight)
    }

    private fun setScaler() {
    }

    private fun removeLarge() {
        if (dashboard != null) {
            if (dashboard != null) {
                DashboardManager.transaction {
                    if (dashboard!!.large > 0) {
                        dashboard!!.large = dashboard!!.large - 1
                    }
                }
            }
        }
    }

    private fun addLarge() {
        if (dashboard != null) {
            DashboardManager.transaction { dashboard!!.large = dashboard!!.large!! + 1 }
        }
    }

    private fun removeNormal() {
        if (dashboard != null) {
            if (dashboard != null) {
                DashboardManager.transaction {
                    if (dashboard!!.normal > 0) {
                        dashboard!!.normal = dashboard!!.normal - 1
                    }
                }
            }
        }
    }

    private fun addNormal() {
        if (dashboard != null) {
            DashboardManager.transaction { dashboard!!.normal = dashboard!!.normal!! + 1 }
        }
    }

    private fun removeMedium() {
        if (dashboard != null) {
            if (dashboard != null) {
                DashboardManager.transaction {
                    if (dashboard!!.medium > 0) {
                        dashboard!!.medium = dashboard!!.medium - 1
                    }
                }
            }
        }
    }

    private fun addMedium() {
        if (dashboard != null) {
            DashboardManager.transaction { dashboard!!.medium = dashboard!!.medium!! + 1 }
        }
    }

    override fun initProperties() {
        super.initProperties()
        val did = arguments!!["did"] as String? ?: return
        dashboard = DashboardManager.getDashboardById(did)
        bindDashboard()
    }

    private fun bindDashboard() {
        setTitle("Dashboard #${dashboard?.position}")
        tvLarge.text = dashboard?.large.toString()
        tvMedium.text = dashboard?.medium.toString()
        tvNormal.text = dashboard?.normal.toString()
        cbMaps.setOnCheckedChangeListener(null)
        cbMaps.isChecked = dashboard?.maps ?: true
        cbMaps.setOnCheckedChangeListener(mapsCheckListener)
        bindMap()
        bindTable()
        bindMaps()
    }

    private fun bindMaps() {
        var count = 0
        if (dashboard?.maps != false) {
            count = 1
        }
        bindTableItem(this, dashboard!!, "map", llMaps, MyApp.Config.LARGE_WIDGET_COUNT, count, 40, true, this::mapListener)

    }

    private val mapsCheckListener = CompoundButton.OnCheckedChangeListener { _, isChecked ->
        if (dashboard != null) {
            DashboardManager.transaction { dashboard!!.maps = isChecked }
            bindDashboard()
        }
    }

    private fun bindTable() {
        if (dashboard != null) {
            bindTableItem(this, dashboard!!, "large", llLarge, MyApp.Config.LARGE_WIDGET_COUNT, dashboard?.large
                    ?: 0, 40, true, this::widgetListener)
            bindTableItem(this, dashboard!!, "normal", llNormal, MyApp.Config.NORMAL_WIDGET_COUNT, dashboard?.normal
                    ?: 0, 40, true, this::widgetListener)
            bindTableItem(this, dashboard!!, "medium", llMedium, MyApp.Config.MEDIUM_WIDGET_COUNT, dashboard?.medium
                    ?: 0, 40, true, this::widgetListener)
        }
    }

    private fun widgetListener(widget: Widget?, type: String, size: Int, row: Int, column: Int) {
        widgetToReplace = widget
        selectWidget(type, size, row, column, widget?.setting)
    }

    private fun mapListener(widget: Widget?, type: String, size: Int, row: Int, column: Int) {
        widgetToReplace = widget
        selectMap(type, size, row, column, widget?.setting)
    }

    private fun deleteWidget(curWidget: Widget): Boolean {
        DashboardManager.transaction {
            curWidget.deleteFromRealm()
        }
        // bindDashboard()
        return true
    }

    private fun selectWidget(type: String, size: Int, i: Int, k: Int, setting: String?) {
        newWidgetPos = size * i + k
        newWidgetType = type
        go(MyApp.Screens.SELECT_CATEGORY, MyApp.Containers.SIMPLE, result = 12, from = this, params = hashMapOf("settings" to setting))
    }

    private fun selectMap(type: String, size: Int, i: Int, k: Int, setting: String?) {
        newWidgetPos = size * i + k
        newWidgetType = type
        go(MyApp.Screens.SELECT_MAP, MyApp.Containers.SIMPLE, result = 12, from = this, params = hashMapOf("height" to dashboard?.mapsHeight, "settings" to setting))
    }

    private fun newWidget(widget: WidgetOption, settings: String?, height: Int?) {
        if (newWidgetPos != null && newWidgetType != null) {
            if (widgetToReplace != null) {
                deleteWidget(widgetToReplace!!)
            }
            DashboardManager.newWidget(dashboard!!.id!!, newWidgetType!!, newWidgetPos!!, widget.name, settings)
            if (height != null) {
                DashboardManager.transaction { dashboard!!.mapsHeight = height }
            }
            bindDashboard()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 12) {
            val widgetOption = data?.getSerializableExtra("widget") as WidgetOption?
            val settings = data?.getStringExtra("setting")
            val height = data?.getIntExtra("height", 50)
            if (resultCode == Activity.RESULT_OK && widgetOption != null) {
                newWidget(widgetOption, settings, height ?: 50)
            } else {
                newWidgetPos = null
                newWidgetType = null
            }
        }
    }


    override val containerView = R.layout.fragment_edit_dashboard

}

