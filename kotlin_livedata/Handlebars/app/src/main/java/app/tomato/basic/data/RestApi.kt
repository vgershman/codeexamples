package app.tomato.basic.data

import android.support.annotation.Keep
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Keep
abstract class RestApi<out T> {

    private var sApi: T? = null

    private var retrofit: Retrofit? = null

    fun rest(): T {
        if (sApi == null) {
            build()
        }
        return sApi as T
    }

    fun retrofit(): Retrofit {
        if (retrofit == null) {
            build()
        }
        return retrofit!!
    }

    private fun build() {
        init()
        val builder = OkHttpClient.Builder()
        interceptors?.forEach { builder.addInterceptor(it) }
        builder.addInterceptor(logger)
        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(15, TimeUnit.SECONDS)
        builder.writeTimeout(50, TimeUnit.SECONDS)

        val client = builder.build()

        retrofit = Retrofit.Builder().baseUrl(host).client(client)
                .addConverterFactory(NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create())).build()

        sApi = retrofit!!.create(clazz as Class<T>)

    }

    abstract fun init()

    var host: String? = null

    var clazz: Any? = null

    var interceptors: ArrayList<Interceptor>? = null
        get

    val logger: HttpLoggingInterceptor
        get() = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)


    fun addInterceptor(interceptor: Interceptor) {
        if (interceptors == null) {
            interceptors = ArrayList()
        }
        interceptors?.add(interceptor)
    }


}
