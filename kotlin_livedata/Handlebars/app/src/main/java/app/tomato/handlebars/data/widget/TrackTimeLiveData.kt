package app.tomato.handlebars.data.widget

import android.arch.lifecycle.LiveData
import app.tomato.handlebars.data.Utils
import app.tomato.handlebars.data.state.StateLive

/**
 * Created by user on 04.03.18.
 */

object TrackTimeListeners {
    private var map = hashMapOf<Boolean, TrackTimeLiveData>()

    fun getListenerFor(exclude: Boolean): TrackTimeLiveData {
        var liveData = map[exclude]
        if (liveData == null) {
            liveData = TrackTimeLiveData(exclude)
            map[exclude] = liveData
        }
        return liveData
    }

    fun updateAll() {
        map.values.forEach {
            it.update()
        }
    }
}

class TrackTimeLiveData(var exclude: Boolean = true) : LiveData<String>() {

    override fun onActive() {
        super.onActive()
        update()
    }

    fun update() {
        Utils.async({ StateLive.calculateTotalTime(System.currentTimeMillis(), exclude) },
                { value = format((it as Long?)?.div(1000f)) })

    }

    companion object {
        fun format(allTimeInSeconds: Float?): String? {
            if (allTimeInSeconds == null) {
                return null
            }
            val hours = allTimeInSeconds.toInt() / 3600
            val minutes = ((allTimeInSeconds - hours * 3600) / 60).toInt()
            var seconds = ((allTimeInSeconds - hours * 3600) - (60 * minutes)).toInt()

            var builder = StringBuilder()

            if (hours > 0) {
                builder.append(hours)
                builder.append(":")
            }

            builder.append(String.format("%02d", minutes))
            if (hours == 0) {
                builder.append(":")
                builder.append(String.format("%02d", seconds))
            }

            return builder.toString()

        }
    }

}