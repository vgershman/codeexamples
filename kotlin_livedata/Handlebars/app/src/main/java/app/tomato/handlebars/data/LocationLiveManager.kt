package app.tomato.handlebars.data

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import app.tomato.handlebars.data.state.StateLive


@SuppressLint("StaticFieldLeak")
/**
 * Created by user on 06.02.18.
 */

object LocationLiveManager : LiveData<Location?>() {

    var context: Context? = null
    private var locationManager: LocationManager? = null
    private var locationListener: LocationListener = object : LocationListener {

        override fun onLocationChanged(location: Location?) {
            if (location != null) {
                asked = false
            }
            value = location
            if (location != null) {
                StateLive.notifyLocation(location)
            }
        }


        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

        }

        override fun onProviderEnabled(provider: String?) {

        }


        override fun onProviderDisabled(provider: String?) {
            check()
        }

    }

    var asked = false

    private fun check() {
        val enabled = (locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER)
                ?: false) || (locationManager?.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                ?: false)
        if (!enabled && !asked) {
            asked = true
            context?.startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }

    }


    @SuppressLint("MissingPermission")
    override fun onActive() {
        locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        check()
        try {
            //locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000L, 1f, locationListener)
            locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000L, 1f, locationListener)
            locationListener.onLocationChanged(locationManager?.getLastKnownLocation(LocationManager.NETWORK_PROVIDER))
        } catch (ex: Exception) {
        }
    }

    override fun onInactive() {
        locationManager?.removeUpdates(locationListener)
    }

}