package app.tomato.handlebars.data.state.track_recording_state

import android.location.Location
import app.tomato.handlebars.data.state.StateLive

/**
 * Created by themylogin on 3/14/18.
 */
abstract class TrackRecordingState() {
    abstract fun tick(time: Long)

    abstract fun notifyLocation(location: Location)

    abstract fun buttonTap()

    abstract fun buttonLongTap()
}
