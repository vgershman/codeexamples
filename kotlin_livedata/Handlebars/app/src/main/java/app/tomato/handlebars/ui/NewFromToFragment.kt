package app.tomato.handlebars.ui

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.tomato.basic.help._TextWatcher
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.R
import app.tomato.handlebars.data.*
import com.google.android.gms.location.places.AutocompletePrediction
import com.google.android.gms.location.places.AutocompletePredictionBuffer
import kotlinx.android.synthetic.main.fragment_custom_to.*
import kotlinx.android.synthetic.main.item_recent.view.*
import kotlinx.android.synthetic.main.item_settle.view.*

/**
 * Created by user on 18.03.18.
 */
class NewFromToFragment : BasicFragment() {

    var from = false
    var backstack = arrayListOf<String>()
    var prev = ""
    var point: Point? = null
    var geocodeManager: GeocodeManager? = null
    private val recentsAdapter = RecentAdapter({ onCurrent() }, { onRecent(it) }, { onRecentLong(it) })

    private fun onRecentLong(it: RecentDto) {
        AlertDialog.Builder(context).setMessage("Удалить " + it.point?.getLocationFullName() + " из списка недавних?")
                .setPositiveButton("Да", { d, _ -> d.cancel(); RecentManager.remove(it) })
                .setNegativeButton("Отмена", { d, _ -> d.cancel() })
                .setNeutralButton("Очистить весь список", { d, _ -> d.cancel(); RecentManager.clear() })
                .create().show()
    }


    private val geocodeAdapter = GeocodeAdapter({ onPredict(it) })

    override fun initProperties() {
        super.initProperties()
        from = arguments!!["from"] as Boolean? ?: from
        if (from) {
            etSearch.hint = "From..."
        } else {
            etSearch.hint = "To..."
        }
    }

    override fun initViews() {
        btnOk.isEnabled = false
        btnOk.alpha = 0.5f
        rv.layoutManager = LinearLayoutManager(context)
        geocodeManager = GeocodeManager(context!!)
        geocodeManager?.observe(this, Observer { bindGeocodeResults(it) })
        RecentManager.observe(this, Observer { bindRecents(it) })
        btnClose.setOnClickListener { onClose() }
        btnOk.setOnClickListener { select() }
        bindDefault()
        etSearch.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                etSearch.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
            } else {
                etSearch.setCompoundDrawablesWithIntrinsicBounds(resources.getDrawable(R.drawable.ic_search_tag), null, null, null)
            }
        }
        etSearch.addTextChangedListener(object : _TextWatcher {

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.isNullOrBlank()) {
                    btnClose.setImageResource(R.drawable.ic_arrow_back_black_48px)
                    bindDefault()
                } else {
                    btnClose.setImageResource(R.drawable.abc_ic_clear_material)
                    bindSearch()
                    geocodeManager?.search(s.toString())
                    if (prev.length - s!!.length == 1) {
                        onDelete(start)
                    }
                }
                if (s!!.length - prev.length == 1) {
                    btnOk.isEnabled = false
                    btnOk.alpha = 0.5f
                }
                prev = s.toString()
            }
        })
    }

    private fun bindRecents(recent: List<RecentDto>?) {
        recentsAdapter.setData(recent)
    }

    private fun onClose() {
        try {
            if (etSearch.text.toString().isNullOrBlank()) {
                if (!etSearch.hasFocus()) {
                    onBackPressed()
                } else {
                    etSearch.clearFocus()
                }
                hideKeyboard(etSearch, context!!)
            } else {
                etSearch.setText("")
                backstack.clear()
                hideKeyboard(etSearch, context!!)
            }
        } catch (ex: Exception) {
        }
    }

    private fun select() {
        if (point != null) {
            RecentManager.recent(point, getStackString())
            SQManager.get(context!!).setPoint(from, point!!)
            onBackPressed()
        }
    }

    private fun onDelete(start: Int) {
        val builder = StringBuilder()
        for (i in 0 until backstack.size) {
            builder.append(backstack[i])
            builder.append(" ")
            if (start <= builder.toString().length) {
                deleteStackTo(i)
                break
            }
        }
    }

    private fun deleteStackTo(limit: Int) {
        val oldStack = backstack
        backstack = arrayListOf()
        backstack.addAll(oldStack.take(limit))
        oldStack.clear()
        etSearch.setText(getStackString() + " ")
        etSearch.setSelection(etSearch.text.toString().length)
        btnOk.isEnabled = false
        btnOk.alpha = 0.5f

    }

    private fun bindGeocodeResults(it: AutocompletePredictionBuffer?) {
        geocodeAdapter.setData(it?.toList())
    }

    private fun bindSearch() {
        rv.adapter = geocodeAdapter
    }

    private fun getStackString(): String {
        val builder = StringBuilder()
        backstack.forEach {
            if (backstack.indexOf(it) != 0 && !it.startsWith(",")) {
                builder.append(" ")
            }
            builder.append(it)
        }
        return builder.toString()
    }

    private fun onPredict(prediction: AutocompletePrediction) {
        val last = backstack.lastOrNull()
        var predict = prediction.getPrimaryText(null).toString()
        if (last != null && predict.contains(last)) {
            predict = predict.substring(predict.indexOf(last) + last.length, predict.length)

        }
        if (predict.isNotBlank() && predict != last) {
            backstack.add(predict)
        }
        etSearch.setText(getStackString() + " ")
        etSearch.setSelection(etSearch.text.toString().length)
        geocodeManager?.retrievePlace(prediction.placeId, {
            if (it.get(0) != null) {
                point = Point(it[0].latLng.latitude, it[0].latLng.longitude,
                        title = prediction.getPrimaryText(null).toString(),
                        subtitle = prediction.getSecondaryText(null).toString())
                btnOk.isEnabled = true
                btnOk.alpha = 1f
            }
        })

    }

    private fun bindDefault() {
        rv.adapter = recentsAdapter
    }

    private fun onCurrent() {
        if (LocationLiveManager.value != null) {
            SQManager.get(context!!).setPoint(from, Point(LocationLiveManager.value!!.latitude, LocationLiveManager.value!!.longitude, "My Location"))
            onBackPressed()
        }
    }

    private fun onRecent(it: RecentDto) {
        btnOk.isEnabled = true
        btnOk.alpha = 1f
        point = it.point
        RecentManager.recent(it.point, it.query!!)
        backstack.clear()
        backstack.add(it.query!!)
        etSearch.setText(getStackString() + " ")
        etSearch.setSelection(etSearch.text.toString().length)

    }

    override val containerView = R.layout.fragment_custom_to

}

class GeocodeAdapter(var click: (prediction: AutocompletePrediction) -> Unit) : RecyclerView.Adapter<GeocodeAdapter.VH>() {

    private var data = arrayListOf<AutocompletePrediction>()

    fun setData(predictions: List<AutocompletePrediction>?) {
        data.clear()
        if (predictions != null) {
            data.addAll(predictions)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent.context).inflate(R.layout.item_settle, parent, false))


    override fun getItemCount() = data.size


    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(data[position], position == data.size - 1)
    }


    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(prediction: AutocompletePrediction, last: Boolean) {
            itemView.setOnClickListener { click(prediction) }
            itemView.tvTitle.text = prediction.getPrimaryText(null)
            itemView.tvDistance.text = prediction.getSecondaryText(null)
            if (last) {
                itemView.ivGoogle.visibility = View.VISIBLE
            } else {
                itemView.ivGoogle.visibility = View.GONE
            }
        }
    }

}

class RecentAdapter(var current: () -> Unit, var click: (recentDto: RecentDto) -> Unit,
                    var long: (recentDto: RecentDto) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var data = arrayListOf<RecentDto>()

    fun setData(recents: List<RecentDto>?) {
        data.clear()
        if (recents != null) {
            data.addAll(recents)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> LocVH(LayoutInflater.from(parent.context).inflate(R.layout.btn_current, parent, false))
            else -> VH(LayoutInflater.from(parent.context).inflate(R.layout.item_recent, parent, false))
        }

    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> 0
            else -> 1
        }
    }


    override fun getItemCount() = data.size + 1


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VH) {
            holder.bind(data[position - 1])
        }
        if (holder is LocVH) {
            holder.bind()
        }
    }


    inner class LocVH(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            itemView.setOnClickListener { current() }
        }

    }

    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(recentDto: RecentDto) {
            itemView.setOnClickListener { click(recentDto) }
            itemView.setOnLongClickListener { long(recentDto);true }
            itemView.tvTitle_.text = recentDto.point?.title
            itemView.tvSubtitle.text = recentDto.point?.subtitle
        }

    }

}