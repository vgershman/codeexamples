package app.tomato.handlebars.ui.widgets

import android.arch.lifecycle.Observer
import android.content.Intent
import android.provider.MediaStore
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import app.tomato.handlebars.data.Track
import app.tomato.handlebars.data.state.StateLive
import app.tomato.handlebars.data.state.track_recording_state.*
import app.tomato.handlebars.data.widget.TrackStatusLiveData
import kotlinx.android.synthetic.main.widget_tool.*


/**
 * Created by user on 03.03.18.
 */

class CameraWidget : ToolWidget() {

    override fun icon() = R.drawable.ic_camera_alt_black_48px

    override fun setup() {

    }

    override fun setupListeners() {
        view?.setOnClickListener { startCamera() }
    }

    private fun startCamera() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(activity?.packageManager) != null) {
            activity?.startActivity(takePictureIntent)
        }

    }

    override fun title() = "Camera"

}

class TrackWidget : ToolWidget() {

    override fun setup() {
        StateLive.observe(this, Observer { bindState(it) })
    }

    private fun bindState(state: TrackRecordingState?) {
        when (state) {
            is Paused -> {
                ivCnt.setImageResource(R.drawable.ic_fiber_manual_record_black_48px)
                tvTtl_.text = "Paused"
            }
            is Autopaused -> {
                ivCnt.setImageResource(R.drawable.ic_fiber_manual_record_gray_48px)
                tvTtl_.text = "Paused(auto)"
            }
            is Recording -> {
                ivCnt.setImageResource(R.drawable.ic_pause_black_24px)
                tvTtl_.text = "Recording"
            }
            is Stopped -> {
                ivCnt.setImageResource(R.drawable.ic_fiber_manual_record_black_48px)
                tvTtl_.text = "Stopped"
            }
            else -> {

            }
        }
    }

    override fun setupListeners() {
        view?.setOnClickListener { StateLive.buttonTap() }
        view?.setOnLongClickListener {
            if (StateLive.value is Recording) {
                editTrack()
            }
            StateLive.buttonLongTap()
            true
        }
    }

    private fun editTrack() {
        go(MyApp.Screens.SAVE_TRACK, MyApp.Containers.SIMPLE)
    }


    override fun icon() = R.drawable.ic_fiber_manual_record_black_48px

    override fun title() = "Track"

}

