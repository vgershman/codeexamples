package app.tomato.basic.help

import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StrikethroughSpan
import android.text.style.TypefaceSpan


class SpanString : SpannableStringBuilder() {


    fun appendLight(text: CharSequence): SpanString {
        val start = length
        append(text)
        setSpan(TypefaceSpan("sans-serif-light"), start, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        return this
    }

    fun appendRegular(text: CharSequence): SpanString {
        val start = length
        append(text)
        setSpan(TypefaceSpan("sans-serif-regular"), start, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        return this
    }

    fun appendMedium(text: CharSequence): SpanString {
        val start = length
        append(text)
        setSpan(TypefaceSpan("sans-serif-medium"), start, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        return this
    }

    fun appendStriked(text: CharSequence): SpanString {
        val start = length
        append(text)
        setSpan(StrikethroughSpan(), start, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        return this
    }

    fun appendColored(text: CharSequence, color: Int): SpanString {
        val start = length
        append(text)
        setSpan(ForegroundColorSpan(color), start, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        setSpan(TypefaceSpan("sans-serif-regular"), start, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        return this
    }

    override fun toString(): String {
        return super.toString()
    }
}
