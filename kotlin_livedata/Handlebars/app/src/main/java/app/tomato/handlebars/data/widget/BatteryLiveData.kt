package app.tomato.handlebars.data.widget

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager

@SuppressLint("StaticFieldLeak")
/**
 * Created by user on 05.03.18.
 */

object BatteryLiveData : LiveData<Int>() {

    var context: Context? = null

    private val changesReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent) {
            update(intent)
        }
    }

    private fun update(battery: Intent) {
        val level = battery?.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
        val scale = battery?.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
        val batteryPct = level / scale.toFloat() * 100
        value = Math.round(batteryPct)
    }

    override fun onActive() {
        super.onActive()
        if (context != null) {
            update(context!!.registerReceiver(changesReceiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED)))
        }
    }

    override fun onInactive() {
        super.onInactive()
        context?.unregisterReceiver(changesReceiver)
    }


}