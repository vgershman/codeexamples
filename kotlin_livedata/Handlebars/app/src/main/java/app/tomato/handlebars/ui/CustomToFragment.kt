package app.tomato.handlebars.ui

import android.arch.lifecycle.Observer
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.R
import app.tomato.handlebars.data.SQManager
import app.tomato.handlebars.data.SettleDto
import app.tomato.handlebars.data.SettlementsManager
import kotlinx.android.synthetic.main.fragment_custom_to.*
import kotlinx.android.synthetic.main.item_settle.view.*

/**
 * Created by user on 17.03.18.
 */

class CustomToFragment : BasicFragment() {

    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        //btnClear.setOnClickListener { etSearch.setText("") }
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = SettlementsAdapter({ onSelect(it) })
        SettlementsManager.observe(this, Observer { (rv.adapter as SettlementsAdapter).setData(it) })
        SettlementsManager.search(context, null)
        etSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length ?: 0 > 2) {
                    SettlementsManager.search(context, s.toString())
                } else {
                    SettlementsManager.search(context, null)
                }
            }
        })
    }

    private fun onSelect(settle: SettleDto) {
        SQManager.get(context!!).setSettlement(settle)
        onBackPressed()
    }

    override val containerView = R.layout.fragment_custom_to

}

class SettlementsAdapter(var click: (settle: SettleDto) -> Unit) : RecyclerView.Adapter<SettlementsAdapter.VH>() {

    private var data = arrayListOf<SettleDto>()

    fun setData(settlements: List<SettleDto>?) {
        data.clear()
        if (settlements != null) {
            data.addAll(settlements)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent.context).inflate(R.layout.item_settle, parent, false))


    override fun getItemCount() = data.size


    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(data[position])
    }


    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(settleDto: SettleDto) {
            itemView.setOnClickListener { click(settleDto) }
            itemView.tvTitle.text = settleDto.title
            itemView.tvDistance.text = String.format("%.2f", (settleDto.distance.div(1000))) + " км"
        }

    }

}