package app.tomato.basic.ui.basic

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import app.tomato.basic.help.ScreenNavigator
import app.tomato.basic.help.ScreenNavigator.navigate
import app.tomato.handlebars.R
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


abstract class BasicFragmentActivity : AppCompatActivity() {
//AppCompatActivity() {

    val fragmentContainer = R.id.fragment_container

    open val defaultScreen: Class<out BasicFragment>? = null

    var currentFragment: BasicFragment? = null
        internal set

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onBackPressed() {
        if (currentFragment == null) {
            proceedBackPressed()
            return
        }
        if (currentFragment!!.onFragmentBackPressed()) {
            proceedBackPressed()
        }
    }

    private fun proceedBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            super.onBackPressed()
        } else {
            finish()
        }
    }

    fun go(screen: Class<out BasicFragment>?, container: Class<out BasicFragmentActivity>? = null, finish: Boolean = false, force: Boolean = false, result: Int? = null, params: HashMap<String, Any?>? = null, args: Bundle? = null, from: BasicFragment? = null, nostack: Boolean = false, add: Boolean = false) {
        navigate(this, screen, container, finish, force, result, params, args, from, nostack, add)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.addOnBackStackChangedListener {
            var fragment = supportFragmentManager.findFragmentById(fragmentContainer)
            if (fragment != null) {
                currentFragment = fragment as BasicFragment
            }
        }
        preInit()
        setContentView(contentViewId)
        init()
        openFirstScreen()
    }

    open fun preInit() {


    }

    protected open fun openFirstScreen() {
        var firstScreen = intent.getSerializableExtra(ScreenNavigator.EXTRA) as Class<out BasicFragment>?
        if (firstScreen == null) {
            firstScreen = defaultScreen
        }
        if (firstScreen != null) {
            go(screen = firstScreen, args = intent.extras)
        }
    }

    abstract fun init()


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (currentFragment != null) {
            currentFragment!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    abstract val contentViewId: Int


}
