package app.tomato.handlebars.data.widget

import android.arch.lifecycle.LiveData
import android.os.Handler
import android.os.Looper
import app.tomato.handlebars.data.LocationLiveManager

import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator
import com.luckycatlabs.sunrisesunset.dto.Location
import java.math.BigDecimal
import java.util.*

/**
 * Created by user on 01.03.18.
 */
object SunsetLiveData : LiveData<String>() {

    private var timer: Timer? = null


    override fun onActive() {
        super.onActive()
        timer?.cancel()
        timer = Timer()
        timer?.schedule(object : TimerTask() {

            override fun run() {
                Handler(Looper.getMainLooper()).post {
                    calculate()
                }

            }

        }, 0, 5 * 60 * 1000L)

    }

    private fun calculate() {
        val originalLocation = LocationLiveManager.value
        if (originalLocation != null) {
            val location = Location(originalLocation.latitude, originalLocation.longitude)
            val calculator = SunriseSunsetCalculator(location, TimeZone.getDefault())
            value = calculator.getOfficialSunsetForDate(Calendar.getInstance())
        }
    }
}

object SunriseLiveData : LiveData<String>() {


    private var timer: Timer? = null

    override fun onActive() {
        super.onActive()
        timer?.cancel()
        timer = Timer()
        timer?.schedule(object : TimerTask() {

            override fun run() {
                Handler(Looper.getMainLooper()).post {
                    calculate()
                }

            }

        }, 0, 5 * 1000L)

    }

    private fun calculate() {
        val originalLocation = LocationLiveManager.value
        if (originalLocation != null) {
            val location = Location(originalLocation.latitude, originalLocation.longitude)
            val calculator = SunriseSunsetCalculator(location, TimeZone.getDefault())
            value = calculator.getOfficialSunriseForDate(Calendar.getInstance())
        }

    }
}

object SunsetSunriseLiveData : LiveData<Pair<String, String>>() {

    init {
        value = Pair("Sunset/Sunrise", "--:--")
    }

    private var timer: Timer? = null

    override fun onActive() {
        super.onActive()
        timer?.cancel()
        timer = Timer()
        timer?.schedule(object : TimerTask() {

            override fun run() {
                Handler(Looper.getMainLooper()).post {
                    calculate()
                }

            }

        }, 0, 5 * 1000L)

    }

    private fun calculate() {
        val originalLocation = LocationLiveManager.value
        if (originalLocation != null) {
            val location = Location(originalLocation.latitude, originalLocation.longitude)
            value = SunsetSunriseLiveDataCalculator.calculate(location, Calendar.getInstance())
        }
    }
}

object SunsetSunriseLiveDataCalculator {
    fun calculate(location: Location, now: Calendar): Pair<String, String> {
        val calculator = SunriseSunsetCalculator(location, now.timeZone)

        val tomorrow = now.clone() as Calendar
        tomorrow.add(Calendar.DATE, 1)

        val events = mutableListOf<Pair<String, Calendar?>>()
        events.add(Pair<String, Calendar?>("Sunrise", calculator.getOfficialSunriseCalendarForDate(now)))
        events.add(Pair<String, Calendar?>("Sunset", calculator.getOfficialSunsetCalendarForDate(now)))
        events.add(Pair<String, Calendar?>("Sunrise", calculator.getOfficialSunriseCalendarForDate(tomorrow)))
        events.add(Pair<String, Calendar?>("Sunset", calculator.getOfficialSunsetCalendarForDate(tomorrow)))

        val possibleEvents = events.filter { it.second != null }.map { Pair<String, Calendar>(it.first, it.second!!) }

        val futureEvents = possibleEvents.filter { it.second.timeInMillis > now.timeInMillis }.toMutableList()
        futureEvents.sortBy { it.second.timeInMillis }

        if (futureEvents.isNotEmpty()) {
            return Pair(futureEvents[0].first, getLocalTimeAsString(futureEvents[0].second))
        } else {
            return Pair(
                    if (location.latitude > BigDecimal(0)) (
                            if (listOf(2, 3, 4, 5, 6, 7).contains(now.get(Calendar.MONTH))) "Sunset" else "Sunrise"
                            ) else (
                            if (listOf(2, 3, 4, 5, 6, 7).contains(now.get(Calendar.MONTH))) "Sunrise" else "Sunset"
                            ),
                    "--:--"
            )
        }
    }

    private fun getLocalTimeAsString(localTime: Calendar): String {
        val hour = localTime.get(Calendar.HOUR_OF_DAY)
        val minutes = localTime.get(Calendar.MINUTE)

        val minuteString = if (minutes < 10) "0" + minutes.toString() else minutes.toString()
        val hourString = if (hour < 10) "0" + hour.toString() else hour.toString()
        return "$hourString:$minuteString"
    }

}
