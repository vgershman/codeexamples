package app.tomato.handlebars.data

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import app.tomato.basic.data.ToastCallback
import app.tomato.handlebars.MyApp
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.AutocompletePredictionBuffer
import com.google.android.gms.location.places.PlaceBuffer
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.SphericalUtil
import io.realm.Realm
import io.realm.Sort
import retrofit2.Call
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


@SuppressLint("StaticFieldLeak")
/**
 * Created by user on 14.03.18.
 */

object SQManager {
    private var instance: SearchQueryManager? = null

    fun get(context: Context): SearchQueryManager {
        if (instance == null) {
            instance = SearchQueryManager(context)
        }
        return instance!!
    }
}

class SearchQuery(var from: Point? = null, var to: Point? = null, var customPoint: Point? = null, var speed: Int = 20, var checkbox: String? = null, var custom_to: String? = null) {
    fun speedInMs(): Float {
        return speed * 5f / 18f
    }
}

class SearchQueryManager(var context: Context) : LiveData<SearchQuery>() {

    override fun onActive() {
        super.onActive()
        if (value == null) {
            val speed = PreferenceManager.getDefaultSharedPreferences(context).getInt("speed", 20)
            value = SearchQuery(speed = speed)
        }
    }

    fun setMyLocation() {
        setPoint(true, Point(LocationLiveManager!!.value?.latitude, LocationLiveManager!!.value?.longitude, "My Location"))
    }


    fun setPoint(from: Boolean, point: Point) {
        if (from) {
            value?.from = point
        } else {
            value?.to = point
        }
        value = value
    }

    fun swap() {
        if (value != null) {
            val old = value?.from
            value?.from = value?.to
            value?.to = old
            value = value
        }
    }

    fun setSpeed(speed: Int) {
        value?.speed = speed
        value = value
    }

    fun setSettlement(settle: SettleDto) {
        value?.customPoint = Point(settle.location[0], settle.location[1], settle.title)
        value?.custom_to = settle.key
        value = value
    }


    fun setCheck(hintDto: HintDto?) {
        if (hintDto != null) {
            if (hintDto.type == "custom_to") {
                value?.checkbox = MyApp.Config.YANDEX_GEOCODE
                value?.custom_to = null //MyApp.Config.YANDEX_GEOCODE
            } else {
                value?.checkbox = hintDto.name
                value?.custom_to = null
            }
        } else {
            value?.checkbox = null
            value?.custom_to = null
        }
        value = value
    }

    override fun onInactive() {
        super.onInactive()
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("speed", value?.speed
                ?: 20).apply()
    }


}

object SearchResultsManager : LiveData<List<TrackDto>>() {

    val timeFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

    var current: Call<List<TrackDto>?>? = null


    fun search(context: Context, searchQuery: SearchQuery?, callback: () -> Unit) {

        val fromIsValid = searchQuery?.from != null
        val customIsValid = searchQuery?.checkbox == MyApp.Config.YANDEX_GEOCODE && searchQuery.custom_to != null
        val toIsValid = searchQuery?.checkbox != MyApp.Config.YANDEX_GEOCODE && searchQuery?.to != null

        if (fromIsValid && (toIsValid || customIsValid)) {
            var map: HashMap<String, Any>? = hashMapOf()
            if (searchQuery?.checkbox != null) {
                if (searchQuery.custom_to != null) {
                    map = hashMapOf(searchQuery.checkbox!! to searchQuery.custom_to!!)
                } else {
                    map = hashMapOf(searchQuery.checkbox!! to 1)
                }
            }
            var toPoint = searchQuery?.to
            if (searchQuery?.custom_to != null) {
                toPoint = null
            }
            current = Rest.rest().search(searchQuery?.from!!.getLocationQuery(), toPoint?.getLocationQuery(), timeFormat.format(Date(System.currentTimeMillis())), searchQuery.speedInMs(), map)
            callback()
            current?.enqueue(object : ToastCallback<List<TrackDto>?>(context) {
                override fun onSuccess(response: List<TrackDto>?) {
                    value = response ?: arrayListOf()
                }

                override fun onError(message: String?) {
                    value = arrayListOf()
                }
            })
        } else {
            value = null
        }
    }

    override fun onInactive() {
        super.onInactive()
        current?.cancel()
        value = null
    }
}

object SettlementsManager : LiveData<List<SettleDto>>() {

    override fun onInactive() {
        super.onInactive()
        value = null
    }

    fun search(context: Context?, query: String?) {
        val location = LocationLiveManager.value
        if (context == null || location == null) {
            return
        }
        val locString = "${location.latitude},${location.longitude}"
        Rest.rest().settlements(locString, query).enqueue(object : ToastCallback<List<SettleDto>?>(context) {
            override fun onSuccess(response: List<SettleDto>?) {
                value = response
            }

            override fun onError(message: String?) {
                value = null
                super.onError(message)
            }
        })
    }
}

object HintsManager : LiveData<List<HintDto>>() {

    fun updateHints(context: Context?, locString: String?) {
        if (context == null || locString == null) {
            value = arrayListOf()
            return
        }
        Rest.rest().hints(locString).enqueue(object : ToastCallback<List<HintDto>>(context!!) {
            override fun onSuccess(response: List<HintDto>) {
                value = response
            }

        })
    }

    override fun onInactive() {
        super.onInactive()
        value = null
    }
}

class GeocodeManager(var context: Context) : LiveData<AutocompletePredictionBuffer>(), GoogleApiClient.ConnectionCallbacks {

    var client: GoogleApiClient? = null

    override fun onActive() {
        super.onActive()
        client = GoogleApiClient.Builder(context).addApi(Places.GEO_DATA_API).addConnectionCallbacks(this).build()
        client!!.connect()
    }

    override fun onConnected(p0: Bundle?) {
        Log.i("googleclient", "connected")
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.i("googleclient", "suspended")
    }

    fun search(query: String) {
        val bounds = toBounds(LatLng(LocationLiveManager.value?.latitude
                ?: 0.0, LocationLiveManager.value?.longitude ?: 0.0), 15.0 * 1000)
        val result = Places.GeoDataApi.getAutocompletePredictions(client, query, bounds, AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_GEOCODE).build())
        result.setResultCallback({ value = it }, 1, TimeUnit.MINUTES)
    }

    fun toBounds(center: LatLng, radiusInMeters: Double): LatLngBounds {
        val distanceFromCenterToCorner = radiusInMeters * Math.sqrt(2.0)
        val southwestCorner = SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 225.0)
        val northeastCorner = SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 45.0)
        return LatLngBounds(southwestCorner, northeastCorner)
    }

    override fun onInactive() {
        super.onInactive()
        client?.disconnect()

    }

    fun retrievePlace(placeId: String?, callback: (result: PlaceBuffer) -> Unit) {
        if (placeId != null) {
            Places.GeoDataApi.getPlaceById(client, placeId).setResultCallback { callback(it) }
        }
    }
}

object RecentManager : LiveData<List<RecentDto>>() {

    override fun onActive() {
        super.onActive()
        update()
    }

    fun recent(point: Point?, query: String) {
        RealmUtil.transaction {
            val query_ = query.trimEnd()
            var recent = Realm.getDefaultInstance().where(RecentDto::class.java).equalTo("query", query_).findFirst()
            if (recent != null) {
                recent.timestamp = System.currentTimeMillis()
            } else {
                recent = RecentDto(query_, System.currentTimeMillis(), point)
                Realm.getDefaultInstance().copyToRealmOrUpdate(recent)
            }
        }
        update()
    }

    fun update() {
        value = Realm.getDefaultInstance().where(RecentDto::class.java).sort("timestamp", Sort.DESCENDING).findAll().take(20)
    }

    fun clear() {
        RealmUtil.transaction {
            Realm.getDefaultInstance().where(RecentDto::class.java).sort("timestamp", Sort.DESCENDING).findAll().deleteAllFromRealm()
        }
        update()
    }

    fun remove(it: RecentDto) {
        RealmUtil.transaction {
            it.deleteFromRealm()
        }
        update()
    }

    override fun onInactive() {
        super.onInactive()
        value = null
    }

}