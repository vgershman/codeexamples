package app.tomato.handlebars.data

import android.annotation.SuppressLint
import android.os.AsyncTask

/**
 * Created by user on 02.04.18.
 */

@SuppressLint("StaticFieldLeak")
object Utils {

    fun async(background: () -> Any?, result: (result: Any?) -> Unit) {


        object : AsyncTask<Void, Void, Any?>() {

            override fun doInBackground(vararg params: Void?): Any? {
                return background()
            }

            override fun onPostExecute(result: Any?) {
                super.onPostExecute(result)
                result(result)
            }

        }.execute()

    }

}