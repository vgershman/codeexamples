package app.tomato.handlebars.data


import android.support.annotation.Keep
import app.tomato.basic.data.RestApi
import app.tomato.handlebars.MyApp.Config.Companion.API_HOST


import okhttp3.Interceptor
import okhttp3.Request
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by user on 06.06.17.
 */
@Keep
interface API {

    @POST("guest/new")
    fun guestNew(): Call<Guest>

    @GET("me")
    fun me(): Call<Me>

    @GET("routes")
    fun listMyRoutes(): Call<List<Route>>

    @GET("tracks/{id}/points.json.gz")
    fun getTrack(@Path("id") trackId: Int): Call<List<LocationDto>>

    @GET("router?profile=road")
    fun search(@Query("from", encoded = true) from: String, @Query("to", encoded = true) to: String?, @Query("start") start: String, @Query("speed") speed: Float,
               @QueryMap map: HashMap<String, Any>? = null): Call<List<TrackDto>?>

    @GET("router/hints")
    fun hints(@Query("location", encoded = true) from: String): Call<List<HintDto>>

    @GET("router/rasp.yandex.net/settlements")
    fun settlements(@Query("location") location: String, @Query("q") query: String? = null): Call<List<SettleDto>?>

    @GET("responses/{id}")
    fun getDelayResponse(@Path("id") key: String): Call<List<TrackDto>?>
}

object Rest : RestApi<API>() {
    override fun init() {
        host = API_HOST
        clazz = API::class.java
        addInterceptor(Interceptor { chain ->
            val newRequestBuilder: Request.Builder
            val request = chain.request()
            newRequestBuilder = request.newBuilder()
            newRequestBuilder.addHeader("Content-Type", "application/json")
            if (AuthManager.isAuth()) {
                newRequestBuilder.addHeader("X-API-Key", AuthManager.accessToken)
            }
            chain.proceed(newRequestBuilder.build())
        })

    }

}

