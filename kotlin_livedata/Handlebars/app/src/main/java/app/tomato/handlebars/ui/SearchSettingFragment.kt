package app.tomato.handlebars.ui

import android.arch.lifecycle.Observer
import android.widget.Toast
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.R
import app.tomato.handlebars.data.SQManager
import kotlinx.android.synthetic.main.fragment_search_settings.*

/**
 * Created by user on 14.03.18.
 */
class SearchSettingFragment : BasicFragment() {

    override val hasToolbar = true

    override val titleId = R.string.ttl_search_settings

    override fun initViews() {
        toolbar?.inflateMenu(R.menu.ok_menu)
        toolbar?.setOnMenuItemClickListener {
            if (it.itemId == R.id.btn_ok) {
                onOK()
            };true
        }
        SQManager.get(context!!).observe(this, Observer {
            etSpeed.setText(it?.speed.toString())
        })
    }

    private fun onOK() {
        val text = etSpeed.text.toString()
        val speed = text.toIntOrNull()
        if (speed != null) {
            SQManager.get(context!!).setSpeed(speed)
            onBackPressed()
        } else {
            Toast.makeText(context, "Неверный формат", Toast.LENGTH_SHORT).show()
        }
    }

    override val containerView = R.layout.fragment_search_settings

}