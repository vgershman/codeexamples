package app.tomato.handlebars.data.state

import android.arch.lifecycle.LiveData
import android.location.Location
import android.os.Handler
import android.os.Looper
import app.tomato.handlebars.data.state.track_recording_state.Recording
import app.tomato.handlebars.data.state.track_recording_state.Stopped
import app.tomato.handlebars.data.state.track_recording_state.TrackRecordingState
import app.tomato.handlebars.data.widget.AverageSpeedListeners
import app.tomato.handlebars.data.widget.CurrentSpeedLiveData
import app.tomato.handlebars.data.widget.DistanceLiveData
import java.util.*

/**
 * Created by themylogin on 3/14/18.
 */
object StateLive : LiveData<TrackRecordingState>() {

    private var timer: Timer? = null
    private var handler: Handler? = null

    init {
        value = Stopped()
    }

    fun setCurrentState(state: TrackRecordingState) {
        value = state
    }

    override fun onActive() {
        super.onActive()
        handler = Handler(Looper.getMainLooper())
        timer?.cancel()
        timer = Timer()
        timer?.schedule(object : TimerTask() {

            override fun run() {
                handler?.post {
                    tick(System.currentTimeMillis())
                }
            }

        }, 0L, 1000L)
    }

    override fun onInactive() {
        super.onInactive()
        timer?.cancel()
    }

    const val maxLocationInterval = 15000L

    const val autopauseEnabled = true
    const val autopausePauseThresholdTime = 15000L
    const val autopausePauseThresholdSpeed = 4.0f / 3.6f
    const val autopauseResumeThresholdTime = 15000L
    const val autopauseResumeThresholdSpeed = 5.0f / 3.6f


    val recentPoints: MutableList<RecentPoint> = mutableListOf()
    val historyPoints: MutableList<HistoryPoint> = mutableListOf()


    fun tick(time: Long) {
        if (this.recentPoints.count() > 0) {
            if (time - this.recentPoints.last().time > this.maxLocationInterval) {
                CurrentSpeedLiveData.setSpeed(null)
            }
        }
        value?.tick(time)
    }

    fun notifyLocation(location: Location) {
        location.time = System.currentTimeMillis() //emulator fix

        CurrentSpeedLiveData.setSpeed(_calculateCurrentSpeed(location))
        this.recentPoints.add(RecentPoint(location.time, location, CurrentSpeedLiveData.value))

        val minRecentPointTime = location.time - maxOf(
                this.maxLocationInterval,
                this.autopausePauseThresholdTime,
                this.autopauseResumeThresholdTime)
        this.recentPoints.removeAll { it.time < minRecentPointTime }


        value?.notifyLocation(location)

        AverageSpeedListeners.updateAll()
        DistanceLiveData.update()
        DistanceLiveData.setDistance(calculateTotalDistance())
    }

    fun buttonTap() {
        value?.buttonTap()
    }

    fun buttonLongTap() {
        value?.buttonLongTap()
    }

    fun _calculateCurrentSpeed(location: Location): Float? {
        if (this.recentPoints.count() > 0) {
            val recentPoint = recentPoints.last()

            val timeDiff = location.time - recentPoint.time

            if (timeDiff > this.maxLocationInterval) {
                return null
            } else {
                return location.distanceTo(recentPoint.location) / (timeDiff / 1000.0f)
            }
        } else {
            return null
        }
    }

    fun calculateAverageSpeed(lastMilliseconds: Long?, lastMeters: Long?, excludeInactive: Boolean): Float? {
        val points = when {
            lastMilliseconds != null -> this.historyPoints.filter { it.time > lastMilliseconds }
            lastMeters != null -> this.historyPoints.sortedByDescending { it.time }
            else -> this.historyPoints
        }

        var distance = 0.0f
        var time = 0L

        if (lastMeters == null) {
            for (i in 0 until points.size - 1) {
                val point = points[i]
                val nextPoint = points[i + 1]
                if (!point.exclude) {
                    distance += point.location.distanceTo(nextPoint.location)
                }
                if (!point.exclude || !excludeInactive) {
                    time += nextPoint.time - point.time
                }
            }
        } else {
            for (i in 1 until points.size) {
                val point = points[i]
                val prevPoint = points[i - 1]
                if (!prevPoint.exclude) {
                    distance += prevPoint.location.distanceTo(point.location)
                    if (distance > lastMeters) {
                        break
                    }
                }
                if (!prevPoint.exclude || !excludeInactive) {
                    time += point.time - prevPoint.time
                }
            }
        }

        val time_ = Math.abs((time))
        if (time_ > 0) {
            return distance / (time_ / 1000.0f)
        } else {
            return null
        }
    }

    fun calculateTotalDistance(): Float? {
        if (this.historyPoints.count() > 1) {
            var distance = 0.0f
            this.historyPoints.forEachIndexed { index, point ->
                if (index < this.historyPoints.count() - 1) {
                    val nextPoint = this.historyPoints[index + 1]

                    if (!point.exclude) {
                        distance += point.location.distanceTo(nextPoint.location)
                    }
                }
            }
            return distance
        } else {
            return null
        }
    }

    fun calculateTotalTime(time: Long, excludeInactive: Boolean): Long? {
        if (historyPoints.isEmpty()) {
            return null
        }
        if (historyPoints.size == 1) {
            return time - historyPoints.first().time
        } else {
            var timeDiff = time - this.historyPoints.first().time
            if (excludeInactive) {
                this.historyPoints.forEachIndexed { index, point ->
                    if (index < this.historyPoints.count() - 1) {
                        val nextPoint = this.historyPoints[index + 1]

                        if (point.exclude) {
                            timeDiff -= nextPoint.time - point.time
                        }
                    }
                }
                val last = historyPoints.lastOrNull()
                if (last?.exclude == true) {
                    timeDiff-= time - last.time
                }
            }

            return timeDiff
        }

    }

    fun onEditBackPressed() {
        StateLive.value = Recording()
    }

    fun onSaveOrDeleteTrack() {
        StateLive.value = Stopped()
    }
}
