package app.tomato.handlebars.ui

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.R
import app.tomato.handlebars.data.*
import app.tomato.handlebars.data.widget.CurrentRouteManager
import app.tomato.handlebars.data.widget.TrackTimeLiveData
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.mapzen.tangram.LngLat
import com.mapzen.tangram.MapController
import com.mapzen.tangram.Marker
import com.mapzen.tangram.SceneError
import com.mapzen.tangram.geometry.Polyline
import kotlinx.android.synthetic.main.fragment_route.*

/**
 * Created by user on 13.03.18.
 */
class RouteFragment : BasicFragment(), MapController.SceneLoadListener {

    private val pointStyle = "{ style: 'lines', color: '#fc00', width: [[2,5px], [20,100px]], order: 2000, collide: false }"
    //  val style = "{ style: lines, interactive: true, color: red, width: [[0,1px], [20,100px]], order: 5000 }"
    private val markerStyle = "{ style: 'points', color: 'white', size: [20px, 20px], order: 2000, collide: false }"

    override fun onSceneReady(sceneId: Int, sceneError: SceneError?) {

    }

    var increasing: Boolean? = null

    var route: Route? = null
    var track: TrackDto? = null
    override val hasToolbar = true

    override fun initViews() {
        val screenWidth = screenWidth(context!!).toInt()
        map.layoutParams = FrameLayout.LayoutParams(screenWidth, screenWidth)
        RoutesLoadingManager.observe(this, Observer { bindLoading() })
    }

    private fun bindLoading() {
        if (track?.id != null) {
            if (RoutesLoadingManager.isLoading(track!!.id!!)) {
                progressBar.visibility = View.VISIBLE
            } else {
                progressBar.visibility = View.GONE
            }
        } else {
            progressBar.visibility = View.GONE
        }

    }

    override fun initProperties() {
        super.initProperties()
        val routeId = arguments!!["rid"] as Int? ?: -1
        track = arguments!!["track"] as TrackDto?
        initMap()
        route = RoutesManager.getRouteById(routeId)
        if (route != null) {
            track = route?.track
            setTitle(route!!.name!!)
        }
        if (track != null) {
            bindTrack()
            RoutesManager.syncRoute(context!!, track!!.id, { bindTrack(it) })
        } else {
            Toast.makeText(context, "Что-то пошло не так", Toast.LENGTH_SHORT).show()
            activity?.finish()
            return
        }
    }

    private fun bindTrack(points: List<LocationDao>) {
        btnGo.visibility = View.VISIBLE
        btnGo.setOnClickListener {
            setActive()
        }

        val formattedPoints = arrayListOf<LngLat>()

        val bounds = LatLngBounds.Builder()
        points.forEach {
            formattedPoints.add(LngLat(it.lon ?: 0.toDouble(), it.lat ?: 0.toDouble()))
            bounds.include(LatLng(it.lat ?: 0.toDouble(), it.lon ?: 0.toDouble()))
        }
        val bounds_ = bounds.build()

        //val data = mapController?.addDataLayer("line")
        //data?.addPolyline(formattedPoints, hashMapOf("color" to "red", "order" to "5000", "width" to "10px"))
        val polyline = Polyline(formattedPoints, null)
        val marker = mapController?.addMarker()
        marker?.setPolyline(polyline)
        marker?.setStylingFromString(pointStyle)

        mapController?.position = (LngLat(bounds_.center.longitude, bounds_.center.latitude))
        mapController?.zoom = 10f


        val startPoint = LngLat(points[0].lon!!, points[0].lat!!)
        var markerStart: Marker? = null
        if (markerStart == null) {
            markerStart = mapController?.addMarker()
            markerStart?.setStylingFromString(markerStyle)
            markerStart?.setDrawable(R.drawable.ic_start)
        }
        markerStart?.setPoint(startPoint)


        val endPoint = LngLat(points[points.size - 1].lon!!, points[points.size - 1].lat!!)
        var markerEnd: Marker? = null
        if (markerEnd == null) {
            markerEnd = mapController?.addMarker()
            markerEnd?.setStylingFromString(markerStyle)
            markerEnd?.setDrawable(R.drawable.ic_end)
        }

        markerEnd?.setPoint(endPoint)
        Handler().postDelayed({ adjustZoom(bounds_) }, 50)
    }

    private fun adjustZoom(bounds: LatLngBounds) {
        try {
            val ne = bounds.northeast
            val screenPoint = mapController?.lngLatToScreenPosition(LngLat(ne.longitude, ne.latitude))
            if (screenPoint != null) {
                if (screenPoint.x < map.width * 0.1f || screenPoint.y < map.height * 0.1f) {
                    if (increasing == true) {
                        return
                    }
                    mapController?.zoom = (mapController?.zoom ?: 10f) - 0.05f
                    increasing = false
                    Handler().postDelayed({ adjustZoom(bounds) }, 5)
                    return
                }
                if (screenPoint.x > map.width * 0.15f || screenPoint.y > map.height * 0.15f) {
                    if (increasing == false) {
                        return
                    }
                    increasing = true
                    mapController?.zoom = (mapController?.zoom ?: 10f) + 0.05f
                    Handler().postDelayed({ adjustZoom(bounds) }, 5)
                    return
                }
            }
        } catch (ex: Exception) {

        }


    }

    private fun setActive() {
        if (CurrentRouteManager.value == route?.track?.id.toString()) {
            Toast.makeText(context, "Already active", Toast.LENGTH_LONG).show()
        } else {
            if (CurrentRouteManager.value == null) {
                CurrentRouteManager.setOn(track?.id.toString())
                activity?.finish()
                Toast.makeText(context, "Route active", Toast.LENGTH_LONG).show()
            } else {
                AlertDialog.Builder(context)
                        .setMessage("Активен другой маршрут. Заменить?")
                        .setPositiveButton("Да", { d, _ -> d.cancel(); CurrentRouteManager.setOn(track?.id.toString());activity?.finish() })
                        .setNegativeButton("Отмена", { d, _ -> d.cancel() })
                        .create().show()
            }
        }
    }

    private fun bindTrack() {
        tvInfo.text = TrackTimeLiveData.format(track?.total_time ?: 0f)
        tvDesc.text = Html.fromHtml(track!!.description)
    }

    override val containerView = R.layout.fragment_route
    private var settings = "osm.yaml"


    private fun initMap() {
        mapController = map?.getMap(this)
        mapController?.loadSceneFile("asset:///$settings")
    }


    var mapController: MapController? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        map?.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        map?.onResume()
    }

    override fun onPause() {
        super.onPause()
        map?.onPause()

    }


    override fun onDestroy() {
        super.onDestroy()
        map?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map?.onLowMemory()
    }

}