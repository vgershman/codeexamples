package app.tomato.handlebars

import android.app.Application
import app.tomato.hadlebars.ui.SplashFragment
import app.tomato.handlebars.MyApp.Config.Companion.MILE
import app.tomato.handlebars.MyApp.Config.Companion.YANDEX_KEY
import app.tomato.handlebars.data.Category
import app.tomato.handlebars.data.WidgetOption
import app.tomato.handlebars.ui.*
import app.tomato.handlebars.ui.container.DashboardActivity
import app.tomato.handlebars.ui.container.FullscreenSimpleActivity
import app.tomato.handlebars.ui.container.SimpleActivity
import app.tomato.handlebars.ui.dashboard.EditDashboardFragment
import app.tomato.handlebars.ui.dashboard.EditDashboardsFragment
import app.tomato.handlebars.ui.widgets.MapWidget
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import io.realm.Realm
import io.realm.RealmConfiguration
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import java.util.*


class MyApp : Application() {


    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        Realm.setDefaultConfiguration(RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build())
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setFontAttrId(R.attr.fontPath)
                .build())
        YandexMetrica.activate(applicationContext, YandexMetricaConfig.newConfigBuilder(YANDEX_KEY).build())

    }

    companion object {
        fun isImperial(): Boolean {
            return Locale.getDefault().country in arrayListOf("US", "LR", "MM", "UK", "NZ", "AUS", "HK")//todo check list of imperial countries
        }

        fun metersToLocal(meters: Float): Float {
            if (isImperial()) {
                return meters / 1000f * MILE
            } else {
                return meters / 1000f
            }
        }
    }

    interface Config {
        companion object {

            val MILE = 0.621371f
            val HOST = "https://handlebarsapp.com"
            val API_HOST = "$HOST/api/v1/"
            val EMBEDDED_HOST = "$HOST/security/"
            val WC_GUEST = "fresh-login?sign_up_access_token=<access_token>"
            val WC_USER = "embedded-login?access_token=<access_token>"


            val LARGE_WIDGET_COUNT = 2
            val NORMAL_WIDGET_COUNT = 3
            val MEDIUM_WIDGET_COUNT = 4
            val DEFAULT_ZOOM = 15f
            val YANDEX_KEY = "583b4fbd-24e4-4f9d-abeb-374eec58059a"
            val YANDEX_GEOCODE = "rasp.yandex.net"
        }
    }

    interface WidgetConfig {
        companion object {
            //percent of widget height
            val TITLE_TOP_MARGIN = .05f
            val TITLE_BOTTOM_MARGIN = .08f
            val TITLE_SIZE = .25f
            val VALUE_SIZE = .55f
            val BIG_VALUE_SCALER = .85f
            val METRICS_SIZE = .2f
            val METRICS_LEFT_MARGIN = .05f
            val ICON_SIZE = .65f
            val ICON_MARGIN = .06f
        }
    }

    interface Widgets {
        companion object {
            val WIDGETS = arrayListOf(
                    Category("Time", arrayListOf(
                            WidgetOption("Current Time", "gauge"),
                            WidgetOption("Sunset", "gauge"),
                            WidgetOption("Sunrise", "gauge"),
                            WidgetOption("Sunset/Sunrise", "gauge"))),
                    Category("Ride", arrayListOf(
                            WidgetOption("Start/Stop", "button"))),
                    Category("Speed", arrayListOf(
                            WidgetOption("Current Speed", "gauge"),
                            WidgetOption("Average Speed", "gauge"))),
                    Category("Distance", arrayListOf(
                            WidgetOption("Total Distance", "gauge"))),
                    Category("Ride Time", arrayListOf(
                            WidgetOption("Total Time", "gauge"))),
                    Category("Media", arrayListOf(
                            WidgetOption("Play/Pause", "button"),
                            WidgetOption("Next", "button"),
                            WidgetOption("Prev", "button"))),
                    Category("Tools", arrayListOf(
                            WidgetOption("Camera", "button"))),
                    Category("System", arrayListOf(
                            WidgetOption("Battery", "gauge"))))
            val MAPS = arrayListOf(
                    Category("Maps", arrayListOf(
                            WidgetOption("2D", type = "map")
                    ))
            )
        }
    }

    interface Screens {
        companion object {
            val SPLASH = SplashFragment::class.java
            val EDIT_DASHBOARDS = EditDashboardsFragment::class.java
            val EDIT_DASHBOARD = EditDashboardFragment::class.java
            val SELECT_CATEGORY = SelectCategoryFragment::class.java
            val SELECT_MAP = SelectMapFragment::class.java
            val SELECT_WIDGET = SelectWidgetFragment::class.java
            val MAP_SETTINGS = MapSettings::class.java
            val MAP = MapWidget::class.java
            val SAVE_TRACK = EditTrackFragment::class.java
            val TIME_SETTINGS = TimeSettings::class.java
            val SPEED_SETTINGS = AverageSpeedSettings::class.java
            val WEB = WebFragment::class.java
            val MY_ROUTES = RoutesFragment::class.java
            val ROUTE = RouteFragment::class.java
            val FIND_ROUTE = FindRouteFragment::class.java
            val FROM_TO = NewFromToFragment::class.java
            val CUSTOM_TO = CustomToFragment::class.java
            val SEARCH_SETTINGS = SearchSettingFragment::class.java
        }
    }

    interface Containers {
        companion object {
            val SIMPLE = SimpleActivity::class.java
            val FULLSCREEN = FullscreenSimpleActivity::class.java
            val DASHBOARDS = DashboardActivity::class.java
        }
    }
}

