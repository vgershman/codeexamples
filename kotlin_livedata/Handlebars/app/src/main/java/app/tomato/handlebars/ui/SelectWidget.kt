package app.tomato.handlebars.ui

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import app.tomato.handlebars.data.Category
import kotlinx.android.synthetic.main.fragment_select.*
import kotlinx.android.synthetic.main.item_select.view.*
import java.io.Serializable


/**
 * Created by user on 01.03.18.
 */

class SelectMapFragment : BaseSelectFragment() {

    override fun listData() = MyApp.Widgets.MAPS

    override fun onSelect(it: Selectable) {
        go(MyApp.Screens.SELECT_WIDGET, params = hashMapOf("category" to it,
                "height" to arguments!!["height"], "settings" to arguments!!["settings"]))
    }

    override val titleId = R.string.ttl_select_category

}

class SelectCategoryFragment : BaseSelectFragment() {

    override fun listData() = MyApp.Widgets.WIDGETS

    override fun onSelect(it: Selectable) {
        go(MyApp.Screens.SELECT_WIDGET, params = hashMapOf("category" to it, "settings" to arguments!!["settings"]))
    }

    override val titleId = R.string.ttl_select_category

}

class SelectWidgetFragment : BaseSelectFragment() {
    override fun listData(): List<Selectable> {
        val category = arguments!!["category"] as Category
        setTitle(category.name)
        return category.widgets
    }

    override fun onSelect(it: Selectable) {
        when (it.name()) {
            "2D" -> go(MyApp.Screens.MAP_SETTINGS, params = hashMapOf("widget" to it, "height" to arguments!!["height"], "settings" to arguments!!["settings"]))
            "Total Time" -> go(MyApp.Screens.TIME_SETTINGS, params = hashMapOf("widget" to it, "settings" to arguments!!["settings"]))
            "Average Speed" -> go(MyApp.Screens.SPEED_SETTINGS, params = hashMapOf("widget" to it, "settings" to arguments!!["settings"]))
            else -> {
                activity?.setResult(Activity.RESULT_OK, Intent().putExtra("widget", it))
                activity?.finish()
            }
        }

    }
}

abstract class BaseSelectFragment : BasicFragment() {

    override val hasToolbar = true

    override fun initViews() {
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = SimpleSelectAdapter({ onSelect(it) })
    }

    override fun initProperties() {
        super.initProperties()
        (rv.adapter as SimpleSelectAdapter).setData(listData())
    }

    abstract fun listData(): List<Selectable>

    abstract fun onSelect(it: Selectable)

    override val containerView = R.layout.fragment_select

}

interface Selectable : Serializable {
    fun name(): String
}

class SimpleSelectAdapter(var click: (item: Selectable) -> Unit) : RecyclerView.Adapter<SimpleSelectAdapter.VH>() {

    private var data = ArrayList<Selectable>()

    fun setData(content: List<Selectable>) {
        data.clear()
        data.addAll(content)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            VH(LayoutInflater.from(parent.context).inflate(R.layout.item_select, parent, false))


    override fun getItemCount() = data.size


    override fun onBindViewHolder(holder: VH, position: Int) {
        holder?.bind(data[position])
    }


    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(selectable: Selectable) {
            itemView.tv_name.text = selectable.name()
            itemView.setOnClickListener { click(selectable) }
        }

    }
}