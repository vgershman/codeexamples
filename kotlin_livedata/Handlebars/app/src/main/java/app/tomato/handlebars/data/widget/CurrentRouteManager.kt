package app.tomato.handlebars.data.widget

import android.arch.lifecycle.LiveData

/**
 * Created by user on 20.03.18.
 */

object CurrentRouteManager : LiveData<String>() {


    fun setOn(trackId: String?) {
        value = trackId
    }

}