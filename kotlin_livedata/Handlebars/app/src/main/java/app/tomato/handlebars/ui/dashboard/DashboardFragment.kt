package app.tomato.handlebars.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import app.tomato.handlebars.data.Dashboard
import app.tomato.handlebars.ui.dashboard.DashboardUtils.bindTableItem
import app.tomato.handlebars.ui.widgets.MapWidget
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.widget_map.*


/**
 * Created by user on 27.02.18.
 */
class DashboardFragment : BasicFragment() {


    var dashboard: Dashboard? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
           //      bindMap()
        return view
    }

    override fun initViews() {

    }

    override fun initProperties() {
        super.initProperties()
        val did = arguments!!["did"] as String
        dashboard = Realm.getDefaultInstance().where(Dashboard::class.java)
                .equalTo("id", did).findFirst()
        bindMap()
        bindWidgets()
    }

    private fun bindWidgets() {
        if (dashboard != null) {
            bindTableItem(this, dashboard!!, "large", llLarge, MyApp.Config.LARGE_WIDGET_COUNT, dashboard!!.large, 0, false,
                    { _, _, _, _, _ -> })
            bindTableItem(this, dashboard!!, "normal", llNormal, MyApp.Config.NORMAL_WIDGET_COUNT, dashboard!!.normal, 0, false,
                    { _, _, _, _, _ -> })
            bindTableItem(this, dashboard!!, "medium", llMedium, MyApp.Config.MEDIUM_WIDGET_COUNT, dashboard!!.medium, 0, false,
                    { _, _, _, _, _ -> })
            bindMaps()
        }
    }

    private fun bindMaps() {
        var count = 0
        if (dashboard?.maps != false) {
            count = 1
        }
        bindTableItem(this, dashboard!!, "map", llMaps, MyApp.Config.LARGE_WIDGET_COUNT, count, 0, false, { _, _, _, _, _ -> })

    }

    private fun bindMap() {
        try {
            if (childFragmentManager.findFragmentById(R.id.mainMap) == null) {
                val mapWidget = MapWidget()
                mapWidget.arguments = Bundle()
                mapWidget.arguments!!.putString("did", dashboard?.id)
                mapWidget.arguments!!.putFloat("zoom", dashboard?.zoom ?: 0f)
                childFragmentManager.beginTransaction().replace(R.id.mainMap, mapWidget).commit()
            }
        } catch (ex: Exception) {

        }
    }

    override val containerView = R.layout.fragment_dashboard

}