package app.tomato.handlebars.data.widget

import android.arch.lifecycle.LiveData
import android.content.Context
import android.os.AsyncTask
import android.preference.PreferenceManager
import app.tomato.handlebars.data.LocationLiveManager.context
import app.tomato.handlebars.data.RealmUtil
import app.tomato.handlebars.data.Track
import io.realm.Realm
import java.util.*

/**
 * Created by user on 03.03.18.
 */
object TrackStatusLiveData : LiveData<Track>() {


    fun init(context: Context) {
        val started = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("track", false)
        value = if (started) {
            initTrack()
        } else {
            null
        }
    }

    fun initTrack(force: Boolean = false): Track? {
        var track = Realm.getDefaultInstance().where(Track::class.java).isNull("name").findFirst()
        if (track == null && !force) {
            track = Track(UUID.randomUUID().toString(), name = null, distance = 0f)
            RealmUtil.transaction { Realm.getDefaultInstance().copyToRealmOrUpdate(track) }
        }
        return track
    }


    fun toggle() {
        value = if (value == null) {
            initTrack()
        } else {
            null
        }

        object : AsyncTask<Unit, Unit, Unit>() {

            override fun doInBackground(vararg params: Unit?) {
                PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("track", value != null).apply()
            }
        }.execute()

    }

    fun resetTrack() {
        var oldTrack = initTrack(true)

        RealmUtil.transaction {
            oldTrack?.deleteFromRealm()
        }

       // DistanceLiveData.update()
        TrackTimeListeners.updateAll()
        AverageSpeedListeners.updateAll()
    }

    fun saveTrack(name: String) {
        var oldTrack = initTrack(true)

        RealmUtil.transaction {
            oldTrack?.name = name
        }

     //   DistanceLiveData.update()
        TrackTimeListeners.updateAll()
        AverageSpeedListeners.updateAll()

    }
}