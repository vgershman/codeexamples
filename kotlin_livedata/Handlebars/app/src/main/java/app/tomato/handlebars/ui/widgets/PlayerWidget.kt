package app.tomato.handlebars.ui.widgets

import android.arch.lifecycle.Observer
import android.view.KeyEvent
import app.tomato.handlebars.R
import app.tomato.handlebars.data.widget.MediaLiveData
import kotlinx.android.synthetic.main.fragment_player.*


/**
 * Created by user on 12.03.18.
 */

class PlayPauseWidget : ToolWidget() {

    var playing = false

    override fun setupListeners() {
        ivCnt.setOnClickListener { onPlay() }
    }

    private fun onPlay() {
        if (playing) {
            proceedCode(KeyEvent.KEYCODE_MEDIA_PAUSE)
        } else {
            proceedCode(KeyEvent.KEYCODE_MEDIA_PLAY)
        }
    }


    override fun setup() {
        MediaLiveData.setContext(activity!!)
        MediaLiveData.observe(this, Observer {
            bindStatus(it)
        })
    }

    private fun bindStatus(musicPlaying: Boolean?) {
        playing = musicPlaying ?: playing
        if (!playing) {
            ivCnt.setImageResource(R.drawable.ic_play_arrow_black_24px)

        } else {
            ivCnt.setImageResource(R.drawable.ic_pause_black_24px)
        }
    }

    override fun icon() = R.drawable.ic_play_arrow_black_24px

    override fun title() = "Play/Pause"


}

class NextWidget : ToolWidget() {

    override fun icon() = R.drawable.ic_skip_next_black_24px


    override fun setupListeners() {
        ivCnt.setOnClickListener { proceedCode(KeyEvent.KEYCODE_MEDIA_NEXT) }
    }


    override fun setup() {

    }

    override fun title() = "Next"


}

class PrevWidget : ToolWidget() {

    override fun icon() = R.drawable.ic_skip_previous_black_24px


    override fun setupListeners() {
        ivCnt.setOnClickListener { proceedCode(KeyEvent.KEYCODE_MEDIA_PREVIOUS) }
    }


    override fun setup() {

    }

    override fun title() = "Prev"


}