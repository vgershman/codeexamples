package app.tomato.handlebars.ui

import android.arch.lifecycle.Observer
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import app.tomato.handlebars.data.Route
import app.tomato.handlebars.data.RoutesLoadingManager
import app.tomato.handlebars.data.RoutesManager
import app.tomato.handlebars.ui.container.ImageViewerActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_routes.*
import kotlinx.android.synthetic.main.item_route.view.*

/**
 * Created by user on 12.03.18.
 */
class RoutesFragment : BasicFragment() {

    override val hasToolbar = true

    override val titleId = R.string.ttl_routes

    private val refreshCallback = { srl.isRefreshing = false }

    override fun initViews() {
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = RoutesAdapter({ openRoute(it) }, { cacheRoute(it) }, { openPreview(it) })
        RoutesManager.observe(this, Observer { (rv.adapter as RoutesAdapter).setData(it) })
        srl.setOnRefreshListener {
            RoutesManager.syncRoutes(context!!, refreshCallback)
        }
        RoutesManager.syncRoutes(context!!, refreshCallback)
        RoutesLoadingManager.observe(this, Observer { rv?.adapter?.notifyDataSetChanged() })

    }

    private fun openPreview(it: Route) {
        if (!it.track?.preview.isNullOrBlank()) {
            startActivity(Intent(context, ImageViewerActivity::class.java).putExtra(ImageViewerActivity.EXTRA_URL, it.track?.preview))
        }
    }

    private fun cacheRoute(route: Route) {
        RoutesManager.syncRoute(context!!, route.track?.id, {
            rv.adapter.notifyDataSetChanged()
        })
    }

    private fun openRoute(route: Route) {
        go(MyApp.Screens.ROUTE, params = hashMapOf("rid" to route.id))
    }


    override val containerView = R.layout.fragment_routes

}

class RoutesAdapter(var routeClick: (route: Route) -> Unit, var cacheClick: (route: Route) -> Unit, var previewClick: (route: Route) -> Unit) : RecyclerView.Adapter<RoutesAdapter.VH>() {

    private val data = arrayListOf<Route>()

    fun setData(routes: List<Route>?) {
        if (routes != null) {
            data.clear()
            data.addAll(routes)
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent.context).inflate(R.layout.item_route, parent, false))


    override fun getItemCount() = data.size


    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(data[position])
    }

    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(route: Route) {
            itemView.ivPreview.setOnClickListener { previewClick(route) }
            itemView.setOnClickListener { routeClick(route) }

            val status = RoutesManager.isCached(route.track?.id)
            when (status) {
                0 -> {
                    itemView.btnCache.setImageResource(R.drawable.ic_file_download_black_36px)
                    itemView.btnCache.setOnClickListener { cacheClick(route) }
                    itemView.pb.visibility = View.GONE
                    itemView.btnCache.visibility = View.VISIBLE
                }
                1 -> {
                    itemView.pb.visibility = View.VISIBLE
                    itemView.btnCache.visibility = View.GONE
                }
                2 -> {
                    itemView.btnCache.setImageResource(R.drawable.ic_file_downloaded_black_36px)
                    itemView.btnCache.setOnClickListener { }
                    itemView.pb.visibility = View.GONE
                    itemView.btnCache.visibility = View.VISIBLE
                }
            }


            itemView.tv_name.text = route.name
            itemView.tv_desc.text = "Distance: ${String.format("%.1f", (route.track?.total_distance?.div(1000)))}km,\nascent: ${Math.round((route.track?.ascent
                    ?: 0.toDouble()))}m"
            if (!route.track?.preview_thumbnail.isNullOrBlank()) {
                Picasso.get().load(route.track?.preview_thumbnail).into(itemView.ivPreview)
            } else {
                itemView.ivPreview.setImageResource(R.drawable.ic_image_black_48px)
            }
        }

    }
}