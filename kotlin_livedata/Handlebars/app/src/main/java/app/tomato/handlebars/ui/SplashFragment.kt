package app.tomato.hadlebars.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Handler
import app.tomato.basic.help.checkPermission
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.R
import app.tomato.handlebars.data.AuthManager
import app.tomato.handlebars.data.widget.TrackStatusLiveData
import app.tomato.handlebars.ui.container.DashboardActivity


/**
 * Created by user on 30.01.18.
 */

class SplashFragment : BasicFragment() {
    override fun initViews() {
        if (checkPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION, 11)) {
            start()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            start()
        }
    }

    private fun start() {
        //AuthManager.init(activity!!)
        TrackStatusLiveData.init(activity!!)
        Handler().postDelayed({
            startActivity(Intent(activity, DashboardActivity::class.java))
            activity?.finish()
        }, 1000L)


    }


    override val containerView = R.layout.fragment_splash

}