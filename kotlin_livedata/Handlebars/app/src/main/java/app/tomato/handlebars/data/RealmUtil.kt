package app.tomato.handlebars.data

import android.util.Log
import io.realm.Realm

/**
 * Created by user on 07.03.18.
 */
object RealmUtil {


    fun transaction(action: () -> Unit) {
        try {
            Realm.getDefaultInstance().beginTransaction()
            action()
            Realm.getDefaultInstance().commitTransaction()
        } catch (ex: Exception) {
            Log.e("Realm", ex.message)
        }


    }
}