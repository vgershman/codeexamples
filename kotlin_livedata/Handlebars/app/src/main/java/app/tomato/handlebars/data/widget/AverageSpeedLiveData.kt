package app.tomato.handlebars.data.widget

import android.arch.lifecycle.LiveData
import app.tomato.handlebars.data.Utils
import app.tomato.handlebars.data.state.StateLive

/**
 * Created by user on 08.03.18.
 */

object AverageSpeedListeners {
    private var map = hashMapOf<String, AverageSpeedLiveData>()

    fun getListenerForSetting(setting: String): AverageSpeedLiveData {
        var liveData = map[setting]
        if (liveData == null) {
            liveData = AverageSpeedLiveData(setting)
            map[setting] = liveData
        }
        return liveData
    }

    fun updateAll() {
        map.values.forEach {
            it.update()
        }
    }
}

class AverageSpeedLiveData(setting: String) : LiveData<Float>() {

    var type: String = "ride"
    var count: Int = 5
    var exclude: Boolean = true

    init {
        var set = setting.split("_")
        type = set[0]
        if (type != "ride") {
            count = Integer.valueOf(set[1])
            exclude = set[2] == "excluded"
        } else {
            exclude = set[1] == "excluded"
        }

    }

    override fun onActive() {
        super.onActive()
        update()
    }

    fun update() {
        var task: () -> Float? = { null }
        when (type) {
            "ride" -> task = { StateLive.calculateAverageSpeed(null, null, exclude) }
            "dst" -> task = { StateLive.calculateAverageSpeed(null, count * 1000L, exclude) }
            "time" -> task = { StateLive.calculateAverageSpeed(count * 100L, null, exclude) }
        }
        Utils.async(task, { value = it as Float? })
    }

}