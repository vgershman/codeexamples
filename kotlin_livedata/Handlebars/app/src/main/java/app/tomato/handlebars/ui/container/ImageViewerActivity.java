package app.tomato.handlebars.ui.container;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import app.tomato.handlebars.R;
import uk.co.senab.photoview.PhotoViewAttacher;


public class ImageViewerActivity extends Activity {

    public static final String EXTRA_URL = "extra_url";
    private boolean mToolbarVisible = true;
    private String mImageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);
        mImageUrl = getIntent().getStringExtra(EXTRA_URL);
        findViewById(R.id.iv_content).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        bindData();
        initButtons();
    }

    private void bindData() {
        if (mImageUrl == null) {
            onBackPressed();
        }
        Picasso.get().load(mImageUrl).into((ImageView) findViewById(R.id.iv_content), new Callback() {
            @Override
            public void onSuccess() {
                new PhotoViewAttacher((ImageView) findViewById(R.id.iv_content));
            }

            @Override
            public void onError(Exception e) {
            }
        });

    }

    @Override
    public void finish() {
        super.finish();
        //overridePendingTransition(R.anim.anim_close, R.anim.anim_close_);
    }

    private void initButtons() {
        findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        findViewById(R.id.iv_content).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mToolbarVisible = !mToolbarVisible;
                findViewById(R.id.toolbar).setVisibility(mToolbarVisible ? View.VISIBLE : View.GONE);
            }
        });
    }
}
