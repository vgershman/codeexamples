package app.tomato.basic.data

import android.support.annotation.Keep

/**
 * Created by user on 06.06.17.
 */

@Keep
data class Res<out T>(val status: String, val error: Error?, val data: T?) {
    val isSuccess: Boolean
        get() = status == "success"
}

@Keep
data class Error(val message: String?, val code: String?)
