package app.tomato.basic.data

import android.content.Context
import android.content.DialogInterface
import android.os.Handler
import android.widget.Toast
import app.tomato.basic.data.connection.ConnectionManager
import app.tomato.basic.help.AlertUtils
import app.tomato.handlebars.R
import app.tomato.handlebars.data.AuthManager
import app.tomato.handlebars.data.ProgressSearchLive
import app.tomato.handlebars.data.Rest
import app.tomato.handlebars.data.TrackDto
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


abstract class AlertCallback<T>(context: Context, val force: Boolean = false, var defaultMessage: String? = null) : BaseCallback<T>(context) {

    override fun onError(message: String?) {
        if (message != null) {
            try {
                AlertUtils.alert(context, force, message!!, R.string.btn_retry, DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.cancel()
                    retry()
                })
            } catch (ex: Exception) {
            }

        } else {

            if (defaultMessage != null) {
                try {
                    AlertUtils.alert(context, force, defaultMessage!!, R.string.btn_close, DialogInterface.OnClickListener { dialogInterface, i ->
                        dialogInterface.cancel()

                    })
                } catch (ex: Exception) {
                }

            } else {
                try {
                    AlertUtils.alert(context, force, R.string.server_error, R.string.btn_retry, DialogInterface.OnClickListener { dialogInterface, i ->
                        dialogInterface.cancel()
                        retry()
                    })
                } catch (ex: Exception) {
                }
            }
        }

    }

    override fun onConnectionFailure() {
        AlertUtils.alert(context, force, R.string.no_connection, R.string.btn_retry, DialogInterface.OnClickListener { dialogInterface, i ->
            dialogInterface.cancel()
            retry()
        })
    }
}

/**
 * Created by user on 26.04.17.
 */

abstract class BaseCallback<T>(protected val context: Context) : Callback<T> {

    private var call: Call<T>? = null

    override fun onResponse(call: Call<T>, response: Response<T>) {
        this.call = call
        onCallback()
        if (response.code() == 202) {
            val header = response.headers()["X-Response-URL"]
            ProgressSearchLive.set(response.headers()["X-Progress"])

            if (header != null) {
                Handler().postDelayed({
                    loadDelayed(header, this)
                }, 5000L)

                return
            }
        }

        if (response.isSuccessful && response.body() != null) {
            try {
                val typed = response.body() as T
                onSuccess(typed)
            } catch (ex: Exception) {
                error(context.getString(R.string.server_error))
            }
        } else {
            if (response.code() == 401 || response.code() == 403) {
                AuthManager.store(null, context)
                AuthManager.init(context)
            } else {
                try {
                    // val errorConverter = Rest.retrofit().responseBodyConverter<Error>(Error::class.java, arrayOfNulls<Annotation>(0))
                    // val error = errorConverter(response.errorBody().string())
                    val res = Gson().fromJson<Res<Any>>(response.errorBody()?.string(), Res::class.java)
                    try {
                        error(res.error?.message ?: context.getString(R.string.server_error))
                    } catch (ex: Exception) {
                        error(context.getString(R.string.server_error))
                    }

                } catch (ex: Exception) {
                    error(context.getString(R.string.server_error))
                }
            }
        }
    }

    private fun loadDelayed(header: String, baseCallback: BaseCallback<T>? = null) {
        val key = header.replace("/api/v1/responses/", "")
        Rest.rest().getDelayResponse(key).enqueue(object : BaseCallback<List<TrackDto>?>(context) {
            override fun onSuccess(response: List<TrackDto>?) {
                try {
                    baseCallback?.onSuccess(response as T)
                } catch (ex: Exception) {
                    baseCallback?.onError("Type error")
                }
            }

            override fun onError(message: String?) {
                baseCallback?.onError(message)
            }

            override fun onConnectionFailure() {
                baseCallback?.onConnectionFailure()
            }

        })
    }

    open fun error(message: String? = null) {
        try {
            if (ConnectionManager.isConnected(context)) {
                onError(message)
            } else {
                onConnectionFailure()
            }
        } catch (ex: Exception) {

        }
    }


    override fun onFailure(call: Call<T>, t: Throwable) {
        this.call = call
        try {
            onCallback()
            error()
        } catch (ex: Exception) {

        }
    }

    protected fun retry() {
        call?.clone()?.enqueue(this)
    }

    protected abstract fun onSuccess(response: T)

    protected abstract fun onError(message: String? = null)

    protected abstract fun onConnectionFailure()

    open fun onCallback() {

    }
}

/**
 * Created by user on 26.04.17.
 */

abstract class ToastCallback<T>(context: Context) : BaseCallback<T>(context) {

    override fun onError(message: String?) {
        if (message != null) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        } else {
            // Toast.makeText(context, R.string.server_error, Toast.LENGTH_LONG).show()
        }
    }

    override fun onConnectionFailure() {
        Toast.makeText(context, R.string.no_connection, Toast.LENGTH_LONG).show()
    }
}