package app.tomato.handlebars.ui

import android.view.View
import android.widget.Toast
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.R
import app.tomato.handlebars.data.LocationLiveManager
import app.tomato.handlebars.data.SQManager
import app.tomato.handlebars.data.SearchQueryManager
import kotlinx.android.synthetic.main.fragment_from_to.*
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.RestrictionsManager.RESULT_ERROR
import android.content.Intent
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import android.support.v4.app.ShareCompat.IntentBuilder
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds


/**
 * Created by user on 14.03.18.
 */


class FromToFragment : BasicFragment() {

    val PLACE_AUTOCOMPLETE_REQUEST_CODE = 23

    private var from = false

    override val hasToolbar = true

    override fun initViews() {
        toolbar?.inflateMenu(R.menu.ok_menu)
        toolbar?.setOnMenuItemClickListener {
            if (it.itemId == R.id.btn_ok) {
                onOK()
            };true
        }
        btnCurrent.setOnClickListener { bindCurrent() }
        btnPlaces.setOnClickListener {
            findPlace()
        }
    }

    fun findPlace() {
        try {
            val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setBoundsBias(LatLngBounds(
                            LatLng(55.558693, 37.337495),
                            LatLng(55.920499, 37.881318)))
                    .build(activity)
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
        } catch (e: GooglePlayServicesRepairableException) {
            // TODO: Handle the error.
        } catch (e: GooglePlayServicesNotAvailableException) {
            // TODO: Handle the error.
        }

    }

    // A place has been received; use requestCode to track the request.
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                val place = PlaceAutocomplete.getPlace(context, data)
                val lat = String.format("%.6f", place.latLng.latitude)
                val lng = String.format("%.6f", place.latLng.longitude)
                etLocation.setText("$lat,$lng")
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                val status = PlaceAutocomplete.getStatus(context, data)
                Toast.makeText(context, status.statusMessage, Toast.LENGTH_SHORT).show()
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private fun onOK() {
        val location = etLocation.text.toString()
        val coords = location.replace(" ", "").split(",")
        var lat: Double? = null
        var lng: Double? = null
        if (coords.size == 2) {
            lat = coords[0].toDoubleOrNull()
            lng = coords[1].toDoubleOrNull()
        }
        if (lat != null && lng != null) {
            //SQManager.get(context!!).setPoint(from, "$lat,$lng")
            onBackPressed()
        } else {
            Toast.makeText(context, "Неверный формат!", Toast.LENGTH_SHORT).show()
        }

    }

    private fun bindCurrent() {
        val location = LocationLiveManager.value
        if (location != null) {
            etLocation.setText("${location.longitude},${location.latitude}")
            etLocation.setSelection(etLocation.text.length)
        }
    }

    override fun initProperties() {
        super.initProperties()
        from = arguments!!["from"] as Boolean? ?: from


        if (from) {
            //etLocation.setText(SQManager.get(context!!).value?.from ?: "")
            setTitle("From")
        } else {
           // etLocation.setText(SQManager.get(context!!).value?.to ?: "")
            setTitle("To")
        }
        if (from) {
            llCurrent.visibility = View.VISIBLE
        }
    }

    override val containerView = R.layout.fragment_from_to

}