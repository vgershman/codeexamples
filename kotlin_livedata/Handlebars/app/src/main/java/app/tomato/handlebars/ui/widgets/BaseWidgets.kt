package app.tomato.handlebars.ui.widgets

import android.arch.lifecycle.Observer
import android.content.Intent
import android.util.TypedValue
import android.view.KeyEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import app.tomato.basic.help.dpToPx
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import kotlinx.android.synthetic.main.widget_gauge.*
import kotlinx.android.synthetic.main.widget_tool.*

/**
 * Created by user on 01.03.18.
 */

abstract class Widget : BasicFragment() {

    var height: Float = 0f

    fun applySize(tv: TextView, size: Float) {
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, size)
    }

    fun applyTitleSize(tv: TextView) {
        applySize(tv, height * MyApp.WidgetConfig.TITLE_SIZE)
        (tv.layoutParams as LinearLayout.LayoutParams).setMargins(0, (height * MyApp.WidgetConfig.TITLE_TOP_MARGIN).toInt(), 0, 0)
    }

    fun applyValueSize(scaler: Float) {
        applySize(tvCnt, height * MyApp.WidgetConfig.VALUE_SIZE * scaler)
        applySize(tvMeter, height * MyApp.WidgetConfig.METRICS_SIZE * scaler)
        (tvMeter.layoutParams as LinearLayout.LayoutParams).setMargins((height * MyApp.WidgetConfig.METRICS_LEFT_MARGIN).toInt(), 0, 0, 0)
        ((llValue.layoutParams) as FrameLayout.LayoutParams).setMargins(0, -(height * MyApp.WidgetConfig.TITLE_BOTTOM_MARGIN).toInt(), 0, 0)
    }


}

class EmptyWidget : Widget() {
    override fun initViews() {

    }

    override val containerView = R.layout.empty_widget
}

abstract class GaugeWidget<T> : Widget() {

    override val containerView = R.layout.widget_gauge

    var widgetId: String? = null

    var observer = Observer<T?> { it -> bindValue(it) }

    override fun initViews() {
        tvTtl.text = title()
        if (getMetric() == null) {
            tvMeter.visibility = View.GONE
        } else {
            tvMeter.visibility = View.VISIBLE
            tvMeter.text = getMetric()
        }
        tvMeter.text = getMetric()
    }

    open fun getEmptyValue(): String = ""

    open fun getMetric(): String? = null

    fun bindValue(value: T?) {
        if (value == null) {
            tvCnt.text = getEmptyValue()
        } else {
            tvCnt.text = format(value!!)
        }
    }

    abstract fun format(value: T): String

    abstract fun setup()

    abstract fun title(): String

    override fun initProperties() {
        super.initProperties()
        widgetId = arguments!!["wid"] as String
        val type = arguments!!["type"] as String
        val padding = arguments!!["padding"] as Int
        val size = arguments!!["size"] as Int
        var width = ((screenWidth(context!!) - dpToPx(context!!, padding + 2 * (size - 1))) / size)
        height = (width * 0.66f)
        applyTitleSize(tvTtl)
        applyValueSize(1f)
        setup()
    }
}

abstract class ToolWidget : Widget() {

    override val containerView = R.layout.widget_tool

    var isEdit: Boolean = false

    override fun initViews() {
        tvTtl_.text = title()
        ivCnt.setImageResource(icon())
        setup()

    }

    abstract fun setupListeners()

    abstract fun setup()

    abstract fun icon(): Int

    abstract fun title(): String

    fun proceedCode(keyCode: Int) {
        val intent = Intent(Intent.ACTION_MEDIA_BUTTON)
        synchronized(this) {
            intent.putExtra(Intent.EXTRA_KEY_EVENT, KeyEvent(KeyEvent.ACTION_DOWN, keyCode))
            context!!.sendOrderedBroadcast(intent, null)

            intent.putExtra(Intent.EXTRA_KEY_EVENT, KeyEvent(KeyEvent.ACTION_UP, keyCode))
            context!!.sendOrderedBroadcast(intent, null)
        }
    }

    override fun initProperties() {
        super.initProperties()
        val type = arguments!!["type"] as String
        val padding = arguments!!["padding"] as Int
        isEdit = padding != 0
        if (!isEdit) {
            setupListeners()
        }
        val size = arguments!!["size"] as Int
        var width = ((screenWidth(context!!) - dpToPx(context!!, padding + 2 * (size - 1))) / size)
        height = width * 0.66f
        applyTitleSize(tvTtl_)
        applyIconSize()
    }

    open fun applyIconSize() {
        ivCnt.layoutParams.height = (height * MyApp.WidgetConfig.ICON_SIZE).toInt()
        (ivCnt.layoutParams as FrameLayout.LayoutParams).setMargins(0, -(height * MyApp.WidgetConfig.ICON_MARGIN).toInt(), 0, 0)
    }
}
