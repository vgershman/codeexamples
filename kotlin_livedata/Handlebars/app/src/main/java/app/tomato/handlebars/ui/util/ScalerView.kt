package app.tomato.handlebars.ui.util

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ScaleGestureDetector


/**
 * Created by user on 03.03.18.
 */
class ScalerView(context: Context?) : View(context) {

    private var mScaleDetector: ScaleGestureDetector? = null
    private val mScaleFactor = 1f

    init {
        mScaleDetector = ScaleGestureDetector(context, ScaleListener())
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {

        mScaleDetector?.onTouchEvent(event)
        return true

    }

    inner class ScaleListener :  ScaleGestureDetector.SimpleOnScaleGestureListener(){

        override fun onScale(detector: ScaleGestureDetector?): Boolean {
            return super.onScale(detector)
        }
    }
}