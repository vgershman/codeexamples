package app.tomato.handlebars.data.widget

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.content.Context
import android.media.AudioManager
import android.os.Handler
import android.os.Looper
import java.util.*


@SuppressLint("StaticFieldLeak")
/**
 * Created by user on 12.03.18.
 */

object MediaLiveData : LiveData<Boolean>() {

    private var audioManager: AudioManager? = null
    private var timer: Timer? = null

    fun setContext(context: Context) {
        audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    }

    override fun onActive() {
        super.onActive()
        timer?.cancel()
        timer = Timer()
        timer?.schedule(object : TimerTask() {

            override fun run() {
                Handler(Looper.getMainLooper()).post {
                    if (audioManager != null) {
                        value = audioManager!!.isMusicActive
                    }
                }

            }

        }, 0, 500L)
    }

    override fun onInactive() {
        super.onInactive()
        timer?.cancel()
    }

}