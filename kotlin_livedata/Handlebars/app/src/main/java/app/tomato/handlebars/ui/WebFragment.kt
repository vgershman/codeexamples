package app.tomato.handlebars.ui

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import app.tomato.handlebars.data.AuthManager
import app.tomato.handlebars.data.MeManager
import kotlinx.android.synthetic.main.fragment_web.*

/**
 * Created by user on 12.03.18.
 */

class WebFragment : BasicFragment() {

    override val hasToolbar = true

    override val titleId = R.string.ttl_profile

    override val containerView = R.layout.fragment_web

    override fun initViews() {
        wv.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                Log.i("webfragment", url)
                if (url == null) {
                    view?.stopLoading()
                    return
                }
                if (url.contains("embedded-login-success")) {
                    val token = Uri.parse(url).getQueryParameter("access_token")
                    if (!token.isNullOrBlank()) {
                        AuthManager.store(token, context!!)
                        MeManager.sync(context!!)
                        view?.stopLoading()
                        activity?.finish()
                        return
                    }

                }
                super.onPageStarted(view, url, favicon)
            }
        }
        wv.settings.javaScriptEnabled = true
        wv.settings.userAgentString = "Mozilla/5.0"
    }

    override fun initProperties() {
        super.initProperties()
        var command = arguments!!["command"] as String?
        if (command == null) {
            activity?.finish()
            return
        }
        loadPageWithToken(command)
    }

    private fun loadPageWithToken(command: String) {
        val namedCommand = command.replace("<access_token>", AuthManager.accessToken!!)
        wv.loadUrl("${MyApp.Config.EMBEDDED_HOST}$namedCommand")
    }

}