package app.tomato.handlebars

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.arch.lifecycle.LifecycleService
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import app.tomato.handlebars.data.LocationDao
import app.tomato.handlebars.data.LocationLiveManager
import app.tomato.handlebars.data.RealmUtil
import app.tomato.handlebars.data.Track
import app.tomato.handlebars.ui.container.SimpleActivity
import io.realm.Realm

/**
 * Created by user on 03.03.18.
 */
class TrackService : LifecycleService() {


    var track: Track? = null
    var restarting = false

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startMe()
        return super.onStartCommand(intent, flags, startId)
    }

    private fun startMe() {
        startForeground()

        if (LocationLiveManager.context == null) {
            LocationLiveManager.context = this
        }

        LocationLiveManager.removeObservers(this)
        LocationLiveManager.observe(this, Observer { onLocationChanged(it) })

        // StateLive.observe(this, Observer {  })

    }


    private fun onLocationChanged(location: Location?) {
    }


    private fun storeLocation(location: Location) {
        val exlcuded = false//TrackStatusLiveData.value == null
        RealmUtil.transaction {
            Realm.getDefaultInstance().copyToRealm(LocationDao(location.latitude, location.longitude, excluded = exlcuded, trackId = track?.id))
        }
    }


    private fun startForeground() {
        val channelId =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createNotificationChannel()
                } else {
                    ""
                }

        val notificationIntent = Intent(this, SimpleActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
        val notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_fiber_manual_record_black_48px)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentTitle("Handlebars")
                .setContentText("Запись трека")
                .setContentIntent(pendingIntent)
                .build()
        startForeground(101, notification)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String {
        val channelId = "my_service"
        val channelName = "My Background Service"
        val chan = NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

}