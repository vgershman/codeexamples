package app.tomato.handlebars.ui.dashboard

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import app.tomato.basic.help.dpToPx
import app.tomato.basic.help.screenHeight
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import app.tomato.handlebars.data.Dashboard
import app.tomato.handlebars.data.DashboardManager
import app.tomato.handlebars.data.Widget
import app.tomato.handlebars.ui.widgets.*

/**
 * Created by user on 28.02.18.
 */
object DashboardUtils {

    @SuppressLint("NewApi")
    fun bindTableItem(fragment: BasicFragment, dashboard: Dashboard, type: String, container: LinearLayout, size: Int, count: Int, padding: Int,
                      showEmpty: Boolean = false,
                      listener: (widget: Widget?, type: String, size: Int, row: Int, column: Int) -> Unit) {
        val widgets = DashboardManager.listWidgetByDashAndType(dashboard.id!!, type)
        container.removeAllViews()
        if (count != 0) {
            for (i in 0 until count) {
                val ll = LinearLayout(fragment.context)
                ll.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                ll.orientation = LinearLayout.HORIZONTAL
                for (k in 0 until size) {
                    val curWidget = getWidgetByTablePosition(widgets, i, k, size)
                    val widgetContainer = FrameLayout(fragment.context!!)
                    val width = ((screenWidth(fragment.context!!) - dpToPx(fragment.context!!, padding + 2 * (size - 1))) / size).toInt()
                    var height = (width * 0.66f).toInt()
                    if (dashboard.mapsHeight != null && type == "map") {
                        height = calculateHeight(fragment.context!!, dashboard.mapsHeight!!, dashboard, padding)
                    }
                    widgetContainer.layoutParams = LinearLayout.LayoutParams(width, height)
                    widgetContainer.id = View.generateViewId()

                    ll.addView(widgetContainer)

                    var widget: BasicFragment? = null
                    if (curWidget != null) {
                        when (curWidget.name) {
                            "Current Time" -> widget = TimeWidget()
                            "Sunset" -> widget = SunsetWidget()
                            "Sunrise" -> widget = SunriseWidget()
                            "Sunset/Sunrise" -> widget = SunsetSunriseWidget()
                            "Current Speed" -> widget = SpeedWidget()
                            "Camera" -> widget = CameraWidget()
                            "Start/Stop" -> widget = TrackWidget()
                            "Total Distance" -> widget = DistanceWidget()
                            "Total Time" -> widget = TotalTimeWidget()
                            "Battery" -> widget = BatteryWidget()
                            "Average Speed" -> widget = AverageSpeedWidget()
                            "Play/Pause" -> widget = PlayPauseWidget()
                            "Next" -> widget = NextWidget()
                            "Prev" -> widget = PrevWidget()
                            "2D" -> {
                                widget = MapWidget()
                                widget.listener = { listener(curWidget, type, size, i, k) }
                            }
                        }
                        if (widget != null) {
                            widget.arguments = Bundle()
                            widget.arguments?.putString("type", type)
                            widget.arguments?.putInt("padding", padding)
                            widget.arguments?.putInt("size", size)
                            widget.arguments?.putString("setting", curWidget.setting)
                            widget.arguments?.putBoolean("options", showEmpty)
                            widget.arguments?.putString("wid", curWidget.id)
                            widget.arguments?.putString("did", curWidget.dashId)
                            widget.arguments?.putFloat("zoom", curWidget.zoom ?: 0f)
                            widget.arguments?.putBoolean("enabledZoom", padding != 0)
                        }
                    } else {
                        if (showEmpty) {
                            widget = EmptyWidget()
                        }
                    }
                    widgetContainer.setOnLongClickListener {
                        listener(curWidget, type, size, i, k)
                        true
                    }
                    try {
                        if (widget != null) {
                            fragment.childFragmentManager.beginTransaction().replace(widgetContainer.id, widget).commit()
                        }
                    } catch (ex: Exception) {

                    }
                    val deleter = View(fragment.context!!)
                    deleter.layoutParams = LinearLayout.LayoutParams(dpToPx(fragment.context!!, 2).toInt(), -1)
                    deleter.setBackgroundResource(R.color.semi_black)
                    ll.addView(deleter)
                }
                container.addView(ll)
                val deleter = View(fragment.context!!)
                deleter.layoutParams = LinearLayout.LayoutParams(-1, dpToPx(fragment.context!!, 2).toInt())
                deleter.setBackgroundResource(R.color.semi_black)
                container.addView(deleter)
            }
        }
    }

    private fun calculateHeight(context: Context, mapsHeight: Int, dashboard: Dashboard, padding: Int): Int {
        var widgetSize = 0f
        val width = (screenWidth(context) - dpToPx(context, padding))
        widgetSize += (dashboard.large * (width * 0.66f / MyApp.Config.LARGE_WIDGET_COUNT))
        widgetSize += (dashboard.medium * (width * 0.66f / MyApp.Config.MEDIUM_WIDGET_COUNT))
        widgetSize += (dashboard.normal * (width * 0.66f / MyApp.Config.NORMAL_WIDGET_COUNT))
        widgetSize += (dashboard.large + dashboard.medium + dashboard.normal) * dpToPx(context, 2)
        val screenHeigth = screenHeight(context)
        val allMapsHeight = screenHeigth - dpToPx(context, padding) - widgetSize
        return (allMapsHeight * (mapsHeight / 100f)).toInt()
    }


    private fun getWidgetByTablePosition(widgetList: List<Widget>, row: Int, column: Int, size: Int): Widget? {
        val pos = row * size + column
        return widgetList.find { it.position == pos }
    }

}