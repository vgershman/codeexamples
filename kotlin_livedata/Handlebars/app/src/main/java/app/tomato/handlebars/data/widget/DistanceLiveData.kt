package app.tomato.handlebars.data.widget

import android.arch.lifecycle.LiveData
import app.tomato.handlebars.data.Utils
import app.tomato.handlebars.data.state.StateLive

/**
 * Created by user on 04.03.18.
 */
object DistanceLiveData : LiveData<Float>() {

//    override fun onActive() {
//        super.onActive()
//        readDistance()
//    }

//    private fun readDistance() {
//        val track = TrackStatusLiveData.initTrack()
//        value = null // track?.distance
//    }

    fun setDistance(distance: Float?) {
        value = distance
    }

    fun update() {
        Utils.async({ StateLive.calculateTotalDistance() },
                { value = it as Float? })
    }

//    fun update() {
//        val track = TrackStatusLiveData.value
//        if (track != null) {
//            var distance = track.distance ?: 0f
//            val points = Realm.getDefaultInstance().where(LocationDao::class.java).equalTo("trackId", track.id)
//                    .sort("timestamp", Sort.DESCENDING).findAll().take(2)
//            if (points.size > 1) {
//                var results = floatArrayOf(0f, 0f, 0f)
//                Location.distanceBetween(points[0]?.lat!!, points[0]?.lon!!, points[1]?.lat!!, points[1]?.lon!!, results)
//                distance += results[0]
//            }
//            RealmUtil.transaction {
//                track.distance = distance
//            }
//            value = track.distance
//        } else {
//            value = null// 0f
//        }
    //   }

}