package app.tomato.handlebars.ui.container

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import app.tomato.basic.help.dpToPx
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.basic.ui.basic.BasicFragmentActivity
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import app.tomato.handlebars.TrackService
import app.tomato.handlebars.data.*
import app.tomato.handlebars.data.state.StateLive
import app.tomato.handlebars.data.state.track_recording_state.*
import app.tomato.handlebars.data.widget.CurrentRouteManager
import app.tomato.handlebars.ui.dashboard.DashboardFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_dashboards.*
import java.util.*


/**
 * Created by user on 27.02.18.
 */


class DashboardActivity : BasicFragmentActivity() {

    var bottomMenu: BottomSheetBehavior<View>? = null

    override fun init() {
        MeManager.observe(this, Observer { bindMe(it) })
        AuthManager.init(this)
        bindMe(null)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        DashboardManager.observe(this, Observer { bindDashboards(it) })
        // TrackStatusLiveData.observe(this, Observer { startTrack(it) })
        StateLive.observe(this, Observer { startTrack(it) })
        bottomMenu = BottomSheetBehavior.from(bottom_sheet)
        bottomMenu?.peekHeight = dpToPx(this, 32).toInt()
        bottomMenu?.isHideable = false
        btnSettings.setOnClickListener {
            Handler().postDelayed({
                bottom_sheet.bringToFront()
            }, 150)

            if (bottomMenu?.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomMenu?.state = BottomSheetBehavior.STATE_EXPANDED
            } else {
                bottomMenu?.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
        btnStopRoute.setOnClickListener {
            CurrentRouteManager.setOn(null)
        }
        btnDashboards.setOnClickListener { editDashboards() }
        btnMyRoutes.setOnClickListener { go(MyApp.Screens.MY_ROUTES, MyApp.Containers.SIMPLE);bottomMenu?.state = BottomSheetBehavior.STATE_COLLAPSED }
        btnFindRoute.setOnClickListener { go(MyApp.Screens.FIND_ROUTE, MyApp.Containers.SIMPLE);bottomMenu?.state = BottomSheetBehavior.STATE_COLLAPSED }
        CurrentRouteManager.observe(this, Observer { bindCurrentRoute(it);bottomMenu?.state = BottomSheetBehavior.STATE_COLLAPSED })
        btnLogout.setOnClickListener { askLogout(); bottomMenu?.state = BottomSheetBehavior.STATE_COLLAPSED }
    }

    override fun onResume() {
        super.onResume()
        Handler().postDelayed({
            bottom_sheet.bringToFront()
        }, 50)

    }

    private fun askLogout() {
        AlertDialog.Builder(this)
                .setMessage("Уверены, что хотите выйти?")
                .setNegativeButton("Отмена", { d, _ -> d.cancel() })
                .setPositiveButton("Да", { d, _ -> d.cancel();logout() })
                .create().show()
    }

    private fun logout() {
        AuthManager.logout(this)
    }

    private fun bindCurrentRoute(it: String?) {
        if (it == null) {
            btnStopRoute.visibility = View.GONE
        } else {
            btnStopRoute.visibility = View.VISIBLE
        }
    }

    private fun bindMe(me: Me?) {
        if (me == null) {
            tvName.text = "My Account"
            ivAvatar.visibility = View.GONE
            btnLogout.visibility = View.GONE
            btnAccount.setOnClickListener {
                go(MyApp.Screens.WEB, MyApp.Containers.SIMPLE, params = hashMapOf("command" to MyApp.Config.WC_GUEST))
                bottomMenu?.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            btnMyRoutes.visibility = View.GONE

        } else {
            tvName.text = me.display_name
            if (me.avatar != null) {
                ivAvatar.visibility = View.VISIBLE
                Picasso.get().load(me.avatar).into(ivAvatar)
            }
            btnLogout.visibility = View.VISIBLE
            btnAccount.setOnClickListener {
                go(MyApp.Screens.WEB, MyApp.Containers.SIMPLE, params = hashMapOf("command" to MyApp.Config.WC_USER))
                bottomMenu?.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            btnMyRoutes.visibility = View.VISIBLE
        }
    }


    override fun preInit() {
        super.preInit()
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    private fun startTrack(state: TrackRecordingState?) {

        when (state) {
            is Recording -> {
                trackPadding.visibility = View.GONE
                startService(Intent(this, TrackService::class.java))
            }
            is Paused -> {
                trackPadding.visibility = View.VISIBLE
                trackPadding.setBackgroundResource(R.drawable.track_padding_red)
            }
            is Autopaused -> {
                trackPadding.visibility = View.VISIBLE
                trackPadding.setBackgroundResource(R.drawable.track_padding)
            }
            is Stopped -> {
                trackPadding.visibility = View.GONE
                stopService(Intent(this, TrackService::class.java))
            }
        }


    }

    override val contentViewId = R.layout.activity_dashboards


    override fun onKeyLongPress(keyCode: Int, event: KeyEvent?): Boolean {
        Toast.makeText(this, "Clcik", Toast.LENGTH_LONG).show()
        return true
    }

    private fun editDashboards() {
        bottomMenu?.state = BottomSheetBehavior.STATE_COLLAPSED
        go(MyApp.Screens.EDIT_DASHBOARDS, MyApp.Containers.SIMPLE)
    }

    private fun bindDashboards(dashboards: List<Dashboard>?) {
        vp.adapter = DashboardsAdapter(supportFragmentManager)
        vp.currentItem = 50
//        if (vp.adapter == null) {
//            vp.adapter = DashboardsAdapter(supportFragmentManager)
//        }
//        (vp.adapter as DashboardsAdapter).update()
//        vp.currentItem = 500
    }
}

class DashboardsAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    // private var fragments = ArrayList<BasicFragment?>()
    private var dashboards = ArrayList<Dashboard>()

    init {
        if (DashboardManager.value!!.isNotEmpty()) {
            while (dashboards.size < 4) {
                DashboardManager.value?.forEach {
                    //fragments.add(null)
                    dashboards.add(it)
                }
            }
        }


    }

    override fun getItem(position: Int): Fragment {
        val fixedPosition = position % (dashboards.size)
        var fragment: BasicFragment? = null //fragments[fixedPosition]
        if (fragment == null) {
            try {
                fragment = DashboardFragment()
                val bundle = Bundle()
                bundle.putString("did", dashboards[fixedPosition].id)
                fragment.arguments = bundle
                //fragments[fixedPosition] = fragment
            } catch (ex: Exception) {
            }

        }
        return fragment!!
    }

    override fun getCount(): Int {
        if (dashboards.size > 0) return 100
        return 0
    }

    fun update() {
        notifyDataSetChanged()
    }

}