package app.tomato.handlebars.data

import android.arch.lifecycle.LiveData
import android.content.Context
import android.os.AsyncTask
import android.preference.PreferenceManager
import app.tomato.basic.data.ToastCallback

/**
 * Created by user on 12.03.18.
 */


object AuthManager {

    var accessToken: String? = null

    fun init(context: Context) {
        accessToken = PreferenceManager.getDefaultSharedPreferences(context).getString("token", null)
        if (!isAuth()) {
            Rest.rest().guestNew().enqueue(object : ToastCallback<Guest>(context) {
                override fun onSuccess(response: Guest) {
                    store(response.access_token, context)
                }
            })
        } else {
            MeManager.sync(context)

        }
    }

    fun isAuth() = accessToken != null

    fun logout(context: Context) {
        accessToken = null
        MeManager.logout()
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("token", null).apply()
        init(context)
    }

    fun store(token: String?, context: Context) {
        accessToken = token
        object : AsyncTask<Unit, Unit, Unit>() {

            override fun doInBackground(vararg params: Unit?) {
                PreferenceManager.getDefaultSharedPreferences(context).edit().putString("token", accessToken).apply()
            }
        }.execute()

    }

}

object MeManager : LiveData<Me>() {

    fun logout() {
        value = null
    }

    fun sync(context: Context) {
        Rest.rest().me().enqueue(object : ToastCallback<Me>(context) {
            override fun onSuccess(response: Me) {
                value = response
            }

        })
    }

}