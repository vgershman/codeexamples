package app.tomato.handlebars.data.widget

import android.arch.lifecycle.LiveData
import android.os.Handler
import android.os.Looper
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by user on 01.03.18.
 */

object TimerLiveData : LiveData<String>() {

    private val format = SimpleDateFormat("HH:mm")
    private var timer: Timer? = null

    override fun onActive() {
        super.onActive()
        timer?.cancel()
        timer = Timer()
        timer?.schedule(object : TimerTask() {

            override fun run() {
                Handler(Looper.getMainLooper()).post {
                    value = format.format(Date(System.currentTimeMillis()))
                }

            }

        }, 0, 500L)
    }

    override fun onInactive() {
        super.onInactive()
        timer?.cancel()
    }


}