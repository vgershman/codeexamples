package app.tomato.handlebars.ui

import android.app.AlertDialog
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.R
import app.tomato.handlebars.data.state.StateLive
import app.tomato.handlebars.data.widget.TrackStatusLiveData
import kotlinx.android.synthetic.main.fragment_edit_track.*

/**
 * Created by user on 07.03.18.
 */
class EditTrackFragment : BasicFragment() {

    override val hasToolbar = true

    override val titleId = R.string.ttl_edit_track

    override fun initViews() {
        btnDelete.setOnClickListener {
            askToDelete()
        }
        btnSave.setOnClickListener { saveTrack() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        StateLive.onEditBackPressed()
    }

    private fun saveTrack() {
        //TrackStatusLiveData.saveTrack(etName.text.toString())
        StateLive.onSaveOrDeleteTrack()
        activity?.finish()
    }

    private fun askToDelete() {
        AlertDialog.Builder(context)
                .setMessage(R.string.alert_delete_track)
                .setNegativeButton("Отмена", { d, _ -> d.cancel() })
                .setPositiveButton("Да", { d, _ -> d.cancel(); saveTrack() })
                .create().show()
    }


    override val containerView = R.layout.fragment_edit_track

}