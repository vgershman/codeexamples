package app.tomato.handlebars.data.state.track_recording_state

import android.location.Location
import app.tomato.handlebars.data.state.HistoryPoint
import app.tomato.handlebars.data.state.StateLive
import app.tomato.handlebars.data.widget.AverageSpeedListeners
import app.tomato.handlebars.data.widget.TrackTimeListeners
import app.tomato.handlebars.data.widget.TrackTimeLiveData

/**
 * Created by themylogin on 3/14/18.
 */

class Recording : TrackRecordingState() {
    override fun tick(time: Long) {
        AverageSpeedListeners.updateAll()
        TrackTimeListeners.updateAll()
        if (StateLive.autopauseEnabled) {
            val minRecentPointTime = time - StateLive.autopausePauseThresholdTime
            val recentPointsWithinThreshold = StateLive.recentPoints.filter {
                it.time > minRecentPointTime
            }
            if (recentPointsWithinThreshold.count() > 0) {
                val shouldPause = recentPointsWithinThreshold.fold(true) { acc, recentPoint ->
                    acc &&
                            recentPoint.speed != null &&
                            (recentPoint.speed!! < StateLive.autopausePauseThresholdSpeed)
                }
                if (shouldPause) {
                    StateLive.historyPoints.removeAll { it.time > minRecentPointTime }

                    if (StateLive.historyPoints.count() > 0) {
                        StateLive.historyPoints.last().exclude = true
                    }

                    StateLive.setCurrentState(Autopaused())
                }
            }
        }
    }

    override fun notifyLocation(location: Location) {
        StateLive.historyPoints.add(HistoryPoint(location.time, location))
    }

    override fun buttonTap() {
        if (StateLive.historyPoints.count() > 0) {
            StateLive.historyPoints.last().exclude = true
        }

        StateLive.setCurrentState(Paused())
    }

    override fun buttonLongTap() {
        if (StateLive.historyPoints.count() > 0) {
            StateLive.historyPoints.last().exclude = true
        }
        StateLive.setCurrentState(Paused())

    }
}
