package app.tomato.basic.data.connection

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

/**
 * Created by user on 26.04.17.
 */

object ConnectionManager {

    fun isConnected(context: Context): Boolean {
        try {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            val isConnected = activeNetwork != null && activeNetwork.isConnected
            val state = if (isConnected) 2 else 1
            return state == 2
        } catch (ex: Exception) {
            return true
        }

    }
}
