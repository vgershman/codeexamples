package app.tomato.handlebars.ui.dashboard

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import app.tomato.handlebars.data.Dashboard
import app.tomato.handlebars.data.DashboardManager
import app.tomato.handlebars.data.Widget
import app.tomato.handlebars.ui.util.SwipeTouchCallback
import kotlinx.android.synthetic.main.fragment_edit_dashboards.*
import kotlinx.android.synthetic.main.item_edit.view.*

/**
 * Created by user on 27.02.18.
 */

class EditDashboardsFragment : BasicFragment() {

    override val hasToolbar = true

    private var touchHelper: ItemTouchHelper? = null

    override val titleId = R.string.ttl_dashboards

    override fun initViews() {
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = DashboardsListAdapter({ editDashboard(it) }, { askDelete(it) }, { drag(it) })
        touchHelper = ItemTouchHelper(SwipeTouchCallback(rv.adapter))
        touchHelper?.attachToRecyclerView(rv)
        DashboardManager.observe(this, Observer { bindDashboards() })
        toolbar?.inflateMenu(R.menu.edit_menu)
        toolbar?.setOnMenuItemClickListener {
            if (it.itemId == R.id.add_dashboard) {
                addDashboard()
            }
            true
        }

    }

    private fun drag(holder: DashboardsListAdapter.VH) {
        touchHelper?.startDrag(holder)
    }

    private fun askDelete(dashboard: Dashboard) {
        if (DashboardManager.value?.size ?: 0 > 1) {
            AlertDialog.Builder(context)
                    .setMessage(R.string.alert_delete)
                    .setPositiveButton("Да", { d, _ -> d.cancel(); deleteDashboard(dashboard) })
                    .setNegativeButton("Отмена", { d, _ -> d.cancel() })
                    .create().show()
        } else {
            AlertDialog.Builder(context)
                    .setMessage("Нельзя удалить последний дашборд")
                    .setPositiveButton("Ок", { d, _ -> d.cancel() })
                    .create().show()
        }
    }

    private fun deleteDashboard(dashboard: Dashboard) {
        DashboardManager.transaction {
            dashboard.deleteFromRealm()
        }
        DashboardManager.fixPositions()
    }

    private fun addDashboard() {
        if (DashboardManager.value?.size ?: 0 < 10) {
            val newDashboard = DashboardManager.newDashboard()
            editDashboard(newDashboard)
        } else {
            AlertDialog.Builder(context)
                    .setMessage("Дашбордов должно быть не больше 10")
                    .setPositiveButton("Ок", { d, _ -> d.cancel() })
                    .create().show()
        }
    }

    private fun editDashboard(dashboard: Dashboard) {
        go(MyApp.Screens.EDIT_DASHBOARD, params = hashMapOf("did" to dashboard.id))
    }

    private fun bindDashboards() {
        rv.adapter.notifyDataSetChanged()
    }

    override val containerView = R.layout.fragment_edit_dashboards
}

class DashboardsListAdapter(var click: (dashboard: Dashboard) -> Unit, var delete: (dashboard: Dashboard) -> Unit, var drag: (holder: VH) -> Unit) : RecyclerView.Adapter<DashboardsListAdapter.VH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent.context).inflate(R.layout.item_edit, parent, false))

    override fun getItemCount() = DashboardManager.value?.size ?: 0

    override fun onBindViewHolder(holder: VH, position: Int) {
        val dashboard = DashboardManager.value!![position]
        holder.bind(dashboard, DashboardManager.listWidgetsByDashboard(dashboard))
    }


    inner class VH(var view: View) : RecyclerView.ViewHolder(view) {
        fun bind(dashboard: Dashboard, widgets: List<Widget>) {
            itemView.tv_name.text = "Dashboard #${dashboard.position}"
            val sb = StringBuilder()
            widgets.forEach {
                sb.append("${it.name}, ")
            }
            itemView.tv_desc.text = sb.dropLast(2)
            itemView.setOnClickListener { click(dashboard) }
            itemView.btnDelete.setOnClickListener { delete(dashboard) }
            itemView.btnOrder.setOnLongClickListener { drag(this);true }
        }

    }
}