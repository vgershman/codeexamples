package app.tomato.basic.help


import android.content.Intent
import android.os.Bundle
import android.util.Log
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.basic.ui.basic.BasicFragmentActivity
import java.io.Serializable


object ScreenNavigator {

    val EXTRA = "screen"


    fun navigate(activity: BasicFragmentActivity?, screen: Class<out BasicFragment>? = null, container: Class<out BasicFragmentActivity>? = null, finish: Boolean = false, force: Boolean = false, result: Int? = null, params: HashMap<String, Any?>? = null, args: Bundle? = null, from: BasicFragment? = null, nostack: Boolean = false, add: Boolean = false) {
        try {
            if (activity == null) return
            hideKeyboard(activity.findViewById(activity.fragmentContainer), activity)
            if (container == null) {
                if (screen == null) return
                openFragment(activity, screen, force, params, args, nostack, add)
            } else {
                openActivity(activity, screen, container, finish, result, params, args, from)
            }
        } catch (ex: Exception) {
            Log.e("ScreenNavigator", ex.message + "")
        }

    }


    private fun openActivity(activity: BasicFragmentActivity, screen: Class<out BasicFragment>?, container: Class<out BasicFragmentActivity>, finish: Boolean, result: Int?, params: HashMap<String, Any?>?, args: Bundle?, from: BasicFragment?) {
        val intent = Intent(activity, container)
        intent.putExtra(EXTRA, screen)
        intent.putExtras(args(args, params))
        if (result == null || result < 1) {
            activity.startActivity(intent)
        } else {
            if (from == null) {
                activity.startActivityForResult(intent, result)
            } else {
                from.startActivityForResult(intent, result)
            }
        }
        if (finish) {
            activity.finish()
        }
    }

    private fun args(args: Bundle?, params: HashMap<String, Any?>?): Bundle {
        val bundle = args ?: Bundle()
        params?.keys?.forEach { it ->
            when (params.getValue(it)) {
                is String -> bundle.putString(it, params[it] as String)
                is Serializable -> bundle.putSerializable(it, params[it] as Serializable)
                is Int -> bundle.putInt(it, params[it] as Int)
            }
        }
        return bundle
    }


    private fun openFragment(activity: BasicFragmentActivity, screen: Class<out BasicFragment>, force: Boolean, params: HashMap<String, Any?>?, args: Bundle?, nostack: Boolean = false, add: Boolean = false) {
        if (activity.currentFragment != null && activity.currentFragment?.javaClass == screen && !force) return
        var transaction = activity.supportFragmentManager.beginTransaction()
        val newFragment = screen.newInstance()
        newFragment.arguments = args(args, params)
        if (!add) {
            transaction.replace(activity.fragmentContainer, newFragment)
        } else {
            transaction.add(activity.fragmentContainer, newFragment)
        }
        if (!nostack) {
            transaction.addToBackStack(screen.name)
        }
        transaction.commit()
        activity.currentFragment = newFragment

    }

}
