package app.tomato.handlebars.data.widget

import android.arch.lifecycle.LiveData
import android.location.Location
import app.tomato.handlebars.data.Utils
import app.tomato.handlebars.data.state.StateLive

/**
 * Created by user on 26.03.18.
 */

object CurrentSpeedLiveData : LiveData<Float>() {

    fun setSpeed(speed: Float?) {
        value = speed
    }

    fun update(location: Location) {
        Utils.async({ StateLive._calculateCurrentSpeed(location) },
                { value = it as Float? })
    }
}