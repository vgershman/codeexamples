package app.tomato.handlebars.data.state

import android.location.Location

/**
 * Created by themylogin on 3/14/18.
 */
data class HistoryPoint(
    var time: Long,
    var location: Location,

    var exclude: Boolean = false
)
