package app.tomato.handlebars.ui.container

import android.arch.lifecycle.Observer
import android.system.Os.bind
import android.view.Window
import android.view.WindowManager
import app.tomato.basic.ui.basic.BasicFragmentActivity
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import app.tomato.handlebars.data.state.StateLive
import app.tomato.handlebars.data.state.track_recording_state.TrackRecordingState


/**
 * Created by user on 30.01.18.
 */


class FullscreenSimpleActivity : SimpleActivity() {

    override fun preInit() {
        super.preInit()
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }
}

open class SimpleActivity : BasicFragmentActivity() {
    override fun init() {
        StateLive.observe(this, Observer { bindState(it) })
    }

    private fun bindState(it: TrackRecordingState?) {
    }


    override val contentViewId = R.layout.activity_simple

    override val defaultScreen = MyApp.Screens.SPLASH

}

