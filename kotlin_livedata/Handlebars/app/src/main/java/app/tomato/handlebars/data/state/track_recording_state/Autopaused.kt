package app.tomato.handlebars.data.state.track_recording_state

import android.location.Location
import app.tomato.handlebars.data.state.HistoryPoint
import app.tomato.handlebars.data.state.StateLive
import app.tomato.handlebars.data.widget.AverageSpeedListeners
import app.tomato.handlebars.data.widget.TrackTimeListeners

/**
 * Created by themylogin on 3/14/18.
 */

class Autopaused : TrackRecordingState() {
    override fun tick(time: Long) {
        AverageSpeedListeners.updateAll()
        TrackTimeListeners.updateAll()
    }

    override fun notifyLocation(location: Location) {
        if (StateLive.autopauseEnabled) {
            val minRecentPointTime = location.time - StateLive.autopauseResumeThresholdTime
            val recentPointsWithinThreshold = StateLive.recentPoints.filter {
                it.time > minRecentPointTime
            }
            if (recentPointsWithinThreshold.count() > 0) {
                val shouldResume = recentPointsWithinThreshold.fold(true) { acc, recentPoint ->
                    acc &&
                            recentPoint.speed != null &&
                            (recentPoint.speed!! > StateLive.autopauseResumeThresholdSpeed)
                }
                if (shouldResume) {
                    recentPointsWithinThreshold.forEach {
                        StateLive.historyPoints.add(HistoryPoint(it.time, it.location))
                    }
                    StateLive.setCurrentState(Recording())
                }
            }
        }
    }

    override fun buttonTap() {
        StateLive.setCurrentState(Recording())
    }

    override fun buttonLongTap() {

    }
}
