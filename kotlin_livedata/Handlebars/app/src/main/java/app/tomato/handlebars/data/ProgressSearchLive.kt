package app.tomato.handlebars.data

import android.arch.lifecycle.LiveData

/**
 * Created by user on 28.03.18.
 */
object ProgressSearchLive : LiveData<String>() {
    fun clear() {
        value = null
    }

    fun set(progress: String?) {
        value = progress
    }
}
