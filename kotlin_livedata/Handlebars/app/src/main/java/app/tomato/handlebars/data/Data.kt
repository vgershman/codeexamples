package app.tomato.handlebars.data

import app.tomato.handlebars.ui.Selectable
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.io.Serializable

/**
 * Created by user on 27.02.18.
 */
open class Dashboard(@PrimaryKey var id: String? = null, var position: Int? = null,
                     var large: Int = 0, var normal: Int = 0, var medium: Int = 0,
                     var maps: Boolean = false, var mapsHeight: Int? = 50, var zoom: Float? = 0f) : RealmObject()

open class Widget(@PrimaryKey var id: String? = null, var dashId: String? = null,
                  var type: String? = null,
                  var position: Int? = null, var name: String? = null,
                  var setting: String? = null, var zoom: Float? = 0f) : RealmObject()

data class Category(var name: String, var widgets: List<WidgetOption>) : Selectable {
    override fun name() = name
}

data class WidgetOption(var name: String, var type: String) : Selectable {
    override fun name() = name
}

open class Track(@PrimaryKey var id: String? = null, var name: String? = null, var distance: Float? = 0f) : RealmObject()

open class LocationDao(var lat: Double? = null, var lon: Double? = null, var timestamp: Long = System.currentTimeMillis(),
                       var excluded: Boolean? = false, var trackId: String? = null) : RealmObject()


data class Guest(var access_token: String)

data class Me(var id: Int, var screen_name: String, var display_name: String?, var avatar: String?)

open class Route(@PrimaryKey var id: Int? = null, var name: String? = null, var offline: Boolean? = null, var track: TrackDto? = null) : RealmObject()

open class TrackDto(@PrimaryKey var id: Int? = null, var start_datetime: String? = null,
                    var end_datetime: String? = null, var total_time: Float? = null,
                    var active_time: Long? = null, var total_distance: Double? = null,
                    var active_distance: Double? = null, var total_speed: Double? = null,
                    var active_speed: Double? = null, var ascent: Double? = null,
                    var descent: Double? = null, var description: String? = null,
                    var preview: String? = null, var preview_thumbnail: String? = null) : RealmObject(), Serializable

open class LocationDto(var datetime: String, var elevation: Double, var location: List<Double>)

data class HintDto(var name: String, var type: String, var title: String, var url: String? = null)

data class SettleDto(var distance: Double, var key: String, var title: String, var location: List<Double>)

open class RecentDto(@PrimaryKey var query: String? = null, var timestamp: Long? = null, var point: Point? = null) : RealmObject()


open class Point(var lat: Double? = null, var lng: Double? = null, var title: String? = null, var subtitle: String? = null) : RealmObject() {
    fun getLocationQuery(): String {
        return "$lat,$lng"
    }

    fun getLocationFullName(): String{
        if(subtitle!=null){
            return "$title, $subtitle"
        }else{
            return "$title"
        }

    }
}

