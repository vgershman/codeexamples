package app.tomato.handlebars.ui

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.view.View
import android.widget.Toast
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.R
import app.tomato.handlebars.data.WidgetOption
import kotlinx.android.synthetic.main.fragment_map_settings.*
import kotlinx.android.synthetic.main.fragment_speed_settings.*
import kotlinx.android.synthetic.main.fragment_time_settings.*

/**
 * Created by user on 03.03.18.
 */

class MapSettings : BasicFragment() {


    var mapNames = arrayOf("OpenStreetMap", "Strava Heat Map")
    var mapTypes = arrayOf("osm.yaml", "strava.yaml")

    var mapType = "osm.yaml"
    var mapSize = 50

    var widget: WidgetOption? = null

    override fun initViews() {
        toolbar?.inflateMenu(R.menu.ok_menu)
        toolbar?.setOnMenuItemClickListener {
            if (it.itemId == R.id.btn_ok) {
                onOK()
            };true
        }
        btnMapType.setOnClickListener { selectMapType() }

        btnH1.setOnClickListener {
            mapSize = 33
            bindMapSize()
        }
        btnH2.setOnClickListener {
            mapSize = 50
            bindMapSize()
        }
        btnH3.setOnClickListener {
            mapSize = 67
            bindMapSize()
        }

    }

    private fun selectMapType() {
        AlertDialog.Builder(context)
                .setTitle("Источник карты")
                .setItems(mapNames, { d, item -> d.cancel(); setMapType(item) })
                .create().show()

    }

    private fun setMapType(item: Int) {
        mapType = mapTypes[item]
        bindMapType()
    }

    private fun onOK() {
        activity?.setResult(Activity.RESULT_OK, Intent()
                .putExtra("widget", widget)
                .putExtra("setting", mapType)
                .putExtra("height", mapSize))
        activity?.finish()
    }

    private fun bindMapSize() {
        btnH1.isSelected = mapSize == 33
        btnH2.isSelected = mapSize == 50
        btnH3.isSelected = mapSize == 67
    }

    override fun initProperties() {
        super.initProperties()
        widget = arguments!!["widget"] as WidgetOption?
        mapSize = arguments!!["height"] as Int? ?: 50
        mapType = arguments!!["settings"] as String? ?: "osm.yaml"
        bindMapSize()
        bindMapType()

    }

    private fun bindMapType() {
        lblMapType.text = mapNames[mapTypes.indexOf(mapType)]
    }

    override val hasToolbar = true

    override val titleId = R.string.ttl_settings

    override val containerView = R.layout.fragment_map_settings

}

class TimeSettings : BasicFragment() {

    var widget: WidgetOption? = null
    var exclude = true


    override val hasToolbar = true

    override val titleId = R.string.ttl_settings

    override fun initViews() {
        toolbar?.inflateMenu(R.menu.ok_menu)
        toolbar?.setOnMenuItemClickListener {
            if (it.itemId == R.id.btn_ok) {
                onOK()
            };true
        }
    }

    override fun initProperties() {
        super.initProperties()
        widget = arguments!!["widget"] as WidgetOption?
        exclude = arguments!!["settings"] as String? ?: "excluded" == "excluded"
        bindExcluded()

    }

    private fun bindExcluded() {
        swTime.isChecked = exclude
        swTime.setOnCheckedChangeListener { _, isChecked ->
            exclude = isChecked
        }
    }

    private fun onOK() {
        var settings = "excluded"
        if (!exclude) {
            settings = "included"
        }
        activity?.setResult(Activity.RESULT_OK, Intent()
                .putExtra("widget", widget)
                .putExtra("setting", settings))
        activity?.finish()
    }

    override val containerView = R.layout.fragment_time_settings

}

class AverageSpeedSettings : BasicFragment() {

    var names = arrayOf("Per ride", "Per last km", "Per last  minutes")
    var types = arrayOf("ride", "dst", "time")

    var widget: WidgetOption? = null
    var type = "ride"
    var count = 5
    var excluded = true

    override fun initViews() {
        toolbar?.inflateMenu(R.menu.ok_menu)
        toolbar?.setOnMenuItemClickListener {
            if (it.itemId == R.id.btn_ok) {
                onOK()
            };true
        }
        btnType.setOnClickListener {
            AlertDialog.Builder(context)
                    .setTitle("Average speed")
                    .setItems(names, { d, item -> d.cancel(); setType(item) })
                    .create().show()


        }
    }

    override fun initProperties() {
        super.initProperties()
        widget = arguments!!["widget"] as WidgetOption?
        var settings = arguments!!["settings"] as String? ?: "ride_excluded"
        bindSetting(settings)

    }

    private fun bindSetting(settings: String) {
        var set = settings.split("_")
        type = set[0]
        if (type != "ride") {
            count = Integer.parseInt(set[1])
            excluded = set[2] == "excluded"
        } else {
            excluded = set[1] == "excluded"
        }
        setType(types.indexOf(type))
        bindExcluded()
    }

    private fun bindExcluded() {
        swSpeed.isChecked = excluded
        swSpeed.setOnCheckedChangeListener { _, isChecked ->
            excluded = isChecked
        }
    }


    private fun setType(item: Int) {
        type = types[item]

        when (type) {
            "ride" -> {
                lblType.text = "Per ride"
                etCount.visibility = View.GONE; lblMetrics.visibility = View.GONE
            }
            "time" -> {
                lblType.text = "Per last"
                etCount.visibility = View.VISIBLE; lblMetrics.visibility = View.VISIBLE
                etCount.setText(count.toString())
                etCount.setSelection(etCount.text.toString().length)
                lblMetrics.text = " min."

            }
            "dst" -> {
                lblType.text = "Per last"
                etCount.visibility = View.VISIBLE; lblMetrics.visibility = View.VISIBLE
                etCount.setText(count.toString())
                etCount.setSelection(etCount.text.toString().length)
                lblMetrics.text = " km"
            }
        }
    }


    private fun onOK() {
        var count = 0
        try {
            count = Integer.valueOf(etCount.text.toString())
        } catch (ex: Exception) {
        }
        if (type != "ride" && count < 1) {
            Toast.makeText(context, "Неверно введены данные", Toast.LENGTH_LONG).show()
            return
        }
        val b = StringBuilder()
        b.append(type)
        if (type != "ride") {
            b.append("_")
            b.append(etCount.text.toString())
        }
        b.append("_")
        if (excluded) {
            b.append("excluded")
        } else {
            b.append("included")
        }

        activity?.setResult(Activity.RESULT_OK, Intent()
                .putExtra("widget", widget)
                .putExtra("setting", b.toString()))
        activity?.finish()
    }

    override val containerView = R.layout.fragment_speed_settings
    override val hasToolbar = true
    override val titleId = R.string.ttl_settings

}