package app.tomato.handlebars.ui

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import app.tomato.handlebars.data.*
import app.tomato.handlebars.ui.container.ImageViewerActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_find_route.*
import kotlinx.android.synthetic.main.item_hint.view.*
import kotlinx.android.synthetic.main.item_route.view.*

/**
 * Created by user on 13.03.18.
 */


class FindRouteFragment : BasicFragment() {

    var firstInit = false
    var firstSearch = true

    override fun initViews() {
        lblNoResults.visibility = View.GONE
        btnSettings.setOnClickListener { go(MyApp.Screens.SEARCH_SETTINGS) }
        btnSwap.setOnClickListener {
            SQManager.get(context!!).swap()
        }
        tv_from.setOnClickListener { selectFrom() }
        tv_to.setOnClickListener { selectTo() }
        btnBack.setOnClickListener { onBackPressed() }
        rvHints.layoutManager = LinearLayoutManager(context)
        rvHints.adapter = HintsAdapter(context!!, { onSelectChanged(it) })
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = SearchAdapter({ openRoute(it) }, { preview(it) })
        LocationLiveManager.context = context
        setFirstFromMyLocation()
        SQManager.get(context!!).observe(this, Observer { proceedSearch(it) })
        SearchResultsManager.observe(this, Observer {
            bindResults(it)
        })
        ProgressSearchLive.observe(this, Observer { bindProgress(it) })
        ProgressSearchLive.clear()
        HintsManager.observe(this, Observer { (rvHints.adapter as HintsAdapter).setData(it) })
    }

    private fun bindProgress(it: String?) {
        if (it.isNullOrEmpty()) {
            progressLabel.visibility = View.GONE
        } else {
            progressLabel.text = it
            progressLabel.visibility = View.VISIBLE
        }
    }

    private fun setFirstFromMyLocation() {
        if (LocationLiveManager.value != null) {
            firstInit = true
            SQManager.get(context!!).setMyLocation()
        } else {
            LocationLiveManager.observe(this, Observer {
                if (it != null && !firstInit) {
                    firstInit = true
                    SQManager.get(context!!).setMyLocation()
                }
            })
        }
    }

    private fun bindResults(it: List<TrackDto>?) {
        (rv.adapter as SearchAdapter).setData(it)
        if (it != null) {
            rv.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            ProgressSearchLive.clear()
        }
        if (it == null) {
            lblNoResults.visibility = View.GONE
            return
        }
        if (it.isEmpty()) {
            lblNoResults.visibility = View.VISIBLE
        }


    }

    private fun onSelectChanged(hint: HintDto?) {
        SQManager.get(context!!).setCheck(hint)
    }

    private fun proceedSearch(searchQuery: SearchQuery?) {
        (rvHints.adapter as HintsAdapter).selectChanged
        SearchResultsManager.search(context!!, searchQuery, {
            ProgressSearchLive.clear()
            rv.visibility = View.INVISIBLE
            lblNoResults.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        })
        if (searchQuery?.checkbox == MyApp.Config.YANDEX_GEOCODE) {
            btnSwap.isEnabled = false
            btnSwap.alpha = 0.5f
            tv_to.text = searchQuery.customPoint?.title ?: ""
        } else {
            btnSwap.alpha = 1f
            btnSwap.isEnabled = true
            tv_to.text = searchQuery?.to?.getLocationFullName() ?: ""
        }
        tv_from.text = searchQuery?.from?.getLocationFullName() ?: ""

        HintsManager.updateHints(context, searchQuery?.from?.getLocationQuery())


    }

    private fun selectTo() {
        if (SQManager.get(context!!).value?.checkbox == MyApp.Config.YANDEX_GEOCODE) {
            go(MyApp.Screens.CUSTOM_TO, add = true)
        } else {
            go(MyApp.Screens.FROM_TO, add = true, params = hashMapOf("from" to false))
        }
    }

    private fun selectFrom() {
        go(MyApp.Screens.FROM_TO, add = true, params = hashMapOf("from" to true))
    }


    private fun preview(it: TrackDto) {
        if (!it.preview.isNullOrBlank()) {
            startActivity(Intent(context, ImageViewerActivity::class.java).putExtra(ImageViewerActivity.EXTRA_URL, it.preview))
        }
    }

    private fun openRoute(track: TrackDto) {
        go(MyApp.Screens.ROUTE, params = hashMapOf("track" to track))
    }

    override val containerView = R.layout.fragment_find_route

}

class SearchAdapter(var click: (route: TrackDto) -> Unit, var preview: (route: TrackDto) -> Unit) : RecyclerView.Adapter<SearchAdapter.VH>() {

    private val data = arrayListOf<TrackDto>()

    fun setData(routes: List<TrackDto>?) {
        if (routes != null) {
            data.clear()
            data.addAll(routes)
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false))


    override fun getItemCount() = data.size


    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(data[position])
    }


    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(route: TrackDto) {
            itemView.ivPreview.setOnClickListener { preview(route) }
            itemView.setOnClickListener { click(route) }
            val h = route.total_time?.div(3600)?.rem(60)
            val m = route.total_time?.div(60)?.rem(60)
            itemView.tv_name.text = "${String.format("%.1f", (route?.active_distance?.div(1000)))} km, $h h $m min"
            if (!route.preview_thumbnail.isNullOrBlank()) {
                Picasso.get().load(route.preview_thumbnail).into(itemView.ivPreview)
            } else {
                itemView.ivPreview.setImageResource(R.drawable.ic_image_black_48px)
            }
            itemView.tv_desc.text = Html.fromHtml(route.description)
        }

    }
}

class HintsAdapter(var context: Context, var selectChanged: (hint: HintDto?) -> Unit) : RecyclerView.Adapter<HintsAdapter.VH>() {

    fun getSelectedPos(): Int {
        val selected = data.find { (it.name == SQManager.get(context).value?.checkbox || (SQManager.get(context).value?.checkbox == MyApp.Config.YANDEX_GEOCODE && it.type == "custom_to")) }
        if (selected != null) {
            return data.indexOf(selected)
        }
        return -1
    }


    fun setData(hints: List<HintDto>?) {
        data.clear()
        if (hints != null) {
            data.addAll(hints)
        }
        notifyDataSetChanged()
    }

    private val data = arrayListOf<HintDto>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent.context).inflate(R.layout.item_hint, parent, false))

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: VH, position: Int) = holder.bind(data[position], position)

    inner class VH(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(hint: HintDto, position: Int) {
            itemView.cb.isChecked = getSelectedPos() == position
            itemView.tvTitle.text = hint.title
            itemView.setOnClickListener {
                if (getSelectedPos() == position) {
                    selectChanged(null)
                } else {
                    selectChanged(hint)
                }

                notifyDataSetChanged()
            }
        }

    }
}