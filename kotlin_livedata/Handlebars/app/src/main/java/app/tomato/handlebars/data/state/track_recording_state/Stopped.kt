package app.tomato.handlebars.data.state.track_recording_state

import android.location.Location
import app.tomato.handlebars.data.state.StateLive

/**
 * Created by themylogin on 3/14/18.
 */

class Stopped : TrackRecordingState() {
    override fun tick(time: Long) {

    }

    override fun notifyLocation(location: Location) {

    }

    override fun buttonTap() {
        StateLive.setCurrentState(Recording())
    }

    override fun buttonLongTap() {

    }
}
