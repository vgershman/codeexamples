package app.tomato.handlebars.data

import android.arch.lifecycle.LiveData
import android.content.Context
import app.tomato.basic.data.ToastCallback
import com.vicpin.krealmextensions.transaction
import io.realm.Realm
import java.text.SimpleDateFormat

/**
 * Created by user on 12.03.18.
 */


object RoutesLoadingManager : LiveData<ArrayList<Int>>() {
    init {
        value = arrayListOf()
    }

    fun loading(id: Int) {
        if (value == null) {
            value = arrayListOf()
        }
        if (!value!!.contains(id)) {
            value!!.add(id)
        }
        value = value
    }

    fun isLoading(id: Int): Boolean {
        if (value == null) {
            value = arrayListOf()
        }
        return value!!.contains(id)
    }

    fun cancelLoading(id: Int) {
        value?.remove(id)
        value = value
    }
}

object RoutesManager : LiveData<List<Route>>() {

    val serverTimeFormat = SimpleDateFormat("yyyy-dd-MM'T'HH:mm:ss")


    override fun onActive() {
        super.onActive()
        readRoutes()
    }

    private fun readRoutes() {
        value = Realm.getDefaultInstance().where(Route::class.java).findAll()
    }

    fun syncRoutes(context: Context, callback: () -> Unit) {
        Rest.rest().listMyRoutes().enqueue(object : ToastCallback<List<Route>>(context) {
            override fun onSuccess(response: List<Route>) {
                storeRoutes(response, context)
            }

            override fun onCallback() {
                super.onCallback()
                callback()
            }
        })
    }

    private fun storeRoutes(response: List<Route>, context: Context) {
        response.forEach {
            if (it.offline == true) {
                if (it.track?.id != null && isCached(it.track?.id) != 0) {
                    syncRoute(context, it.track?.id, {})
                }
            }
        }
        RealmUtil.transaction {
            Realm.getDefaultInstance().copyToRealmOrUpdate(response)
        }
        readRoutes()
    }

    fun isCached(trackId: Int?): Int {
        if (trackId == null) {
            return 0
        } else {
            if (RoutesLoadingManager.isLoading(trackId)) {
                return 1
            }
            return if (Realm.getDefaultInstance().where(LocationDao::class.java).equalTo("trackId", trackId.toString()).count() != 0L) {
                2
            } else {
                0
            }
        }
    }

    fun getRouteById(routeId: Int): Route? {
        return Realm.getDefaultInstance().where(Route::class.java).equalTo("id", routeId).findFirst()
    }

    fun getTrackById(trackId: Int): List<LocationDao> {
        return Realm.getDefaultInstance().where(LocationDao::class.java).equalTo("trackId", trackId.toString()).sort("timestamp").findAll()
    }

    fun syncRoute(context: Context, id: Int?, callback: (points: List<LocationDao>) -> Unit) {

        if (id != null) {
            if (Realm.getDefaultInstance().where(LocationDao::class.java).equalTo("trackId", id.toString()).count() == 0L) {
                RoutesLoadingManager.loading(id)
                Rest.rest().getTrack(id).enqueue(object : ToastCallback<List<LocationDto>>(context) {
                    override fun onSuccess(response: List<LocationDto>) {
                        Realm.getDefaultInstance().transaction {
                            response.forEach {
                                val locationDao = LocationDao(lat = it.location[0], lon = it.location[1], timestamp = serverTimeFormat.parse(it.datetime).time,
                                        trackId = id.toString(), excluded = false)
                                Realm.getDefaultInstance().copyToRealm(locationDao)
                            }
                        }
                        callback(getTrackById(id))
                    }

                    override fun onCallback() {
                        super.onCallback()
                        RoutesLoadingManager.cancelLoading(id)
                    }
                })
            } else {
                callback(getTrackById(id))
            }
        }
    }
}