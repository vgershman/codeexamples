package app.tomato.handlebars.ui.widgets

import android.arch.lifecycle.Observer
import android.graphics.RectF
import android.location.Location
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import app.tomato.basic.help.dpToPx
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.handlebars.MyApp
import app.tomato.handlebars.R
import app.tomato.handlebars.data.DashboardManager
import app.tomato.handlebars.data.LocationDao
import app.tomato.handlebars.data.LocationLiveManager
import app.tomato.handlebars.data.RoutesManager
import app.tomato.handlebars.data.widget.CurrentRouteManager
import com.mapzen.tangram.*
import com.mapzen.tangram.geometry.Polyline
import kotlinx.android.synthetic.main.widget_map.*

/**
 * Created by user on 02.03.18.
 */

class MapWidget : BasicFragment(), MapController.SceneLoadListener {


    private val BORDER_WIDTH = 5f
    private var BORDER_SIZE = 0f

    private val lineStyle = "{ style: 'lines', color: '#fc00', width: [[2,5px], [20,100px]], order: 2000, collide: false }"
    private val pointStyle = "{ style: 'points', color: 'white', size: [20px, 20px], order: 2000, collide: false }"
    private var settings = "osm.yaml"
    var listener: () -> Unit = {}
    private var wid: String? = null
    private var did: String? = null
    private var zoom: Float = 0f

    private var showSettings: Boolean = false
    private var enableZoom: Boolean = false
    private var touchEnabled: Boolean = false
    private var first: Boolean = true
    private var zooming: Boolean = false
    private var routeMarker: Marker? = null
    private var startMarker: Marker? = null
    private var endMarker: Marker? = null
    var trackId: String? = null
    val borderRects = arrayListOf<RectF>()


    private var startTask: MyAsyncTask? = null
    private var endTask: MyAsyncTask? = null

    var myMarkerPoint: LngLat? = null
    var trackPositionIndex = -1

    override fun onSceneReady(sceneId: Int, sceneError: SceneError?) {
        LocationLiveManager.context = activity
        if (zoom > 0f) {
            mapController?.zoom = zoom
        } else {
            mapController?.zoom = MyApp.Config.DEFAULT_ZOOM
        }
        LocationLiveManager.observe(this, Observer { onLocationChanged(it) })
        CurrentRouteManager.observe(this, Observer { bindTrack(it) })
    }

    private fun bindTrack(it: String?) {
        trackId = it
        if (it == null) {
            crosess?.removeAllViews()
            if (routeMarker != null) {
                mapController?.removeMarker(routeMarker)
            }
            if (endMarker != null) {
                mapController?.removeMarker(endMarker)
            }
            if (startMarker != null) {
                mapController?.removeMarker(startMarker)
            }
        } else {
            initBorders()
            bindTrack(RoutesManager.getTrackById(it.toInt()))
        }
    }

    private fun initBorders() {
        borderRects.clear()
        val widthCount = Math.round(map.width / BORDER_SIZE)
        val heightCount = Math.round(map.height / BORDER_SIZE)
        for (i in 0 until widthCount - 1) {
            borderRects.add(RectF(i * BORDER_SIZE, 0f, (i + 1) * BORDER_SIZE, BORDER_WIDTH))
            borderRects.add(RectF(i * BORDER_SIZE, map.height.toFloat() - BORDER_WIDTH, (i + 1) * BORDER_SIZE, map.height.toFloat()))
        }

        for (i in 0 until heightCount - 1) {
            borderRects.add(RectF(0f, i * BORDER_SIZE, BORDER_WIDTH, (i + 1) * BORDER_SIZE))
            borderRects.add(RectF(map.width - BORDER_WIDTH, i * BORDER_SIZE, map.width.toFloat(), (i + 1) * BORDER_SIZE))
        }
    }

    private fun onLocationChanged(it: Location?) {
        try {
            if (it != null) {
                myMarkerPoint = LngLat(it.longitude, it.latitude)
                if (trackId != null) {
                    findMeOnTrack(it)
                    if (trackPositionIndex != -1) {
                        crossss()
                    }
                }
                bindMyLocation()
            }
        } catch (ex: Exception) {
            //wtf maybe
        }

    }

    private fun findMeOnTrack(myLocation: Location) {

        var track = RoutesManager.getTrackById(trackId!!.toInt())
        var closestDistance = Float.MAX_VALUE

        track.forEach {
            val trackPoint = Location("")
            trackPoint.latitude = it.lat ?: 0.toDouble()
            trackPoint.longitude = it.lon ?: 0.toDouble()
            val distance = myLocation.distanceTo(trackPoint)
            if (distance < closestDistance && distance < 50) {
                myMarkerPoint = LngLat(it.lon ?: 0.toDouble(), it.lat ?: 0.toDouble())
                closestDistance = distance
                trackPositionIndex = track.indexOf(it)
            }
        }

    }

    private fun bindMyLocation() {
        if (myMarkerPoint != null) {
            if (markerMe == null) {
                markerMe = mapController?.addMarker()
                markerMe?.setStylingFromString(pointStyle)
                markerMe?.setDrawable(R.drawable.ic_logo_main)
            }
            markerMe?.setPoint(myMarkerPoint)
            if (!touchEnabled || first) {
                first = false
                mapController?.setPositionEased(myMarkerPoint, 200)
            }
        }
    }


    private fun crossss() {
        startTask?.cancel(false)
        endTask?.cancel(false)

        startTask = MyAsyncTask(this, "start")
        startTask?.execute()

        endTask = MyAsyncTask(this, "end")
        endTask?.execute()

    }

    private fun bindTrack(points: List<LocationDao>) {
        val formattedPoints = arrayListOf<LngLat>()
        points.forEach {
            formattedPoints.add(LngLat(it.lon ?: 0.toDouble(), it.lat ?: 0.toDouble()))
        }
        val polyline = Polyline(formattedPoints, null)
        if (routeMarker != null) {
            mapController?.removeMarker(routeMarker)
        }
        routeMarker = mapController?.addMarker()

        routeMarker?.setPolyline(polyline)
        routeMarker?.setStylingFromString(lineStyle)
        // mapController?.setPositionEased(formattedPoints[0], 200)
        //mapController?.zoom = 12f

        val startPoint = LngLat(points[0].lon!!, points[0].lat!!)
        if (startMarker == null) {
            startMarker = mapController?.addMarker()
            startMarker?.setStylingFromString(pointStyle)
            startMarker?.setDrawable(R.drawable.ic_start)
        }
        startMarker?.setPoint(startPoint)


        val endPoint = LngLat(points[points.size - 1].lon!!, points[points.size - 1].lat!!)
        if (endMarker == null) {
            endMarker = mapController?.addMarker()
            endMarker?.setStylingFromString(pointStyle)
            endMarker?.setDrawable(R.drawable.ic_end)
        }
        endMarker?.setPoint(endPoint)

        //crossss()
    }


    private fun checkNearBorder(rectA: RectF, rectB: RectF): Boolean {
        val nearWidth = Math.abs(rectA.left - rectB.left) == BORDER_SIZE
        val nearHeight = Math.abs(rectA.top - rectB.top) == BORDER_SIZE
        val sameLeft = rectA.left == rectA.left && rectA.width() == rectB.width()
        val sameTop = rectA.top == rectB.top && rectA.height() == rectB.height()
        if (sameLeft && nearHeight) {
            return true
        }
        if (sameTop && nearWidth) {
            return true
        }
        return false
    }

    private fun pointVisible(point: LocationDao): Boolean {
        if (mapController == null || map == null) {
            return false
        }
        val screenPoint = mapController!!.lngLatToScreenPosition(LngLat(point.lon!!, point.lat!!))
        if (screenPoint.x < 0 || screenPoint.x > map.width) {
            return false
        }
        if (screenPoint.y < 0 || screenPoint.y > map.height) {
            return false
        }
        return true
    }

    private fun findCross(tag: String): RectF? {

        var points = RoutesManager.getTrackById(trackId?.toIntOrNull() ?: -1)

        if (points.isEmpty()) {
            return null
        }

        if (tag == "end") {
            points = points.subList(trackPositionIndex, points.size)
        }
        if (tag == "start") {
            points = points.subList(0, trackPositionIndex).reversed()
        }


        points.forEachIndexed { index, first ->
            if (index + 1 < points.size) {
                val second = points[index + 1]
                val firstScreen = mapController?.lngLatToScreenPosition(LngLat(first.lon!!, first.lat!!))
                val secondScreen = mapController?.lngLatToScreenPosition(LngLat(second.lon!!, second.lat!!))
                val rect = RectF()

                rect.set(Math.min(firstScreen!!.x, secondScreen!!.x), Math.min(firstScreen.y, secondScreen.y),
                        Math.max(firstScreen.x, secondScreen.x), Math.max(firstScreen.y, secondScreen.y))

                borderRects.forEach {
                    if (RectF.intersects(it, rect)) {
                        return it
                    }
                }
            }
        }

        return null
    }


    private fun bindCross(rect: RectF, tag: String) {
        if (crosess == null) {
            return
        }
        var crossView = crosess.findViewWithTag<ImageView>(tag)
        crosess.removeView(crossView)
        val size = dpToPx(context!!, 20).toInt()

        crossView = ImageView(context)
        crossView.tag = tag
        if (tag == "start") {
            crossView.setImageResource(R.drawable.ic_start)
        }
        if (tag == "end") {
            crossView.setImageResource(R.drawable.ic_end)
        }
        crossView.layoutParams = FrameLayout.LayoutParams(size, size)


        var marginLeft = rect.centerX() - size / 2
        if (marginLeft < 0) {
            marginLeft = 0.toFloat()
        }
        if (marginLeft >= map.width - size) {
            marginLeft = map.width.toFloat() - size
        }
        var marginTop = rect.centerY() - size / 2
        if (marginTop < 0) {
            marginTop = 0.toFloat()
        }
        if (marginTop >= map.height - size) {
            marginTop = map.height.toFloat() - size
        }
        (crossView.layoutParams as FrameLayout.LayoutParams).setMargins(marginLeft.toInt(), marginTop.toInt(), 0, 0)

        crosess.addView(crossView)
    }


    override fun initViews() {
        BORDER_SIZE = dpToPx(context!!, 5)


    }

    override fun initProperties() {
        super.initProperties()
        settings = (arguments!!["setting"] ?: "osm.yaml").toString()
        showSettings = (arguments!!["options"] as Boolean?) ?: false
        wid = arguments!!["wid"] as String?
        did = arguments!!["did"] as String?
        zoom = arguments!!["zoom"] as Float? ?: 0f
        enableZoom = arguments!!["enabledZoom"] as Boolean? ?: false
        touchEnabled = arguments!!["enabledTouch"] as Boolean? ?: false
        initMap()
        checkOptions()
        checkActions()

    }

    private fun checkActions() {
        if (!touchEnabled) {
            disableTouch()
        }
        if (!enableZoom && !touchEnabled) {
            enabledOpenFullscreen()
        }
    }

    private fun initMap() {
        mapController = map?.getMap(this)
        mapController?.loadSceneFile("asset:///$settings")
    }

    private fun checkOptions() {
        if (showSettings) {
            llMapSettings.visibility = View.VISIBLE
            btnMapSettings.setOnClickListener { listener() }
            btnZoomIn.setOnTouchListener { _, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> startZooming(1)
                    MotionEvent.ACTION_UP -> cancelZoom()
                }
                true
            }

            btnZoomOut.setOnTouchListener { _, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> startZooming(-1)
                    MotionEvent.ACTION_UP -> cancelZoom()
                }
                true
            }
        }
    }

    private fun startZooming(i: Int) {
        zooming = true
        changeZoom(i)
    }

    private fun cancelZoom() {
        zooming = false
    }

    private fun changeZoom(i: Int) {
        Handler().postDelayed({
            if (zooming) {
                if (mapController != null) {
                    mapController!!.zoom += i * 0.05f
                    if (mapController!!.zoom >= 15f && settings == "strava.yaml") {
                        mapController!!.zoom = 15f
                    }
                }
                changeZoom(i)
            }
        }, 50)
    }

    private fun enabledOpenFullscreen() {
        mapController?.setDoubleTapResponder { _, _ -> openFullscreen() }
    }

    private fun openFullscreen(): Boolean {
        go(MyApp.Screens.MAP, MyApp.Containers.FULLSCREEN,
                params = hashMapOf("enabledTouch" to true, "did" to did, "zoom" to zoom, "wid" to wid, "setting" to arguments!!["setting"]))
        return true
    }

    private fun disableTouch() {
        mapController?.setPanResponder(object : TouchInput.PanResponder {

            override fun onFling(posX: Float, posY: Float, velocityX: Float, velocityY: Float) = true

            override fun onPan(startX: Float, startY: Float, endX: Float, endY: Float) = true

        })
        mapController?.setRotateResponder { _, _, _ -> true }
        if (!enableZoom) {
            mapController?.setScaleResponder { _, _, _, _ -> onCheckZoom();true }
        }
        mapController?.setTapResponder(object : TouchInput.TapResponder {

            override fun onSingleTapUp(x: Float, y: Float) = true


            override fun onSingleTapConfirmed(x: Float, y: Float) = true
        })
        mapController?.setShoveResponder { _ -> true }
    }

    private fun onCheckZoom() {
        if (mapController!!.zoom >= 15f && settings == "strava.yaml") {
            mapController!!.zoom = 15f
        }
    }

    var markerMe: Marker? = null

    var mapController: MapController? = null

    override val containerView = R.layout.widget_map


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        map?.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        map?.onResume()
    }

    override fun onPause() {
        super.onPause()
        saveZoom()
        map?.onPause()

    }

    private fun saveZoom() {
        if (enableZoom || touchEnabled) {
            if (wid != null) {
                val widget = DashboardManager.getWidgetById(wid!!)
                if (widget != null) {
                    DashboardManager.transaction {
                        widget.zoom = mapController?.zoom
                    }
                }
            } else {
                if (did != null) {
                    val dash = DashboardManager.getDashboardById(did!!)
                    if (dash != null) {
                        DashboardManager.transaction {
                            dash.zoom = mapController?.zoom
                        }
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        map?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map?.onLowMemory()
    }

    companion object {
        private class MyAsyncTask(private val mapWidget: MapWidget, private val tag: String) : AsyncTask<Unit, Unit, Unit>() {

            var result_: RectF? = null

            override fun doInBackground(vararg params: Unit?) {
                result_ = mapWidget.findCross(tag)//mapWidget.bindCrosses(RoutesManager.getTrackById(mapWidget.trackId!!.toInt()))
            }

            override fun onPostExecute(result: Unit?) {
                super.onPostExecute(result)
                if (result_ != null) {
                    mapWidget.bindCross(result_!!, tag)
                }
            }

        }
    }
}