package app.tomato.basic.data

import app.tomato.mainpeople.FcmManager
import app.tomato.mainpeople.data.AuthManager
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.util.*

class AddCookiesInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
        builder.addHeader("MP-device-type", "android")
        builder.addHeader("MP-locale", Locale.getDefault().language.toLowerCase() + "-" + Locale.getDefault().language.toUpperCase())
        if (FcmManager.value != null) {
            builder.addHeader("MP-token", FcmManager.value)
        }
        if (AuthManager.value != null) {
            builder.addHeader("MP-app-id", AuthManager.value!!.unique)
            builder.addHeader("MP-version", AuthManager.value!!.version)
            val s_builder = StringBuilder()
            AuthManager.value!!.cookies.forEach {
                s_builder.append(it.split(";")[0])
                s_builder.append(";")
            }
            builder.addHeader("Cookie", s_builder.toString())
        }

        return chain.proceed(builder.build())
    }
}