package app.tomato.basic.ui.basic


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import android.view.View
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.fragment_tabs_base.*
import java.util.*


/**
 * Created by user on 12.05.16.
 */
abstract class BasicTabsFragment : BasicFragment() {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initTabs()
        super.onViewCreated(view, savedInstanceState)
    }


    override fun reload() {
        (vp_fragments.adapter as TabsAdapter).reload()
    }

    protected fun initTabs() {
        setTabs()
    }

    protected fun setTabs() {
        tabs!!.tabMode = TabLayout.MODE_SCROLLABLE
        var titles = context.resources.getStringArray(titlesId)
        vp_fragments.adapter = TabsAdapter(childFragmentManager, titles, screens)
        tabs!!.setupWithViewPager(vp_fragments)
    }


    protected abstract val titlesId: Int

    protected abstract val screens: Array<Class<out BasicFragment>>


    override val containerView = R.layout.fragment_tabs_base

}

class TabsAdapter(fm: FragmentManager, private val titles: Array<String>? = null, private val screens: Array<Class<out BasicFragment>>) : FragmentPagerAdapter(fm) {
    private var fragments = ArrayList<BasicFragment?>()


    init {
        for (i in screens.indices) {
            fragments.add(null)
        }
    }

    override fun getItem(position: Int): Fragment {
        var fragment: BasicFragment? = fragments[position]
        if (fragment == null) {
            try {
                fragment = screens[position].newInstance()
                fragments[position] = fragment
            } catch (ex: Exception) {
                Log.e("tabs", ex.message + "")
            }

        }
        return fragment!!
    }

    override fun getCount(): Int {
        return screens.size
    }


    override fun getPageTitle(position: Int): CharSequence {
        if (titles != null) {
            return titles[position]
        } else {
            return ""
        }
    }

    fun reload() {
        fragments.forEach {
            it?.reload()
        }
    }

}
