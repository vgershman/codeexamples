package app.tomato.basic.data

import android.support.annotation.Keep
import app.tomato.mainpeople.MyApp
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.RequestBody
import retrofit2.Converter
import java.io.IOException
import java.lang.reflect.Type


@Keep
abstract class RestApi<out T> {

    private var sApi: T? = null


    fun setNewHost(newHost: String) {

        host = newHost
        sApi = build()
    }

    fun rest(): T {
        if (sApi == null) {
            sApi = build()
        }
        return sApi as T
    }

    private fun build(): T {
        interceptors?.clear()
        init()
        val builder = OkHttpClient.Builder()
        interceptors?.forEach { builder.addInterceptor(it) }
        builder.addInterceptor(logger)
        val client = builder.build()
        return Retrofit.Builder().baseUrl(host).client(client).addConverterFactory(GsonConverterFactory.create(GsonBuilder().create())).build()
                .create(clazz as Class<T>)

    }

    abstract fun init()

    var host: String? = null

    var clazz: Any? = null

    var interceptors: ArrayList<Interceptor>? = null
        get

    val logger: HttpLoggingInterceptor
        get() = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)


    fun addInterceptor(interceptor: Interceptor) {
        if (interceptors == null) {
            interceptors = ArrayList()
        }
        interceptors?.add(interceptor)
    }


}
