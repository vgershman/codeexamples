package app.tomato.basic.help

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface

/**
 * Created by user on 26.04.17.
 */

object AlertUtils {

    private var isShown: Boolean = false

    fun alert(context: Context, force: Boolean, message: Int, btn: Int, listener: DialogInterface.OnClickListener) {
        if (!isShown) {
            AlertDialog.Builder(context).setCancelable(!force).setMessage(message).setPositiveButton(btn, listener).setOnCancelListener { isShown = false }.create().show()
            isShown = true
        }
    }
    fun alert(context: Context, force: Boolean, message: String, btn: Int, listener: DialogInterface.OnClickListener) {
        if (!isShown) {
            AlertDialog.Builder(context).setCancelable(!force).setMessage(message).setPositiveButton(btn, listener).setOnCancelListener { isShown = false }.create().show()
            isShown = true
        }
    }
}
