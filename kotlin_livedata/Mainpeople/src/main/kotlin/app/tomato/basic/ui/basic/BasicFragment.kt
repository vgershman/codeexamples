package app.tomato.basic.ui.basic

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import app.tomato.basic.help.hideKeyboard
import com.mainpeople.android.R


abstract class BasicFragment : Fragment() {

    private var myActivity: BasicFragmentActivity? = null

    open var toolbar: Toolbar? = null
        internal set

    override fun getContext(): Context {
        return super.getContext()!!
    }

    fun go(screen: Class<out BasicFragment>?, container: Class<out BasicFragmentActivity>? = null, finish: Boolean = false, force: Boolean = false, result: Int? = null, params: HashMap<String, Any?>? = null, from: BasicFragment? = null, pop: Boolean = false) {
        myActivity?.go(screen, container, finish, force, result, params, null, from, pop = pop)
    }
    

    fun arguments(): Bundle {
        return arguments!!
    }

    protected fun onBackPressed() {
        hideKeyboard(view, activity)
        myActivity?.onBackPressed()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        myActivity = activity as BasicFragmentActivity
        val main = inflater.inflate(containerView, container, false)
        if (!hasToolbar) {
            return main
        } else {
            val wrapper = inflater.inflate(R.layout.fragment_toolbar_base, container, false)
            wrapper.findViewById<FrameLayout>(R.id.mainContainer).addView(main)
            return wrapper
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        setTitle()
        initViews()
    }

    private fun initToolbarNavigation() {
        if (hasToolbar) {
            toolbar?.setNavigationIcon(R.drawable.ic_back)
            toolbar?.setNavigationOnClickListener({ onBackPressed() })

        }
    }


    private fun setTitle() {
        if (hasToolbar && titleId != -1) {
        }
    }

    protected fun setTitle(title: String) {
        if (hasToolbar) {
            toolbar!!.title = title
        }
    }


    private fun initToolbar() {
        if (hasToolbar) {
            toolbar = view?.findViewById<Toolbar>(R.id.tbStory)
        }
        if (titleId != -1) {
            toolbar?.setTitle(titleId)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initProperties()
        initToolbarNavigation()
    }

    protected abstract fun initViews()

    protected open fun initProperties() {

    }


    protected open val titleId: Int = -1

    abstract val containerView: Int


    open val hasToolbar: Boolean = false


    open fun onFragmentResume() {}

    open fun onFragmentBackPressed(): Boolean {
        return true
    }

    open fun reload() {
    }
}

