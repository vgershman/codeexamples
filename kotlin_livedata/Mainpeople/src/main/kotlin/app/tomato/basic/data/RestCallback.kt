package app.tomato.basic.data

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.support.annotation.Keep
import android.widget.Toast
import app.tomato.basic.data.connection.ConnectionManager
import app.tomato.basic.help.AlertUtils
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.data.*
import com.google.gson.Gson
import com.mainpeople.android.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


@Keep
abstract class BaseCallback<T>(val context: Context) : Callback<T> {

    private var call: Call<T>? = null

    override fun onResponse(call: Call<T>, response: Response<T>) {
        this.call = call
        if (response.isSuccessful && response.body() != null) {
            //  onSuccess(response.body() as T)
            if (response.body() is ResponseDto) {
                if (processServerCommands(response.body() as ResponseDto)) {
                    safeSuccess(response.body() as T)
                }
            } else {
                safeSuccess(response.body() as T)
            }
        } else {
            //            if (response.code() == 401) {
//                try {
//                    //AuthManager.logout(context as Activity, false)
//                //} catch (ex: Exception) {
//                    try {
//                        error(error(error()))
//                    } catch (ex: Exception) {
//
//                    }
//                //}
//            } else {
//                try {
//                    error(Gson().fromJson(response.errorBody()?.string(), Error::class.java))
//                } catch (ex: Exception) {
//                }
//
//            }
            try {
                error(Gson().fromJson(response.errorBody()?.string(), Error::class.java))
            } catch (ex: Exception) {
                error(error(error()))
            }
        }

    }


    private fun processServerCommands(responseDto: ResponseDto): Boolean {
        if (!responseDto.serverUrl.isNullOrEmpty()) {
            openUrl(responseDto.serverUrl!!)
            return true
        }
        if (!responseDto.serverMessage.isNullOrEmpty()) {
            showMessage(responseDto)
            return false
        }
        if (responseDto.serverAction.equals("logout")) {
            if (context is Activity) {
                AuthManager.logout(this.context)
                return false
            } else {
                return true
            }
        }
        if (responseDto.serverCommand != null) {
            if ("termsConfirmation".equals(responseDto.serverCommand!!.action) && responseDto.serverCommand?.url != null) {
                showConfirmTerms(responseDto)
                return false
            } else {
                return true
            }
        }
        return true
    }

    private fun showConfirmTerms(responseDto: ResponseDto) {
        AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.alert_terms))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.alert_terms_read), { _, _ -> openUrl(MyApp.Config.PDF_VIEW + responseDto.serverCommand?.url) })
                .setNegativeButton(context.getString(R.string.alert_terms_confirm), { d, _ -> confirm();d.cancel() })
                .setOnCancelListener { safeSuccess(responseDto as T) }
                .create().show()

    }

    private fun confirm() {
        AccountManager.confirm(context)
    }

    private fun showMessage(responseDto: ResponseDto) {
        AlertDialog.Builder(context)
                .setMessage(responseDto.serverMessage)
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.alert_message_ok), { d, _ -> d.cancel() })
                .setOnCancelListener { safeSuccess(responseDto as T) }
                .create().show()
    }

    private fun openUrl(serverUrl: String) {
        if (context is Activity) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(serverUrl)
            context.startActivity(intent)
        }
    }

    protected fun error(error: Error? = null) {

        if (ConnectionManager.isConnected(context)) {
            onError(error)
        } else {
            onConnectionFailure()
        }
        if (error != null) {
            processServerCommands(error)
        }
    }


    override fun onFailure(call: Call<T>, t: Throwable) {
        this.call = call
        try {
            error()
        } catch (ex: Exception) {

        }

    }

    protected fun retry() {
        call?.clone()?.enqueue(this)
    }

    fun safeSuccess(response: T) {
        try {
            onSuccess(response)
        } catch (ex: Exception) {

        }
    }

    abstract fun onSuccess(response: T)

    protected abstract fun onError(error: Error?)

    protected abstract fun onConnectionFailure()
}

@Keep
class UnitCallback<T>(context: Context, var success: (response: T) -> Unit) : BaseCallback<T>(context) {
    override fun onSuccess(response: T) {
        success(response)
    }

    override fun onError(error: Error?) {
    }

    override fun onConnectionFailure() {
    }

}

@Keep
abstract class AlertCallback<T>(context: Context, val force: Boolean = false) : BaseCallback<T>(context) {

    override fun onError(error: Error?) {
        var errString = context.getString(R.string.server_error)
        if (error != null) {
            errString = LocalizationManager.getError(error.errCode) ?: error.errCode
        }
        try {
            AlertUtils.alert(context, force, errString, R.string.btn_retry, DialogInterface.OnClickListener { dialogInterface, i ->
                dialogInterface.cancel()
                retry()
            })
        } catch (ex: Exception) {
        }

    }

    override fun onConnectionFailure() {
        AlertUtils.alert(context, force, R.string.no_connection, R.string.btn_retry, DialogInterface.OnClickListener { dialogInterface, i ->
            dialogInterface.cancel()
            retry()
        })
    }
}

@Keep
abstract class ToastCallback<T>(context: Context) : BaseCallback<T>(context) {

    override fun onError(error: Error?) {
        var errString = context.getString(R.string.server_error)
        if (error != null) {
            errString = LocalizationManager.getError(error.errCode) ?: error.errCode
        }
        Toast.makeText(context, errString, Toast.LENGTH_LONG).show()

    }


    override fun onConnectionFailure() {
        Toast.makeText(context, R.string.no_connection, Toast.LENGTH_LONG).show()
    }
}