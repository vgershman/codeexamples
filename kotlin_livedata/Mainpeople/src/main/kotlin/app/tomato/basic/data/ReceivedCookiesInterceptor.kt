package app.tomato.basic.data

import app.tomato.mainpeople.data.AccountManager
import app.tomato.mainpeople.data.AuthManager
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException


/**
 * Created by user on 16.08.17.
 */
class ReceivedCookiesInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalResponse = chain.proceed(chain.request())
        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
            var cookies = HashSet<String>()
            for (header in originalResponse.headers("Set-Cookie")) {
                cookies.add(header)
            }
            AuthManager.setCookies(cookies)
        }
        return originalResponse
    }
}