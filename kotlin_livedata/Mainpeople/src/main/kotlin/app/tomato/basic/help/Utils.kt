package app.tomato.basic.help

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader


fun checkPermission(activity: Activity?, permission: String, id: Int): Boolean {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || ActivityCompat.checkSelfPermission(activity!!, permission) == PackageManager.PERMISSION_GRANTED) return true
    ActivityCompat.requestPermissions(activity!!, arrayOf(permission), id)
    return false
}

fun checkPermission(activity: Activity?, permissions: Array<String>, id: Int): Boolean {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || ActivityCompat.checkSelfPermission(activity!!, permissions[0]) == PackageManager.PERMISSION_GRANTED) return true
    ActivityCompat.requestPermissions(activity!!, permissions, id)
    return false
}


fun hideKeyboard(input: View?, context: Context?) {
    try {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(input?.windowToken, 0)
    } catch (ex: Exception) {
        var remx = ex.message;
    }
}

fun showKeyboard(input: View?) {
    try {
        val imm = input!!.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(input, InputMethodManager.SHOW_IMPLICIT)
    } catch (ex: Exception) {
    }
}


fun screenHeight(context: Context): Float {
    val displayMetrics = context.resources.displayMetrics
    return displayMetrics.heightPixels.toFloat()
}

fun dpToPx(context1: Context, dp: Int): Float {
    val displayMetrics = context1.resources.displayMetrics
    return displayMetrics.density * dp
}

fun screenWidth(context1: Context): Float {
    val displayMetrics = context1.resources.displayMetrics
    return displayMetrics.widthPixels.toFloat()
}


fun readIS(inputStream: InputStream): String {
    val r = BufferedReader(InputStreamReader(inputStream))
    val total = StringBuilder()
    var line: String
    try {
        line = r.readLine()
        while (line != null) {
            total.append(line).append('\n')
            line = r.readLine()
        }
    } catch (ex: Exception) {
        return ""
    }

    return total.toString()
}


