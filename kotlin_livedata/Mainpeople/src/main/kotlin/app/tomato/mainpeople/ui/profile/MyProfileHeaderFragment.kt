package app.tomato.mainpeople.ui.profile

import android.arch.lifecycle.Observer
import android.view.View
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.data.AccountManager
import app.tomato.mainpeople.data.Payments
import app.tomato.mainpeople.data.User
import app.tomato.mainpeople.ui.container.BottomSheetActivity
import com.mainpeople.android.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_header.*

/**
 * Created by user on 19.08.17.
 */
class MyProfileHeaderFragment : BasicFragment() {

    private var statsMode = false

    override fun initViews() {
        AccountManager.observe(this, Observer { bind(it) })
        btnSettings.setOnClickListener { go(MyApp.Screens.SETTINGS, MyApp.Containers.SIMPLE, result = 6) }
        btnWallet.setOnClickListener {
            Payments.fillAccount(activity as BottomSheetActivity)
        }
    }


    override val containerView = R.layout.fragment_header

    fun bind(user: User?) {
        if (user != null) {
            Picasso.get().load(user.avatar_url).resize(screenWidth(context).toInt(), 0).into(ivAvatar)
            tvName.text = user.getUserFullName() //user.name + " " + user.surname
            tvLocation.text = user.city?.name + ", " + user.city?.country_name
            tvFollowers.text = user.followersCount?.toString()
            tvFollowing.text = user.followingCount?.toString()
            if (user.ranking != null) {
                tvRankings.text = user.ranking?.toString()
            } else {
                tvRankings.text = "-"
            }
            btnRankings.setOnClickListener { go(MyApp.Screens.RANKINGS, MyApp.Containers.SIMPLE) }
            btnStatistics.setOnClickListener { statsMode = !statsMode; bindStats(user) }
            tvAmount.text = "$" + user.bill?.Balance?.output?.split(".")?.get(0)
            btnWallet.visibility = if (user.showBalance) View.VISIBLE else View.INVISIBLE
            bindStats(user)


            if (user.followersCount != null) {
                btnFollowers.setOnClickListener { go(MyApp.Screens.FOllOWERS, MyApp.Containers.SIMPLE, params = hashMapOf("user" to user)) }
            }
            if (user.followingCount != null) {
                btnFollowings.setOnClickListener { go(MyApp.Screens.FOLLOWING, MyApp.Containers.SIMPLE, params = hashMapOf("user" to user)) }
            }
        }
    }

    private fun bindStats(user: User) {
        if (statsMode) {
            tvCount.text = "$" + user.statistics?.USER_SPENDING.toString()
            tvWhat.text = getString(R.string.lbl_contributed)
        } else {
            tvCount.text = "$" + user.statistics?.USER_COLLECTION.toString()
            tvWhat.text = getString(R.string.lbl_raised)
        }
    }


}