package app.tomato.mainpeople.ui.profile

import android.content.res.ColorStateList
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mainpeople.android.R
import app.tomato.mainpeople.data.FollowingsManager
import app.tomato.mainpeople.data.Like
import app.tomato.mainpeople.data.User
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_relations.view.*

/**
 * Created by user on 16.08.17.
 */
class RelationsAdapter(var userClick: (user: User) -> Unit, var relateClick: (user: User) -> Unit) : RecyclerView.Adapter<RelationsAdapter.VH>() {

    private val data = ArrayList<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_relations, parent, false))

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder?.bind(data[position])
    }

    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var ivAvatar = innerView.ivAvatar
        var tvAuthor = innerView.tvUserName
        var tvInfo = innerView.tvInfo
        var btnRelate = innerView.btnRelate

        fun bind(user: User) {
            Picasso.get().load(user.avatar_url).placeholder(R.drawable.ic_launcher).transform(CropCircleTransformation()).fit().into(ivAvatar)
            tvAuthor.text = user.getUserFullName()
            itemView.setOnClickListener { userClick(user) }
            tvInfo.text = user.city?.name

//            btnRelate.imageTintList = ColorStateList.valueOf(btnRelate.context.resources.getColor(R.color.blue))
            if (FollowingsManager.value?.contains(user) == true) {
                btnRelate.setImageResource(R.drawable.ic_followed_blue)
            } else {
                btnRelate.setImageResource(R.drawable.ic_follow_blue)
            }
            btnRelate.setOnClickListener { relateClick(user) }
        }

    }

    fun setData(users: List<User>) {
        data.clear()
        data.addAll(users)
        notifyDataSetChanged()
    }
}