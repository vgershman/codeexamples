package app.tomato.mainpeople.ui.messages

import android.support.v7.widget.RecyclerView
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.tomato.mainpeople.MentionHelper
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Group
import app.tomato.mainpeople.data.GroupNotificationWrapper
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_group.view.*

/**
 * Created by user on 18.08.17.
 */
class GroupsAdapter(var groupClick: (group: Group) -> Unit, var notificatinsClick: () -> Unit, var hashTagClick: (tag: String) -> Unit) : RecyclerView.Adapter<GroupsAdapter.VH>() {


    private var data = ArrayList<GroupNotificationWrapper>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_group, parent, false))

    override fun onBindViewHolder(holder: VH, position: Int) {
        var item = data[position]
        if (item.notifications != null) {
            holder?.bindNotification(item)
        } else {
            holder?.bindGroup(item.group!!)
        }
    }

    override fun getItemCount() = data.size


    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var ivAvatar = innerView.ivAvatar
        var tvName = innerView.tvGroupName
        var tvLast = innerView.tvInfo
        var tvWhen = innerView.tvWhen
        var tvNewCount = innerView.tvNotification


        fun bindGroup(group: Group) {
            Picasso.get().load(group.members[0].avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
            tvName.text = group.members[0].getUserFullName() //group.members[0].name + " " + group.members[0].surname
            tvLast.text = group.last_message?.content

            tvLast.setLinkTextColor(itemView.context.resources.getColor(R.color.blue))
            MentionHelper.Creator.create(itemView.context.resources.getColor(R.color.blue), object : MentionHelper.OnHashTagClickListener {
                override fun onHashTagClicked(hashTag: String) {
                    hashTagClick(hashTag.substring(1))
                }

            }).handle(tvLast)
            Linkify.addLinks(tvLast, Linkify.WEB_URLS)


            tvWhen.text = group.last_message?.getCreatedNice()
            tvNewCount.text = group.unreadMessagesCount.toString()
            if (group.unreadMessagesCount == 0) {
                tvNewCount.visibility = View.INVISIBLE
            } else {
                tvNewCount.visibility = View.VISIBLE
            }
            itemView.setOnClickListener { groupClick(group) }
        }

        fun bindNotification(wrapper: GroupNotificationWrapper) {
            try {
                ivAvatar.setImageDrawable(ivAvatar.context.resources.getDrawable(R.drawable.ic_launcher))
                tvName.text = itemView.context.getString(R.string.item_notifications)
                tvLast.text = ""
                tvWhen.text = wrapper.notifications!![0].getCreatedNice()
                val unreadCount = wrapper.notifications!!.count { it.read == null }
                tvNewCount.text = unreadCount.toString()
                if (unreadCount == 0) {
                    tvNewCount.visibility = View.INVISIBLE
                } else {
                    tvNewCount.visibility = View.VISIBLE
                }
                itemView.setOnClickListener { notificatinsClick() }
            } catch (ex: Exception) {

            }
        }

    }

    fun setData(response: List<GroupNotificationWrapper>) {
        data.clear()
        data.addAll(response)
        notifyDataSetChanged()
    }
}