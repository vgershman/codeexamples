package app.tomato.mainpeople.ui.welcome

import android.text.TextUtils
import android.view.WindowManager
import android.widget.Toast
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.ui.basic.BasicFragment
import com.mainpeople.android.R
import app.tomato.mainpeople.data.AccountManager
import app.tomato.mainpeople.data.Rest
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.fragment_restore.*

/**
 * Created by user on 31.08.17.
 */
class RestorePassword : BasicFragment() {

    var login: String? = null

    override fun initViews() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        btnBack.setOnClickListener { onBackPressed() }
        btnResend.setOnClickListener { resend() }
        btnReset.setOnClickListener { validate() }
    }

    private fun resend() {
        Rest.rest().restoreByEmail(login!!).enqueue(object : ToastCallback<LinkedTreeMap<String, Any?>>(context) {
            override fun onSuccess(response: LinkedTreeMap<String, Any?>) {

            }
        })
    }

    private fun validate() {
        hideKeyboard(etPassword, context)
        val password = etPassword.text.toString()
        var confirm = etConfirm.text.toString()
        val code = etCode.text.toString()
        if (code.length < 5) {
            Toast.makeText(context, R.string.error_code_short, Toast.LENGTH_SHORT).show()
            return
        }
        if (password.length < 6) {
            Toast.makeText(context, R.string.error_password_short, Toast.LENGTH_SHORT).show()
            return
        }
        if (!confirm.equals(password)) {
            Toast.makeText(context, R.string.error_password_match, Toast.LENGTH_SHORT).show()
            return
        }
        Rest.rest().restoreStep2(login!!, code, password).enqueue(object : ToastCallback<LinkedTreeMap<String, Any?>>(context) {
            override fun onSuccess(response: LinkedTreeMap<String, Any?>) {
                activity?.finish()
            }

        })
    }


    override fun initProperties() {
        super.initProperties()
        login = arguments!!["email"] as String?
    }


    override val containerView = R.layout.fragment_restore

}