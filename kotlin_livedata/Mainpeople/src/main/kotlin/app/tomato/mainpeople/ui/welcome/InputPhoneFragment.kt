package app.tomato.mainpeople.ui.welcome

import android.text.TextUtils
import android.widget.Toast
import app.tomato.basic.data.AlertCallback
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Rest
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.fragment_input_phone.*

/**
 * Created by user on 01.09.17.
 */
class InputPhoneFragment : BasicFragment() {
    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        btnNext.setOnClickListener { validate() }
    }

    private fun validate() {
        hideKeyboard(etPhone, context)
        val phone = ccp.selectedCountryCodeWithPlus + etPhone.text.toString()
        if (!isValid(phone)) {
            Toast.makeText(context, R.string.error_wrong_phone, Toast.LENGTH_SHORT).show()
            return
        }
        askForCode(phone)
    }

    private fun askForCode(phone: String) {
        Rest.rest().phoneAskCode(phone).enqueue(object : AlertCallback<LinkedTreeMap<String, Any?>>(context) {
            override fun onSuccess(response: LinkedTreeMap<String, Any?>) {
                go(MyApp.Screens.CONFIRM_PHONE, params = hashMapOf("phone" to phone))
            }
        })
    }


    fun isValid(target: String): Boolean {
        if (TextUtils.isEmpty(target)) {
            return false
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches()
        }
    }

    override val containerView = R.layout.fragment_input_phone

}