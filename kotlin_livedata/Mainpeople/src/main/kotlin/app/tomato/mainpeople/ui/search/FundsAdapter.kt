package app.tomato.mainpeople.ui.search

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Fund
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_fund.view.*

/**
 * Created by user on 16.08.17.
 */
class FundsAdapter(var fundClick: (fund: Fund) -> Unit) : RecyclerView.Adapter<FundsAdapter.VH>() {

    private val data = ArrayList<Fund>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_fund, parent, false))

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder?.bind(data[position])
    }

    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var ivAvatar = innerView.ivAvatar
        var tvAuthor = innerView.tvFundName
        var tvMission = innerView.tvMission

        fun bind(fund: Fund) {
            Picasso.get().load(fund.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
            tvAuthor.text = fund.name
            itemView.setOnClickListener { fundClick(fund) }
            tvMission.text = fund.mission
        }

    }

    fun setData(funds: List<Fund>) {
        data.clear()
        data.addAll(funds)
        notifyDataSetChanged()
    }
}