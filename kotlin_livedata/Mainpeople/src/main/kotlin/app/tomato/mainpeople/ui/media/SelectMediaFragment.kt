package app.tomato.mainpeople.ui.media

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.hardware.Camera
import android.media.CamcorderProfile
import android.media.MediaRecorder
import android.net.Uri
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
import android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.OrientationEventListener
import android.view.View
import android.widget.TextView
import app.tomato.basic.help.checkPermission
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.data.ImagesManager
import com.bumptech.glide.Glide
import com.github.hiteshsondhi88.libffmpeg.FFmpeg
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.fragment_media_selector.*
import java.io.File


/**
 * Created by user on 14.08.17.
 */
class SelectMediaFragment : BasicFragment() {

    var takingPic = false

    var recordedFile: File? = null

    var recording = false

    var mCameraId: Int = -1

    var camType = Camera.CameraInfo.CAMERA_FACING_BACK

    var mediaRecorder: MediaRecorder? = null

    private var mCamera: Camera? = null

    private var mMedia: String? = null

    private var mVideo = false

    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        rv.layoutManager = GridLayoutManager(context, 4)
        rv.adapter = SelectMediaAdapter({ openCamera() }, { select(it) })
        btnVideo.setOnClickListener {
            if (!recording) {
                toggleMediaType()
            }
        }
        btnChangeCamera.setOnClickListener {
            if (camera_preview.visibility == View.VISIBLE) {
                if (camType == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    camType = Camera.CameraInfo.CAMERA_FACING_FRONT
                } else {
                    camType = Camera.CameraInfo.CAMERA_FACING_BACK
                }
                if (mCamera != null) {
                    mCamera!!.stopPreview()
                    mCamera!!.release()
                    camera_preview.removeAllViews()
                }
                initCamera()
            }
        }
    }

    override fun initProperties() {
        super.initProperties()
        mVideo = (arguments!!["video"] as Boolean?) ?: false
        initNextBtn()
        bindHeader()
        bindControls()
        tryLoadData()
        openCamera()
    }

    private fun toggleMediaType() {
        try {
            ivVideo.release()
        } catch (ex: Exception) {
        }
        mVideo = !mVideo
        tryLoadData()
        stopCamera()
        openCamera()
        bindControls()
    }

    private fun bindControls() {
        btnShut.colorFilter = null
        if (mVideo) {
            lblRec.visibility = View.VISIBLE
            btnShut.setOnClickListener { startRecord() }
        } else {
            lblRec.visibility = View.GONE
            btnShut.setOnClickListener { takePicture() }
        }
    }

    protected var mOrientation = -1

    private val ORIENTATION_PORTRAIT_NORMAL = 1
    private val ORIENTATION_PORTRAIT_INVERTED = 2
    private val ORIENTATION_LANDSCAPE_NORMAL = 3
    private val ORIENTATION_LANDSCAPE_INVERTED = 4


    private var orientationListener: OrientationEventListener? = null

    override fun onResume() {
        super.onResume()
        if (orientationListener == null) {
            orientationListener = object : OrientationEventListener(context) {
                override fun onOrientationChanged(orientation: Int) {

                    if (orientation >= 315 || orientation < 45) {
                        if (mOrientation != ORIENTATION_PORTRAIT_NORMAL) {
                            mOrientation = ORIENTATION_PORTRAIT_NORMAL
                        }
                    } else if (orientation in 225..314) {
                        if (mOrientation != ORIENTATION_LANDSCAPE_NORMAL) {
                            mOrientation = ORIENTATION_LANDSCAPE_NORMAL
                        }
                    } else if (orientation in 135..224) {
                        if (mOrientation != ORIENTATION_PORTRAIT_INVERTED) {
                            mOrientation = ORIENTATION_PORTRAIT_INVERTED
                        }
                    } else { // orientation <135 && orientation > 45
                        if (mOrientation != ORIENTATION_LANDSCAPE_INVERTED) {
                            mOrientation = ORIENTATION_LANDSCAPE_INVERTED
                        }
                    }


                }

            }

        }
        if (orientationListener?.canDetectOrientation() == true) {
            orientationListener?.enable()
        }
    }


    override fun onPause() {
        super.onPause()
        try {
            orientationListener?.disable()
            ivVideo.release()
        } catch (ex: Exception) {
        }
    }

    private fun openCamera() {
        try {
            ivVideo.release()
        } catch (ex: Exception) {
        }
        mMedia = null
        camera_preview.visibility = View.VISIBLE
        initCamera()
        mediaContainer.visibility = View.INVISIBLE
        initNextBtn()
    }

    private fun initNextBtn() {
        btnNext.isEnabled = mMedia != null
        btnNext.setOnClickListener {
            if (!recording) {
                if (!mVideo) {
                    go(MyApp.Screens.TUNE, params = hashMapOf("media" to mMedia))
                } else {
                    go(MyApp.Screens.TRIM, params = hashMapOf("media" to mMedia))
                }
            }
        }
    }

    private fun bindHeader() {
        var lp = mediaContainer.layoutParams
        lp.height = screenWidth(context).toInt()
        lp.width = screenWidth(context).toInt()
        mediaContainer.layoutParams = lp

        var lp1 = ivAvatar.layoutParams
        lp1.height = screenWidth(context).toInt()
        lp1.width = screenWidth(context).toInt()
        ivAvatar.layoutParams = lp1
        ivVideo.layoutParams = lp1
    }


    fun startRecord() {
        if (camera_preview.visibility == View.VISIBLE && mCamera != null) {
            btnShut.setColorFilter(resources.getColor(R.color.red))
            btnShut.setOnClickListener { stopRecord() }
            if (mCamera == null) {
                return
            }
            mCamera!!.unlock()
            mediaRecorder = MediaRecorder()
            mediaRecorder?.setCamera(mCamera)
            mediaRecorder?.setAudioSource(MediaRecorder.AudioSource.CAMCORDER)
            mediaRecorder?.setVideoSource(MediaRecorder.VideoSource.CAMERA)
            if (camType == Camera.CameraInfo.CAMERA_FACING_BACK) {
                mediaRecorder?.setOrientationHint(90)
            } else {
                mediaRecorder?.setOrientationHint(270)
            }

            mediaRecorder?.setProfile(CamcorderProfile.get(mCameraId, CamcorderProfile.QUALITY_720P))
            recordedFile = getOutputMediaFile(MEDIA_TYPE_VIDEO)
            mediaRecorder?.setOutputFile(recordedFile.toString())
            mediaRecorder?.setPreviewDisplay((camera_preview.getChildAt(0) as CameraPreview).getSurface())
            try {
                mediaRecorder?.prepare()
                mediaRecorder?.start()
                recording = true
                activity?.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(recordedFile)))
                flush()

            } catch (ex: Exception) {
            }
        }
    }

    private fun updateVideoList(path: File) {
        activity?.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(path)))
        Handler().postDelayed({ tryLoadData();(rv.adapter as SelectMediaAdapter).setSelected(1) }, 1000)
    }

    private fun cropVideo() {
        val realPath = recordedFile!!.path
        val outPut = recordedFile!!.path.replace(".mp4", "_crop.mp4")
        val complexCommand = arrayOf("-y", "-i", realPath!!, "-vf", "crop=720:720:0:0", "-preset", "ultrafast", "-strict", "-2", "-c:v", "libx264", "-c:a", "copy", outPut)
        var pd: ProgressDialog? = null
        try {

            FFmpeg.getInstance(context).execute(complexCommand, object : FFmpegExecuteResponseHandler {
                override fun onSuccess(message: String) {
                    updateVideoList(File(outPut))

                }

                override fun onProgress(message: String) {

                }

                override fun onFailure(message: String) {
                    updateVideoList(File(realPath))
                }

                override fun onStart() {
                    pd = ProgressDialog.show(context, "", getString(R.string.alert_crop_video_loading))
                }

                override fun onFinish() {
                    if (pd != null) {
                        pd!!.cancel()
                    }
                }
            })
        } catch (e: Exception) {
            if (pd != null) {
                pd!!.cancel()
            }
            Log.e("videoCrop", e.message)
        }

    }

    private fun flush() {
        if (recording) {
            toggleVisibility(lblRec)
            Handler().postDelayed({ activity?.runOnUiThread { flush() } }, 500)
        } else {
            lblRec.visibility = View.VISIBLE
        }
    }

    private fun toggleVisibility(lblRec: TextView) {
        if (lblRec.visibility == View.VISIBLE) {
            lblRec.visibility = View.GONE
        } else {
            lblRec.visibility = View.VISIBLE
        }

    }

    private fun stopRecord() {
        recording = false
        bindControls()
        mediaRecorder?.stop()
        mediaRecorder?.release()
        cropVideo()
        stopCamera()
        camera_preview.visibility = View.GONE
    }

    private fun takePicture() {
        if (camera_preview.visibility != View.VISIBLE || takingPic || mCamera == null) {
            return
        }
        takingPic = true
        mCamera!!.takePicture(null, null, NewPhotoHandler(mOrientation,camType == Camera.CameraInfo.CAMERA_FACING_BACK, context, object : NewPhotoHandler.PhotoHandlerCallback {
            override fun onPhotoSaved() {
                takingPic = false
                loadData()
                (rv.adapter as SelectMediaAdapter).setSelected(1)
            }

            override fun onPhotoError() {
                takingPic = false
            }

        }))
    }

    private fun getOutputMediaFile(type: Int): File? {
        val mediaStorageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "MainPeople")
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }
        val timeStamp = System.currentTimeMillis()
        val mediaFile: File
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = File(mediaStorageDir.path + File.separator + "IMG_" + timeStamp + ".jpg")
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = File(mediaStorageDir.path + File.separator + "VID_" + timeStamp + ".mp4")
        } else {
            return null
        }
        return mediaFile
    }


    private fun findBackFacingCamera(type: Int): Int {
        mCameraId = -1
        val numberOfCameras = Camera.getNumberOfCameras()
        for (i in 0..numberOfCameras - 1) {
            val info = Camera.CameraInfo()
            Camera.getCameraInfo(i, info)
            if (info.facing == type) {
                mCameraId = i
                break
            }
        }
        return mCameraId
    }

    private fun initCamera() {
        if (checkPermission(activity, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO), 2)) {
            mCamera = getCameraInstance()
            camera_preview?.removeAllViews()
            camera_preview?.addView(CameraPreview(context, mCamera!!, mCameraId))
        }
    }

    fun getCameraInstance(): Camera? {
        findBackFacingCamera(camType)
        var c: Camera? = null
        try {
            c = Camera.open(mCameraId)
            val params = c.parameters
            val bestSize = getOptimalPictureSize(params.supportedPictureSizes)
            if (bestSize != null) {
                params.setPictureSize(bestSize.width, bestSize.height)
            }
            c.parameters = params
        } catch (e: Exception) {
        }
        return c
    }

    private fun getOptimalPictureSize(supported: List<Camera.Size>): Camera.Size? {

        supported.sortedBy { it.height * it.width }.forEach {
            if (Math.min(it.height, it.width) >= screenWidth(context)) {
                return it
            }
        }
        return null
    }

    private fun tryLoadData() {
        loadData()
    }

    private fun loadData() {
        if (mVideo) {
            (rv.adapter as SelectMediaAdapter).setData(ImagesManager.listVideos(context))
        } else {
            (rv.adapter as SelectMediaAdapter).setData(ImagesManager.listGallery(context))
        }

    }

    private fun select(it: String) {
        mMedia = it
        stopCamera()
        camera_preview.visibility = View.GONE
        mediaContainer.visibility = View.VISIBLE
        if (mVideo) {
            ivAvatar.visibility = View.GONE
            ivVideo.visibility = View.VISIBLE
            setUpVideo(it)
        } else {
            ivVideo.visibility = View.GONE
            ivAvatar.visibility = View.VISIBLE
            Glide.with(context).load(File(mMedia)).into(ivAvatar)
        }
        initNextBtn()
    }

    private fun stopCamera() {
        if (mCamera != null) {
            mCamera!!.stopPreview()
            mCamera!!.release()
            mCamera = null
            camera_preview.removeAllViews()
        }
    }

    private fun setUpVideo(it: String) {
        ivVideo.visibility = View.VISIBLE
        ivVideo.setDataSource(it)
        ivVideo.isLooping = true
        //  ivVideo.setVolume(0f, 0f)
        ivVideo.prepareAsync { ivVideo.start(); fix() }

    }

    private fun fix() {
        if (ivVideo.surfaceTexture != null) {
            ivVideo.onSurfaceTextureAvailable(ivVideo.surfaceTexture, 0, 0)
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        initCamera()
        tryLoadData()

    }

    override val containerView = R.layout.fragment_media_selector

}