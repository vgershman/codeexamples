package app.tomato.mainpeople.ui.welcome

import android.text.TextUtils
import android.view.WindowManager
import android.widget.Toast
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.AccountManager
import app.tomato.mainpeople.data.Rest
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.fragment_forgot.*

/**
 * Created by user on 31.08.17.
 */
class ForgotPassword : BasicFragment() {

    override fun initViews() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        btnBack.setOnClickListener { onBackPressed() }
        btnReset.setOnClickListener { validate() }
    }

    private fun validate() {
        hideKeyboard(etEmail, context)
        val login = etEmail.text.toString()
        if (!isValid(login)) {
            Toast.makeText(context, R.string.error_wrong_email, Toast.LENGTH_SHORT).show()
            return
        }
        restore(login)
    }

    private fun restore(login: String) {
        Rest.rest().restoreByEmail(login).enqueue(object : ToastCallback<LinkedTreeMap<String, Any?>>(context) {
            override fun onSuccess(response: LinkedTreeMap<String, Any?>) {
                go(MyApp.Screens.RESTORE, pop = true, params = hashMapOf("email" to login))
            }
        })
    }


    override fun initProperties() {
        super.initProperties()
        val email = arguments!!["email"] as String?
        if (email != null) {
            etEmail.setText(email)

        }
    }

    fun isValid(target: String): Boolean {
        if (TextUtils.isEmpty(target)) {
            return false
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()// || android.util.Patterns.PHONE.matcher(target).matches()
        }
    }

    override val containerView = R.layout.fragment_forgot

}