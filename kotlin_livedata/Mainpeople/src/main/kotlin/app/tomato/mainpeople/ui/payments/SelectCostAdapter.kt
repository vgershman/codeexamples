package app.tomato.mainpeople.ui.payments

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mainpeople.android.R
import app.tomato.mainpeople.ui.container.BottomAdapter
import kotlinx.android.synthetic.main.item_cost.view.*

/**
 * Created by user on 21.08.17.
 */
class SelectCostAdapter(var data : ArrayList<String>, var click: (cost: String) -> Unit) : BottomAdapter() {



    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VH).bind(data[position])
    }


    override fun getItemCount() = data.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_cost, parent, false))
    }

    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {
        var tvCost = innerView.tvCost

        fun bind(cost: String) {
            tvCost.text = "$" + cost
            itemView.setOnClickListener { click(cost) }
        }
    }
}