package app.tomato.mainpeople.ui.media

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.widget.SeekBar
import android.widget.Toast
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.data.PhotoFilters
import com.mainpeople.android.R
import com.yalantis.ucrop.callback.BitmapCropCallback
import com.yalantis.ucrop.view.CropImageView
import com.yalantis.ucrop.view.TransformImageView
import com.yalantis.ucrop.view.widget.HorizontalProgressWheelView
import jp.co.cyberagent.android.gpuimage.*
import kotlinx.android.synthetic.main.fragment_tune.*
import java.io.File
import java.util.*


/**
 * Created by user on 14.08.17.
 */
class TuneFragment : BasicFragment() {


    private var media: String? = null

    private var currentCrop = false


    var group: GPUImageFilterGroup? = null

    private var currentFilter = GPUImageFilter()
    private var brightFilter = GPUImageBrightnessFilter()
    private var contrastFilter = GPUImageContrastFilter()
    private var exposureFilter = GPUImageExposureFilter()

    private var filterEnabled = false

    private var editEnabled = false

    private var cropEnabled = false

    private var eBright = 50
    private var eContrast = 30
    private var eClarity = 50

    private var firstCrop = 2

    private var mCurrentName: String? = null

    override fun initViews() {
        clearPreviousPreviews()
        initNavigation()
        initTab()
        setUpCrop()
        initFilters()
        initEdit()
        mCurrentName = UUID.randomUUID().toString()
        btnReset.setOnClickListener { reset() }
    }

    private fun reset() {
        go(MyApp.Screens.TUNE, force = true, pop = true, params = hashMapOf("media" to media))
    }


    private fun initEdit() {
        seekBright.progress = eBright
        seekContrast.progress = eContrast
        seekClarity.progress = eClarity
        seekBright.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                eBright = p1
                editEnabled = true
                changeBright()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }
        })
        seekContrast.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                eContrast = p1
                editEnabled = true
                changeContrast()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }
        })
        seekClarity.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                eClarity = p1
                editEnabled = true
                changeClarity()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }
        })
        changeBright()
        changeContrast()
        changeClarity()
    }

    private fun updateFilter() {
        group = GPUImageFilterGroup()
        group!!.addFilter(currentFilter)
        group!!.addFilter(brightFilter)
        group!!.addFilter(contrastFilter)
        group!!.addFilter(exposureFilter)
        ivGPU?.filter = group
        checkReset()
    }

    private fun checkReset() {
        if (filterEnabled || editEnabled || cropEnabled) {
            btnReset.visibility = View.VISIBLE
        } else {
            btnReset.visibility = View.GONE
        }
    }

    private fun changeClarity() {
        var param = ((eClarity - 50).toFloat() / 50) * 5.toFloat()
        //val filter = GPUImageExposureFilter(param)
        exposureFilter.setExposure(param)
        //gpuImage?.setFilter(filter)
        updateFilter()
    }

    private fun changeContrast() {
        var param = ((eContrast).toFloat() / 40) + 0.25.toFloat()
//        val filter = GPUImageContrastFilter(param)
//        gpuImage?.setFilter(filter)
        contrastFilter.setContrast(param)
        updateFilter()
    }

    private fun changeBright() {
        var normal = eBright - 50
        var param = (normal.toFloat() / 50)
        brightFilter.setBrightness(param * 0.85.toFloat())
        updateFilter()
    }

    private fun clearPreviousPreviews() {
        val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        val file = File(path, "MainpeoplePreviews")
        if (file.exists() && file.isDirectory) {
            file.listFiles().forEach {
                try {
                    it.delete()
                } catch (ex: Exception) {
                }
            }
        }
    }

    private fun initGPU() {
        var lp = ucrop.layoutParams
        lp.height = screenWidth(context).toInt()
        lp.width = screenWidth(context).toInt()
        ucrop.layoutParams = lp
        ivGPU.setScaleType(GPUImage.ScaleType.CENTER_INSIDE)
        ivGPU.setBackgroundColor(0f, 0f, 0f)
        setGPU(Uri.fromFile(File(media)))
        setUpFilters()
        updateFilter()
    }

    private fun setUpFilters() {
        PhotoFilters.init(context)
        rvFilters.adapter = FiltersAdapter(media!!, { filter(it) })
    }

    private fun setGPU(uri: Uri) {
        val viewPortSize = screenWidth(context).toInt()
        var bitmap = BitmapFactory.decodeFile(uri.path)
        if (bitmap == null) {
            Toast.makeText(context, "Bad file", Toast.LENGTH_LONG).show()
            onBackPressed()
        }
        var orientation = ExifInterface.ORIENTATION_NORMAL
        try {
            orientation = ExifInterface(uri.encodedPath).getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
        } catch (ex: Exception) {

        }

        // orientation = ExifInterface.ORIENTATION_ROTATE_90
        var rotated: Bitmap?
        rotated = if (orientation != ExifInterface.ORIENTATION_NORMAL) {
            val matrix = Matrix()
            when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> matrix.postRotate(90f)
                ExifInterface.ORIENTATION_ROTATE_270 -> matrix.postRotate(270f)
                ExifInterface.ORIENTATION_ROTATE_180 -> matrix.postRotate(180f)
            }
            Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        } else {
            bitmap
        }
        val widthIsBigger = (rotated.width > rotated.height)
        var newWidth: Int
        var newHeigth: Int
        if (widthIsBigger) {
            newWidth = viewPortSize
            newHeigth = ((rotated.height * newWidth).toFloat() / rotated.width.toFloat()).toInt()
        } else {
            newHeigth = viewPortSize
            newWidth = ((rotated.width * newHeigth).toFloat() / rotated.height.toFloat()).toInt()
        }
        try {
            rotated.recycle()
            bitmap.recycle()
        } catch (ex: Exception) {

        }
        var gpuLP = ivGPU.layoutParams
        gpuLP.width = newWidth
        gpuLP.height = newHeigth
        ivGPU.layoutParams = gpuLP
        var rotation = Rotation.NORMAL
        try {
            when (orientation) {
                ExifInterface.ORIENTATION_NORMAL -> rotation = Rotation.NORMAL
                ExifInterface.ORIENTATION_ROTATE_270 -> rotation = Rotation.ROTATION_270
                ExifInterface.ORIENTATION_ROTATE_180 -> rotation = Rotation.ROTATION_180
                ExifInterface.ORIENTATION_ROTATE_90 -> rotation = Rotation.ROTATION_90
            }
        } catch (ex: Exception) {

        }
        ivGPU.setImage(uri)
        ivGPU.gpuImage.setRotation(rotation)
        updateFilter()
        ivGPU.visibility = View.VISIBLE
        back.visibility = View.GONE
        view?.requestLayout()

    }

    private fun filter(filter: FilterDto) {
        if (filter.filter != null) {
            currentFilter = filter.filter!!
            filterEnabled = true
        } else {
            filterEnabled = false
            currentFilter = GPUImageFilter()
        }
        updateFilter()

    }

    private fun initFilters() {
        rvFilters.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvFilters.setHasFixedSize(true)
    }

    private fun initNavigation() {
        btnNext.setOnClickListener { proceedMedia() }
        btnClose.setOnClickListener { onBackPressed() }
    }

    private fun proceedMedia() {
        if (currentCrop) {
            ucrop.cropImageView.cropAndSaveImage(Bitmap.CompressFormat.JPEG, 100, object : BitmapCropCallback {
                override fun onBitmapCropped(resultUri: Uri, offsetX: Int, offsetY: Int, imageWidth: Int, imageHeight: Int) {
                    setResult(resultUri.path)
                }

                override fun onCropFailure(t: Throwable) {
                }
            })
        } else {
            ivGPU.saveToPictures("MainPictures", mCurrentName) {
                setResult(getRealPathFromURI(context, it))

            }
        }
    }

    private fun setResult(path: String?) {
        if (path != null) {
            activity?.setResult(Activity.RESULT_OK, Intent().putExtra("media", path))
        } else {
            activity?.setResult(Activity.RESULT_CANCELED)
        }
        activity?.finish()
    }


    private fun initTab() {
        btnFilter.setOnClickListener { hideAll(); rvFilters.visibility = View.VISIBLE; toGPU() }//ivGPU.visibility = View.VISIBLE }
        btnEdit.setOnClickListener { hideAll(); paneEdit.visibility = View.VISIBLE; toGPU() }//ivGPU.visibility = View.VISIBLE }
        btnCrop.setOnClickListener { hideAll(); paneCrop.visibility = View.VISIBLE; toCrop() }// ucrop.visibility = View.VISIBLE }
    }

    private fun toGPU() {
        if (currentCrop) {
            currentCrop = false
            cropAndSaveImage()
        } else {
            ivGPU.visibility = View.VISIBLE
            back.visibility = View.GONE
        }
    }

    private fun toCrop() {
        currentCrop = true
        ivGPU.setImage(Uri.fromFile(File(media)))
        ivGPU.saveToPictures("MainPeople", mCurrentName) {
            setImageData(it, true)
        }
    }


    private fun hideAll() {
        back.visibility = View.VISIBLE
        ivGPU.visibility = View.INVISIBLE
        ucrop.visibility = View.INVISIBLE
        rvFilters.visibility = View.GONE
        paneEdit.visibility = View.GONE
        paneCrop.visibility = View.GONE

    }


    override fun initProperties() {
        super.initProperties()
        media = arguments!!["media"] as String?
        initGPU()
    }

    override val containerView = R.layout.fragment_tune

    private fun setUpCrop() {
        ucrop.cropImageView.setTransformImageListener(mImageListener)
        ucrop.overlayView.isFreestyleCropEnabled = false
        ucrop.overlayView.setDimmedColor(resources.getColor(R.color.statusBar))
        setupAspectRatioWidget()
        setupRotateWidget()
        setupScaleWidget()
    }

    fun getRealPathFromURI(context: Context, contentUri: Uri): String? {
        var cursor: Cursor? = null
        try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = context.contentResolver.query(contentUri, proj, null, null, null)
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(column_index)
        } catch (ex: Exception) {
            return null
        } finally {
            if (cursor != null) {
                cursor.close()
            }

        }
    }


    private fun setImageData(uri: Uri, show: Boolean) {

        val path = getRealPathFromURI(context, uri)
        var newUri: Uri
        if (path != null) {
            newUri = Uri.fromFile(File(getRealPathFromURI(context, uri)))
        } else {
            newUri = uri
        }


        try {
            var bitmap = BitmapFactory.decodeFile(newUri.path)
            ucrop?.cropImageView?.maxBitmapSize = (Math.max(bitmap.height, bitmap.width) * 1.5f).toInt()
            bitmap.recycle()
            ucrop?.cropImageView?.setImageUri(newUri, newUri)
            if (show) {
                ucrop?.visibility = View.VISIBLE
                back.visibility = View.VISIBLE
            }

        } catch (e: Exception) {
            Log.e("TuneFragment", e.message.toString())
        }
    }

    private val mImageListener = object : TransformImageView.TransformImageListener {
        override fun onRotate(currentAngle: Float) {
            if (firstCrop > 0) {
                firstCrop--
            } else {
                cropEnabled = true
                checkReset()
            }
            text_view_rotate?.text = String.format(Locale.getDefault(), "%.1f°", currentAngle)
        }

        override fun onScale(currentScale: Float) {
            if (firstCrop > 0) {
                firstCrop--
            } else {
                cropEnabled = true
                checkReset()
            }
            text_view_scale?.text = String.format(Locale.getDefault(), "%d%%", (currentScale * 100).toInt())
        }

        override fun onLoadComplete() {
            ucrop?.animate()?.alpha(1f)?.setDuration(300)?.interpolator = AccelerateInterpolator()

        }

        override fun onLoadFailure(e: Exception) {

        }

    }

    private fun cropAndSet() {
        ucrop.cropImageView.cropAndSaveImage(Bitmap.CompressFormat.JPEG, 100, object : BitmapCropCallback {
            override fun onBitmapCropped(resultUri: Uri, offsetX: Int, offsetY: Int, imageWidth: Int, imageHeight: Int) {
                setGPU(resultUri)
            }

            override fun onCropFailure(t: Throwable) {
            }
        })
    }

    private fun setupAspectRatioWidget() {
        btn1_1.setOnClickListener { v -> setRatio(1f);v.isSelected = true }
        btn2_3.setOnClickListener { v -> setRatio(2f / 3f);v.isSelected = true }
        btn3_4.setOnClickListener { v -> setRatio(4f / 3f);v.isSelected = true }
        btn16_9.setOnClickListener { v -> setRatio(16f / 9f);v.isSelected = true }
        wrapper_reset_ratio.setOnClickListener { setRatio(CropImageView.SOURCE_IMAGE_ASPECT_RATIO) }
    }

    private fun unselectAllRatios() {
        btn1_1.isSelected = false
        btn2_3.isSelected = false
        btn3_4.isSelected = false
        btn16_9.isSelected = false
    }

    private fun setRatio(ratio: Float) {
        unselectAllRatios()
        ucrop.cropImageView.targetAspectRatio = ratio
        ucrop.cropImageView.setImageToWrapCropBounds()
    }

    private fun setupRotateWidget() {
        rotate_scroll_wheel.setScrollingListener(object : HorizontalProgressWheelView.ScrollingListener {
            override fun onScroll(delta: Float, totalDistance: Float) {
                ucrop.cropImageView.postRotate(delta / 42)
            }

            override fun onScrollEnd() {
                ucrop.cropImageView.setImageToWrapCropBounds()
            }

            override fun onScrollStart() {
                ucrop.cropImageView.cancelAllAnimations()
            }
        })
        rotate_scroll_wheel.setMiddleLineColor(Color.WHITE)
        wrapper_reset_rotate.setOnClickListener({ resetRotation() })
        wrapper_rotate_by_angle.setOnClickListener({ rotate(90) })
    }

    private fun setupScaleWidget() {
        scale_scroll_wheel.setScrollingListener(object : HorizontalProgressWheelView.ScrollingListener {
            override fun onScroll(delta: Float, totalDistance: Float) {
                if (delta > 0) {
                    ucrop.cropImageView.zoomInImage(ucrop.cropImageView.currentScale + delta * ((ucrop.cropImageView.maxScale - ucrop.cropImageView.minScale) / 15000))
                } else {
                    ucrop.cropImageView.zoomOutImage(ucrop.cropImageView.currentScale + delta * ((ucrop.cropImageView.maxScale - ucrop.cropImageView.minScale) / 15000))
                }
            }

            override fun onScrollEnd() {
                ucrop.cropImageView.setImageToWrapCropBounds()
            }

            override fun onScrollStart() {
                ucrop.cropImageView.cancelAllAnimations()
            }
        })
        scale_scroll_wheel.setMiddleLineColor(Color.WHITE)
    }

    private fun resetRotation() {
        ucrop.cropImageView.postRotate(-ucrop.cropImageView.currentAngle)
        ucrop.cropImageView.setImageToWrapCropBounds()
    }

    private fun rotate(angle: Int) {
        ucrop.cropImageView.postRotate(angle.toFloat())
        ucrop.cropImageView.setImageToWrapCropBounds()
    }

    private fun cropAndSaveImage() {
        //wait = true
        ivGPU.filter = GPUImageFilter()
        ivGPU.saveToPictures("MainPeople", mCurrentName) {
            setImageData(it, false)
            Handler().postDelayed({ cropAndSet() }, 1000)
        }

    }

}