package app.tomato.mainpeople.ui.settings

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mainpeople.android.R

import app.tomato.mainpeople.data.Notification
import app.tomato.mainpeople.data.Story
import app.tomato.mainpeople.data.User
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_notification.view.*


class NotificationsAdapter(var userClick: (user: User) -> Unit, var storyClick: (story: Story) -> Unit) : RecyclerView.Adapter<NotificationsAdapter.VH>() {

    private var data = ArrayList<Notification>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_notification, parent, false))

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder?.bindNotification(data[position])
    }

    override fun getItemCount() = data.size


    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var ivAvatar = innerView.ivAvatar
        var tvContent = innerView.tvUserName
        var tvInfo = innerView.tvInfo
        var ivContent = innerView.btnRelate


        fun bindNotification(notification: Notification) {
            Picasso.get().load(notification.user_sender?.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
            ivAvatar.setOnClickListener { userClick(notification.user_sender!!) }
            tvInfo.text = notification.getCreatedNice()
            when (notification.type) {

                4 -> {
                    tvContent.text = notification.user_sender?.getUserFullName() + " " + itemView.context.getString(R.string.notification_new_post)
                    Picasso.get().load(notification.post?.content).resize(250, 0).into(ivContent)
                    itemView.setOnClickListener { storyClick(notification.post!!) }
                    ivContent.setOnClickListener { storyClick(notification.post!!) }
                }
                5 -> {
                    tvContent.text = notification.user_sender?.getUserFullName() + " " + itemView.context.getString(R.string.notification_supports_post) + " " + notification.like?.cost?.split(".")?.get(0) + "$"
                    Picasso.get().load(notification.post?.content).resize(250, 0).into(ivContent)
                    itemView.setOnClickListener { storyClick(notification.post!!)  }
                    ivContent.setOnClickListener { storyClick(notification.post!!) }
                }
                6 -> {
                    tvContent.text = notification.user_sender?.getUserFullName() + " " + itemView.context.getString(R.string.notification_comment)
                    Picasso.get().load(notification.post?.content).resize(250, 0).into(ivContent)
                    itemView.setOnClickListener { storyClick(notification.post!!)  }
                    ivContent.setOnClickListener { storyClick(notification.post!!) }
                }
                13 -> {
                    tvContent.text = notification.user_sender?.getUserFullName() + " " + itemView.context.getString(R.string.notification_comment_)
                    Picasso.get().load(notification.post?.content).resize(250, 0).into(ivContent)
                    itemView.setOnClickListener { storyClick(notification.post!!) }
                    ivContent.setOnClickListener { storyClick(notification.post!!) }
                }
                7 -> {
                    if(notification.comment==null) {
                        tvContent.text = notification.user_sender?.getUserFullName() + " " + itemView.context.getString(R.string.notification_mention_)
                    }else{
                        tvContent.text = notification.user_sender?.getUserFullName() + " " + itemView.context.getString(R.string.notification_mention_comment)
                    }
                    Picasso.get().load(notification.post?.content).resize(250, 0).into(ivContent)
                    itemView.setOnClickListener { storyClick(notification.post!!) }
                    ivContent.setOnClickListener { storyClick(notification.post!!) }
                }
                1 -> {
                    tvContent.text = notification.user_sender?.getUserFullName() + " " + itemView.context.getString(R.string.notification_follows)
                    Picasso.get().load(R.drawable.ic_follow_blue).into(ivContent)
                    itemView.setOnClickListener { userClick(notification.user_sender!!) }
                    ivContent.setOnClickListener {}
                }
                2 -> {
                    tvContent.text = notification.user_sender?.getUserFullName() + " " + itemView.context.getString(R.string.notification_friends)
                    Picasso.get().load(R.drawable.ic_follow_blue).into(ivContent)
                    itemView.setOnClickListener { userClick(notification.user_sender!!) }
                    ivContent.setOnClickListener {}
                }
                3, 10 -> {
                    tvContent.text = notification.user_sender?.getUserFullName() + " " + itemView.context.getString(R.string.notification_contact_registred)
                    Picasso.get().load(R.drawable.ic_follow_blue).into(ivContent)
                    itemView.setOnClickListener { userClick(notification.user_sender!!) }
                    ivContent.setOnClickListener {}
                }


            }

        }

    }


    fun setData(response: List<Notification>) {
        data.clear()
        data.addAll(response)
        notifyDataSetChanged()
    }
}