package app.tomato.mainpeople.ui.settings

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Card
import kotlinx.android.synthetic.main.item_card.view.*

/**
 * Created by user on 30.08.17.
 */
class CardsAdapter(var clicked: (card: Card) -> Unit) : RecyclerView.Adapter<CardsAdapter.VH>() {

    private var data = ArrayList<Card>()

    fun setData(cards: List<Card>) {
        data.clear()
        data.addAll(cards)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder?.bind(data[position])
    }

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_card, parent, false))


    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {
        var tvCard = innerView.tvCard
        fun bind(card: Card) {
            tvCard.text = card.name
            itemView.setOnClickListener { clicked(card) }
        }

    }
}