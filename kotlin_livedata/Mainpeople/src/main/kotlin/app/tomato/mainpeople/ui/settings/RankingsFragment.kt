package app.tomato.mainpeople.ui.settings

import android.support.v7.widget.LinearLayoutManager
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Ranking
import app.tomato.mainpeople.data.Rest
import app.tomato.mainpeople.data.User
import app.tomato.mainpeople.ui.profile.RankingsAdapter
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.fragment_funds_list.*

/**
 * Created by user on 14.08.17.
 */
class RankingsFragment : BasicFragment() {

    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = RankingsAdapter({ go(MyApp.Screens.USER, params = hashMapOf("user" to it)) })
        loadData()
    }

    private fun loadData() {
        Rest.rest().listTopPeople().enqueue(object : ToastCallback<LinkedTreeMap<String, Ranking>>(context) {
            override fun onSuccess(response: LinkedTreeMap<String, Ranking>) {
                var users = ArrayList<User>()
                response.keys.forEach { users.add(response[it]?.user!!) }
                (rv.adapter as RankingsAdapter).setData(users)
            }

        })
    }

    override val containerView = R.layout.fragment_rankings


}