package app.tomato.mainpeople.ui.main

import android.os.Handler
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.basic.ui.basic.BasicTabsFragment
import app.tomato.basic.ui.basic.TabsAdapter
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.fragment_tabs_base.*
import kotlinx.android.synthetic.main.top_tab_spinner.view.*

/**
 * Created by user on 14.08.17.
 */
class FeedTabsFragment : BasicTabsFragment() {

    override val titlesId = R.array.feed

    override val screens: Array<Class<out BasicFragment>> = arrayOf(WorldFeedFragment::class.java, FriendsFeedFragment::class.java, TopFeedFragment::class.java)

    var firstTime = true

    override fun initViews() {
        tabs.tabMode = TabLayout.MODE_FIXED
        val customView = LayoutInflater.from(context).inflate(R.layout.top_tab_spinner, tabs, false)
        val adapter = TabSpinnerAdapter(context, vp_fragments)
        customView.spinnerTab.adapter = adapter
        vp_fragments.setCurrentItem(0, false)
        firstTime = true
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                adapter.notifyDataSetChanged()
                if (tab?.position == 2) {
                    ((vp_fragments.adapter as TabsAdapter).getItem(2) as FeedFragment).reload()
                }
            }

        })
        customView.spinnerTab.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                adapter.selected = -1
                adapter.notifyDataSetChanged()
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentTop = resources.getStringArray(R.array.tab_keys)[p2]
                if (firstTime) {
                    firstTime = false
                    return
                }
                adapter.selected = p2
                adapter.notifyDataSetChanged()
                ((vp_fragments.adapter as TabsAdapter).getItem(2) as FeedFragment).reload()
                vp_fragments.setCurrentItem(2, true)

            }

        }
        tabs.getTabAt(2)?.customView = customView
    }

    var currentTop: String = "all"

}