package app.tomato.mainpeople.ui.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.tomato.mainpeople.data.User
import com.mainpeople.android.R
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_relations.view.*

/**
 * Created by user on 16.08.17.
 */
class MentionsAdapter(var userClick: (user: User) -> Unit) : RecyclerView.Adapter<MentionsAdapter.VH>() {

    private val data = ArrayList<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_suggestion, parent, false))

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder?.bind(data[position])
    }

    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var ivAvatar = innerView.ivAvatar
        var tvAuthor = innerView.tvUserName

        fun bind(user: User) {
            Picasso.get().load(user.avatar_url).placeholder(R.drawable.ic_launcher).transform(CropCircleTransformation()).fit().into(ivAvatar)
            tvAuthor.text = user.getUserFullName()
            itemView.setOnClickListener { userClick(user) }
        }

    }

    fun setData(users: List<User>) {
        data.clear()
        data.addAll(users)
        notifyDataSetChanged()
    }
}