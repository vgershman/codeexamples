package app.tomato.mainpeople.ui.media

import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Environment
import android.view.animation.AccelerateInterpolator
import app.tomato.basic.ui.basic.BasicFragment
import com.mainpeople.android.R
import com.yalantis.ucrop.callback.BitmapCropCallback
import com.yalantis.ucrop.view.CropImageView
import com.yalantis.ucrop.view.TransformImageView
import com.yalantis.ucrop.view.widget.HorizontalProgressWheelView
import kotlinx.android.synthetic.main.fragment_crop.*
import java.io.File
import java.util.*

open class CropFragment : BasicFragment() {

    private var media: String? = null

    override fun initViews() {
        setUpCrop()

    }


    override fun initProperties() {
        super.initProperties()
        media = arguments!!["media"] as String?
        setImageData()
    }

    override val containerView = R.layout.fragment_crop

    private fun setUpCrop() {
        ucrop.cropImageView.setTransformImageListener(mImageListener)
        ucrop.overlayView.isFreestyleCropEnabled = true
        ucrop.overlayView.setDimmedColor(resources.getColor(R.color.statusBar))
        ucrop.overlayView.setOverlayViewChangeListener { unselectAllRatios() }
        setupAspectRatioWidget()
        setupRotateWidget()
        setupScaleWidget()
    }

    private fun setImageData() {
        val inputUri = Uri.fromFile(File(media))
        val outputUri = Uri.fromFile(File(Environment.DIRECTORY_PICTURES, "MainPeopleCrop"))
        try {
            ucrop?.cropImageView?.setImageUri(inputUri, outputUri)
        } catch (e: Exception) {
        }
    }

    private val mImageListener = object : TransformImageView.TransformImageListener {
        override fun onRotate(currentAngle: Float) {

            text_view_rotate?.text = String.format(Locale.getDefault(), "%.1f°", currentAngle)
        }

        override fun onScale(currentScale: Float) {
            text_view_scale?.text = String.format(Locale.getDefault(), "%d%%", (currentScale * 100).toInt())
        }

        override fun onLoadComplete() {
            ucrop?.animate()?.alpha(1f)?.setDuration(300)?.interpolator = AccelerateInterpolator()
        }

        override fun onLoadFailure(e: Exception) {

        }

    }

    private fun setupAspectRatioWidget() {
        btn1_1.setOnClickListener { v -> setRatio(1f);v.isSelected = true }
        btn2_3.setOnClickListener { v -> setRatio(2f / 3f);v.isSelected = true }
        btn3_4.setOnClickListener { v -> setRatio(4f / 3f);v.isSelected = true }
        btn16_9.setOnClickListener { v -> setRatio(16f / 9f);v.isSelected = true }
        wrapper_reset_ratio.setOnClickListener { setRatio(CropImageView.SOURCE_IMAGE_ASPECT_RATIO) }
    }

    private fun unselectAllRatios() {
        btn1_1.isSelected = false
        btn2_3.isSelected = false
        btn3_4.isSelected = false
        btn16_9.isSelected = false
    }

    private fun setRatio(ratio: Float) {
        unselectAllRatios()
        ucrop.cropImageView.targetAspectRatio = ratio
        ucrop.cropImageView.setImageToWrapCropBounds()
    }

    private fun setupRotateWidget() {
        rotate_scroll_wheel.setScrollingListener(object : HorizontalProgressWheelView.ScrollingListener {
            override fun onScroll(delta: Float, totalDistance: Float) {
                ucrop.cropImageView.postRotate(delta / 42)
            }

            override fun onScrollEnd() {
                ucrop.cropImageView.setImageToWrapCropBounds()
            }

            override fun onScrollStart() {
                ucrop.cropImageView.cancelAllAnimations()
            }
        })
        rotate_scroll_wheel.setMiddleLineColor(Color.WHITE)
        wrapper_reset_rotate.setOnClickListener({ resetRotation() })
        wrapper_rotate_by_angle.setOnClickListener({ rotate(90) })
    }

    private fun setupScaleWidget() {
        scale_scroll_wheel.setScrollingListener(object : HorizontalProgressWheelView.ScrollingListener {
            override fun onScroll(delta: Float, totalDistance: Float) {
                if (delta > 0) {
                    ucrop.cropImageView.zoomInImage(ucrop.cropImageView.currentScale + delta * ((ucrop.cropImageView.maxScale - ucrop.cropImageView.minScale) / 15000))
                } else {
                    ucrop.cropImageView.zoomOutImage(ucrop.cropImageView.currentScale + delta * ((ucrop.cropImageView.maxScale - ucrop.cropImageView.minScale) / 15000))
                }
            }

            override fun onScrollEnd() {
                ucrop.cropImageView.setImageToWrapCropBounds()
            }

            override fun onScrollStart() {
                ucrop.cropImageView.cancelAllAnimations()
            }
        })
        scale_scroll_wheel.setMiddleLineColor(Color.WHITE)
    }

    private fun resetRotation() {
        ucrop.cropImageView.postRotate(-ucrop.cropImageView.currentAngle)
        ucrop.cropImageView.setImageToWrapCropBounds()
    }

    private fun rotate(angle: Int) {
        ucrop.cropImageView.postRotate(angle.toFloat())
        ucrop.cropImageView.setImageToWrapCropBounds()
    }

    protected fun cropAndSaveImage() {
        ucrop.cropImageView.cropAndSaveImage(Bitmap.CompressFormat.JPEG, 90, object : BitmapCropCallback {
            override fun onBitmapCropped(resultUri: Uri, offsetX: Int, offsetY: Int, imageWidth: Int, imageHeight: Int) {
            }

            override fun onCropFailure(t: Throwable) {
            }
        })
    }

}