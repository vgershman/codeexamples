package app.tomato.mainpeople.ui.search

import android.arch.lifecycle.Observer
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.data.UnitCallback
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.data.*
import app.tomato.mainpeople.ui.container.BottomSheetActivity
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.fragment_search_.*

/**
 * Created by user on 29.11.17.
 */


abstract class BaseSearchFragment : BasicFragment() {


    override val containerView = R.layout.fragment_search_


    var loadingFund = false


    private fun like(it: Story?) {
        Payments.support(it!!, activity as BottomSheetActivity)
    }

    private fun openLikes(it: Story?) {
        go(MyApp.Screens.SUPPORTERS, MyApp.Containers.SIMPLE, params = hashMapOf("story" to it))
    }

    protected var searchAdapter = SearchAdapter({ openFund(it) }, { proceedUsers(it) }, { proceedTag(it) }, { openStory(it) }, { openLikes(it) }, { like(it) }, { proceedStringTag(it) }, {proceedMention(it)})


    private fun proceedMention(id: Int?) {
        if (id != null) {
            Rest.rest().user(id.toString()).enqueue(UnitCallback(context, {
                proceedUsers(it)
            }))
        }
    }


    private fun openFund(it: String) {
        if (loadingFund) return
        loadingFund = true
        Rest.rest().fund(it).enqueue(object : ToastCallback<Fund>(context) {

            override fun onSuccess(response: Fund) {
                go(MyApp.Screens.FUND, MyApp.Containers.SIMPLE, params = hashMapOf("fund" to response))
                loadingFund = false
            }

            override fun onError(error: Error?) {
                super.onError(error)
                loadingFund = false
            }

            override fun onConnectionFailure() {
                super.onConnectionFailure()
                loadingFund = false
            }
        })
    }


    private fun openStory(it: Story) {
        go(MyApp.Screens.STORY, MyApp.Containers.SIMPLE, params = hashMapOf("story" to it))
    }

    override fun initViews() {
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = searchAdapter
        SearchManager.observe(this, Observer { bindData(it) })
        bindData(SearchManager.value)
    }

    abstract fun bindData(response: SearchResult?)

    private fun proceedStringTag(tag: String) {
        (parentFragment as NewSearchFragment?)?.tag = tag
    }

    private fun proceedTag(tag: Tag?) {
        (parentFragment as NewSearchFragment?)?.tag = tag?.name
    }


    protected fun showNotFound(id: Int) {
        rv.visibility = View.INVISIBLE
        lblError.text = getString(id)
        lblError.visibility = View.VISIBLE
    }

    private fun proceedUsers(user: User?) {
        if (user != null) {
            go(MyApp.Screens.USER, MyApp.Containers.SIMPLE, params = hashMapOf("user" to user))
        }
    }

}