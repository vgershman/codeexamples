package app.tomato.mainpeople.ui.payments

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.tomato.mainpeople.data.Card
import app.tomato.mainpeople.ui.container.BottomAdapter
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.item_pay_card.view.*

/**
 * Created by user on 21.08.17.
 */
class SelectCardAdapter(var data: ArrayList<Card>, var click: (card: Card?) -> Unit) : BottomAdapter() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == data.size) {
            (holder as VH).bind(null)
        } else {
            (holder as VH).bind(data[position])
        }
    }


    override fun getItemCount() = data.size + 1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_pay_card, parent, false))
    }


    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {
        var tvCost = innerView.tvCost
        var ivType = innerView.iv_pay_type

        fun bind(card: Card?) {
            if (card != null) {
                if ("Paypal".equals(card.name, true)) {
                    tvCost.text = itemView.context.getString(R.string.options_paypal)
                    ivType.setImageResource(R.drawable.mark_paypal)
                } else {
                    if ("GPay".equals(card.name, true)) {
                        tvCost.text = itemView.context.getString(R.string.options_gpay)
                        ivType.setImageResource(R.drawable.mark_gpay)
                    } else {
                        tvCost.text = card.name
                        ivType.setImageResource(R.drawable.ic_card)
                    }
                }

            } else {
                tvCost.text = itemView.context.getString(R.string.options_new_card)
            }
            itemView.setOnClickListener { click(card) }
        }
    }
}