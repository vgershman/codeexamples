package app.tomato.mainpeople.ui.welcome

import android.app.Activity
import android.content.Intent
import android.os.Handler
import android.view.View
import android.widget.Toast
import app.tomato.basic.data.AlertCallback
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.AccountManager
import app.tomato.mainpeople.data.Rest
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.fragment_confirm_phone.*

/**
 * Created by user on 01.09.17.
 */
class ConfirmPhoneFragment : BasicFragment() {


    private var coundDown = 120
    private var mHandler: Handler? = null
    private var phone: String? = null

    override fun initViews() {
        btnNext.setOnClickListener { validate() }
    }

    private fun validate() {
        hideKeyboard(etCode, context)
        val code = etCode.text.toString()
        if (etCode.length() < 4) {
            Toast.makeText(context, getString(R.string.error_code_short), Toast.LENGTH_SHORT).show()
            return
        }
        confirmCode(code)
    }

    private fun confirmCode(code: String) {
        Rest.rest().confirmPhone(phone!!, code).enqueue(object : AlertCallback<LinkedTreeMap<String, Any?>>(context) {
            override fun onSuccess(response: LinkedTreeMap<String, Any?>) {
                AccountManager.loadUser(context)
                AccountManager.value?.phone_validated = ""
                activity?.setResult(Activity.RESULT_OK)
                activity?.finish()
            }
        })
    }


    override fun initProperties() {
        super.initProperties()
        phone = arguments!!["phone"] as String
        bind()
    }

    private fun bind() {
        lblPhone.text = phone
        lblPhone.visibility = View.VISIBLE
        bindTimer()
    }

    private fun bindTimer() {
        try {
            if (mHandler == null) {
                mHandler = Handler()
            }
            if (coundDown > 0) {
                coundDown--
            } else {
                mHandler?.removeCallbacksAndMessages(null)
            }
            var minutes = (coundDown.toFloat() / 60).toInt()
            var seconds = coundDown - (minutes * 60)
            var counterString = String.format("%02d", minutes) + ":" + String.format("%02d", seconds)
            lblCallTimer.text = getString(R.string.lblTimer).replace("<timer>", counterString)
            lblCallTimer.visibility = View.VISIBLE
            mHandler?.postDelayed({ bindTimer() }, 1000)
        } catch (ex: Exception) {

        }
    }

    override val containerView = R.layout.fragment_confirm_phone

}