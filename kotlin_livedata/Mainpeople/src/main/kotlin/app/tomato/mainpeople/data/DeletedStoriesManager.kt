package app.tomato.mainpeople.data

import android.arch.lifecycle.LiveData

/**
 * Created by user on 15.09.17.
 */
object DeletedStoriesManager : LiveData<ArrayList<String>>() {

    fun addDeletedStory(id: String) {
        var result = value
        if (result == null) {
            result = ArrayList()
        }
        if (!result.contains(id)) {
            result.add(id)
        }
        value = result
    }

}

