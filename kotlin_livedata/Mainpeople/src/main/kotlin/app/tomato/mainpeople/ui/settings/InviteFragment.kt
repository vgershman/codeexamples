package app.tomato.mainpeople.ui.settings

import android.arch.lifecycle.Observer
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.help._TextWatcher
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.*
import app.tomato.mainpeople.ui.container.BottomSheetActivity
import app.tomato.mainpeople.ui.payments.SelectCostAdapter
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.fragment_contacts.*
import java.util.*

/**
 * Created by user on 11.09.17.
 */
class InviteFragment : BasicFragment() {

    var timer: Timer? = null

    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        etSearch.addTextChangedListener(object : _TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.length!! > 0) {
                    btnClear.visibility = View.VISIBLE

                } else {
                    btnClear.visibility = View.GONE
                }
                scheduleQuery(p0.toString())
            }
        })
        btnClear.setOnClickListener { etSearch.setText("") }
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = ContactsAdapter({ openUser(it) }, { invite(it) }, { toggle(it) })
        PhoneManager.init(context)
        ContactsManager.init(context)
        FollowingsManager.init(context)
        PhoneManager.observe(this, Observer { (rv.adapter as ContactsAdapter).setPhones(it) })
        ContactsManager.observe(this, Observer { (rv.adapter as ContactsAdapter).setUsers(it?.contactsInMP) })
        FollowingsManager.observe(this, Observer { rv.adapter.notifyDataSetChanged() })
    }

    private fun scheduleQuery(query: String) {
        if (timer != null) {
            timer!!.cancel()
        }
        timer = kotlin.concurrent.timer("searcher", false, initialDelay = 500L, period = 10000, action = {
            try {
                activity?.runOnUiThread {
                    try {
                        (rv.adapter as ContactsAdapter).setQuery(query)
                    } catch (ex: Exception) {
                    }
                }; timer?.cancel()
            } catch (ex: Exception) {
            }
        })

    }


    override fun onPause() {
        super.onPause()
        if (timer != null) {
            timer!!.cancel()
        }
    }

    private fun toggle(it: User) {
        var relation = 1
        if (FollowingsManager.value?.contains(it) == true) {
            relation = 0
        }
        Rest.rest().relateUser(it.id!!, relation).enqueue(object : ToastCallback<Relation>(context) {
            override fun onSuccess(response: Relation) {
                FollowingsManager.init(context)
                AccountManager.loadUser(context)
            }
        })
    }

    private fun openUser(it: User) {
        go(MyApp.Screens.USER, params = hashMapOf("user" to it))
    }

    private fun invite(it: PhoneUser) {
        if (it.phones.size == 0) {
            return
        }
        if (it.phones.size == 1) {
            sendInvite(it.phones[0])
        } else {
            (activity as BottomSheetActivity).bottomMenu(SelectCostAdapter(it.phones, { onSelectPhone(it) }))
        }

    }

    private fun onSelectPhone(it: String) {
        (activity as BottomSheetActivity).hideBottomMenu()
        sendInvite(it)
    }

    private fun sendInvite(phone: String) {
        Rest.rest().invite(phone).enqueue(object : ToastCallback<LinkedTreeMap<String, Any?>>(context) {
            override fun onSuccess(response: LinkedTreeMap<String, Any?>) {
                Toast.makeText(context, getString(R.string.tst_invite_sent), Toast.LENGTH_SHORT).show()
            }
        })
    }

    override val containerView = R.layout.fragment_contacts

}