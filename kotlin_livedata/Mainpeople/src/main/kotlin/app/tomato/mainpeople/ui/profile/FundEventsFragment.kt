package app.tomato.mainpeople.ui.profile

import android.support.v7.widget.LinearLayoutManager
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.ui.basic.BasicFragment
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Fund
import app.tomato.mainpeople.data.Rest
import kotlinx.android.synthetic.main.fragment_list.*

/**
 * Created by user on 14.08.17.
 */
class FundEventsFragment : BasicFragment() {

    override fun initViews() {
        lv.layoutManager = LinearLayoutManager(context)
        var fund = (parentFragment as FundProfileFragment).fund
        if (fund?.campaigns != null && fund.campaigns.size > 0) {
            lv.adapter = FundEventsAdapter(fund)
        } else {
            loadCampaigns()
        }

    }

    private fun loadCampaigns() {
        Rest.rest().fund((parentFragment as FundProfileFragment).fund?.id!!).enqueue(object : ToastCallback<Fund>(context) {
            override fun onSuccess(response: Fund) {
                lv.adapter = FundEventsAdapter(response)
            }

        })
    }

    override val containerView = R.layout.fragment_list

}