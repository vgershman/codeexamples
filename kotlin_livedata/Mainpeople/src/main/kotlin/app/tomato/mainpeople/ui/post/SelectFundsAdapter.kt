package app.tomato.mainpeople.ui.post

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Fund
import app.tomato.mainpeople.data.GrouppedFund
import app.tomato.mainpeople.data.LocalizationManager
import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_fund.view.*
import kotlinx.android.synthetic.main.item_fund_group.view.*

/**
 * Created by user on 25.08.17.
 */
class SelectFundsAdapter(var hideInfo: Boolean = false, var fundSelected: (fund: Fund) -> Unit, var fundPreview: (fund: Fund) -> Unit, context: Context, data: List<GrouppedFund>)
    : ExpandableRecyclerAdapter<SelectFundsAdapter.GroupVH, SelectFundsAdapter.FundVH>(context, data) {


    private var openFirst = true

    override fun onCreateChildViewHolder(p0: ViewGroup?) = FundVH(LayoutInflater.from(p0?.context).inflate(R.layout.item_fund, p0, false))

    override fun onCreateParentViewHolder(p0: ViewGroup?): GroupVH {
        var parentHolder = GroupVH(LayoutInflater.from(p0?.context).inflate(R.layout.item_fund_group, p0, false))
        parentHolder.setRotation(0f)
        parentHolder.setCustomClickableViewAndItem(R.id.iv_drop)
        return parentHolder
    }

    override fun onBindParentViewHolder(p0: GroupVH?, p1: Int, p2: Any?) {
        p0?.bind(p2 as GrouppedFund)
    }

    override fun onBindChildViewHolder(p0: FundVH?, p1: Int, p2: Any?) {
        p0?.bind(p2 as Fund)
    }

    inner class GroupVH(innerView: View) : ParentViewHolder(innerView) {

        override fun cancelAnimation() {
        }

        override fun setMainItemClickToExpand() {

        }

        var tvGroupName = innerView.tvGroupName

        fun bind(group: GrouppedFund) {
            if (openFirst) {
                openFirst = false
                try {
                    Handler().postDelayed(kotlin.run {
                        Runnable {
                            try {
                                itemView.callOnClick()
                            } catch (ex: Exception) {
                            }
                        }
                    }, 300)
                } catch (ex: Exception) {
                }
            }
            tvGroupName.text = LocalizationManager.getEntry(group.key) ?: group.key
        }

    }


    inner class FundVH(innerView: View) : ChildViewHolder(innerView) {
        var ivAvatar = innerView.ivAvatar
        var tvAuthor = innerView.tvFundName
        var tvMission = innerView.tvMission
        var ivInfo = innerView.btnInfo

        fun bind(fund: Fund) {
            Picasso.get().load(fund.avatar_url).transform(CropCircleTransformation()).into(ivAvatar)
            tvAuthor.text = fund.name
            itemView.setOnClickListener { fundSelected(fund) }
            tvMission.text = fund.mission
            if (!hideInfo) {
                ivInfo.visibility = View.VISIBLE
            }
            ivInfo.setOnClickListener { fundPreview(fund) }
        }

    }

}