package app.tomato.mainpeople.ui

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import app.tomato.basic.data.UnitCallback
import app.tomato.mainpeople.EditText_
import app.tomato.mainpeople.data.Rest
import app.tomato.mainpeople.data.User
import app.tomato.mainpeople.ui.main.MentionsAdapter

/**
 * Created by user on 11.12.17.
 */
object MentionsUtil {

    fun setup(editText: EditText_, list: RecyclerView, mentions: HashMap<String, Int?>, postId: String? = null) {
        list.layoutManager = LinearLayoutManager(list.context)
        list.adapter = MentionsAdapter({ proceedSelectMention(it, editText, list, mentions) })
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                checkNeedSuggestion(p0.toString(), list, postId, editText.selectionStart)
                checkMentions(editText, mentions)
                if (p0 == null || p0.isBlank()) {
                    list.visibility = View.GONE
                }
            }
        })

        editText.selectionListener = object : EditText_.SelectionListener {
            override fun onSelectionChanged(start: Int, end: Int) {
                checkNeedSuggestion(editText.text.toString(), list, postId, start)
            }
        }

    }

    private fun checkMentions(editText: EditText, mentions: HashMap<String, Int?>) {
        mentions.keys.forEach {
            if (!editText.text.toString().contains(it)) {
                mentions.remove(it)
            }
        }
    }

    fun proceedSelectMention(it: User, editText: EditText, list: RecyclerView, mentions: HashMap<String, Int?>) {

        var cursor = editText.selectionStart

        list.visibility = View.GONE
        var lastIndex = editText.text.lastIndexOf("@", cursor)
        if (lastIndex < 0) {
            lastIndex = editText.text.length
        }
        var end = editText.text.toString().substring(lastIndex)
        var endOfWord = end.indexOf(" ")
        if(endOfWord < 0){
            endOfWord = end.length
        }
        endOfWord += lastIndex


        editText.text.replace(lastIndex, endOfWord, "@" + it.getUserFullName().replace(" ", "") + " ")
        if (!mentions.containsKey("@" + it.getUserFullName().replace(" ", ""))) {
            mentions.put("@" + it.getUserFullName().replace(" ", ""), it.id?.toIntOrNull())
        }
    }


    private fun checkNeedSuggestion(query: String, list: RecyclerView, postId: String?, start: Int) {

        var start_ = start
        if (start_ > 1) {
            start_--
        }
        var wordStart = query.lastIndexOf(" ", start_)
        if (wordStart == -1) {
            wordStart = 0
        }
        var words = query.substring(wordStart).split(" ")
        var wordToCheck = if (words[0].isBlank() && words.size > 1) {
            words[1]
        } else {
            words[0]
        }
        if (wordToCheck.startsWith("@")) {
            var search = ""
            if (wordToCheck.length > 1) {
                search = wordToCheck.substring(1)
            }
            queryMentions(search, list, postId)
        } else {
            list.visibility = View.GONE
        }
    }

    private fun queryMentions(query: String, list: RecyclerView, postId: String?) {
        Rest.rest().suggestMentions(query, postId).enqueue(UnitCallback(list.context, {
            (list.adapter as MentionsAdapter).setData(it.mentions)
            if (it.mentions.isNotEmpty()) {
                list.visibility = View.VISIBLE
            } else {
                list.visibility = View.GONE
            }
        }))
    }

}