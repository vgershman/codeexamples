package app.tomato.mainpeople.ui.media

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mainpeople.android.R
import app.tomato.mainpeople.data.PhotoFilters
import com.bumptech.glide.Glide
import jp.co.cyberagent.android.gpuimage.GPUImage
import jp.co.cyberagent.android.gpuimage.GPUImageFilter
import jp.co.cyberagent.android.gpuimage.Rotation
import kotlinx.android.synthetic.main.fragment_tune.*
import kotlinx.android.synthetic.main.list_thumbnail_item.view.*
import java.io.File
import java.util.*
import kotlin.collections.HashMap


/**
 * Created by user on 25.08.17.
 */
class FiltersAdapter(var media: String, var filterClick: (filter: FilterDto) -> Unit) : RecyclerView.Adapter<FiltersAdapter.VH>() {

    var mGpuImage: GPUImage? = null
    var previewMap = HashMap<Int, Uri>()

    var selectedPos = 0


    private fun createPreview(context: Context?, filter: GPUImageFilter?, created: (uri: Uri) -> Unit) {
        if (mGpuImage == null) {
            mGpuImage = GPUImage(context)
            val original = BitmapFactory.decodeFile(media) ?: return
            var newHeight = 0
            var newWidth = 0
            var orientation = ExifInterface.ORIENTATION_NORMAL
            try {
                orientation = ExifInterface(media).getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
            } catch (ex: Exception) {

            }
            //orientation = ExifInterface.ORIENTATION_ROTATE_90
            var rotated: Bitmap
            rotated = if (orientation != ExifInterface.ORIENTATION_NORMAL) {
                val matrix = Matrix()
                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> matrix.postRotate(90f)
                    ExifInterface.ORIENTATION_ROTATE_270 -> matrix.postRotate(270f)
                    ExifInterface.ORIENTATION_ROTATE_180 -> matrix.postRotate(180f)
                }
                Bitmap.createBitmap(original, 0, 0, original.width, original.height, matrix, true)
            } else {
                original
            }
            val widthIsBigger = (rotated.width > rotated.height)
            var scaled: Bitmap
            if (widthIsBigger) {
                newHeight = 300
                newWidth = Math.round(rotated.width.toFloat() * 300.toFloat() / rotated.height.toFloat())
                scaled = Bitmap.createScaledBitmap(rotated, newWidth, newHeight, true)
            } else {
                newWidth = 300
                newHeight = Math.round(rotated.height.toFloat() * 300.toFloat() / rotated.width.toFloat())
                scaled = Bitmap.createScaledBitmap(rotated, newWidth, newHeight, true)
            }
            original.recycle()
            rotated.recycle()
            mGpuImage!!.setImage(scaled)
        }
        if (filter != null) {
            mGpuImage!!.setFilter(filter)
        } else {
            mGpuImage!!.setFilter(GPUImageFilter())
        }
        mGpuImage!!.requestRender()
        mGpuImage!!.saveToPictures("MainpeoplePreviews", UUID.randomUUID().toString(), { created.invoke(it) })

    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder?.bind(PhotoFilters.filters[position], position)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val vh = VH(LayoutInflater.from(parent?.context).inflate(R.layout.list_thumbnail_item, parent, false))
        return vh
    }


    override fun getItemCount() = PhotoFilters.filters.size


    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {
        var tvName = innerView.tvName
        var ivPreview = innerView.ivThumb
        var ivSelected = innerView.ivSelected


        fun bind(filterDto: FilterDto, position: Int) {
            tvName.text = filterDto.name
            tvName.isSelected = position == selectedPos
            if (position == selectedPos) {
                ivSelected.visibility = View.VISIBLE
            } else {
                ivSelected.visibility = View.GONE
            }
            itemView.setOnClickListener { filterClick(filterDto);selectedPos = position;notifyDataSetChanged() }
            if (previewMap[position] != null) {
                Glide.with(ivPreview.context).load(previewMap[position]).into(ivPreview)
            } else {
                createPreview(itemView.context!!, filterDto.filter, {
                    previewMap.put(position, it)
                    Glide.with(ivPreview.context).load(it).into(ivPreview)
                })
            }
        }

    }

    fun select(position: Int) {
        selectedPos = 0
        notifyDataSetChanged()
    }


}

data class FilterDto(var name: String, var filter: GPUImageFilter?)