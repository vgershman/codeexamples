package app.tomato.mainpeople.ui.messages

import android.arch.lifecycle.Observer
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Group
import app.tomato.mainpeople.data.GroupNotificationWrapper
import app.tomato.mainpeople.data.NotificationsManager
import kotlinx.android.synthetic.main.fragment_messages.*

/**
 * Created by user on 14.08.17.
 */
class ChatListFragment : BasicFragment() {

    override fun initViews() {
        swl.setColorSchemeColors(resources.getColor(R.color.blue))
        swl.setOnRefreshListener {
            NotificationsManager.update(context)
            Handler().postDelayed({
                try {
                    swl.isRefreshing = false
                } catch (ex: Exception) {
                }
            }, 1500)
        }
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = GroupsAdapter({ openGroup(it) }, { openNotifications() }, { proceedHashtag(it) })
        NotificationsManager.observe(this, Observer { bindData(it?.first) })
    }

    private fun bindData(data: List<GroupNotificationWrapper>?) {
        if (data != null) {
            (rv.adapter as GroupsAdapter).setData(data)
        }
    }

    private fun proceedHashtag(tag: String) {
        go(MyApp.Screens.MODAL_SEARCH, MyApp.Containers.SIMPLE, params = hashMapOf("query" to "#" + tag))
    }

    private fun openNotifications() {
        go(MyApp.Screens.NOTIFICATIONS, MyApp.Containers.SIMPLE)
    }


    private fun openGroup(group: Group) {
        go(MyApp.Screens.CHAT, MyApp.Containers.SIMPLE, params = hashMapOf("user" to group.members[0], "group" to group))
    }


    override val containerView = R.layout.fragment_messages
}