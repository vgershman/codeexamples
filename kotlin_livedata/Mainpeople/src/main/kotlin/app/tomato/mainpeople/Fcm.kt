package app.tomato.mainpeople

/**
 * Created by user on 01.09.17.
 */

import android.arch.lifecycle.LiveData
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Handler
import android.os.Looper
import android.preference.PreferenceManager
import app.tomato.mainpeople.data.DeletedStoriesManager
import app.tomato.mainpeople.data.NotificationsManager
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


object FcmManager : LiveData<String>() {


    fun init(context: Context) {
        value = PreferenceManager.getDefaultSharedPreferences(context).getString("push_token", FirebaseInstanceId.getInstance().token)
    }


    fun set(context: Context, token: String?) {
        object : AsyncTask<Void, Void, Unit>() {

            override fun doInBackground(vararg p0: Void?) {
                PreferenceManager.getDefaultSharedPreferences(context).edit().putString("push_token", token).apply()
            }

            override fun onPostExecute(result: Unit?) {
                value = token
            }
        }.execute()


    }

}

class FirebaseRegistrationService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        val refreshedToken = FirebaseInstanceId.getInstance().token
        FcmManager.set(this, refreshedToken)
    }

}

class FirebaseNotificationService : FirebaseMessagingService() {


//    override fun zzx(p0: Intent?):{
//
//    }

//    override fun handleIntent(p0: Intent) {
//        if (p0?.extras?.getString("type")?.toIntOrNull() ?: 0 == 11) {
//            NotificationsManager.update(this)
//            Handler(Looper.getMainLooper()).post { DeletedStoriesManager.addDeletedStory(p0?.extras?.getString("post_id") ?: "") }
//
//        } else {
//            super.handleIntent(p0)
//        }
//    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        NotificationsManager.update(this)
    }
}
