package app.tomato.mainpeople.ui.post

import android.view.View
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.data.EditPost
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Rest
import app.tomato.mainpeople.data.Story
import app.tomato.mainpeople.ui.MentionsUtil
import com.bumptech.glide.Glide
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.fragment_edit_post.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import java.util.logging.Handler

/**
 * Created by user on 25.08.17.
 */
class EditPostFragment : BasicFragment() {

    private var story: Story? = null
    var mentions = HashMap<String, Int?>()

    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        btnNext.setOnClickListener { next() }
        MentionsUtil.setup(etComment, mentionsLv, mentions, null)
        KeyboardVisibilityEvent.setEventListener(activity) { isOpen ->
            if (!isOpen) {
                try {
                    mentionsLv.visibility = View.GONE
                } catch (ex: Exception) {

                }

            }
        }
        view?.setOnClickListener { hideKeyboard(etComment, context); }

    }

    override fun onFragmentBackPressed(): Boolean {
        activity?.finish()
        return false
    }

    private fun next() {
        hideKeyboard(etComment, context)
        tryEditPost()
    }


    override fun onPause() {
        super.onPause()
        try {
            ivVideo.release()
        } catch (ex: Exception) {

        }
    }


    override fun onResume() {
        super.onResume()
        try {
            bindMedia()
        } catch (ex: Exception) {

        }
    }

    private fun tryEditPost() {
        val title = etComment.text.toString()
        Rest.rest().editPost(story?.id!!, EditPost(title, mentions)).enqueue(object : ToastCallback<LinkedTreeMap<String, Any?>>(context) {
            override fun onSuccess(response: LinkedTreeMap<String, Any?>) {
                onBackPressed()
            }

        })
    }


    override fun initProperties() {
        super.initProperties()
        story = arguments!!["story"] as Story?
        if (story == null) {
            onBackPressed()
        }
        story?.mentions?.keys?.forEach {
            mentions.put(it, story?.mentions?.get(it))
        }
        bindStory()
        bindMedia()
    }

    private fun bindStory() {
        tvFundSelected.setTextColor(resources.getColor(R.color.textDark))
        tvFundSelected.text = story?.fund_name
        tvCostSelected.text = "$ " + story?.collected
        etComment.setText(story?.title + " ")
        try {
            etComment.setSelection(etComment.text.toString().length)
        } catch (ex: Exception) {
        }
        android.os.Handler().postDelayed({ mentionsLv.visibility = View.GONE }, 500L)

    }


    private fun bindMedia() {
        val viewPortSize = screenWidth(context).toInt()
        var gpuLP = ivContent.layoutParams
        gpuLP.width = viewPortSize
        gpuLP.height = viewPortSize
        ivContent.layoutParams = gpuLP
        ivVideo.layoutParams = gpuLP
        Glide.with(context).load(story?.content).into(ivContent)
        ivContent.visibility = View.VISIBLE
        ivVideo.visibility = View.GONE
        if (story?.video != null) {
            try {
                ivVideo.stop()
                ivVideo.release()
            } catch (ex: Exception) {
            }

            ivContent.visibility = View.GONE
            ivVideo.visibility = View.VISIBLE
            ivVideo.setDataSource(story?.video!!)
            ivVideo.isLooping = true
            ivVideo.prepareAsync { ivVideo.start();ivVideo.pause() }
        }
    }

    override val containerView = R.layout.fragment_edit_post


}