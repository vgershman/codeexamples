package app.tomato.mainpeople.ui.search

import android.view.View
import kotlinx.android.synthetic.main.fragment_tabs_search.*

/**
 * Created by user on 09.09.17.
 */

class ModalSearchFragment : NewSearchFragment() {

    override fun initViews() {
        super.initViews()
        btnClose.visibility = View.VISIBLE
        padder.visibility = View.VISIBLE
        btnClose.setOnClickListener { onBackPressed() }
    }

    override fun initProperties() {
        super.initProperties()
        var query = arguments!!["query"] as String?
        if (query != null) {
            etSearch.setText(query)
            vp_fragments.setCurrentItem(1, false)
        }
    }
}