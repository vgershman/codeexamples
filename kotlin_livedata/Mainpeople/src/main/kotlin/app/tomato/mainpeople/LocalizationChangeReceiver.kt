package app.tomato.mainpeople

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import app.tomato.mainpeople.data.LocalizationManager

/**
 * Created by user on 31.08.17.
 */
class LocalizationChangeReceiver : BroadcastReceiver(){
    override fun onReceive(p0: Context, p1: Intent) {
        LocalizationManager.init(p0)
    }
}