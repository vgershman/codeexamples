package app.tomato.mainpeople.ui.post

import android.app.Activity
import android.content.Intent
import android.os.Handler
import android.view.View
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.AccountManager
import app.tomato.mainpeople.data.Fund
import app.tomato.mainpeople.data.Payments
import app.tomato.mainpeople.ui.MentionsUtil
import app.tomato.mainpeople.ui.container.BottomSheetActivity
import app.tomato.mainpeople.ui.payments.SelectCostAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_new_post_mentions.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent

/**
 * Created by user on 25.08.17.
 */
class NewPostFragment : BasicFragment() {

    private var media: String? = null

    private var isVideo: Boolean = false

    private var fund: Fund? = null

    private var cost: Int = 1

    var mentions = HashMap<String, Int?>()

    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        btnSelectFund.setOnClickListener { hideKeyboard(etComment, context);go(MyApp.Screens.SELECT_FUNDS, MyApp.Containers.SIMPLE, result = 4, from = this@NewPostFragment) }
        btnSelectCost.setOnClickListener { selectCost() }
        btnNext.setOnClickListener { next() }
        view?.setOnClickListener { hideKeyboard(etComment, context); }


        bindFund()
        bindCost()
        MentionsUtil.setup(etComment, mentionsLv, mentions, null)
        KeyboardVisibilityEvent.setEventListener(activity) { isOpen ->
            if (!isOpen) {
                try {
                    mentionsLv.visibility = View.GONE
                } catch (ex: Exception) {

                }

            }
        }

    }

    override fun onFragmentBackPressed(): Boolean {
        activity?.finish()
        return false
    }

    private fun next() {
        btnNext.isEnabled = false
        Handler().postDelayed({
            try {
                btnNext.isEnabled = true
            } catch (ex: Exception) {

            }
        }, 1000L)
        hideKeyboard(etComment, context)
        if (isVideo) {
            ivVideo.pause()
        }
        if (fund != null) {
            checkPhoneConfirmed()
        }
    }


    override fun onPause() {
        super.onPause()
        try {
            ivVideo.release()
        } catch (ex: Exception) {

        }
    }


    override fun onResume() {
        super.onResume()
        try {
            bindMedia()
        } catch (ex: Exception) {

        }
    }

    private fun checkPhoneConfirmed() {

        if (AccountManager.value?.phone_validated != null) {
            tryCreatePost()
        } else {
            go(MyApp.Screens.INPUT_PHONE, MyApp.Containers.DARK, result = 34, from = this)
        }
    }

    private fun tryCreatePost() {
        val title = etComment.text.toString()
        var type = 1
        if (isVideo) {
            type = 3
        }

        Payments.createPost(activity as BottomSheetActivity, mentions, fund!!, cost, title, media!!, type, { activity?.finish() })
    }

    private fun bindCost() {
        tvCostSelected.text = "$ " + cost.toString()
    }

    private fun selectCost() {
        hideKeyboard(etComment, context)
        (activity as BottomSheetActivity).bottomMenu(SelectCostAdapter(MyApp.Config.SUPPORT_COST, { setCost(it) }))
    }

    private fun setCost(it: String) {
        (activity as BottomSheetActivity).hideBottomMenu()
        cost = Integer.valueOf(it)
        bindCost()

    }

    override fun initProperties() {
        super.initProperties()
        media = arguments!!["media"] as String
        isVideo = arguments!!["video"] as Boolean
        bindMedia()
    }


    private fun bindMedia() {
        val viewPortSize = screenWidth(context).toInt()
        var gpuLP = ivContent.layoutParams
        gpuLP.width = viewPortSize
        gpuLP.height = viewPortSize
        ivContent.layoutParams = gpuLP
        ivVideo.layoutParams = gpuLP
        Glide.with(context).load(media).into(ivContent)
        ivContent.visibility = View.VISIBLE
        ivVideo.visibility = View.GONE
        if (isVideo) {
            try {
                ivVideo.stop()
                ivVideo.release()
            } catch (ex: Exception) {
            }

            ivContent.visibility = View.GONE
            ivVideo.visibility = View.VISIBLE
            ivVideo.setDataSource(media!!)
            ivVideo.isLooping = true
            ivVideo.prepareAsync { ivVideo.start();ivVideo.pause() }
        }

    }

    override val containerView = R.layout.fragment_new_post_mentions


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 4 && resultCode == Activity.RESULT_OK) {
            fund = data?.getSerializableExtra("fund") as Fund?
            bindFund()
        }
        if (requestCode == 34 && resultCode == Activity.RESULT_OK) {
            next()
        }
    }

    private fun bindFund() {
        if (fund != null) {
            tvFundSelected.text = fund!!.name
            tvFundSelected.setTextColor(resources.getColor(R.color.textDark))
        } else {
            tvFundSelected.setTextColor(resources.getColor(R.color.gray))
            tvFundSelected.text = getString(R.string.hnt_nonprofit)
        }
    }
}