package app.tomato.mainpeople.ui.profile

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mainpeople.android.R
import app.tomato.mainpeople.data.User
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_ranking.view.*


/**
 * Created by user on 16.08.17.
 */
class RankingsAdapter(var userClick: (user: User) -> Unit) : RecyclerView.Adapter<RankingsAdapter.VH>() {

    private val data = ArrayList<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_ranking, parent, false))

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder?.bind(data[position])
    }

    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var ivAvatar = innerView.ivAvatar
        var tvAuthor = innerView.tvAuthorName
        var tvCost = innerView.btnPay

        fun bind(user: User) {
            Picasso.get().load(user.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
            tvAuthor.text = user.getUserFullName() //user.name + user.surname
            itemView.setOnClickListener { userClick(user) }
            tvCost.text = "$ " + user.ranking_collected
        }

    }

    fun setData(users: List<User>) {
        data.clear()
        data.addAll(users)
        notifyDataSetChanged()
    }
}