package app.tomato.mainpeople.ui.search

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.tomato.mainpeople.data.*
import app.tomato.mainpeople.ui.main.FeedAdapter
import com.mainpeople.android.R
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_fund_full.view.*
import kotlinx.android.synthetic.main.item_tag.view.*
import kotlinx.android.synthetic.main.item_user_new.view.*

/**
 * Created by user on 18.08.17.
 */
class SearchAdapter(var fundClick: (fund: String) -> Unit, var userClick: (user: User) -> Unit, var tagClick: (tag: Tag) -> Unit, var postClick: (story: Story) -> Unit, var likesClick: (story: Story?) -> Unit, var like: (story: Story?) -> Unit, var hashTag: (tag: String) -> Unit, var mentionClick: (id: Int?) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

    val PADDING = -1
    val TAG = 0
    val USER = 1
    val FUND = 2
    val POST = 3

    private var data: SearchResult = SearchResult()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var paddingOffset = 0
        var tagsOffset = data.hashtag.result.size
        var usersOffset = data.user.result.size
        var fundsOffset = data.fund.result.size
        if (holder is VHTag) {
            holder.bindTag(data.hashtag.result[position - paddingOffset])
        }
        if (holder is VHUser) {
            holder.bindUser(data.user.result[position - paddingOffset - tagsOffset].toUser())
        }
        if (holder is VHFund) {
            holder.bindFund(data.fund.result[position - paddingOffset - tagsOffset - usersOffset])
        }
        if (holder is FeedAdapter.VHStory) {
            holder.bind(data.post.result[position - paddingOffset - tagsOffset - usersOffset - fundsOffset], false)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TAG) {
            return VHTag(LayoutInflater.from(parent?.context).inflate(R.layout.item_tag, parent, false))
        }
        if (viewType == USER) {
            return VHUser(LayoutInflater.from(parent?.context).inflate(R.layout.item_user_new, parent, false))
        }
        if (viewType == FUND) {
            return VHFund(LayoutInflater.from(parent?.context).inflate(R.layout.item_fund_full, parent, false))
        }
        if (viewType == POST) {
            return FeedAdapter.VHStory(LayoutInflater.from(parent?.context).inflate(R.layout.item_feed, parent, false), userClick, postClick, fundClick, likesClick, like, hashTag, mentionClick)
        }
        if (viewType == PADDING) {
            return VHPadding(LayoutInflater.from(parent?.context).inflate(R.layout.search_fix, parent, false))
        }
        return VHPadding(TextView(parent?.context))

    }


    override fun getItemViewType(position: Int): Int {
        var paddingOffset = 0
        var tagsOffset = data.hashtag.result.size
        var usersOffset = data.user.result.size
        var fundsOffset = data.fund.result.size
        var postOffset = data.post.result.size

//        if (position == 0) {
//            return PADDING
//        }
        if (tagsOffset != 0 && position < paddingOffset + tagsOffset) {
            return TAG
        }

        if (usersOffset != 0 && position < paddingOffset + tagsOffset + usersOffset) {
            return USER
        }
        if (fundsOffset != 0 && position < paddingOffset + tagsOffset + usersOffset + fundsOffset) {
            return FUND
        }

        if (postOffset != 0 && position < paddingOffset + tagsOffset + usersOffset + fundsOffset + postOffset) {
            return POST
        }
        return PADDING
    }

    override fun getItemCount(): Int {
        var count = 0
        //count += 1 //padding
        count += data.hashtag.result.size
        count += data.user.result.size
        count += data.fund.result.size
        count += data.post.result.size
        return count
    }

    inner class VHTag(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var tvHashtag = innerView.tvHashtagName

        fun bindTag(tag: Tag) {
            tvHashtag.text = tag.name
            itemView.setOnClickListener { tagClick(tag) }
        }


    }

    inner class VHUser(innerView: View) : RecyclerView.ViewHolder(innerView) {


        var ivAvatar = innerView.ivAvatar_
        var tvAuthor = innerView.tvAuthorName_
        var tvInfo = innerView.tvPostDateTime_
        var tvCost = innerView.btnPay_


        fun bindUser(user: User) {
            itemView.setOnClickListener { userClick(user) }
            Picasso.get().load(user.avatar_url).placeholder(R.drawable.ic_launcher).transform(CropCircleTransformation()).fit().into(ivAvatar)
            tvAuthor.text = user?.getUserFullName()
            tvInfo.text = user.city?.name
            tvCost.text = "$" + user.statistics?.USER_COLLECTION
        }
    }

    inner class VHFund(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var ivImage = innerView.ivFundImage
        var tvFundName = innerView.tvFundName
        var tvFundMission = innerView.tvFundMission
        var tvFundLink = innerView.tvFundLink

        fun bindFund(fund: Fund) {
            Picasso.get().load(fund.photo).into(ivImage)
            tvFundName.text = fund.name
            tvFundMission.text = fund.mission
            tvFundLink.text = fund.link
            itemView.setOnClickListener { fundClick(fund.id!!) }
        }
    }

    inner class VHPadding(innerView: View) : RecyclerView.ViewHolder(innerView)


    fun setData(response: SearchResult) {
        data = response
        notifyDataSetChanged()
    }

}

