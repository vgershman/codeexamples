package app.tomato.mainpeople.ui.profile

import android.view.View
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.help.dpToPx
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.basic.ui.basic.TabsAdapter
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.data.*
import app.tomato.mainpeople.ui.main.UserCollectedFeedFragment
import app.tomato.mainpeople.ui.main.UserFeedFragment
import app.tomato.mainpeople.ui.main.UserGridFeedFragment
import com.mainpeople.android.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*

/**
 * Created by user on 14.08.17.
 */


interface ProfileFragment {
    fun getCurrentUser(): User
}

class UserProfileFragment : BasicFragment(), ProfileFragment {

    override fun getCurrentUser() = user!!


    private var amIFollower: Boolean? = null
    var user: User? = null

    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        vpProfile.adapter = TabsAdapter(childFragmentManager, screens = arrayOf(UserFeedFragment::class.java, UserGridFeedFragment::class.java, UserCollectedFeedFragment::class.java))
        tabs.setupWithViewPager(vpProfile)
        btnChat.setOnClickListener { go(MyApp.Screens.CHAT, MyApp.Containers.SIMPLE, params = hashMapOf("user" to user)) }
        btnFollow.setOnClickListener { toggleFollow() }
        setupTabIcons()
        val viewPortSize = screenWidth(context).toInt()
        var lp = ivAvatar.layoutParams
        lp.width = viewPortSize
        lp.height = viewPortSize + dpToPx(context, 70).toInt()
        ivAvatar.layoutParams = lp

    }

    private fun loadRelation() {
        Rest.rest().getRelation(user?.id!!).enqueue(object : ToastCallback<List<Relation>>(context) {
            override fun onSuccess(response: List<Relation>) {
                amIFollower = response.size > 0
                bindRelation()
            }
        })
    }

    private fun toggleFollow() {
        Rest.rest().relateUser(user?.id!!, if (amIFollower!!) 0 else 1).enqueue(object : ToastCallback<Relation>(context) {
            override fun onSuccess(response: Relation) {
                FollowingsManager.init(context)
                AccountManager.loadUser(context)
                loadRelation()
            }
        })
    }

    override fun initProperties() {
        super.initProperties()
        user = arguments!!["user"] as User?
        if (!AccountManager.value?.id.equals(user?.id)) {
            btnChat.visibility = View.VISIBLE
        } else {
            btnSettings.visibility = View.VISIBLE
            btnSettings.setOnClickListener { go(MyApp.Screens.SETTINGS, MyApp.Containers.SIMPLE, result = 6) }
        }

        loadDetailed()
        bindUser()
        loadRelation()
    }

    private fun loadDetailed() {
        Rest.rest().user(user?.id!!).enqueue(object : ToastCallback<User>(context) {
            override fun onSuccess(response: User) {
                user = response
                bindUser()
            }

        })
    }

    private fun bindUser() {
        loadImage()
        tvName.text = user?.getUserFullName() //user?.name + " " + user?.surname
        tvLocation.text = user?.city?.name + ", " + user?.city?.country_name
        tvFollowers.text = user?.followersCount?.toString()
        tvFollowing.text = user?.followingCount?.toString()
        tvRankings.text = user?.ranking?.toString()
        btnRankings.setOnClickListener { go(MyApp.Screens.RANKINGS) }
        tvCount.text = "$" + user?.statistics?.USER_COLLECTION.toString()
        if (user?.relation != null) {
            bindRelation()
        }
        if (user?.followersCount != null) {
            btnFollowers.setOnClickListener { go(MyApp.Screens.FOllOWERS, params = hashMapOf("user" to user)) }
        }
        if (user?.followingCount != null) {
            btnFollowings.setOnClickListener { go(MyApp.Screens.FOLLOWING, params = hashMapOf("user" to user)) }
        }

    }

    private fun loadImage() {
        Picasso.get().load(user?.avatar_url).resize(screenWidth(context).toInt(), 0).into(ivAvatar)
    }


    private fun bindRelation() {
        if (AccountManager.value?.id.equals(user?.id)) {
            btnFollow.visibility = View.GONE
            return
        }
        btnFollow.visibility=View.VISIBLE
        try {
            if (amIFollower != null) {
                if (amIFollower!!) {
                    btnFollow.setImageResource(R.drawable.ic_followed)
                } else {
                    btnFollow.setImageResource(R.drawable.ic_follow_white)
                }

                btnFollow.setOnClickListener { toggleFollow() }
            }
        } catch (ex: Exception) {

        }

    }


    private fun setupTabIcons() {
        tabs.getTabAt(0)?.icon = resources.getDrawable(R.drawable.ic_user_feed)
        tabs.getTabAt(1)?.icon = resources.getDrawable(R.drawable.ic_user_grid)
        tabs.getTabAt(2)?.icon = resources.getDrawable(R.drawable.ic_user_sort)
    }


    override val containerView = R.layout.fragment_profile

}