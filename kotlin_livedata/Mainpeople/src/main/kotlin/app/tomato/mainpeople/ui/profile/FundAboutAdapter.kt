package app.tomato.mainpeople.ui.profile

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Fund
import app.tomato.mainpeople.data.User
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.fund_about.view.*
import kotlinx.android.synthetic.main.item_user.view.*

/**
 * Created by user on 18.08.17.
 */
class FundAboutAdapter(var fund: Fund, var onLink: (url: String?) -> Unit, var userClick: (user: User) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var data = ArrayList<User>()

    fun setData(users: List<User>) {
        data.clear()
        data.addAll(users)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VH) {
            holder.bind()
        }
        if (holder is VHUser) {
            holder.bindUser(data[position - 1])
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 0) {
            return VH(LayoutInflater.from(parent?.context).inflate(R.layout.fund_about, parent, false))
        } else {
            return VHUser(LayoutInflater.from(parent?.context).inflate(R.layout.item_user, parent, false))
        }
    }


    override fun getItemViewType(position: Int) = if (position == 0) 0 else 1


    override fun getItemCount() = 1 + data.size


    inner class VHUser(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var tvName = innerView.tvUserName
        var tvCity = innerView.tvUserCity
        var ivAvatar = innerView.ivUserAvatar


        fun bindUser(user: User) {
            itemView.setOnClickListener { userClick(user) }
            Picasso.get().load(user.avatar_url).placeholder(R.drawable.ic_launcher).transform(CropCircleTransformation()).fit().into(ivAvatar)
            tvName.text = user?.getUserFullName() //user.name + " " + user.surname
            tvCity.text = user.city?.name
        }
    }


    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var tvDesc = innerView.tvFundDesc
        var tvLink = innerView.tvLink
        var llLink = innerView.llLink

        fun bind() {
            tvDesc.text = fund.description
            tvLink.text = fund.link
            llLink.setOnClickListener { onLink(fund.link) }
        }

    }

}