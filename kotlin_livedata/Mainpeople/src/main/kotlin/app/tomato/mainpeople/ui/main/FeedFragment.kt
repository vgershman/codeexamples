package app.tomato.mainpeople.ui.main

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.data.UnitCallback
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.MyApp.Containers.Companion.SIMPLE
import app.tomato.mainpeople.MyApp.Screens.Companion.STORY
import app.tomato.mainpeople.MyApp.Screens.Companion.SUPPORTERS
import app.tomato.mainpeople.MyApp.Screens.Companion.USER
import com.mainpeople.android.R
import app.tomato.mainpeople.data.*
import app.tomato.mainpeople.ui.container.BottomSheetActivity
import app.tomato.mainpeople.ui.profile.ProfileFragment
import kotlinx.android.synthetic.main.fragment_feed_list.*
import kotlinx.android.synthetic.main.fragment_list_swl.*
import retrofit2.Call
import kotlin.collections.HashMap


/**
 * Created by user on 14.08.17.
 */
abstract class FeedFragment : BasicFragment() {

    var loadingFund = false

    override val containerView = R.layout.fragment_feed_list

    open var showPadding: Boolean = false

    var storyToSupport: Story? = null

    var page: Int = 0

    var visibleThreshold = 5


    var loading = false

    var force = false

    var allDataLoaded = false

    var feedCallback: ToastCallback<List<Story>>? = null

    fun initFeedback() {

        feedCallback = object : ToastCallback<List<Story>>(context) {
            override fun onSuccess(response: List<Story>) {
                try {
                    swl.isRefreshing = false
                    StoriesManager.addStories(response)
                    if (response.size < 10) {
                        allDataLoaded = true
                    }
                    proceedData(response)
                    if (force) {
                        force = false
                    }
                } catch (ex: Exception) {
                }
                loading = false
            }

            override fun onConnectionFailure() {
                super.onConnectionFailure()
                swl.isRefreshing = false
                loading = false
            }

            override fun onError(error: Error?) {
                super.onError(error)
                swl.isRefreshing = false
                loading = false
            }

        }

    }

    fun proceedData(response: List<Story>) {
        (lv_video.adapter as FeedAdapter).setData(force, response)
    }


    override fun initViews() {
        initFeedback()
        StoriesManager.observe(this, Observer<HashMap<String, StoryInfo>> { lv_video.adapter?.notifyDataSetChanged() })
        DeletedStoriesManager.observe(this, Observer { onStoryDeleted() })
        lv_video.layoutManager = LinearLayoutManager(context)
        lv_video.adapter = FeedAdapter(showPadding,
                { go(USER, SIMPLE, params = hashMapOf("user" to it)) },
                { go(STORY, SIMPLE, params = hashMapOf("story" to it)) },
                { go(SUPPORTERS, SIMPLE, params = hashMapOf("story" to it)) },
                { openFund(it) },
                { support(it) },
                { go(MyApp.Screens.MODAL_SEARCH, MyApp.Containers.SIMPLE, params = hashMapOf("query" to "#" + it)) },
                { proceedMention(it) })
        try {
            loadData()
        } catch (ex: Exception) {

        }
        swl.setColorSchemeColors(resources.getColor(R.color.blue))
        swl.setOnRefreshListener {
            page = 0
            allDataLoaded = false
            try {
                loadData()
                force = true
            } catch (ex: Exception) {

            }
        }

        lv_video.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = lv_video.layoutManager.itemCount
                val lastVisibleItemPosition = (lv_video.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                if (!allDataLoaded && !loading && lastVisibleItemPosition + visibleThreshold > totalItemCount) {
                    page++
                    loadData()
                }

            }

        })

    }

    private fun proceedMention(id: Int?) {
        if (id != null) {
            Rest.rest().user(id.toString()).enqueue(UnitCallback(context, {
                go(USER, SIMPLE, params = hashMapOf("user" to it))
            }))
        }
    }

    private fun onStoryDeleted() {
        try {
            (lv_video.adapter as FeedAdapter).onStoryDeleted()
        } catch (ex: Exception) {

        }
    }

    fun support(it: Story) {
        storyToSupport = it
        if (AccountManager.value?.phone_validated != null) {
            Payments.support(it, activity as BottomSheetActivity)
        } else {
            go(MyApp.Screens.INPUT_PHONE, MyApp.Containers.DARK, result = 34, from = this)
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 34 && resultCode == Activity.RESULT_OK && storyToSupport != null) {
            support(storyToSupport!!)
        }
    }

    private fun openFund(it: String) {
        if (loadingFund) return
        loadingFund = true
        Rest.rest().fund(it).enqueue(object : ToastCallback<Fund>(context) {

            override fun onSuccess(response: Fund) {
                go(MyApp.Screens.FUND, MyApp.Containers.SIMPLE, params = hashMapOf("fund" to response))
                loadingFund = false
            }

            override fun onError(error: Error?) {
                super.onError(error)
                loadingFund = false
            }

            override fun onConnectionFailure() {
                super.onConnectionFailure()
                loadingFund = false
            }
        })
    }

    override fun reload() {
        try {
            (lv_video.layoutManager as LinearLayoutManager).scrollToPosition(0)
        } catch (ex: Exception) {

        }
        page = 0
        loadData()
        force = true
    }

    fun loadData() {
        loading = true
        if (feedCallback != null) {
            request().enqueue(feedCallback)
        }
    }

    abstract fun request(): Call<List<Story>>

}


class WorldFeedFragment : FeedFragment() {

    override fun request() = Rest.rest().feedWorld(page)

}

class FriendsFeedFragment : FeedFragment() {

    override fun request() = Rest.rest().feedFriends(page)

}

class UserFeedFragment : FeedFragment() {


    override fun request() = Rest.rest().feedUser((parentFragment as ProfileFragment).getCurrentUser().id!!, page)

    override var showPadding = true


}

class UserCollectedFeedFragment : FeedFragment() {


    override fun request() = Rest.rest().feedUserCollected((parentFragment as ProfileFragment).getCurrentUser().id!!)

    override var showPadding = true

}

class TopFeedFragment : FeedFragment() {


    override fun request(): Call<List<Story>> {
        var sort = "all"
        if (parentFragment != null) {
            sort = (parentFragment as FeedTabsFragment).currentTop
        }
        return Rest.rest().feedTop(sort, page)
    }


}

class UserGridFeedFragment : BasicFragment() {

    var page: Int = 0

    var visibleThreshold = 5

    var previousTotalItemCount: Int = 0

    var loading = false

    var force = false


    override val containerView = R.layout.fragment_list_swl

    override fun initViews() {
        DeletedStoriesManager.observe(this, Observer { onStoryDeleted() })
        lv.layoutManager = GridLayoutManager(context, 3)
        lv.adapter = FeedGridAdapter(
                { go(STORY, SIMPLE, params = hashMapOf("story" to it)) })
        try {
            loadData()
        } catch (ex: Exception) {

        }
        swl_.setColorSchemeColors(resources.getColor(R.color.blue))
        swl_.setOnRefreshListener {
            page = 0
            try {
                loadData()
                force = true
            } catch (ex: Exception) {

            }

        }

        lv.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = lv.layoutManager.itemCount

                val lastVisibleItemPosition = (lv.layoutManager as GridLayoutManager).findLastVisibleItemPosition()

                if (loading && totalItemCount > previousTotalItemCount) {
                    loading = false
                    previousTotalItemCount = totalItemCount
                }

                if (!loading && lastVisibleItemPosition + visibleThreshold > totalItemCount) {
                    page++
                    loadData()
                }

            }

        })
    }

    private fun onStoryDeleted() {
        try {
            (lv.adapter as FeedGridAdapter).onStoryDeleted()
        } catch (ex: Exception) {

        }
    }

    override fun reload() = loadData()

    fun loadData() {
        loading = true
        Rest.rest().feedUser((parentFragment as ProfileFragment).getCurrentUser().id!!, page).enqueue(object : ToastCallback<List<Story>>(context) {
            override fun onSuccess(response: List<Story>) {
                try {
                    swl_.isRefreshing = false
                    StoriesManager.addStories(response)
                    (lv.adapter as FeedGridAdapter).setData(force, response)
                    if (force) {
                        force = false
                    }
                } catch (ex: Exception) {
                    var exm = ex.message
                }
            }

            override fun onError(error: Error?) {
                super.onError(error)
                swl_.isRefreshing = false
            }

            override fun onConnectionFailure() {
                super.onConnectionFailure()
                swl_.isRefreshing = false
            }
        })
    }
}