package app.tomato.mainpeople.ui.settings

import android.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.ui.basic.BasicFragment
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Card
import app.tomato.mainpeople.data.Rest
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.fragment_cards.*

/**
 * Created by user on 14.08.17.
 */
class EditPaymentFragment : BasicFragment() {
    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = CardsAdapter({ proceed(it) })
        loadData()
    }

    private fun proceed(it: Card) {
        AlertDialog.Builder(context).setMessage(getString(R.string.alert_remove_card).replace("<CARD>", it.name!!, true))
                .setNegativeButton(getString(R.string.alert_remove_card_cancel), { dialogInterface, _ -> dialogInterface.cancel() })
                .setPositiveButton(getString(R.string.alert_remove_card_confirm), { dialogInterface, _ -> dialogInterface.cancel(); deleteCard(it) })
                .create().show()
    }

    private fun deleteCard(it: Card) {
        Rest.rest().deleteCard(it.id!!).enqueue(object : ToastCallback<LinkedTreeMap<String, Any?>>(context) {
            override fun onSuccess(response: LinkedTreeMap<String, Any?>) {
                loadData()
            }

        })
    }

    private fun loadData() {
        Rest.rest().listCards().enqueue(object : ToastCallback<List<Card>>(context) {
            override fun onSuccess(response: List<Card>) {
                (rv.adapter as CardsAdapter).setData(response)
            }
        })
    }

    override val containerView = R.layout.fragment_cards

}