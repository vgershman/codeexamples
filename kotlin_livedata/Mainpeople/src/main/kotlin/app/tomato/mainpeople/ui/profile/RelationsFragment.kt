package app.tomato.mainpeople.ui.profile

import android.arch.lifecycle.Observer
import android.support.v7.widget.LinearLayoutManager
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.data.UnitCallback
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.*
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.fragment_relations.*


/**
 * Created by user on 18.08.17.
 */
abstract class RelationsFragment : BasicFragment() {

    protected var user: User? = null

    abstract var title: Int

    abstract fun counter(): String

    override fun initProperties() {
        super.initProperties()
        user = arguments!!["user"] as User
        if (user != null) {
            bindUser()
            loadData()
        }
    }

    private fun bindUser() {
        tvContributorsCount.text = counter()
        Picasso.get().load(user?.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
        tvAuthorName.text = user?.getUserFullName() //user?.name + " " + user?.surname
        tvPostDateTime.text = user?.getCreatedNice()
    }

    abstract fun loadData()

    override fun initViews() {
        FollowingsManager.observe(this, Observer { update() })
        FollowingsManager.init(context)
        btnClose.setOnClickListener { onBackPressed() }
        tvToolbar.text = getText(title)
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = RelationsAdapter({ openUser(it) }, { relateUser(it) })
    }

    private fun update() {
        if (rv.adapter != null) {
            rv.adapter.notifyDataSetChanged()
        }
    }

    private fun relateUser(it: User) {
        var relation = 1
        if (FollowingsManager.value?.contains(it) == true) {
            relation = 0
        }
        Rest.rest().relateUser(it.id!!, relation).enqueue(object : ToastCallback<Relation>(context) {
            override fun onSuccess(response: Relation) {
                FollowingsManager.init(context)
                AccountManager.loadUser(context)
            }
        })
    }


    private fun openUser(user: User?) {
        go(MyApp.Screens.USER, MyApp.Containers.SIMPLE, params = hashMapOf("user" to user))
    }

    override val containerView = R.layout.fragment_relations
}

class FollowersFragment : RelationsFragment() {


    override var title = R.string.ttl_followers

    override fun counter() = user?.followersCount.toString() + " " + getString(R.string.lbl_followers_count)

    override fun loadData() {
        //Rest.rest().listFollowings(AccountManager.value?.id!!).enqueue(UnitCallback<Followings>(context)

        Rest.rest().listFollowers(user?.id!!).enqueue(object : ToastCallback<Followers>(context) {
            override fun onSuccess(response: Followers) {
                (rv.adapter as RelationsAdapter).setData(response.followers.map { it.relation_user!! })
            }

        })
    }
}

class FollowingsFragment : RelationsFragment() {

    override fun counter() = user?.followingCount.toString() + " " + getString(R.string.lbl_followings_count)

    override var title = R.string.ttl_followings


    override fun loadData() {
        Rest.rest().listFollowings(user?.id!!).enqueue(object : ToastCallback<Followings>(context) {
            override fun onSuccess(response: Followings) {
                (rv.adapter as RelationsAdapter).setData(response.following.map { it.relation_user!! })
            }

        })
    }
}