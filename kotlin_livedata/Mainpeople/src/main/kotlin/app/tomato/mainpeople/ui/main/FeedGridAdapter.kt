package app.tomato.mainpeople.ui.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.tomato.basic.help.screenWidth
import app.tomato.mainpeople.data.DeletedStoriesManager
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Story
import app.tomato.mainpeople.data.User
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_feed_grid.view.*

/**
 * Created by user on 16.08.17.
 */
class FeedGridAdapter(var openStory: (story: Story) -> Unit) : RecyclerView.Adapter<FeedGridAdapter.VH>() {

    private var mData = ArrayList<Story>()

    override fun getItemCount() = mData.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(mData[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_feed_grid, parent, false))
    }

    inner class VH(innerView: View, var ivImage: ImageView = innerView.ivImageGrid, var tvCount: TextView = innerView.btnPay) : RecyclerView.ViewHolder(innerView) {

        fun bind(story: Story) {
            val size = Math.round(screenWidth(ivImage.context) / 3)
            var lp = ivImage.layoutParams
            lp.width = size
            lp.height = size
            ivImage.layoutParams = lp
            tvCount.text = "$" + story.collected
            Picasso.get().load(story.content).resize(size, 0).into(ivImage)
            ivImage.setOnClickListener { openStory(story) }
        }

    }

    fun onStoryDeleted() {
        var data = ArrayList<Story>()
        data.addAll(mData)
        mData.clear()
        mData.addAll(data.filterNot { DeletedStoriesManager.value?.contains(it.id) ?: false })
        notifyDataSetChanged()
    }

    fun setData(force: Boolean = false, data: List<Story>) {
        if (force) {
            mData.clear()
        }
        mData.addAll(data)
        onStoryDeleted()
    }


}