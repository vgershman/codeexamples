package app.tomato.mainpeople.data

import android.content.Context
import app.tomato.mainpeople.ui.media.FilterDto
import jp.co.cyberagent.android.gpuimage.GPUImageSepiaFilter
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter
import jp.co.cyberagent.android.gpuimage.GPUImageGrayscaleFilter
import android.R.raw
import com.mainpeople.android.R
import jp.co.cyberagent.android.gpuimage.GPUImageFilter
import java.io.IOException


/**
 * Created by user on 25.08.17.
 */
object PhotoFilters {

    var filters = ArrayList<FilterDto>()


    fun init(context: Context) {
        filters.add(FilterDto(context.getString(R.string.filter_original), null))
        filters.add(FilterDto("Swamp", getEffect(context, R.raw.cross_1)))
        filters.add(FilterDto("Land", getEffect(context, R.raw.cross_2)))
        filters.add(FilterDto("Old Paper", getEffect(context, R.raw.cross_3)))
        filters.add(FilterDto("Brume", getEffect(context, R.raw.cross_4)))
        filters.add(FilterDto("Ketro", getEffect(context, R.raw.cross_5)))
        filters.add(FilterDto("Azure", getEffect(context, R.raw.cross_6)))
        filters.add(FilterDto("Cooper", getEffect(context, R.raw.cross_7)))
        filters.add(FilterDto("Lomo", getEffect(context, R.raw.cross_8)))
        filters.add(FilterDto("Astia", getEffect(context, R.raw.cross_9)))
        filters.add(FilterDto("Noir", getEffect(context, R.raw.cross_10)))

    }

    fun getEffect(context: Context, raw: Int): GPUImageFilter? {
        try {
            val ins = context.resources.openRawResource(raw)
            val filter = GPUImageToneCurveFilter()
            filter.setFromCurveFileInputStream(ins)
            ins.close()
            return filter
        } catch (e: Exception) {
            return null
        }

    }


}