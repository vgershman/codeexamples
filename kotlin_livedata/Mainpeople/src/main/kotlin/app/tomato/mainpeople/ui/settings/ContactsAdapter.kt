package app.tomato.mainpeople.ui.settings

import android.content.res.ColorStateList
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mainpeople.android.R
import app.tomato.mainpeople.data.FollowingsManager
import app.tomato.mainpeople.data.PhoneUser
import app.tomato.mainpeople.data.User
import app.tomato.mainpeople.data.UserWithBigCityWTF
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_contact.view.*

/**
 * Created by user on 11.09.17.
 */
class ContactsAdapter(var userClick: (user: User) -> Unit, var contactClick: (contact: PhoneUser) -> Unit, var relate: (user: User) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var filteredPhones = ArrayList<PhoneUser>()
    var filteredUsers = ArrayList<UserWithBigCityWTF>()

    var phones = ArrayList<PhoneUser>()

    var mQuery: String = ""

    var users = ArrayList<UserWithBigCityWTF>()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VH) {
            holder?.bind(filteredUsers[position], position == 0)
        }
        if (holder is VHPhone) {
            holder?.bind(filteredPhones[position - filteredUsers.size], position - filteredUsers.size == 0)
        }
    }

    override fun getItemCount() = filteredUsers.size + filteredPhones.size

    override fun getItemViewType(position: Int): Int {
        if (filteredUsers.size > 0 && position < filteredUsers.size) {
            return 0
        } else {
            return 1
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 0) {
            return VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_contact, parent, false))
        }
        if (viewType == 1) {
            return VHPhone(LayoutInflater.from(parent?.context).inflate(R.layout.item_contact, parent, false))
        }
        return VH(TextView(parent.context))
    }


    inner class VHPhone(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var tvName = itemView.tvUserName
        var tvInfo = itemView.tvInfo
        var ivAvatar = itemView.ivAvatar
        var llHeader = itemView.llHeader
        var tvHeader = itemView.tvHeader
        var btnInvite = itemView.btnInvite
        var btnRelate = itemView.btnRelate

        fun bind(phoneUser: PhoneUser, showTitle: Boolean) {
            if (showTitle) {
                llHeader.visibility = View.VISIBLE
                tvHeader.text = itemView.context.getString(R.string.lbl_your_contacts)
            } else {
                llHeader.visibility = View.GONE
            }
            tvName.text = phoneUser.fullname
            tvInfo.text = ""
            btnRelate.visibility = View.GONE
            btnInvite.visibility = View.VISIBLE
            btnInvite.setOnClickListener { contactClick(phoneUser) }
            Picasso.get().load(phoneUser.avatar).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
            itemView.setOnClickListener { }
        }
    }

    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var tvName = itemView.tvUserName
        var tvInfo = itemView.tvInfo
        var ivAvatar = itemView.ivAvatar
        var llHeader = itemView.llHeader
        var tvHeader = itemView.tvHeader
        var btnInvite = itemView.btnInvite
        var btnRelate = itemView.btnRelate


        fun bind(user: UserWithBigCityWTF, showTitle: Boolean) {
            if (showTitle) {
                llHeader.visibility = View.VISIBLE
                tvHeader.text = itemView.context.getString(R.string.lbl_already)
            } else {
                llHeader.visibility = View.GONE
            }
          //  btnRelate.imageTintList = ColorStateList.valueOf(btnRelate.context.resources.getColor(R.color.blue))
            if (FollowingsManager.value?.contains(user.toUser()) == true) {
                btnRelate.setImageResource(R.drawable.ic_followed_blue)
            } else {
                btnRelate.setImageResource(R.drawable.ic_follow_blue)
            }
            btnRelate.visibility = View.VISIBLE
            btnInvite.visibility = View.GONE
            btnRelate.setOnClickListener { relate(user.toUser()) }
            tvName.text = user.getUserFullName()
            tvInfo.text = user.City?.name
            Picasso.get().load(user.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
            itemView.setOnClickListener { userClick(user.toUser()) }
        }


    }

    fun setPhones(it: List<PhoneUser>?) {
        phones.clear()
        if (it != null) {
            phones.addAll(it)
        }
        filter()
        notifyDataSetChanged()
    }

    fun setUsers(it: List<UserWithBigCityWTF>?) {
        users.clear()
        if (it != null) {
            users.addAll(it)
        }
        filter()
        notifyDataSetChanged()
    }

    fun setQuery(query: String) {
        mQuery = query
        filter()
        notifyDataSetChanged()
    }

    private fun filter() {
        filteredPhones.clear()
        filteredPhones.addAll(phones.filter { it.fullname!!.startsWith(mQuery, true) })

        filteredUsers.clear()
        filteredUsers.addAll(users.filter { it.getUserFullName().startsWith(mQuery, true) })

        notifyDataSetChanged()
    }
}