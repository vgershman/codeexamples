package app.tomato.mainpeople.ui.container;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mainpeople.android.R;

import uk.co.senab.photoview.PhotoViewAttacher;


public class ImageViewerActivity extends Activity {

    public static final String EXTRA_URL = "extra_url";
    private boolean mToolbarVisible = true;
    private String mImageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);
        mImageUrl = getIntent().getStringExtra(EXTRA_URL);
        findViewById(R.id.iv_content).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        bindData();
        initButtons();
    }

    private void bindData() {
        if (mImageUrl == null) {
            onBackPressed();
        }
        Glide.with(this).load(mImageUrl).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                new PhotoViewAttacher((ImageView) findViewById(R.id.iv_content));
                return false;
            }
        }).into((ImageView) findViewById(R.id.iv_content));

//        Picasso.with(this).load(mImageUrl).into(), new Callback() {
//            @Override
//            public void onSuccess() {
//
//            }
//
//            @Override
//            public void onError() {
//            }
//        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.anim_close, R.anim.anim_close_);
    }

    private void initButtons() {
        findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        findViewById(R.id.iv_content).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mToolbarVisible = !mToolbarVisible;
                findViewById(R.id.toolbar).setVisibility(mToolbarVisible ? View.VISIBLE : View.GONE);
            }
        });
    }
}
