package app.tomato.mainpeople.ui.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Like
import app.tomato.mainpeople.data.User
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_like.view.*

/**
 * Created by user on 16.08.17.
 */
class ContributorsAdapter(var userClick: (user: User) -> Unit) : RecyclerView.Adapter<ContributorsAdapter.VH>() {

    private val data = ArrayList<Like>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent.context).inflate(R.layout.item_like, parent, false))

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(data[position])
    }

    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var ivAvatar = innerView.ivAvatar
        var tvAuthor = innerView.tvAuthorName
        var tvInfo = innerView.tvPostDateTime
        var tvCost = innerView.btnPay

        fun bind(like: Like) {
            Picasso.get().load(like.user?.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
            tvAuthor.text = like.user?.getUserFullName()
            itemView.setOnClickListener { userClick(like.user!!) }
            tvCost.text = "$ " + like.cost?.split(".")!![0]
            tvInfo.text = like.getCreatedNice()
        }

    }

    fun setData(comments: List<Like>) {
        data.clear()
        data.addAll(comments)
        notifyDataSetChanged()
    }
}