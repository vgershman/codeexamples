package app.tomato.mainpeople.ui.media

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import app.tomato.basic.help.screenWidth
import com.mainpeople.android.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_select_media.view.*
import java.io.File


/**
 * Created by user on 24.08.17.
 */
class SelectMediaAdapter(var cameraClick: () -> Unit, var itemClick: (uri: String) -> Unit) : RecyclerView.Adapter<SelectMediaAdapter.VH>() {

    private var data = ArrayList<String>()

    private var selectedPos = 0

    private var forceSelect = false

    fun setData(data: ArrayList<String>) {
        this.data = data
        selectedPos = 0
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        if (position == 0) {
            holder?.bindCamera(selectedPos == position)
        } else {
            holder?.bind(data[position - 1], position, selectedPos == position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_select_media, parent, false))


    override fun getItemCount() = data.size + 1


    inner class VH(innerView: View, var ivImage: ImageView = innerView.ivImageGrid) : RecyclerView.ViewHolder(innerView) {

        var ivSelect = innerView.ivSelected

        fun bind(uri: String, position: Int, selected: Boolean) {
            if (selected) {
                if (forceSelect) {
                    forceSelect = false
                    itemClick(uri)
                }
                ivSelect.visibility = View.VISIBLE
            } else {
                ivSelect.visibility = View.GONE
            }
            val size = Math.round(screenWidth(ivImage.context) / 4)
            var lp = itemView.layoutParams
            lp.width = size
            lp.height = size
            itemView.layoutParams = lp
            Glide.with(ivImage.context).load(File(uri)).into(ivImage)
            ivImage.setOnClickListener { selectedPos = position;notifyDataSetChanged();itemClick(uri) }
        }

        fun bindCamera(selected: Boolean) {
            if (selected && forceSelect) {
                forceSelect = false
                cameraClick()
            }
            ivSelect.visibility = View.GONE
            val size = Math.round(screenWidth(ivImage.context) / 4)
            var lp = itemView.layoutParams
            lp.width = size
            lp.height = size
            itemView.layoutParams = lp
            ivImage.scaleType = ImageView.ScaleType.CENTER
            ivImage.setImageResource(R.drawable.ic_camera_big)
            ivImage.setOnClickListener { selectedPos = 0;notifyDataSetChanged();cameraClick() }
        }

    }

    fun setSelected(i: Int) {
        selectedPos = i
        forceSelect = true
        notifyDataSetChanged()
    }

}
