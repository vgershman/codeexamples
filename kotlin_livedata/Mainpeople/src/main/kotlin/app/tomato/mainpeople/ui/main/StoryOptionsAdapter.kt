package app.tomato.mainpeople.ui.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mainpeople.android.R
import app.tomato.mainpeople.ui.container.BottomAdapter
import kotlinx.android.synthetic.main.item_cost.view.*

/**
 * Created by user on 01.09.17.
 */
class StoryOptionsAdapter(var myPost: Boolean, var canEdit: Boolean, var context: Context, var click: (position: Int) -> Unit) : BottomAdapter() {


    private var data = arrayListOf(context.getString(R.string.option_share), context.getString(R.string.option_save), thirdOption(), context.getString(R.string.option_edit_post))

    private fun thirdOption(): String {
        if (myPost) {
            return context.getString(R.string.option_delete_post)
        } else {
            return context.getString(R.string.option_report)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VH).bind(data[position], position)
    }


    override fun getItemCount(): Int {
        if (myPost && canEdit) {
            return data.size
        } else {
            return data.size - 1
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_cost, parent, false))

    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {
        var tvMenu = innerView.tvCost

        fun bind(item: String, position: Int) {
            tvMenu.text = item
            itemView.setOnClickListener { click(position) }
        }
    }

}