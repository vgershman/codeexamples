package app.tomato.mainpeople.ui.media

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.hardware.Camera
import android.provider.MediaStore

class NewPhotoHandler(var orientation: Int, var back: Boolean, var context: Context, private val callback: PhotoHandlerCallback?) : Camera.PictureCallback {


    override fun onPictureTaken(data: ByteArray, camera: Camera) {
        val original = BitmapFactory.decodeByteArray(data, 0, data.size)
        val min = Math.min(original.width, original.height)
        var widthMin = false
        if (min == original.width) {
            widthMin = true
        }
        val rotated: Bitmap
        if (!widthMin) {
            val matrix = Matrix()

            when (orientation) {
                1 -> matrix.postRotate(90f)
                2 -> matrix.postRotate(270f)
                3 -> matrix.postRotate(0f)
                4 -> matrix.postRotate(180f)
            }

            if (!back) {
                matrix.preScale(-1.0f, 1.0f)
            }
            rotated = Bitmap.createBitmap(original, 0, 0, original.width, original.height, matrix, true)
        } else {
            rotated = original
        }
        var min_ = Math.min(rotated.width, rotated.height)
        val last = Bitmap.createBitmap(rotated, 0, 0, min_, min_)
        MediaStore.Images.Media.insertImage(context.contentResolver, last, "", "")
        try {
            original.recycle()
            rotated.recycle()
            last.recycle()
        } catch (ex: Exception) {
            var exm = ex.message
        }
        callback?.onPhotoSaved()
        camera.startPreview()
    }


    interface PhotoHandlerCallback {
        fun onPhotoSaved()
        fun onPhotoError()
    }

}
