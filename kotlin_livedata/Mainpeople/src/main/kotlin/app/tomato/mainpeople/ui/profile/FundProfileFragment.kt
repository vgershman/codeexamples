package app.tomato.mainpeople.ui.profile

import android.view.View
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.basic.ui.basic.TabsAdapter
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.data.Error
import app.tomato.mainpeople.data.Fund
import app.tomato.mainpeople.data.Rest
import com.mainpeople.android.R
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.fragment_fund.*

/**
 * Created by user on 14.08.17.
 */
class FundProfileFragment : BasicFragment() {

    var fund: Fund? = null

    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        vpFund.adapter = TabsAdapter(childFragmentManager, titles = arrayOf(getString(R.string.tab_about), getString(R.string.tab_events)), screens = arrayOf(FundAboutFragment::class.java, FundEventsFragment::class.java))
        tabs.setupWithViewPager(vpFund)
    }

    override fun initProperties() {
        super.initProperties()
        fund = arguments!!["fund"] as Fund?
        loadImage()
        bindFund()
    }

    private fun bindFund() {


        tvFundName.text = fund?.name
        tvInfo.text = fund?.mission
        if (fund?.statistics?.FUND_DONATION != null) {
            tvEarned.text = "$ " + fund?.statistics?.FUND_DONATION
            tvEarned.visibility = View.VISIBLE
        } else {
            reloadFund()
        }
        tvEarned.text = "$ " + fund?.statistics?.FUND_DONATION

    }

    private fun reloadFund() {
        Rest.rest().fund(fund!!.id!!).enqueue(object : ToastCallback<Fund>(context) {

            override fun onSuccess(response: Fund) {
                fund = response
                bindFund()
            }

            override fun onError(error: Error?) {

            }

            override fun onConnectionFailure() {
            }
        })
    }

    private fun loadImage() {
        Picasso.get().load(fund?.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivRound)
        val viewPortSize = screenWidth(context).toInt()
        var lp = ivAvatar.layoutParams
        lp.width = viewPortSize
        lp.height = viewPortSize
        ivAvatar.layoutParams = lp
        Picasso.get().load(fund?.photo).resize(viewPortSize, 0).into(ivAvatar)

//
    }


    override val containerView = R.layout.fragment_fund
}