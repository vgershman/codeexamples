package app.tomato.mainpeople.data

import android.arch.lifecycle.LiveData
import android.content.Context
import app.tomato.basic.data.UnitCallback

/**
 * Created by user on 31.08.17.
 */
object NotificationsManager : LiveData<Pair<List<GroupNotificationWrapper>, Int>>() {

    val notificationToShow = arrayListOf(1, 2, 3, 4, 5, 6, 7, 10, 13)

    var localGroups: List<Group> = ArrayList()

    var localNotifications: List<Notification> = ArrayList()

    fun update(context: Context) {
        loadGroups(context, { groups -> loadNotifications(context, { notifications -> value = proceed(groups, notifications) }) })
    }

    private fun proceed(groups: List<Group>, notifications: List<Notification>): Pair<List<GroupNotificationWrapper>, Int> {
        localGroups = groups
        localNotifications = notifications
        var list = ArrayList<GroupNotificationWrapper>()
        var unreadCount = 0
        groups.forEach { list.add(GroupNotificationWrapper(it)); unreadCount += it.unreadMessagesCount }

        if (notifications.size > 0) {
            var notifications_ = notifications.filter { notificationToShow.contains(it.type) }
            unreadCount += notifications_.count { it.read == null }
            list.add(GroupNotificationWrapper(notifications = notifications_))
        }
        return Pair(list.sortedByDescending { it.timestamp() }, unreadCount)
    }

    private fun readLocal(notification: Notification) {
        localNotifications.filter { it.created!! > notification.created!! }.forEach { it.read = "read" }
        value = proceed(localGroups, localNotifications)
    }

    private fun readLocal(message: Message) {
        localGroups.find { it.id == message.im_group_id }?.unreadMessagesCount = 0
        value = proceed(localGroups, localNotifications)
    }


    private fun loadGroups(context: Context, callback: (groups: List<Group>) -> Unit) {
        Rest.rest().listGroups().enqueue(UnitCallback(context, callback))
    }


    private fun loadNotifications(context: Context, callback: (notifications: List<Notification>) -> Unit) {
        Rest.rest().listNotifications().enqueue(UnitCallback(context, callback))
    }

    fun read(context: Context, notification: Notification) {
        readLocal(notification)
        Rest.rest().readNotification(Integer.valueOf(notification.id!!)).enqueue(UnitCallback(context, { update(context) }))
    }

    fun readExact(context: Context, notificationsIds: ArrayList<Int>) {
        Rest.rest().readExactNotifications(notificationsIds).enqueue(UnitCallback(context, { update(context) }))
    }

    fun read(context: Context, message: Message) {
        readLocal(message)
        Rest.rest().readMessage(message.im_group_id!!, message.id!!).enqueue(UnitCallback(context, { update(context) }))
    }

}
