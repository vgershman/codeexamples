package app.tomato.mainpeople.ui.search

import android.view.View
import app.tomato.mainpeople.data.SearchResult
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.fragment_search_.*

/**
 * Created by user on 29.11.17.
 */

class SearchPostsFragment : BaseSearchFragment() {


    override fun bindData(response: SearchResult?) {
        if (response != null && response.post.result.isNotEmpty()) {
            var results = SearchResult(post = response.post)
            searchAdapter.setData(results)
            rv.visibility = View.VISIBLE
            lblError.visibility = View.GONE
        } else {
            showNotFound(R.string.posts_not_found)
        }
    }
}

class SearchUsersFragment : BaseSearchFragment() {


    override fun bindData(response: SearchResult?) {
        if (response != null && response.user.result.isNotEmpty()) {
            var results = SearchResult(user = response.user)
            searchAdapter.setData(results)
            rv.visibility = View.VISIBLE
            lblError.visibility = View.GONE
        } else {
            showNotFound(R.string.users_not_found)
        }


    }
}

class SearchTagsFragment : BaseSearchFragment() {


    override fun bindData(response: SearchResult?) {
        if (response != null && response.hashtag.result.isNotEmpty()) {
            var results = SearchResult(hashtag = response.hashtag)
            searchAdapter.setData(results)
            rv.visibility = View.VISIBLE
            lblError.visibility = View.GONE
        } else {
            showNotFound(R.string.tags_not_found)
        }
    }
}