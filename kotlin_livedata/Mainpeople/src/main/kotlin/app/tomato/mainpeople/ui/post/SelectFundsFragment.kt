package app.tomato.mainpeople.ui.post

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Fund
import app.tomato.mainpeople.data.GrouppedFund
import app.tomato.mainpeople.data.Rest
import kotlinx.android.synthetic.main.fragment_funds_list.*
import java.io.Serializable

/**
 * Created by user on 14.08.17.
 */
class SelectFundsFragment : BasicFragment() {


    override fun initViews() {
        btnClose.setOnClickListener { activity?.setResult(Activity.RESULT_CANCELED);activity?.finish(); }
        rv.layoutManager = LinearLayoutManager(context)
        loadData()
    }

    private fun loadData() {
        Rest.rest().listFundsGrouped().enqueue(object : ToastCallback<List<GrouppedFund>>(context) {
            override fun onSuccess(response: List<GrouppedFund>) {
                val adapter = SelectFundsAdapter(false, { bindFund(it) }, { openFund(it) }, context, response)
                //adapter.setParentAndIconExpandOnClick(true)
                rv.adapter = adapter
            }

        })
    }

    private fun openFund(it: Fund) {
        go(MyApp.Screens.FUND, params = hashMapOf("fund" to it))
    }

    private fun bindFund(it: Fund) {
        activity?.setResult(Activity.RESULT_OK, Intent().putExtra("fund", it as Serializable))
        activity?.finish()
    }

    override val containerView = R.layout.fragment_funds_list

}