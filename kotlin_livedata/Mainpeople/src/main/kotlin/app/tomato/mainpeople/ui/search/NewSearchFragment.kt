package app.tomato.mainpeople.ui.search

import android.support.design.widget.TabLayout
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.basic.ui.basic.BasicTabsFragment
import app.tomato.mainpeople.data.SearchManager
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.fragment_tabs_search.*
import java.util.*

/**
 * Created by user on 29.11.17.
 */
open class NewSearchFragment : BasicTabsFragment() {

    override val titlesId = R.array.search

    private var timer: Timer? = null


    override val screens: Array<Class<out BasicFragment>> =
            arrayOf(SearchFundsFragment::class.java, SearchPostsFragment::class.java, SearchUsersFragment::class.java, SearchTagsFragment::class.java)


    override fun initViews() {
        tabs.tabMode = TabLayout.MODE_SCROLLABLE
        btnClear.setOnClickListener { etSearch.setText(""); hideKeyboard(etSearch, context) }
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.length!! > 0) {
                    btnClear.visibility = View.VISIBLE
                    scheduleQuery(p0.toString())
                } else {
                    btnClear.visibility = View.GONE
                    SearchManager.trends(context)
                }

            }
        })
        SearchManager.trends(context)

    }

    private fun scheduleQuery(query: String) {
        if (timer != null) {
            timer!!.cancel()
        }
        timer = kotlin.concurrent.timer("searcher", false, initialDelay = 1000L, period = 10000, action = {
            try {
                activity?.runOnUiThread {
                    try {
                        SearchManager.search(query, context)
                        hideKeyboard(etSearch, context)
                    } catch (ex: Exception) {
                    }
                }; timer?.cancel()
            } catch (ex: Exception) {
            }
        })

    }

    fun setTag(tag: String?) {
        if (tag == null) {
            return
        }
        hideKeyboard(etSearch, context)
        etSearch.setText(tag)
        vp_fragments.setCurrentItem(1, true)
    }

    override val containerView = R.layout.fragment_tabs_search

}
