package app.tomato.mainpeople.ui.search

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.tomato.basic.help.screenWidth
import app.tomato.mainpeople.data.*
import app.tomato.mainpeople.ui.main.FeedAdapter
import com.mainpeople.android.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_funds_preview.view.*
import kotlinx.android.synthetic.main.item_grid.view.*
import kotlinx.android.synthetic.main.item_search_header.view.*
import kotlinx.android.synthetic.main.item_search_users.view.*
import kotlinx.android.synthetic.main.item_tag.view.*
import java.util.*

/**
 * Created by user on 18.08.17.
 */
class TrendsAdapter(var fundsClick: () -> Unit, var usersClick: (user: User?) -> Unit, var tagClick: (tag: Tag?) -> Unit, var postClick: (post: Story?) -> Unit,
                    var fundClick: (fund: String?) -> Unit, var likesClick: (story: Story?) -> Unit, var like: (story: Story?) -> Unit, var hashTag: (tag: String) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

    val TAG = 1
    val FUNDS = 0
    val USERS = 2
    val HEADER = 3
    val POST = 4
    private var data: SearchResult = SearchResult()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var fundsOffset = 0
        if (data.fund.result.size > 0) {
            fundsOffset = 1
        }
        var usersOffset = 0
        if (data.user.result.size > 0) {
            usersOffset = 1
        }
        var tagsOffset = 0
        if (data.hashtag.result.size > 0) {
            tagsOffset = data.hashtag.result.size + 1
        }

        if (holder is VHFunds) {
            holder.bindFunds(data.fund.result)
        }
        if (holder is VHUsers) {
            holder.bindUsers(data.user.result)
        }
        if (holder is VHTag) {
            holder.bindTag(data.hashtag.result[position - fundsOffset - usersOffset - 1])
        }
        if (holder is FeedAdapter.VHStory) {
            holder.bind(data.post.result[position - fundsOffset - usersOffset - tagsOffset - 1], false)
        }
        if (holder is VHHeader) {
            if (position == fundsOffset + usersOffset) {
                holder.bindHeader(holder.itemView.context.getString(R.string.lbl_trending_tags), { tagClick })
            }

            if (position == fundsOffset + usersOffset + tagsOffset) {
                holder.bindHeader(holder.itemView.context.getString(R.string.lbl_trending_posts), { tagClick })
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == FUNDS) {
            return VHFunds(LayoutInflater.from(parent?.context).inflate(R.layout.item_funds_preview, parent, false))
        }
        if (viewType == TAG) {
            return VHTag(LayoutInflater.from(parent?.context).inflate(R.layout.item_tag, parent, false))
        }
        if (viewType == USERS) {
            return VHUsers(LayoutInflater.from(parent?.context).inflate(R.layout.item_search_users, parent, false))
        }
        if (viewType == HEADER) {
            return VHHeader(LayoutInflater.from(parent?.context).inflate(R.layout.item_search_header, parent, false))
        }
        if (viewType == POST) {
            return FeedAdapter.VHStory(LayoutInflater.from(parent?.context).inflate(R.layout.item_feed, parent, false), usersClick, postClick, fundClick, likesClick, like, hashTag, {})
        }
        return VHTag(TextView(parent?.context))

    }


    override fun getItemViewType(position: Int): Int {
        var fundsOffset = 0
        if (data.fund.result.size > 0) {
            fundsOffset = 1
            if (position == 0) {
                return FUNDS
            }
        }
        var usersOffset = 0
        if (data.user.result.size > 0) {
            usersOffset = 1
            if (position == fundsOffset) {
                return USERS
            }
        }
        var tagOffset = 0
        if (data.hashtag.result.size > 0) {
            tagOffset = data.hashtag.result.size + 1
            if (position == fundsOffset + usersOffset) {
                return HEADER
            } else {
                if (position < fundsOffset + usersOffset + tagOffset) {
                    return TAG
                }
            }
        }
        if (data.post.result.isNotEmpty()) {
            return POST
        }
        return TAG
    }

    override fun getItemCount(): Int {
        var count = 0
        if (data.fund.result.size > 0) {
            count++
        }
        if (data.user.result.size > 0) {
            count++
        }
        if (data.hashtag.result.size > 0) {
            count += data.hashtag.result.size + 1
        }
        if (data.post.result.isNotEmpty()) {
            count += (data as SearchPosts).post.result.size
        }
        return count
    }

    inner class VHHeader(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var tvHeader = innerView.tvHeader

        fun bindHeader(title: String, listener: (any: Any?) -> Unit) {
            tvHeader.text = title
            itemView.setOnClickListener { listener(null) }
        }
    }

    inner class VHUsers(innerView: View) : RecyclerView.ViewHolder(innerView) {


        var grid = innerView.grid
        var btn = innerView.btnGoUsers


        fun bindUsers(users: List<UserWithBigCityWTF
                >) {
            btn.setOnClickListener { usersClick(null) }
            grid.layoutManager = GridLayoutManager(grid.context, 5)
            // grid.adapter = GridAdapter(users, { usersClick(it) })
        }
    }

    inner class VHFunds(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var ivImage = innerView.ivImage
        var tvFundsCount = innerView.tvFundsCount

        fun bindFunds(funds: List<Fund>) {
            var pos = Random().nextInt(funds.size)
            var fund = funds[pos]
            Picasso.get().load(fund.photo).resize(screenWidth(itemView.context).toInt(), 0).into(ivImage)
            tvFundsCount.text = funds.size.toString() + itemView.context.getString(R.string.lbl_nonprofits_count)
            itemView.setOnClickListener { fundsClick() }
        }
    }

    inner class VHTag(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var tvHashtag = innerView.tvHashtagName

        fun bindTag(tag: Tag) {
            tvHashtag.text = tag.name
            itemView.setOnClickListener { tagClick(tag) }
        }

    }

    fun setData(response: SearchResult) {
        data = response
        notifyDataSetChanged()
    }

}

class GridAdapter(var data: List<User>, var userClick: (user: User) -> Unit) : RecyclerView.Adapter<GridAdapter.VH>() {

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_grid, parent, false))

    override fun onBindViewHolder(holder: VH, position: Int) = holder?.bind(data[position])!!

    inner class VH(innerView: View, var ivImage: ImageView = innerView.ivImageGrid) : RecyclerView.ViewHolder(innerView) {

        fun bind(user: User) {
            val size = Math.round(screenWidth(ivImage.context) / 5)
            var lp = ivImage.layoutParams
            lp.width = size
            lp.height = size
            ivImage.layoutParams = lp
            Picasso.get().load(user.avatar_url).resize(size, size).placeholder(R.drawable.ic_launcher).into(ivImage)
            ivImage.setOnClickListener { userClick(user) }
        }

    }
}