package app.tomato.mainpeople.ui.media

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Environment
import app.tomato.basic.ui.basic.BasicFragment
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.fragment_trim.*
import life.knowledge4.videotrimmer.interfaces.OnTrimVideoListener

/**
 * Created by user on 02.09.17.
 */
class TrimFragment : BasicFragment() {

    override fun initViews() {
        btnReset.setOnClickListener { timeLine.reset() }
        btnNext.setOnClickListener { timeLine.save() }
    }

    override fun initProperties() {
        super.initProperties()
        var media = arguments!!["media"] as String
        timeLine.setVideoURI(Uri.parse(media))
        timeLine.setDestinationPath(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).absolutePath + "MainPeople/")
        timeLine.setOnTrimVideoListener(object : OnTrimVideoListener {
            override fun getResult(uri: Uri?) {
                try {
                    activity?.setResult(Activity.RESULT_OK, Intent().putExtra("video", true).putExtra("media", uri?.path))
                    activity?.finish()
                } catch (ex: Exception) {
                }

            }

            override fun cancelAction() {
            }

        })
    }

    override val containerView = R.layout.fragment_trim

}