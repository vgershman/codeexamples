package app.tomato.mainpeople.data

import android.os.Parcel
import android.support.annotation.Keep
import android.text.format.DateUtils
import com.bignerdranch.expandablerecyclerview.Model.ParentObject
import com.linkedin.android.spyglass.suggestions.interfaces.Suggestible
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

val serverFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

//basic

@Keep
open class ResponseDto(var serverCommand: ServerCommand? = null, var serverUrl: String? = null, var serverMessage: String? = null, var serverAction: String? = null)

@Keep
data class ServerCommand(var action: String? = null, var url: String? = null)

@Keep
open class Dto(public var id: String? = null, var created: String? = null) : ResponseDto(), Serializable {
    open fun getCreatedNice(): String {
        try {
            serverFormat.timeZone = TimeZone.getTimeZone("GMT")
            return DateUtils.getRelativeTimeSpanString(serverFormat.parse(created?.split(".")?.get(0)).time, System.currentTimeMillis(), 0).toString()
        } catch (ex: Exception) {
            return ""
        }
    }
}

//tools
@Keep
data class Error(var errCode: String? = null) : ResponseDto()

@Keep
data class License(var url: String? = null) : ResponseDto()

@Keep
data class Localization(var version: Int, var errors: HashMap<String, String>, var entries: HashMap<String, String>) : ResponseDto()

// content
@Keep
open class User(
        var name: String? = null, var surname: String? = null, var avatar_url: String? = null,
        var city: City? = null,
        var gender: Int? = null, var email: String? = null,
        var phone: String? = null, var phone_validated: String? = null,
        var ranking: Int? = null, var ranking_collected: Int? = null, var relation: Relation? = null, var paypal: String? = null,
        var permissions: Int? = null,
        var followersCount: Int? = null, var followingCount: Int? = null, var statistics: Statistics? = null, var showBalance: Boolean = false, var bill: Bill? = null) : Dto() {

    fun getUserFullName() = name + " " + surname

    override fun equals(other: Any?): Boolean {
        if (id == null || other == null || other !is User || other.id == null) return false
        return other.id.equals(id)
    }
}

@Keep
data class Fund(var photo: String? = null, var link: String? = null, var mission: String? = null,
                var description: String? = null, var weight: Int = 0, var campaigns: List<Campaign> = ArrayList()) : User()

@Keep
open class Story(var title: String? = null,
                 var content: String? = null, var City: City? = null, var fund_name: String? = null, var fund: String? = null,
                 var author: User? = null, var commentsCount: Int = 0, var likesCount: Int = 0, var video: String? = null,
                 var mentions: Map<String, Int>? = null,
                 var collected: Int = 0) : Dto() {
    fun canEdit(): Boolean {
        return true
    }
}


// internal
@Keep
data class Bill(var Balance: Balance) : Dto()

@Keep
data class Balance(var output: String? = null) : Serializable

@Keep
data class Comment(
        var content: String? = null,
        var mentions: Map<String, Int>? = null,
        var author: User? = null) : Dto()

@Keep
open class Like(
        var cost: String? = null,
        var user: User? = null) : Dto()

@Keep
data class TryLike(
        var status: Int = 0, var result: String? = null, var url: String? = null) : Like()

@Keep
data class RelationUser(
        var user_id: String? = null, var type: Int? = null, var relation_user: User? = null) : Dto()

@Keep
data class Tag(var name: String? = null) : Dto()

@Keep
data class Ranking(var amount: String? = null, var user: User? = null)

@Keep
data class Campaign(var title: String? = null, var user_id: String? = null, var weight: Int = 0) : Dto()

@Keep
data class Relation(var type: Int, var opposite: Int) : Serializable

@Keep
data class City(
        var name: String? = null, var country_name: String? = null) : Dto()

@Keep
data class Group(var creator_id: String? = null, var deleted: Boolean = false,
                 var unreadMessagesCount: Int = 0, var members: List<User> = ArrayList(), var last_message: Message? = null) : Dto()

@Keep
data class Message(var user_id: String? = null,
                   var im_group_id: String? = null, var content: String? = null, var read: Any? = null, var deleted: Boolean = false,
                   var author: User? = null) : Dto()

@Keep
data class Notification(
        var type: Int? = null,
        var user_sender: User? = null,
        var read: String? = null,
        var like: Like? = null,
        var post: Story? = null,
        var comment: Comment? = null) : Dto()

@Keep
data class Card(var id: String? = null, var name: String? = null) : Serializable

@Keep
data class Statistics(var USER_SPENDING: Int? = null, var USER_COLLECTION: Int? = null, var FUND_DONATION: Int? = null) : Serializable

//wrappers

@Keep
data class Comments(var count: Int = 0, var comments: List<Comment> = ArrayList())

@Keep
data class Results<T>(var result: List<T> = ArrayList())

@Keep
data class SearchResult(var user: Results<UserWithBigCityWTF> = Results(), var hashtag: Results<Tag> = Results(), var fund: Results<Fund> = Results(), var post: Results<Story> = Results(), var trendings: Boolean = false) : Serializable

@Keep
open class BasicSearch : Serializable

@Keep
data class SearchPosts(var post: Results<Story> = Results()) : BasicSearch()

@Keep
data class Mentions(var mentions: List<User>)

@Keep
data class Followers(var followers: List<RelationUser>)

@Keep
data class Followings(var following: List<RelationUser>)

@Keep
data class Likes(var count: Int = 0, var likes: List<Like> = ArrayList())

@Keep
data class NotificationsCount(var count: Int = 0)

@Keep
data class GroupNotificationWrapper(var group: Group? = null, var notifications: List<Notification>? = null) : Serializable {
    fun timestamp(): Long {
        if (group != null) {
            return serverFormat.parse(group!!.last_message?.created?.split(".")?.get(0)).time
        }
        if (notifications != null && notifications!!.size > 0) {
            return serverFormat.parse(notifications!![0].created?.split(".")?.get(0)).time
        }
        return 0
    }
}

@Keep
open class StoryPayWrapper(var result: String? = null, var url: String? = null) : Story()


@Keep
data class UserFillWrapper(var result: String? = null, var url: String? = null) : User()

@Keep
data class GrouppedFund(var key: String? = null, var funds: ArrayList<Fund> = ArrayList()) : ParentObject {
    override fun setChildObjectList(p0: MutableList<Any>?) {
        funds.clear()
        funds.addAll(p0 as List<Fund>)
    }

    override fun getChildObjectList() = funds

}

@Keep
data class StoryInfo(var commentsCount: Int, var likesCount: Int, var collected: Int)

@Keep
data class PhoneUser(var fullname: String? = null, var avatar: String? = null, var phones: ArrayList<String> = ArrayList<String>())

@Keep
open class UserWithBigCityWTF(
        var name: String? = null, var surname: String? = null, var avatar_url: String? = null,
        var City: City? = null,
        var gender: Int? = null, var email: String? = null,
        var phone: String? = null, var phone_validated: String? = null,
        var ranking: Int? = null, var ranking_collected: Int? = null, var relation: Relation? = null, var paypal: String? = null,
        var followersCount: Int? = null, var followingCount: Int? = null, var statistics: Statistics? = null, var showBalance: Boolean = false, var bill: Bill? = null) : Dto() {
    fun getUserFullName() = name + " " + surname

    fun toUser(): User {
        val user = User(name, surname, avatar_url, City, gender, email, phone, phone_validated, ranking, ranking_collected, relation, paypal, followersCount, followingCount, null, statistics, showBalance, bill)
        user.id = id
        return user
    }

    override fun equals(other: Any?): Boolean {
        if (id == null || other == null || other !is User || other.id == null) return false
        return other.id.equals(id)
    }
}

@Keep
data class ContactsDto(var registredHashPhones: List<String>, var contactsInMP: List<UserWithBigCityWTF>) : ResponseDto()

