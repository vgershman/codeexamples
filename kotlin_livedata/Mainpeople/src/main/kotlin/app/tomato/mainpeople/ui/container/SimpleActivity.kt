package app.tomato.mainpeople.ui.container

import app.tomato.mainpeople.MyApp.Screens.Companion.SLIDER
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.activity_simple.*


/**
 * Created by user on 06.06.17.
 */
open class SimpleActivity : BottomSheetActivity() {
    override fun bottomView() = bottom_sheet

    override fun backerView() = backer
    override val contentViewId = R.layout.activity_simple

    override val defaultScreen = SLIDER

    override fun init() {
        super.init()
    }
}

class DarkActivity : SimpleActivity()