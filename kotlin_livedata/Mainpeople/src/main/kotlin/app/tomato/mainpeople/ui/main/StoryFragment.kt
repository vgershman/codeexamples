package app.tomato.mainpeople.ui.main

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DownloadManager
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CoordinatorLayout
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.data.UnitCallback
import app.tomato.basic.help.checkPermission
import app.tomato.basic.help.dpToPx
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.help.screenWidth
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.data.*
import app.tomato.mainpeople.ui.MentionsUtil
import app.tomato.mainpeople.ui.container.BottomSheetActivity
import app.tomato.mainpeople.ui.container.ImageViewerActivity
import com.bumptech.glide.Glide
import com.google.gson.internal.LinkedTreeMap
import com.mainpeople.android.R
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.fragment_story.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener


/**
 * Created by user on 14.08.17.
 */
class StoryFragment : BasicFragment() {


    var loadingFund = false
    var story: Story? = null
    var mute = true
    var mentions = HashMap<String, Int?>()
    var keyboardOpen = false


    override fun initViews() {
        view?.setOnClickListener { hideKeyboard(etMessage, context) }
        val size = screenWidth(context).toInt()
        var contentLP = header.layoutParams
        contentLP.height = size
        contentLP.width = size
        header.layoutParams = contentLP
        ivVideo.layoutParams = contentLP
        userContainer.setOnClickListener { openUser(story?.author) }
        btnClose.setOnClickListener { onBackPressed() }
        lv.layoutManager = LinearLayoutManager(context)
        etMessage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                btnSend.isEnabled = p0?.length!! > 0
            }
        })
        MentionsUtil.setup(etMessage, mentionsLv, mentions, story?.id)

        btnSend.isEnabled = false
        btnSend.setOnClickListener { sendComment() }
        btnMore.setOnClickListener { showStoryOptions() }

        KeyboardVisibilityEvent.setEventListener(activity) { isOpen ->
            keyboardOpen = isOpen
            if (!isOpen) {
                try {
                    mentionsLv.visibility = View.GONE
                } catch (ex: Exception) {

                }

            }
        }

    }


    private fun showStoryOptions() {
        (activity as BottomSheetActivity).bottomMenu(StoryOptionsAdapter(isPostMine(), story?.canEdit() == true, context, { onOptionSelected(it) }))
    }

    private fun isPostMine(): Boolean {
        if (story?.author?.id == null) {
            return false
        }
        return story?.author?.id.equals(AccountManager.value?.id)
    }

    private fun onOptionSelected(option: Int) {
        (activity as BottomSheetActivity).hideBottomMenu()
        when (option) {
            0 -> share()
            1 -> save()
            2 -> if (isPostMine()) {
                askForDelete()
            } else {
                report()
            }
            3 -> editPost()
        }
    }

    private fun editPost() {
        go(MyApp.Screens.EDIT_POST, MyApp.Containers.DARK, params = hashMapOf("story" to story), result = 39, from = this)
    }

    private fun askForDelete() {
        AlertDialog.Builder(context).setMessage(getString(R.string.alert_delete_post))
                .setNegativeButton(getString(R.string.alert_delete_post_cancel), { dialogInterface, _ -> dialogInterface.cancel() })
                .setPositiveButton(getString(R.string.alert_delete_post_ok), { dialogInterface, i -> dialogInterface.cancel(); delete() })
                .create().show()
    }

    private fun delete() {
        if (story?.id != null) {
            DeletedStoriesManager.addDeletedStory(story?.id!!)
            Rest.rest().deletePost(story?.id!!).enqueue(object : ToastCallback<LinkedTreeMap<String, Any?>>(context) {
                override fun onSuccess(response: LinkedTreeMap<String, Any?>) {
                    onBackPressed()
                }
            })
        }
    }

    private fun report() {
        Rest.rest().report(story?.id!!).enqueue(object : ToastCallback<LinkedTreeMap<String, Any?>>(context) {
            override fun onSuccess(response: LinkedTreeMap<String, Any?>) {
            }
        })
    }

    private fun save() {
        if (checkPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE, 77)) {
            var req: DownloadManager.Request
            if (story?.video != null) {
                req = DownloadManager.Request((Uri.parse(story?.video)))
            } else {
                req = DownloadManager.Request((Uri.parse(story?.content)))
            }
            req.setVisibleInDownloadsUi(true)
            req.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/Mainpeople" + "/" + story?.content)
            (context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager).enqueue(req)
        }
    }

    private fun share() {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.putExtra(Intent.EXTRA_TEXT, MyApp.Config.PROD_HOST + "/post/" + story?.id!!)
        shareIntent.type = "text/plain"
        startActivity(Intent.createChooser(shareIntent, getString(R.string.alert_share)))
    }


    private fun openUser(user: User?) {
        go(MyApp.Screens.USER, MyApp.Containers.SIMPLE, params = hashMapOf("user" to user))
    }

    fun support() {
        if (AccountManager.value?.phone_validated != null) {
            Payments.support(story!!, activity as BottomSheetActivity)
        } else {
            go(MyApp.Screens.INPUT_PHONE, MyApp.Containers.DARK, result = 34, from = this)
        }

    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        save()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 34 && resultCode == Activity.RESULT_OK) {
            support()
        }
        if (requestCode == 39) {
            try {
                Rest.rest().story(story?.id!!).enqueue(UnitCallback(context, {
                    story = it
                    bindStory()
                    (lv.adapter as CommentsAdapter).updateStory(story!!)
                }))
            } catch (ex: Exception) {

            }
        }
    }


    override fun onPause() {
        super.onPause()
        try {
            ivVideo.stopPlayback()
            ivVideo.release()
        } catch (ex: Exception) {

        }
    }


    private fun sendComment() {
        hideKeyboard(etMessage, context)
        val comment = etMessage.text.toString()
        Rest.rest().sendComment(story?.id!!, SendComment(comment, mentions)).enqueue(object : ToastCallback<Comment>(context) {
            override fun onSuccess(response: Comment) {
                loadData(true)
                etMessage.setText("")
                //lv.scrollToPosition(lv.adapter.itemCount - 1)

            }
        })
    }


    override fun onResume() {
        super.onResume()
        if (story?.video != null) {
            bindVideo(story?.video)
        }
    }

    override fun initProperties() {
        super.initProperties()
        story = arguments!!["story"] as Story
        var commentId = arguments!!["comment"] as String?
        lv.adapter = CommentsAdapter(story!!,
                { openUser(it) },
                { support() },
                { searchTag(it) },
                { openMention(it) },
                { answer(it) })
        StoriesManager.observe(this, Observer { bindStory(); lv.adapter.notifyDataSetChanged() })
        val size = screenWidth(context).toInt()
        Glide.with(context).load(story?.content).placeholder(R.drawable.placeholder).override(size, size).into(header)
        // Picasso.with(context).load(story?.content).placeholder(R.drawable.placeholder).resize(size, 0).into(header)
        header.setOnClickListener {
            if (!keyboardOpen) {
                openFullscreen(story?.content)
            } else {
                hideKeyboard(etMessage, context)
            }
        }
        tvFund.text = story?.fund_name
        tvFund.setOnClickListener { openFund(story?.fund!!) }


        if (story?.video != null) {
            bindVideo(story?.video)
        }
        loadData(commentId != null)
    }

    private fun answer(it: User?) {
        MentionsUtil.proceedSelectMention(it!!, etMessage, mentionsLv, mentions)
    }

    private fun openMention(id: Int?) {
        if (id != null) {
            Rest.rest().user(id.toString()).enqueue(UnitCallback(context, {
                openUser(it)
            }))
        }
    }


    private fun openFullscreen(content: String?) {
        if (content != null) {
            val intent = Intent(context, ImageViewerActivity::class.java)
            intent.putExtra(ImageViewerActivity.EXTRA_URL, content)
            context.startActivity(intent)
            activity?.overridePendingTransition(R.anim.anim_open, R.anim.anim_close)
        }
    }


    private fun openFund(it: String) {
        if (loadingFund) return
        loadingFund = true
        Rest.rest().fund(it).enqueue(object : ToastCallback<Fund>(context) {

            override fun onSuccess(response: Fund) {
                go(MyApp.Screens.FUND, MyApp.Containers.SIMPLE, params = hashMapOf("fund" to response))
                loadingFund = false
            }

            override fun onError(error: Error?) {
                super.onError(error)
                loadingFund = false
            }

            override fun onConnectionFailure() {
                super.onConnectionFailure()
                loadingFund = false
            }
        })
    }

    private fun searchTag(it: String) {
        go(MyApp.Screens.MODAL_SEARCH, MyApp.Containers.SIMPLE, params = hashMapOf("query" to "#" + it))
    }


    private fun bindStory() {
        try {
            if (story != null) {
                val collected = StoriesManager.get(story!!.id!!)?.collected ?: story?.collected
                Picasso.get().load(story?.author?.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
                if (story?.City?.name != null) {
                    tvPostDateTime.text = story?.City?.name + ", " + story?.getCreatedNice()
                } else {
                    tvPostDateTime.text = story?.getCreatedNice()
                }
                btnPay.text = "$ " + collected.toString()
                btnPayContainer.setOnClickListener { go(MyApp.Screens.SUPPORTERS, MyApp.Containers.SIMPLE, params = hashMapOf("story" to story)) }
                tvAuthorName.text = story?.author?.getUserFullName()
            }
        } catch (ex: Exception) {

        }
    }

    private fun bindVideo(video: String?) {
        header.visibility = View.INVISIBLE
        ivVideo.visibility = View.VISIBLE
        ivVideo.setOnClickListener {
            if (!mute) {
                ivVideo.setVolume(0f)
            } else {
                ivVideo.setVolume(0.6f)
            }
            mute = !mute
        }
        ivVideo.setVolume(0f)
        ivVideo.setOnPreparedListener { ivVideo.start() }
        ivVideo.setVideoURI(Uri.parse(video))
    }

    private fun loadData(force: Boolean = false) {
        StoriesManager.updateStory(context, story?.id!!)
        Rest.rest().listComments(story?.id!!).enqueue(object : ToastCallback<Comments>(context) {
            override fun onSuccess(response: Comments) {
                try {
                    (lv.adapter as CommentsAdapter).setData(response.comments)
                    if (force) {
                        val params = appbar.layoutParams as CoordinatorLayout.LayoutParams
                        (params.behavior as AppBarLayout.Behavior?)?.topAndBottomOffset = -(lv.height - screenWidth(context).toInt() + dpToPx(context, 120).toInt())
                        (lv.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(lv.adapter.itemCount - 1, 0)
                    }
                } catch (ex: Exception) {
                }

            }
        })
    }


    override val containerView = R.layout.fragment_story

}