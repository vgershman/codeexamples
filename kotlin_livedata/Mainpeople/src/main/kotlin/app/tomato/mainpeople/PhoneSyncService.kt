package app.tomato.mainpeople

import android.app.IntentService
import android.content.Intent
import android.os.AsyncTask
import app.tomato.basic.data.UnitCallback
import app.tomato.mainpeople.data.PhoneManager
import app.tomato.mainpeople.data.PhoneUser
import app.tomato.mainpeople.data.Rest
import com.google.common.hash.Hashing
import java.nio.charset.Charset
import java.util.*

/**
 * Created by user on 11.09.17.
 */
class PhoneSyncService : IntentService("PhoneSync") {


    override fun onHandleIntent(p0: Intent?) {
        tryGetPhones()
        getMyPhones()
    }

    private fun tryGetPhones() {

    }


    private fun getMyPhones() {
        object : AsyncTask<Void, Void, Unit>() {

            val contacts = ArrayList<PhoneUser>()

            override fun doInBackground(vararg p0: Void?) {
                contacts.clear()
                contacts.addAll(PhoneManager.getPhones(this@PhoneSyncService))
            }

            override fun onPostExecute(result: Unit?) {
                super.onPostExecute(result)
                sendMyPhones(contacts)
            }
        }.execute()
    }

    private fun sendMyPhones(contacts: ArrayList<PhoneUser>) {
        var phoneHashes = ArrayList<String>()
        contacts.forEach { it.phones.forEach { phoneHashes.add(hashPhone(it)) } }
        Rest.rest().contacts(phoneHashes).enqueue(UnitCallback(this, {}))
    }

    private fun hashPhone(phone: String?): String {
        return Hashing.sha1().hashString(phone, Charset.defaultCharset()).toString()
    }
}