package app.tomato.mainpeople.ui.welcome

import android.Manifest
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.pm.PackageManager
import android.view.WindowManager
import app.tomato.basic.data.AlertCallback
import app.tomato.basic.help.checkPermission
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.FcmManager
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.PhoneSyncService
import com.mainpeople.android.R
import app.tomato.mainpeople.data.*
import app.tomato.mainpeople.ui.media.CameraPreview
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_media_selector.*

/**
 * Created by user on 14.08.17.
 */
class LoginFragment : BasicFragment() {


    private var alreadyOpen = false

    private var pushToken: String? = null

    override fun initViews() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        btnBack.setOnClickListener { onBackPressed() }
        btnRegister.setOnClickListener { go(MyApp.Screens.REGISTRATION, pop = true) }
        btnLogin.setOnClickListener { validate() }
        btnForget.setOnClickListener { go(MyApp.Screens.FORG0T, MyApp.Containers.SIMPLE, params = hashMapOf("email" to etEmail.text.toString())) }
        AccountManager.observe(this, Observer { proceed(it) })
        FcmManager.observe(this, Observer { pushToken = it })
    }

    private fun proceed(user: User?) {
        if (user != null && !alreadyOpen) {
            alreadyOpen = true
            LocalizationManager.init(context)
            proceedPhones()

        }
    }

    private fun proceedPhones() {
        if (checkPermission(activity, arrayOf(Manifest.permission.READ_CONTACTS), 2)) {
            activity?.startService(Intent(activity, PhoneSyncService::class.java))
            go(null, MyApp.Containers.TABBED, finish = true)
        }
    }


    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on [.requestPermissions].
     *
     *
     * **Note:** It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     *

     * @param requestCode The request code passed in [.requestPermissions].
     * *
     * @param permissions The requested permissions. Never null.
     * *
     * @param grantResults The grant results for the corresponding permissions
     * *     which is either [android.content.pm.PackageManager.PERMISSION_GRANTED]
     * *     or [android.content.pm.PackageManager.PERMISSION_DENIED]. Never null.
     * *
     * *
     * @see .requestPermissions
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        try {
            if (requestCode == 2 && grantResults[0].equals(PackageManager.PERMISSION_GRANTED)) {
                proceedPhones()
            } else {
                go(null, MyApp.Containers.TABBED, finish = true)
            }
        } catch (ex: Exception) {
            go(null, MyApp.Containers.TABBED, finish = true)
        }
    }

    private fun validate() {
        hideKeyboard(etEmail, context)
        val email = etEmail.text.toString()
        val password = etPassword.text.toString()
        AccountManager.login(context, email, password, pushToken)
    }

    override val containerView = R.layout.fragment_login
}