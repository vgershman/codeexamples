package app.tomato.mainpeople.ui.welcome

import android.arch.lifecycle.Observer
import android.content.Intent
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.basic.ui.basic.TabsAdapter
import app.tomato.mainpeople.FcmManager
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.AccountManager
import app.tomato.mainpeople.data.AuthInfo
import app.tomato.mainpeople.data.AuthManager
import app.tomato.mainpeople.data.LocalizationManager
import app.tomato.mainpeople.ui.container.TabbedActivity
import kotlinx.android.synthetic.main.fragment_splash.*

class SliderFragment : BasicFragment() {

    override fun initViews() {
        AuthManager.init(context)
//        FcmManager.init(context)
        AuthManager.observe(this, Observer { proceed(it) })
    }

    private fun proceed(authInfo: AuthInfo?) {
        if (authInfo != null && authInfo.isAuth()) {
            LocalizationManager.init(context)
            val intent = Intent(context, TabbedActivity::class.java)
            intent.putExtras(activity?.intent)
            startActivity(intent)
            activity?.finish()
            //go(null, MyApp.Containers.TABBED, finish = true)
        } else {
            vpSlider.adapter = TabsAdapter(childFragmentManager, screens = arrayOf(Slide1Fragment::class.java, Slide2Fragment::class.java))
            ciSlider.setViewPager(vpSlider)
            btnSkip.setOnClickListener { go(screen = MyApp.Screens.WELCOME) }
        }
        AuthManager.removeObserver { this }
    }

    override val containerView = R.layout.fragment_splash
}

class Slide1Fragment : BasicFragment() {
    override fun initViews() {
    }

    override val containerView = R.layout.fragment_slide1
}

class Slide2Fragment : BasicFragment() {
    override fun initViews() {
    }

    override val containerView = R.layout.fragment_slide2
}