package app.tomato.mainpeople.ui.search

import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.data.UnitCallback
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.MyApp.Containers.Companion.SIMPLE
import app.tomato.mainpeople.MyApp.Screens.Companion.FUNDS
import app.tomato.mainpeople.MyApp.Screens.Companion.STORY
import app.tomato.mainpeople.data.*
import app.tomato.mainpeople.ui.container.BottomSheetActivity
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.fragment_search.*
import java.util.*

/**
 * Created by user on 14.08.17.
 */
open class SearchFragment : BasicFragment() {

    var loadingFund = false

    private var timer: Timer? = null

    private var trendsAdapter = TrendsAdapter({ openFunds() }, { proceedUsers(it) }, { proceedTag(it) }, { openStory(it!!) }, { openFund(it!!) }, { openLikes(it) }, { like(it) }, { proceedStringTag(it) })

    private fun like(it: Story?) {
        Payments.support(it!!, activity as BottomSheetActivity)
    }

    private fun openLikes(it: Story?) {
        go(MyApp.Screens.SUPPORTERS, SIMPLE, params = hashMapOf("story" to it))
    }

    private var searchAdapter = SearchAdapter({ openFund(it) }, { proceedUsers(it) }, { proceedTag(it) }, { openStory(it) }, { openLikes(it) }, { like(it) }, { proceedStringTag(it) }, {proceedMention(it)})

    private fun proceedMention(id: Int?) {
        if (id != null) {
            Rest.rest().user(id.toString()).enqueue(UnitCallback(context, {
                proceedUsers(it)
            }))
        }
    }


    private fun openFund(it: String) {
        if (loadingFund) return
        loadingFund = true
        Rest.rest().fund(it).enqueue(object : ToastCallback<Fund>(context) {

            override fun onSuccess(response: Fund) {
                go(MyApp.Screens.FUND, MyApp.Containers.SIMPLE, params = hashMapOf("fund" to response))
                loadingFund = false
            }

            override fun onError(error: Error?) {
                super.onError(error)
                loadingFund = false
            }

            override fun onConnectionFailure() {
                super.onConnectionFailure()
                loadingFund = false
            }
        })
    }


    private fun openStory(it: Story) {
        go(STORY, SIMPLE, params = hashMapOf("story" to it))
    }

    override fun initViews() {
        btnClear.setOnClickListener { etSearch.setText(""); hideKeyboard(etSearch, context) }
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.length!! > 0) {
                    btnClear.visibility = View.VISIBLE
                    scheduleQuery(p0.toString())
                } else {
                    btnClear.visibility = View.GONE
                    searchTrends()
                }

            }

        })
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = trendsAdapter//TrendsAdapter({ openFunds() }, { proceedUsers(it) }, { proceedTag(it) })
        searchTrends()
    }

    private fun proceedStringTag(tag: String) {
        hideKeyboard(etSearch, context)
        etSearch.setText("#" + tag)
    }

    private fun proceedTag(tag: Tag?) {
        hideKeyboard(etSearch, context)
        if (tag == null) {
            return
        }
        etSearch.setText(tag.name)
    }


    private fun proceedUsers(user: User?) {
        if (user != null) {
            go(MyApp.Screens.USER, SIMPLE, params = hashMapOf("user" to user))
        } else {
            // go(MyApp.Screens.PEOPLE, MyApp.Containers.SIMPLE, params = hashMapOf("users" to data?.user))
        }
    }

    private fun openFunds() {
        go(FUNDS, SIMPLE)
    }


    private fun scheduleQuery(query: String) {
        if (timer != null) {
            timer!!.cancel()
        }
        timer = kotlin.concurrent.timer("searcher", false, initialDelay = 500L, period = 10000, action = {
            try {
                activity?.runOnUiThread {
                    try {
                        search(query)
                    } catch (ex: Exception) {
                    }
                }; timer?.cancel()
            } catch (ex: Exception) {
            }
        })

    }

    private fun search(query: String) {
        if (query.isEmpty()) {
            searchTrends()
        } else {
            hideKeyboard(etSearch, context)
            Rest.rest().search(query).enqueue(object : ToastCallback<SearchResult>(context) {
                override fun onSuccess(response: SearchResult) {
                    searchAdapter.setData(response)
                    rv.adapter = searchAdapter
                }
            })
        }
    }

    private fun searchTrends() {
        rv.adapter = trendsAdapter
        Rest.rest().trends().enqueue(object : ToastCallback<SearchResult>(context) {
            override fun onSuccess(response: SearchResult) {
                trendsAdapter.setData(response)
            }
        })
    }


    override val containerView = R.layout.fragment_search

}