package app.tomato.mainpeople.ui.messages

import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.MyApp.Containers.Companion.SIMPLE
import app.tomato.mainpeople.MyApp.Screens.Companion.USER
import com.mainpeople.android.R
import app.tomato.mainpeople.data.*
import kotlinx.android.synthetic.main.fragment_chat_list.*
import java.util.*

/**
 * Created by user on 14.08.17.
 */
class ChatFragment : BasicFragment() {

    var data = ArrayList<Message>()
    var lastVisiblePosition = 0
    var timer: Timer? = null
    var previousCount = 0

    var groupId: String? = null
    var user: User? = null

    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        rv.layoutManager = LinearLayoutManager(context)
        view?.setOnClickListener { hideKeyboard(etMessage, context) }
        rv.adapter = ChatAdapter({ openUser(it) }, { proceedHashtag(it) })
        etMessage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                btnSend.isEnabled = p0?.length!! > 0
            }
        })

        btnSend.isEnabled = false
        btnSend.setOnClickListener { sendMessage() }
    }

    private fun proceedHashtag(tag: String) {
        go(MyApp.Screens.MODAL_SEARCH, MyApp.Containers.SIMPLE, params = hashMapOf("query" to "#" + tag))
    }

    private fun openUser(user: User) {
        go(USER, SIMPLE, params = hashMapOf("user" to user))
    }

    override fun initProperties() {
        super.initProperties()
        groupId = (arguments!!["group"] as Group?)?.id
        user = arguments!!["user"] as User
//        if (group != null) {
//            tvToolbar.text = group!!.members[0].getUserFullName()
//            loadData()
//        }
        tvToolbar.text = user!!.getUserFullName()

    }

    override fun onResume() {
        super.onResume()
        setUpTimer()
    }

    private fun setUpTimer() {
        timer = kotlin.concurrent.timer("searcher", false, initialDelay = 0, period = 5000, action = {
            try {
                activity?.runOnUiThread {
                    try {
                        loadData()
                    } catch (ex: Exception) {
                    }
                }
            } catch (ex: Exception) {
            }
        })
    }


    override fun onPause() {
        super.onPause()
        if (timer != null) {
            timer!!.cancel()
        }
        markLastVisibleRead()
    }

    private fun markLastVisibleRead() {
        try {
            if (lastVisiblePosition >= data.size) {
                lastVisiblePosition = data.size - 1
            }
            val message = data[lastVisiblePosition]
            NotificationsManager.read(context, message)
        } catch (ex: Exception) {
        }
    }


    private fun sendMessage() {
        hideKeyboard(etMessage, context)
        val message = etMessage.text.toString()
        Rest.rest().sendMessage(user!!.id!!, message).enqueue(object : ToastCallback<Message>(context) {
            override fun onSuccess(response: Message) {
                groupId = response.im_group_id
                loadData()
                etMessage.setText("")
                rv.smoothScrollToPosition(rv.adapter.itemCount - 1)

            }
        })
    }

    private fun loadData() {
        if (groupId != null) {
            Rest.rest().listMessages(groupId!!).enqueue(object : ToastCallback<List<Message>>(context) {
                override fun onSuccess(response: List<Message>) {
                    if (response.size > previousCount) {
                        previousCount = data.size
                        if (response.size > 0) {
                            NotificationsManager.read(context, response[0])
                        }
                        data.clear()
                        data.addAll(response.reversed())
                        lastVisiblePosition = data.size
                        (rv.adapter as ChatAdapter).setData(data)
                        (rv.scrollToPosition(data.size))
                    }
                }

            })
        }
    }

    override val containerView = R.layout.fragment_chat_list

}