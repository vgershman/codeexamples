package app.tomato.mainpeople.ui.search

import android.support.v7.widget.LinearLayoutManager
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Fund
import app.tomato.mainpeople.data.Rest
import kotlinx.android.synthetic.main.fragment_funds_list.*

/**
 * Created by user on 14.08.17.
 */
class FundsFragment : BasicFragment() {
    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = FundsAdapter({ go(MyApp.Screens.FUND, params = hashMapOf("fund" to it)) })
        loadFunds()
    }

    private fun loadFunds() {
        Rest.rest().listFunds().enqueue(object : ToastCallback<List<Fund>>(context) {
            override fun onSuccess(response: List<Fund>) {
                (rv.adapter as FundsAdapter).setData(response)
            }

        })
    }

    override val containerView = R.layout.fragment_funds_list

}