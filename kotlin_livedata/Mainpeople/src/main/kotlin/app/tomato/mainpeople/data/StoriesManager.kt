package app.tomato.mainpeople.data

import android.arch.lifecycle.LiveData
import android.content.Context
import app.tomato.basic.data.ToastCallback

/**
 * Created by user on 31.08.17.
 */
object StoriesManager : LiveData<HashMap<String, StoryInfo>>() {


    fun addStories(stories: List<Story>) {
        var oldValue = value
        if (oldValue == null) {
            oldValue = HashMap<String, StoryInfo>()
        }
        stories.forEach {
            var storyInfo = StoryInfo(it.commentsCount, it.likesCount, it.collected)
            if (!oldValue!!.containsKey(it.id)) {
                oldValue!!.put(it.id!!, storyInfo)
            } else {
                oldValue!![it.id!!] = storyInfo
            }
            value = oldValue
        }
    }

    fun get(key: String) = value?.get(key)

    fun updateStory(context: Context, storyId: String) {
        Rest.rest().story(storyId).enqueue(object : ToastCallback<Story>(context) {
            override fun onSuccess(response: Story) {
                addStories(arrayListOf(response))
            }
        })
    }

}