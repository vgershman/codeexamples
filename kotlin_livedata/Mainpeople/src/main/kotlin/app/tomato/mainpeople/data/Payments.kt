package app.tomato.mainpeople.data

import android.app.AlertDialog
import android.app.ProgressDialog
import android.preference.PreferenceManager
import app.tomato.basic.data.AlertCallback
import app.tomato.basic.data.ToastCallback
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.ui.container.BottomSheetActivity
import app.tomato.mainpeople.ui.payments.SelectCardAdapter
import app.tomato.mainpeople.ui.payments.SelectCostAdapter
import com.mainpeople.android.R
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File

/**
 * Created by user on 22.08.17.
 */
object Payments {

    val WEB_REQUEST_CODE = 69

    var mainCallback: (() -> Unit)? = null

    var activity: BottomSheetActivity? = null

    fun fillAccount(activity: BottomSheetActivity) {
        this.activity = activity
        activity.webRequestCode = WEB_REQUEST_CODE
        selectFillAmount { AccountManager.loadUser(activity) }
    }

    fun support(story: Story, activity: BottomSheetActivity) {
        this.activity = activity
        activity.webRequestCode = WEB_REQUEST_CODE
        selectCostFor(story)
    }

    fun createPost(activity: BottomSheetActivity, mentions: HashMap<String, Int?>, fund: Fund, cost: Int, title: String, image: String, type: Int, resultCallback: () -> Unit) {
        GPayUtil.setup(activity)
        this.activity = activity
        mainCallback = resultCallback
        activity.webRequestCode = WEB_REQUEST_CODE
        tryToCreatePost(mentions, fund, cost, title, image, type)
    }

    private fun tryToCreatePost(mentions: HashMap<String, Int?>, fund: Fund, cost: Int, title: String, image: String, type: Int) {
        var titleBody = RequestBody.create(MediaType.parse("text/plain"), title)
        var mediaBody: RequestBody? = null
        if (type == 1) {
            mediaBody = RequestBody.create(MediaType.parse("image/jpeg"), File(image))
        }
        if (type == 3) {
            mediaBody = RequestBody.create(MediaType.parse("video/mp4"), File(image))
        }
        val progress = ProgressDialog.show(activity, "", activity?.getString(R.string.alert_loading), true, false)

        var callback = object : AlertCallback<StoryPayWrapper>(activity!!) {
            override fun onSuccess(response: StoryPayWrapper) {
                progress.cancel()
                if (response.url == null) {
                    mainCallback?.invoke()
                } else {
                    loadCards { webPay(response.url!!, fund.name, it, { mainCallback?.invoke() }) }
                }
            }

            override fun onError(error: Error?) {
                progress.cancel()
                var message = LocalizationManager.getError(error?.errCode) ?: error?.errCode
                AlertDialog.Builder(context).setMessage(message)
                        .setNegativeButton(context.getString(R.string.alert_payment_cancel), { dialogInterface, _ -> dialogInterface.cancel() })
                        .setPositiveButton(context.getString(R.string.alert_payment_fill), { dialogInterface, _ ->
                            dialogInterface.cancel()
                            selectFillAmount { tryToCreatePost(mentions, fund, cost, title, image, type) }
                        })
                        .create().show()

            }

            override fun onConnectionFailure() {
                super.onConnectionFailure()
                progress.cancel()
            }
        }

        if (type == 1) {
            Rest.rest().createPost(titleBody, type, mentions, cost, fund.id!!.toInt(), mediaBody).enqueue(callback)
        }

        if (type == 3) {
            Rest.rest().createVideoPost(titleBody, type, mentions, cost, fund.id!!.toInt(), mediaBody).enqueue(callback)
        }
    }

    private fun selectCostFor(story: Story) = activity?.bottomMenu(SelectCostAdapter(MyApp.Config.SUPPORT_COST, { onSelectCost(story, it) }))

    private fun onSelectCost(story: Story, cost: String) {
        activity?.hideBottomMenu()
        tryToLike(story, cost)
    }

    private fun tryToLike(story: Story, cost: String): Unit = Rest.rest().like(story.id!!, cost.toInt()).enqueue(object : ToastCallback<TryLike>(activity!!) {
        override fun onSuccess(response: TryLike) {
            if (response.status == 1) {
                StoriesManager.updateStory(context, story.id!!)
                AccountManager.loadUser(context)
                return
            }
            if (response.status == 0 && response.url != null) {
                loadCards { webPay(response.url!!, story.fund_name, it, { StoriesManager.updateStory(context, story.id!!) }) }
            }
        }

        override fun onError(error: Error?) {
            var message = LocalizationManager.getError(error?.errCode) ?: error?.errCode
            AlertDialog.Builder(context).setMessage(message)
                    .setNegativeButton(context.getString(R.string.alert_payment_cancel), { dialogInterface, _ -> dialogInterface.cancel() })
                    .setPositiveButton(context.getString(R.string.alert_payment_fill), { dialogInterface, _ ->
                        dialogInterface.cancel()
                        selectFillAmount { tryToLike(story, cost) }
                    })
                    .create().show()
        }
    })

    private fun selectFillAmount(callback: () -> Unit) {
        activity?.bottomMenu(SelectCostAdapter(MyApp.Config.FILL_AMOUNTS, { onSelectFillAmount(it, callback) }))
    }

    private fun onSelectFillAmount(amount: String, callback: () -> Unit) {
        loadCards { tryToFill(amount, it, callback) }
    }

    private fun tryToFill(amount: String, card: Card?, callback: () -> Unit) {
        Rest.rest().fillAccount(amount, card?.id).enqueue(object : ToastCallback<UserFillWrapper>(activity!!) {

            override fun onSuccess(response: UserFillWrapper) {
                AccountManager.loadUser(context)
                if (response.result == null && response.url == null) {
                    callback()
                }
                if (response.url != null) {
                    webPay(response.url!!, null, card, callback)
                }
            }
        })
    }

    private fun loadCards(callback: (card: Card?) -> Unit) = Rest.rest().listCards().enqueue(object : ToastCallback<List<Card>>(activity!!) {
        override fun onSuccess(response: List<Card>) = proceedCards(response, callback)
    })

    private fun proceedCards(response: List<Card>, callback: (card: Card?) -> Unit) {
        if (response.size == 0 && AccountManager.value?.paypal == null) {
            callback(null)
        } else {
            selectPaymentMethod(AccountManager.value?.paypal, response, callback)
        }
    }

    private fun selectPaymentMethod(paypal: String?, response: List<Card>, callback: (card: Card?) -> Unit) {
        var responseCards = ArrayList<Card>()
        responseCards.addAll(response)
        var cards = ArrayList<Card>()
        if (activity != null) {
            val lastCardId = PreferenceManager.getDefaultSharedPreferences(activity).getString("last_card", null)
            var lastCard: Card? = null
            if (lastCardId != null) {
                lastCard = cards.find { it.id.equals(lastCardId) }
            }
            if (lastCard != null) {
                cards.add(lastCard)
                responseCards.remove(lastCard)
            }
        }
        cards.addAll(responseCards)
        if (cards.find { it.name.equals("gpay", true) } == null) {
            cards.add(Card("gpay", activity?.getString(R.string.gpay)))
        }
        if (cards.find { it.name.equals("paypal", true) } == null) {
            cards.add(Card(paypal, activity?.getString(R.string.paypal)))
        }

        activity?.bottomMenu(SelectCardAdapter(cards, { onSelectCard(it, callback) }))
    }

    private fun onSelectCard(card: Card?, callback: (card: Card?) -> Unit) {
        if (activity != null) {
            PreferenceManager.getDefaultSharedPreferences(activity).edit().putString("last_card", card?.id).apply()
        }
        activity?.hideBottomMenu()
        callback(card)
    }

    private fun webPay(url: String, fund: String?, card: Card?, callback: () -> Unit) {
        activity?.afterWebCallback = { callback() }
        if (card?.id.equals("gpay")) {
            GPayUtil.pay(10, activity!!)
            return
        }

        var url_ = url
        if (card != null) {
            url_ += "&card_id=" + card.id
            url_ += "&card_name=" + card.name
        }
        if (fund != null) {
            url_ += "&fund_name=" + fund
        }
        activity?.go(MyApp.Screens.WEB, MyApp.Containers.SIMPLE, result = WEB_REQUEST_CODE, params = hashMapOf("url" to url_))
    }


}