package app.tomato.mainpeople.ui.welcome

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import app.tomato.basic.help._TextWatcher
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import kotlinx.android.synthetic.main.fragment_register.*


/**
 * Created by user on 14.08.17.
 */
class RegistrationFragment : BasicFragment() {


    private var name: String? = null
    private var surname: String? = null
    private var sex: Int? = null
    private var email: String? = null
    private var placeId: String? = null

    val PLACE_AUTOCOMPLETE_REQUEST_CODE = 37

    override fun initViews() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        btnBack.setOnClickListener { onBackPressed() }
        val adapter = ArrayAdapter.createFromResource(activity!!, R.array.sex_array, R.layout.item_sex_selection)
        adapter.setDropDownViewResource(R.layout.item_sex_drop)
        spinnerSex.adapter = adapter
        spinnerSex.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                sex = Math.abs(1 - p2)
            }

        }
        btnLogin.setOnClickListener { go(MyApp.Screens.LOGIN, pop = true) }
        btnDone.setOnClickListener { next() }
        setUpCity()
    }

    private fun next() {
        hideKeyboard(etName, context)
        if (validate()) {
            go(MyApp.Screens.PASSWORD, params = hashMapOf("city" to placeId, "name" to name, "surname" to surname, "sex" to sex, "email" to email))
        }
    }

    private fun validate(): Boolean {
        name = etName.text.toString()
        if (name!!.replace(Regex.fromLiteral(" "), "").length < 2) {
            Toast.makeText(context, R.string.error_short_name, Toast.LENGTH_SHORT).show()
            return false
        }
        surname = etLastName.text.toString()
        if (surname!!.replace(Regex.fromLiteral(" "), "").length < 2) {
            Toast.makeText(context, R.string.error_surname_short, Toast.LENGTH_SHORT).show()
            return false
        }
        email = etEmail.text.toString()
        if (!isValidEmail(email!!)) {
            Toast.makeText(context, R.string.error_wrong_email, Toast.LENGTH_SHORT).show()
            return false
        }
        if (placeId == null) {
            Toast.makeText(context, getString(R.string.error_city), Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    private fun setUpCity() {
        btnCity_.setOnClickListener { openPlaceApi() }
        etCity.addTextChangedListener(object : _TextWatcher {
            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.length > 0) {
                    deleteCity.visibility = View.VISIBLE
                } else {
                    deleteCity.visibility = View.GONE
                }
            }

        })
        deleteCity.setOnClickListener { placeId = null; etCity.setText("") }
    }

    fun isValidEmail(target: String): Boolean {
        if (TextUtils.isEmpty(target)) {
            return false
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

    private fun openPlaceApi() {
        val typeFilter = AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build()



        try {
            var intentBuilder = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
            intentBuilder.zzh(etCity.text.toString())
            intentBuilder.setFilter(typeFilter)

            startActivityForResult(intentBuilder.build(activity), PLACE_AUTOCOMPLETE_REQUEST_CODE)
        } catch (e: Exception) {
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode === RESULT_OK) {
                val place = PlaceAutocomplete.getPlace(context, data)
                etCity.setText(place.name)
                placeId = place.id
            }
        }
    }

    override val containerView = R.layout.fragment_register

}