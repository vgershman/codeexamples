package app.tomato.mainpeople.ui.settings

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.preference.PreferenceManager
import android.view.View
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.AccountManager
import app.tomato.mainpeople.data.AuthManager
import app.tomato.mainpeople.data.Rest
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.fragment_settings.*

/**
 * Created by user on 14.08.17.
 */
class SettingsFragment : BasicFragment() {

    val PLACE_AUTOCOMPLETE_REQUEST_CODE = 37


    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        btnEditCity.setOnClickListener { openPlaceApi() }
        btnEditProfile.setOnClickListener { go(MyApp.Screens.EDIT_ME, MyApp.Containers.DARK) }
        btnPhoto.setOnClickListener { go(MyApp.Screens.SELECT_MEDIA, MyApp.Containers.SIMPLE, result = 54, from = this) }
        btnPayment.setOnClickListener { go(MyApp.Screens.CARDS) }
        btnLogout.setOnClickListener { AuthManager.logout(activity!!) }
        btnFeedback.setOnClickListener { go(MyApp.Screens.FEEDBACK, MyApp.Containers.DARK) }
        btnInvite.setOnClickListener { go(MyApp.Screens.INVITE, MyApp.Containers.SIMPLE) }
        AccountManager.observe(this, Observer { bindProfile() })
        bindHost()
    }

    private fun bindHost() {
        if (AccountManager.value?.permissions != null) {
            btnHost.visibility = View.VISIBLE
        }
        var host = PreferenceManager.getDefaultSharedPreferences(context).getString("host", MyApp.Config.PROD_HOST)
        btnHost.text = host
        btnHost.setOnClickListener {
            if (host.equals(MyApp.Config.HOST)) {
                host = MyApp.Config.PROD_HOST
            } else {
                host.equals(MyApp.Config.PROD_HOST)
                host = MyApp.Config.HOST
            }
            Rest.setNewHost(host)
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString("host", host).apply()
            bindHost()
        }

    }

    private fun openPlaceApi() {
        val typeFilter = AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build()

        try {
            var intentBuilder = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
            intentBuilder.zzh(tvCity.text.toString())
            intentBuilder.setFilter(typeFilter)
            startActivityForResult(intentBuilder.build(activity), PLACE_AUTOCOMPLETE_REQUEST_CODE)
        } catch (e: Exception) {
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode === Activity.RESULT_OK) {
                val place = PlaceAutocomplete.getPlace(context, data)
                tvCity.text = place.name
                AccountManager.updateCity(context, place.id)
            }
        }
        if (requestCode == 54 && resultCode == Activity.RESULT_OK && data != null) {
            val media = data.getStringExtra("media")
            if (media != null) {
                AccountManager.uploadAvatar(context, media)
            }
        }
    }

    private fun bindProfile() {
        if (AccountManager.value != null) {
            Picasso.get().load(AccountManager.value?.avatar_url).placeholder(R.drawable.ic_launcher).into(ivAvatarBig)
            Picasso.get().load(AccountManager.value?.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatarSmall)
            tvUserName.text = AccountManager.value?.getUserFullName() //AccountManager.value?.name + " " + AccountManager.value?.surname
            tvCity.text = AccountManager.value?.city?.name
        }
    }

    override val containerView = R.layout.fragment_settings

}