package app.tomato.mainpeople.ui.main

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.data.*
import com.mainpeople.android.R
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.fragment_story_supporters.*

/**
 * Created by user on 14.08.17.
 */
class StorySupportersFragment : BasicFragment() {

    private var story: Story? = null

    override fun initViews() {
        ivAvatar.setOnClickListener { openUser(story?.author!!) }
        btnClose.setOnClickListener { onBackPressed() }
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = ContributorsAdapter({ openUser(it) })
    }


    private fun openUser(user: User?) {
        go(MyApp.Screens.USER, MyApp.Containers.SIMPLE, params = hashMapOf("user" to user))
    }


    override fun initProperties() {
        super.initProperties()
        story = arguments!!["story"] as Story
        bindStory(-1)
        loadData()
    }

    private fun bindStory(likesSum: Int) {
        val likesCount = StoriesManager.get(story?.id!!)?.likesCount ?: story?.likesCount ?: 0
        tvContributorsCount.text = likesCount.toString() + " " + context.getString(R.string.lbl_contributors)
        Picasso.get().load(story?.author?.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
        tvPostDateTime.text = story?.getCreatedNice()
        if (likesSum == -1) {
            btnPay.visibility = View.INVISIBLE
        } else {
            btnPay.visibility = View.VISIBLE
        }
        val collected = StoriesManager.get(story?.id!!)?.collected ?: story?.collected ?: 0
        btnPay.text = "$ " + (collected - likesSum).toString()
        tvAuthorName.text = story?.author?.getUserFullName()
    }

    private fun loadData() {
        Rest.rest().story(story?.id!!).enqueue(object : ToastCallback<Story>(context) {
            override fun onSuccess(response: Story) {
                StoriesManager.addStories(arrayListOf(response))
                loadDonations()
            }
        })
    }

    private fun loadDonations() {
        Rest.rest().listContributors(story?.id!!).enqueue(object : ToastCallback<Likes>(context) {
            override fun onSuccess(response: Likes) {
                var sum = response.likes.sumBy { it.cost?.toDoubleOrNull()?.toInt() ?: 0 }
                bindStory(sum)
                (rv.adapter as ContributorsAdapter).setData(response.likes)
            }
        })
    }


    override val containerView = R.layout.fragment_story_supporters

}