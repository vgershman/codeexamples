package app.tomato.mainpeople.ui.main

import android.content.Context
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import app.tomato.basic.help.screenWidth
import com.mainpeople.android.R

/**
 * Created by user on 16.08.17.
 */
class TabSpinnerAdapter(context: Context, var vp: ViewPager) : ArrayAdapter<String>(context, 0, context.resources.getStringArray(R.array.tab_values)) {

    var selected = -1

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view_ = convertView
        if (view_ == null) {
            view_ = LayoutInflater.from(context).inflate(R.layout.top_tab, parent, false)
        }
        view_!!.findViewById<TextView>(R.id.text1).isSelected = vp.currentItem == 2
        return view_
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view_ = convertView
        if (view_ == null) {
            view_ = LayoutInflater.from(context).inflate(R.layout.top_tab_dropdown, parent, false)
            var lp = view_.layoutParams
            lp.width = (screenWidth(context) / 3).toInt()
            view_.layoutParams = lp
        }
        view_!!.findViewById<TextView>(R.id.text1).text = getItem(position)
        view_.findViewById<TextView>(R.id.text1).isSelected = position == selected
        return view_
    }


}