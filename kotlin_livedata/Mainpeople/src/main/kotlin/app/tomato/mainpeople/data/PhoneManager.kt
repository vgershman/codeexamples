package app.tomato.mainpeople.data

import android.arch.lifecycle.LiveData
import android.content.Context
import android.provider.ContactsContract
import android.telephony.PhoneNumberUtils
import android.util.Log
import android.widget.Toast
import app.tomato.basic.data.UnitCallback
import com.google.i18n.phonenumbers.PhoneNumberUtil
import java.util.*

/**
 * Created by user on 19.11.15.
 */
object PhoneManager : LiveData<List<PhoneUser>>() {


    fun init(context: Context) {
        value = getPhones(context)
    }


    fun getPhones(context: Context): List<PhoneUser> {
        val contactDtos = ArrayList<PhoneUser>()
        try {
            val cr = context.contentResolver
            val cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)
            while (cursor!!.moveToNext()) {
                val contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null)
                var phoneList = ArrayList<String>()
                while (phones!!.moveToNext()) {
                    var number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    number = PhoneNumberUtil.normalizeDigitsOnly(number)
                    phoneList.add(number)
                }
                var name = cursor.getString(cursor.getColumnIndex("display_name"))
                var avatar = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI))
                if (name.contains(",")) {
                    val tmp = name.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    if (tmp.size > 0) {
                        val additional = tmp[1]
                        if (additional.contains("(") ||
                                additional.contains(")") ||
                                additional.contains("+")) {
                            name = tmp[0]
                        }
                    }
                }
                if (phoneList.size > 0) {
                    val phoneUser = PhoneUser(name, avatar, phoneList)
                    contactDtos.add(phoneUser)
                }
                phones.close()
            }
            cursor.close()

        } catch (ex: Exception) {
            Log.e("PhoneManager", ex.message)
        }
        return contactDtos.sortedBy { it.fullname }
    }
}

object ContactsManager : LiveData<ContactsDto>() {

    fun init(context: Context) {
        Rest.rest().contactList().enqueue(UnitCallback(context, { value = it }))
    }


}
