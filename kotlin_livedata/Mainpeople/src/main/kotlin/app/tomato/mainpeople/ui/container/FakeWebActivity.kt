package app.tomato.mainpeople.ui.container

import android.app.Activity
import android.content.Intent
import android.os.Bundle

/**
 * Created by user on 27.09.17.
 */
class FakeWebActivity : Activity() {

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        finish()
    }
}