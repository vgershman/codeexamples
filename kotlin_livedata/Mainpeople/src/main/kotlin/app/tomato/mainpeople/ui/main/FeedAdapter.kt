package app.tomato.mainpeople.ui.main

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.tomato.basic.help.screenWidth
import app.tomato.mainpeople.MentionHelper
import app.tomato.mainpeople.data.DeletedStoriesManager
import app.tomato.mainpeople.data.StoriesManager
import app.tomato.mainpeople.data.Story
import app.tomato.mainpeople.data.User
import app.tomato.mainpeople.ui.container.ImageViewerActivity
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.mainpeople.android.R
import com.squareup.picasso.Picasso
import im.ene.toro.ToroPlayer
import im.ene.toro.ToroUtil
import im.ene.toro.exoplayer.SimpleExoPlayerViewHelper
import im.ene.toro.media.PlaybackInfo
import im.ene.toro.widget.Container
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_feed.view.*


/**
 * Created by user on 16.08.17.
 */
class FeedAdapter(var forcePadding: Boolean = false,
                  var userClick: (user: User) -> Unit, var openStory: (story: Story) -> Unit,
                  var openLikes: (story: Story) -> Unit,
                  var openFund: (fundId: String) -> Unit,
                  var like: (story: Story) -> Unit,
                  var hashTag: (tag: String) -> Unit,
                  var mentionClick: (id: Int?) -> Unit) : RecyclerView.Adapter<FeedAdapter.VHStory>() {

    private var mData = ArrayList<Story>()

    override fun getItemCount() = mData.size


    override fun onBindViewHolder(holder: VHStory, position: Int) {
        (holder as VHStory).bind(mData[position], position == 0 && forcePadding)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHStory {
        return VHStory(LayoutInflater.from(parent.context).inflate(R.layout.item_feed, parent, false), userClick, openStory, openFund, openLikes, like, hashTag, mentionClick)
    }


    fun onStoryDeleted() {
        var data = ArrayList<Story>()
        data.addAll(mData)
        mData.clear()
        mData.addAll(data.filterNot { DeletedStoriesManager.value?.contains(it.id) ?: false })
        notifyDataSetChanged()
    }

    fun setData(force: Boolean = false, data: List<Story>) {
        if (force) {
            mData.clear()
        }
        mData.addAll(data)
        onStoryDeleted()
    }

    class VHStory(innerView: View, var userClick: (user: User) -> Unit, var openStory: (story: Story) -> Unit,
                  var openFund: (fundId: String) -> Unit, var openLikes: (story: Story) -> Unit, var like: (story: Story) -> Unit, var hashTag: (tag: String) -> Unit, var mentionClick: (id: Int?) -> Unit) : RecyclerView.ViewHolder(innerView), ToroPlayer {

        var helper: SimpleExoPlayerViewHelper? = null

        var mediaUri: Uri? = null


        override fun isPlaying(): Boolean {
            return helper != null && helper!!.isPlaying
        }

        override fun onContainerScrollStateChange(container: Container?, newState: Int) {

        }

        override fun getPlayerView(): SimpleExoPlayerView = itemView.player


        override fun pause() {
            if (helper != null) helper!!.pause()
        }

        override fun wantsToPlay(): Boolean {
            return ToroUtil.visibleAreaOffset(this, itemView.parent) >= 0.85
        }


        override fun play() {
            playerView.player.volume = 0f
            if (helper != null) helper!!.play()
        }

        override fun getCurrentPlaybackInfo(): PlaybackInfo {
            return if (helper != null) helper!!.latestPlaybackInfo else PlaybackInfo()
        }

        override fun release() {
            if (helper != null) {
                helper!!.release()
                helper = null
            }
        }


        override fun initialize(container: Container, playbackInfo: PlaybackInfo?) {
            if (helper == null && mediaUri != null) {
                helper = SimpleExoPlayerViewHelper(container, this, mediaUri)
            }
            if (helper != null) {
                helper!!.initialize(playbackInfo)
            }
        }


        override fun getPlayerOrder(): Int {
            return adapterPosition
        }

        var ivAvatar = innerView.ivAvatar
        var tvAuthor = innerView.tvAuthorName
        var tvInfo = innerView.tvPostDateTime
        var tvPay = innerView.btnPay
        var tvFund = innerView.tvFund
        var ivContent = innerView.ivContent
        var tvDesc = innerView.tvDesc
        var tvComments = innerView.tvCommentsCounter
        var tvLikesCounter = innerView.tvCounter
        var topPadding = innerView.pdTop
        var btnWithYou = innerView.btnWithYou


        fun bind(story: Story, showPadding: Boolean) {
            var contentLP = ivContent.layoutParams
            val size = screenWidth(itemView.context).toInt()
            contentLP.height = size
            contentLP.width = size
            ivContent.layoutParams = contentLP
            playerView.layoutParams = contentLP

            if (story.video != null) {
                mediaUri = Uri.parse(story.video)
                playerView.visibility = View.VISIBLE
                ivContent.visibility = View.GONE
                itemView.contentWrapper.setOnClickListener { toggleSound() }

            } else {
                playerView.visibility = View.GONE
                ivContent.visibility = View.VISIBLE
                ivContent.setOnClickListener { openFullscreen(story.content) }
            }

            itemView.userContainer.setOnClickListener { userClick(story.author!!) }
            tvAuthor.text = story.author?.getUserFullName()

            tvDesc.text = story.title
            tvDesc.setLinkTextColor(itemView.context.resources.getColor(R.color.blue))
            MentionHelper.Creator.create(itemView.context.resources.getColor(R.color.blue), object : MentionHelper.OnHashTagClickListener {
                override fun onHashTagClicked(hashTag: String) {
                    if (hashTag.startsWith("@")) {
                        var id = story.mentions?.get(hashTag)
                        mentionClick(id)
                    } else {
                        hashTag(hashTag.substring(1))
                    }
                }

            }).handle(tvDesc)

            Linkify.addLinks(tvDesc, Linkify.WEB_URLS)

            tvFund.text = story.fund_name
            tvFund.setOnClickListener { openFund(story.fund!!) }

            val collected = StoriesManager.get(story.id!!)?.collected ?: story.collected
            val commentsCount = StoriesManager.get(story.id!!)?.commentsCount ?: story.commentsCount
            val likesCount = StoriesManager.get(story.id!!)?.likesCount ?: story.likesCount


            tvPay.text = "$ " + collected.toString()
            tvComments.text = commentsCount.toString()
            tvLikesCounter.text = likesCount.toString() + " " + itemView.context.getString(R.string.lbl_contributors)
            itemView.btnPayContainer.setOnClickListener { openLikes(story) }
            btnWithYou.setOnClickListener { like(story) }
            if (story.City?.name != null) {
                tvInfo.text = story.City?.name + ", " + story.getCreatedNice()
            } else {
                tvInfo.text = story.getCreatedNice()
            }
            Picasso.get().load(story.author?.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
            Glide.with(ivContent.context).load(story.content).override(size, size).placeholder(R.drawable.placeholder).into(ivContent)
            // Picasso.with(ivContent.context).load(story.content).resize(size, s).placeholder(R.drawable.placeholder).into(ivContent)
            if (showPadding) {
                topPadding.visibility = View.VISIBLE
            } else {
                topPadding.visibility = View.GONE
            }
            itemView.storyContainer.setOnClickListener { openStory(story) }
        }


        private fun toggleSound() {
            if (isPlaying) {
                if (playerView.player.volume < 0.5f) {
                    playerView.player.volume = 0.6f
                } else {
                    playerView.player.volume = 0f
                }
            }
        }

        private fun openFullscreen(content: String?) {
            if (content != null) {
                val intent = Intent(itemView.context, ImageViewerActivity::class.java)
                intent.putExtra(ImageViewerActivity.EXTRA_URL, content)
                itemView.context.startActivity(intent)
                (itemView.context as Activity).overridePendingTransition(R.anim.anim_open, R.anim.anim_close)
            }
        }

    }
}