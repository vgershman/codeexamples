package app.tomato.mainpeople.ui.payments

import android.app.Activity
import android.graphics.Bitmap
import android.webkit.WebView
import android.webkit.WebViewClient
import app.tomato.basic.ui.basic.BasicFragment
import com.mainpeople.android.R
import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.fragment_web.*

/**
 * Created by user on 21.08.17.
 */

class WebFragment : BasicFragment() {

    var done=false

    override fun initViews() {
        btnClose.setOnClickListener { activity?.finish() }
        toolbar?.setNavigationOnClickListener { activity?.finish() }
        webView.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                YandexMetrica.reportEvent("onPageStarted", mapOf("url" to url))
                if (url == null) {
                    view?.stopLoading()
                    return
                }
                if (url.startsWith("mainpeople://") && url.contains("cancel")) {
                    onUserCancel()
                    view?.stopLoading()
                    return
                }
                if (url.startsWith("mainpeople://") && url.contains("success")) {
                    onSuccess()
                    view?.stopLoading()
                    return
                }
                super.onPageStarted(view, url, favicon)
            }
//
//            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
//                YandexMetrica.reportEvent("onOverride", mapOf("url" to url))
//                if (url == null) {
//                    return false
//                }
//                if (url.startsWith("mainpeople://") && url.contains("cancel")) {
//                    onUserCancel()
//                    return false
//                }
//                if (url.startsWith("mainpeople://") && url.contains("success")) {
//                    onSuccess()
//                    return false
//                }
//                webView.loadUrl(url)
//                return false
//            }
        }
        webView.settings.javaScriptEnabled = true
    }

    private fun onSuccess() {
        if(done){
            return
        }
        done=true
        activity?.setResult(Activity.RESULT_OK)
        activity?.finish()
    }

    private fun onUserCancel() {
        if(done){
            return
        }
        done=true
        activity?.setResult(Activity.RESULT_CANCELED)
        activity?.finish()
    }

    override fun initProperties() {
        super.initProperties()
        var url = arguments!!["url"] as String
        loadUrl(url)
    }

    private fun loadUrl(url: String) {
        webView.loadUrl(url)
    }


    override val containerView = R.layout.fragment_web

}