package app.tomato.mainpeople.data

import android.arch.lifecycle.LiveData
import android.content.Context
import app.tomato.basic.data.ToastCallback

/**
 * Created by user on 29.11.17.
 */
object SearchManager : LiveData<SearchResult>() {

    var trends: SearchResult? = null

    override fun onActive() {
        super.onActive()
        value = trends
    }

    fun search(query: String, context: Context) {
        if (query.isNullOrBlank()) {
            trends(context)
            return
        }
        Rest.rest().search(query).enqueue(object : ToastCallback<SearchResult>(context) {
            override fun onSuccess(response: SearchResult) {
                value = response
            }
        })
    }

    fun trends(context: Context) {
        if (trends != null) {
            value = trends
            return
        }
        Rest.rest().trends().enqueue(object : ToastCallback<SearchResult>(context) {
            override fun onSuccess(response: SearchResult) {
                response.trendings = true
                trends = response
                value = response

            }
        })
    }
}