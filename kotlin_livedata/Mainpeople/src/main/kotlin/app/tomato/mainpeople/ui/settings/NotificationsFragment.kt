package app.tomato.mainpeople.ui.settings

import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.*
import kotlinx.android.synthetic.main.fragment_notifications.*
import java.util.*

/**
 * Created by user on 14.08.17.
 */
class NotificationsFragment : BasicFragment() {

    var data = ArrayList<Notification>()
    var lastVisiblePosition = 0
    var timer: Timer? = null
    var loading = false

    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = NotificationsAdapter({ openUser(it) }, { openStory(it) })
        rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    calculateLastVisible()
                }
            }
        })
        NotificationsManager.observe(this, android.arch.lifecycle.Observer { bindData(it?.first) })
    }

    private fun bindData(list: List<GroupNotificationWrapper>?) {
        var notificationsList = list?.find { it.notifications!=null }
        if (notificationsList?.notifications!!.isNotEmpty()) {
            data.clear()
            data.addAll(notificationsList.notifications!!.sortedByDescending { it.created })
            (rv.adapter as NotificationsAdapter).setData(data)
            Handler().postDelayed({
                try {
                    rv.scrollBy(0, 1)
                } catch (ex: Exception) {
                }
            }, 100)
        }
    }

    private fun calculateLastVisible() {
        try {
            val lastVisible = (rv.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
            if (lastVisible > lastVisiblePosition) {
                lastVisiblePosition = lastVisible
                scheduleMarkRead()
            }
        } catch (ex: Exception) {

        }
    }

    override fun onPause() {
        super.onPause()
        if (timer != null) {
            timer!!.cancel()
        }
        markLastVisibleRead()
    }

    private fun scheduleMarkRead() {
        if (timer != null) {
            timer!!.cancel()
        }
        timer = kotlin.concurrent.timer("searcher", false, initialDelay = 1000L, period = 10000, action = {
            try {
                activity?.runOnUiThread {
                    try {
                        markLastVisibleRead()
                    } catch (ex: Exception) {
                    }
                }; timer?.cancel()
            } catch (ex: Exception) {
            }
        })
    }

    private fun markLastVisibleRead() {
        try {
            val notification = data[lastVisiblePosition]
            NotificationsManager.read(context, notification)
        } catch (ex: Exception) {
        }
    }


    private fun openUser(user: User?) {
        if (loading) return
        loading = true
        if (user?.id != null) {
            Rest.rest().user(user.id!!).enqueue(object : ToastCallback<User>(context) {
                override fun onSuccess(response: User) {
                    go(MyApp.Screens.USER, MyApp.Containers.SIMPLE, params = hashMapOf("user" to user))
                }
            })
        }

    }

    private fun openStory(story: Story) {
        if (loading) return
        loading = true

        if (story.id != null) {
            Rest.rest().story(story.id!!).enqueue(object : ToastCallback<Story>(context) {
                override fun onSuccess(response: Story) {
                    go(MyApp.Screens.STORY, MyApp.Containers.SIMPLE, params = hashMapOf("story" to response))
                    loading = false
                }

                override fun onError(error: Error?) {
                    super.onError(error)
                    loading = false
                }

                override fun onConnectionFailure() {
                    super.onConnectionFailure()
                    loading = false
                }
            })
        }
    }



    override val containerView = R.layout.fragment_notifications

}