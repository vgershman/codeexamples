package app.tomato.mainpeople.data

import android.app.Activity
import android.arch.lifecycle.LiveData
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.preference.PreferenceManager
import app.tomato.basic.data.AlertCallback
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.data.UnitCallback
import app.tomato.mainpeople.FcmManager
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.ui.container.SimpleActivity
import com.google.gson.internal.LinkedTreeMap
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File
import java.util.*
import kotlin.collections.HashSet


/**
 * Created by user on 06.06.17.
 */


object AuthManager : LiveData<AuthInfo>() {

    var logoutCalled = false

    fun init(context: Context) {
        var host = PreferenceManager.getDefaultSharedPreferences(context).getString("host", MyApp.Config.PROD_HOST)
        Rest.setNewHost(host)
        var unique = PreferenceManager.getDefaultSharedPreferences(context).getString("unique", null)
        if (unique == null) {
            unique = UUID.randomUUID().toString()
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString("unique", unique).apply()
        }
        var version: String? = null
        try {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            version = pInfo.versionName + " (" + pInfo.versionCode + ")"
        } catch (e: Exception) {
        }
        var cookies = PreferenceManager.getDefaultSharedPreferences(context).getStringSet("cookies", HashSet<String>()) as HashSet<String>
        value = AuthInfo(cookies, unique, version)
    }

    fun setCookies(cookies: HashSet<String>) {
        value!!.cookies = cookies
    }


    fun save(context: Context) {
        object : AsyncTask<Void, Void, Unit>() {
            override fun doInBackground(vararg p0: Void?) = PreferenceManager.getDefaultSharedPreferences(context).edit().putStringSet("cookies", value!!.cookies).apply()
        }.execute()
    }

    fun logout(context: Activity, force: Boolean = true) {
        if (logoutCalled) return
        logoutCalled = true
        if (force) {
            Rest.rest().logout().enqueue(object : ToastCallback<LinkedTreeMap<String, Any?>>(context) {
                override fun onSuccess(response: LinkedTreeMap<String, Any?>) {
                    logoutCalled = false
                    performLogout(context)
                }

                override fun onError(error: Error?) {
                    logoutCalled = false
                    performLogout(context)
                }

                override fun onConnectionFailure() {
                    logoutCalled = false
                    performLogout(context)
                }
            })
        } else {
            logoutCalled = false
            performLogout(context)
        }
    }

    private fun performLogout(context: Activity) {
        AccountManager.value = null
        value = null
        PreferenceManager.getDefaultSharedPreferences(context).edit().putStringSet("cookies", null).apply()
        killAll(context)
        context.startActivity(Intent(context, SimpleActivity::class.java))
    }

    private fun killAll(context: Activity) {
        context.finishAffinity()
    }


}


data class AuthInfo(var cookies: HashSet<String>, var unique: String, var version: String?) {
    fun isAuth(): Boolean {
        return cookies.size > 0
    }
}


object AccountManager : LiveData<User>() {


    fun init(context: Context) {
        loadUser(context)
    }


    fun login(context: Context, email: String, password: String, pushToken: String?) {
        Rest.rest().login(email, password, pushToken).enqueue(object : AlertCallback<User>(context) {
            override fun onSuccess(response: User) {
                AuthManager.save(context)
                value = response
            }
        })
    }

    fun register(context: Context, name: String, surname: String, sex: Int, email: String, password: String, city: String, pushToken: String?) {
        Rest.rest().register(name, surname, sex, email, password, city, pushToken).enqueue(object : AlertCallback<User>(context) {
            override fun onSuccess(response: User) {
                AuthManager.save(context)
                value = response
            }
        })
    }

    fun loadUser(context: Context) {
        Rest.rest().me().enqueue(object : ToastCallback<User>(context) {
            override fun onSuccess(response: User) {
                value = response
            }
        })
    }

    fun updateCity(context: Context, placeId: String) {
        Rest.rest().update(value?.name!!, value?.surname!!, value?.gender!!, placeId, FcmManager.value).enqueue(object : ToastCallback<User>(context) {
            override fun onSuccess(response: User) {
                loadUser(context)
            }

        })
    }

    fun update(context: Context, name: String, surname: String, sex: Int) {
        Rest.rest().update(name, surname, sex, "", FcmManager.value!!).enqueue(object : ToastCallback<User>(context) {
            override fun onSuccess(response: User) {
                loadUser(context)
            }
        })
    }

    fun uploadAvatar(context: Context, media: String?) {
        var imageBody = RequestBody.create(MediaType.parse("image/jpeg"), File(media))
        Rest.rest().uploadAvatar(imageBody).enqueue(object : ToastCallback<LinkedTreeMap<String, Any?>>(context) {
            override fun onSuccess(response: LinkedTreeMap<String, Any?>) {
                loadUser(context)
            }
        })

    }

    fun updatePushToken(context: Context, token: String?) {
        if (token != null && AccountManager.value != null) {
            Rest.rest().update(AccountManager.value!!.name!!, AccountManager.value!!.surname!!, AccountManager.value!!.gender!!, "", token).enqueue(object : ToastCallback<User>(context) {
                override fun onSuccess(response: User) {
                    loadUser(context)
                }
            })
        }
    }

    fun confirm(context: Context) {
        Rest.rest().confirmTerms(1).enqueue(UnitCallback(context, { loadUser(context) }))
    }


}