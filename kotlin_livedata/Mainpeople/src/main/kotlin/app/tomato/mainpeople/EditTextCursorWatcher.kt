package app.tomato.mainpeople

import android.content.Context
import android.util.AttributeSet
import android.widget.EditText
import android.widget.Toast

class EditText_ : EditText {

    var selectionListener: SelectionListener? = null

    constructor(context: Context, attrs: AttributeSet,
                defStyle: Int) : super(context, attrs, defStyle)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context) : super(context)


    override fun onSelectionChanged(selStart: Int, selEnd: Int) {
        selectionListener?.onSelectionChanged(selStart, selEnd)

    }

    interface SelectionListener {
        fun onSelectionChanged(start: Int, end: Int)
    }
}