package app.tomato.mainpeople.data

import android.support.annotation.Keep
import app.tomato.basic.data.AddCookiesInterceptor
import app.tomato.basic.data.ReceivedCookiesInterceptor
import app.tomato.basic.data.RestApi
import app.tomato.mainpeople.MyApp.Config.Companion.HOST
import com.google.gson.internal.LinkedTreeMap
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by user on 06.06.17.
 */
@Keep
interface API {

    companion object {
        const val VERSION = "api/2.0/"
    }

    @GET(VERSION + "search/mentions")
    fun suggestMentions(@Query("query") query: String? = null, @Query("post_id") postId: String? = null): Call<Mentions>

    @GET(VERSION + "user/suggestFriends")
    fun contactList(): Call<ContactsDto>


    @FormUrlEncoded
    @POST(VERSION + "user/invitations")
    fun invite(@Field("phone") phone: String): Call<LinkedTreeMap<String, Any?>>

    @FormUrlEncoded
    @POST(VERSION + "user/contacts")
    fun contacts(@Field("contacts") contacts: List<String>): Call<LinkedTreeMap<String, Any?>>

    @Multipart
    @POST(VERSION + "user/review/sendMail")
    fun feedback(@Part("review") feedback: RequestBody, @Part("file\"; filename=\"image.jpg") image: RequestBody?): Call<LinkedTreeMap<String, Any?>>


    @POST(VERSION + "post/reportAbuse/{id}")
    fun report(@Path("id") id: String): Call<LinkedTreeMap<String, Any?>>

    @GET(VERSION + "tools/phone/startValidate")
    fun phoneAskCode(@Query("phone") phone: String): Call<LinkedTreeMap<String, Any?>>

    @GET(VERSION + "tools/phone/finishValidate")
    fun confirmPhone(@Query("phone") phone: String, @Query("code") code: String): Call<LinkedTreeMap<String, Any?>>


    @FormUrlEncoded
    @POST(VERSION + "forgetPassword")
    fun restoreByEmail(@Field("email") email: String): Call<LinkedTreeMap<String, Any?>>


    @FormUrlEncoded
    @POST(VERSION + "forgetPassword/restore")
    fun restoreStep2(@Field("email") email: String, @Field("token") token: String, @Field("password") password: String): Call<LinkedTreeMap<String, Any?>>

    @GET(VERSION + "tools/licenceAgreement")
    fun getLicense(@Query("placeId") city: String): Call<License>


    @FormUrlEncoded
    @POST(VERSION + "user/login")
    fun login(@Field("login") login: String, @Field("password") password: String, @Field("token") token: String?): Call<User>


    @FormUrlEncoded
    @POST(VERSION + "user")
    fun register(@Field("name") name: String, @Field("surname") surname: String, @Field("gender") sex: Int,
                 @Field("email") email: String, @Field("password") password: String, @Field("placeId") city: String, @Field("token") token: String?): Call<User>


    @Multipart
    @POST(VERSION + "user/avatar")
    fun uploadAvatar(@Part("avatar\"; filename=\"image.jpg") image: RequestBody): Call<LinkedTreeMap<String, Any?>>

    @FormUrlEncoded
    @PUT(VERSION + "user")
    fun update(@Field("name") name: String, @Field("surname") surname: String, @Field("gender") sex: Int,
               @Field("placeId") city: String, @Field("token") token: String?): Call<User>

    @FormUrlEncoded
    @PUT(VERSION + "user")
    fun confirmTerms(@Field("terms_confirmed") flag: Int): Call<User>


    @GET(VERSION + "user/{id}")
    fun user(@Path("id") id: String): Call<User>

    @GET(VERSION + "user/logout")
    fun logout(): Call<LinkedTreeMap<String, Any?>>

    @GET(VERSION + "feed/world")
    fun feedWorld(@Query("page") page: Int): Call<List<Story>>

    @GET(VERSION + "feed/top/{param}")
    fun feedTop(@Path("param") param: String, @Query("page") page: Int): Call<List<Story>>

    @GET(VERSION + "feed/friends")
    fun feedFriends(@Query("page") page: Int): Call<List<Story>>

    @GET(VERSION + "user/fund/{id}")
    fun fund(@Path("id") id: String): Call<Fund>

    @GET(VERSION + "user/fund/list")
    fun listFunds(): Call<List<Fund>>

    @GET(VERSION + "user/fund/list?groupped=true")
    fun listFundsGrouped(): Call<List<GrouppedFund>>

    @GET(VERSION + "feed/{user}/profile")
    fun feedUser(@Path("user") userId: String, @Query("page") page: Int): Call<List<Story>>

    @GET(VERSION + "feed/{user}/profile?order=collected")
    fun feedUserCollected(@Path("user") userId: String): Call<List<Story>>

    @GET(VERSION + "post/{id}/comment")
    fun listComments(@Path("id") storyId: String): Call<Comments>


    @GET(VERSION + "post/{id}")
    fun story(@Path("id") storyId: String): Call<Story>

    @POST(VERSION + "post/{id}/comment")
    fun sendComment(@Path("id") storyId: String, @Body comment: SendComment): Call<Comment>


    @FormUrlEncoded
    @POST(VERSION + "im")
    fun sendMessage(@Field("userIds[0]") storyId: String, @Field("content") message: String): Call<Message>


    @GET(VERSION + "like/{id}/detailed")
    fun listContributors(@Path("id") storyId: String): Call<Likes>

    @GET(VERSION + "relation/followLists/{id}?followers=true")
    fun listFollowers(@Path("id") userId: String): Call<Followers>


    @GET(VERSION + "relation/followLists/{id}?following=true")
    fun listFollowings(@Path("id") userId: String): Call<Followings>


    @GET(VERSION + "statistic/user/top/collection")
    fun listTopPeople(): Call<LinkedTreeMap<String, Ranking>>

    @GET(VERSION + "im/group")
    fun listGroups(): Call<List<Group>>


    @GET(VERSION + "im/group/{id}/messages")
    fun listMessages(@Path("id") groupId: String): Call<List<Message>>


    @FormUrlEncoded
    @POST(VERSION + "relation/{id}")
    fun relateUser(@Path("id") userId: String, @Field("type") relation: Int): Call<Relation>


    @GET(VERSION + "relation/{id}")
    fun getRelation(@Path("id") userId: String): Call<List<Relation>>


    @GET(VERSION + "im/unreadCount")
    fun notificationsCount(): Call<NotificationsCount>

    @GET(VERSION + "notifications")
    fun listNotifications(): Call<List<Notification>>


    @FormUrlEncoded
    @POST(VERSION + "im/group/{id}/read")
    fun readMessage(@Path("id") groupId: String, @Field("messageId") id: String): Call<LinkedTreeMap<String, Any?>>

    @FormUrlEncoded
    @POST(VERSION + "notifications/read")
    fun readNotification(@Field("id") id: Int): Call<LinkedTreeMap<String, Any?>>

    @FormUrlEncoded
    @POST(VERSION + "notifications/read")
    fun readExactNotifications(@Field("id[]") ids: ArrayList<Int>): Call<LinkedTreeMap<String, Any?>>

    @GET(VERSION + "search")
    fun search(@Query("query") query: String): Call<SearchResult>

    @GET(VERSION + "search/trends")
    fun trends(): Call<SearchResult>


    @GET(VERSION + "user")
    fun me(): Call<User>


    @GET(VERSION + "card")
    fun listCards(): Call<List<Card>>


    @PUT(VERSION + "post/{id}")
    fun editPost(@Path("id") postId: String, @Body body: EditPost): Call<LinkedTreeMap<String, Any?>>

    @DELETE(VERSION + "post/{id}")
    fun deletePost(@Path("id") postId: String): Call<LinkedTreeMap<String, Any?>>

    @DELETE(VERSION + "card/{id}")
    fun deleteCard(@Path("id") cardId: String): Call<LinkedTreeMap<String, Any?>>

    @FormUrlEncoded
    @POST(VERSION + "like/{id}")
    fun like(@Path("id") storyId: String, @Field("cost") cost: Int): Call<TryLike>


    @FormUrlEncoded
    @POST(VERSION + "payment/pay")
    fun fillAccount(@Field("amount") amount: String, @Field("card_id") cardId: String?): Call<UserFillWrapper>


    @Multipart
    @POST(VERSION + "post")
    fun createPost(@Part("title") title: RequestBody, @Part("type") type: Int,
                   @Part("mentions") mentions: HashMap<String, Int?>?,
                   @Part("cost") cost: Int, @Part("fund") fundId: Int,
                   @Part("image\"; filename=\"image.jpg") image: RequestBody?): Call<StoryPayWrapper>

    @Multipart
    @POST(VERSION + "post")
    fun createVideoPost(@Part("title") title: RequestBody, @Part("type") type: Int,
                        @Part("mentions") mentions: HashMap<String, Int?>?,
                        @Part("cost") cost: Int, @Part("fund") fundId: Int,
                        @Part("video\"; filename=\"video.mp4") image: RequestBody?): Call<StoryPayWrapper>

    @GET(VERSION + "tools/localizationRef?version=1")
    fun getLocalization(@Query("lang") lang: String): Call<Localization>
}

data class SendComment(var content: String, var mentions: HashMap<String, Int?>? = null)

data class EditPost(var title: String, var mentions: HashMap<String, Int?>? = null)


object Rest : RestApi<API>() {
    override fun init() {
        if (host == null) {
            host = HOST
        }
        clazz = API::class.java
        addInterceptor(AddCookiesInterceptor())
        addInterceptor(ReceivedCookiesInterceptor())

    }

}



