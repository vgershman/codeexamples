package app.tomato.mainpeople.ui.container

import android.app.Activity
import android.arch.lifecycle.Observer

import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.AsyncTask
import android.os.Handler
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.ui.basic.TabsAdapter
import app.tomato.mainpeople.MyApp.Screens.Companion.FEEDS
import app.tomato.mainpeople.MyApp.Screens.Companion.ME
import app.tomato.mainpeople.MyApp.Screens.Companion.MESSAGES
import app.tomato.mainpeople.MyApp.Screens.Companion.SEARCH
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.activity_tabbed.*
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TableLayout
import app.tomato.mainpeople.FcmManager
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.MyApp.Screens.Companion.NEW_POST
import app.tomato.mainpeople.MyApp.Screens.Companion.NEW_SEARCH
import app.tomato.mainpeople.MyApp.Screens.Companion.SELECT_MEDIA
import app.tomato.mainpeople.data.*
import com.getbase.floatingactionbutton.FloatingActionsMenu
import com.mainpeople.android.R.id.*
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation


/**
 * Created by user on 15.08.17.
 */
class TabbedActivity : BottomSheetActivity() {


    override fun onBackerTouch() {
        super.onBackerTouch()
        if (multiple_actions.isExpanded) {
            multiple_actions.collapse()
        }
    }


    override fun bottomView() = bottom_sheet!!

    override fun backerView() = backer!!

    var counter: TextView? = null

    var profileAvatar: ImageView? = null

    override fun init() {
        super.init()
        AccountManager.init(this)
        AccountManager.observe(this, Observer { bindUser(it) })
        FcmManager.observe(this, Observer { AccountManager.updatePushToken(this, it) })
        NotificationsManager.observe(this, Observer { toggleCounter(it?.second) })
        initFAB()
        vpFragments.adapter = TabsAdapter(supportFragmentManager, screens = arrayOf(FEEDS, NEW_SEARCH, MESSAGES, ME))
        tabs.setupWithViewPager(vpFragments)
        vpFragments.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                if (position == 3) {
                    supportFragmentManager.findFragmentById(R.id.fragmentMe).view?.visibility = View.VISIBLE
                } else {
                    supportFragmentManager.findFragmentById(R.id.fragmentMe).view?.visibility = View.GONE
                }
            }

        })
        setupTabIcons()
        NotificationsManager.update(this)
        proceedLauncherParams()
    }

    private fun proceedLauncherParams() {
        if (intent != null && intent.extras != null) {
            var type = intent.extras.get("type") as String?
            var postId = intent.extras.get("post_id") as String?
            var commentId = intent.extras.get("comment_id") as String?
            var userId = intent.extras.get("user_id") as String?
            var notification_ids = intent.extras.get("notification_ids") as String?

            when (type?.toIntOrNull() ?: 0) {
                4, 5, 6, 7, 13 -> {
                    openPost(postId, commentId)
                    markNotificationRead(notification_ids)
                }
                1, 2, 3, 10 -> {
                    openUser(userId)
                    markNotificationRead(notification_ids)
                }
                9 -> {
                    openNotifications()
                }
            }
        }
    }

    private fun markNotificationRead(notification_ids: String?) {
        var ids = ArrayList<Int>()
        notification_ids?.split(",")?.forEach {
            var intId = it.toIntOrNull(); if (intId != null) {
            ids.add(intId)
        }
        }
        NotificationsManager.readExact(this, ids)
    }

    private fun openNotifications() {
        go(MyApp.Screens.NOTIFICATIONS, MyApp.Containers.SIMPLE)
    }

    private fun openUser(userId: String?) {
        if (userId != null) {
            Rest.rest().user(userId).enqueue(object : ToastCallback<User>(this) {
                override fun onSuccess(response: User) {
                    go(MyApp.Screens.USER, MyApp.Containers.SIMPLE, params = hashMapOf("user" to response))
                }
            })
        }
    }


    private fun openPost(postId: String?, commentId: String?) {
        if (postId != null) {
            Rest.rest().story(postId).enqueue(object : ToastCallback<Story>(this) {
                override fun onSuccess(response: Story) {
                    go(MyApp.Screens.STORY, MyApp.Containers.SIMPLE, params = hashMapOf("story" to response, "comment" to commentId))
                }
            })
        }
    }

    private fun toggleCounter(count: Int?) {
        counter?.text = count.toString()
        if (count == 0) {
            counter?.visibility = View.GONE
        } else {
            counter?.visibility = View.VISIBLE
        }
    }

    private fun initFAB() {
        multiple_actions.setOnFloatingActionsMenuUpdateListener(object : FloatingActionsMenu.OnFloatingActionsMenuUpdateListener {
            override fun onMenuCollapsed() {
                backerView().visibility = View.GONE
            }

            override fun onMenuExpanded() {
                backerView().visibility = View.VISIBLE
            }
        })
        action_a.setOnClickListener { multiple_actions.collapse(); startVideo() }
        action_b.setOnClickListener { multiple_actions.collapse(); startPhoto() }
    }

    private fun startPhoto() {
        Handler().postDelayed({ this.runOnUiThread { go(SELECT_MEDIA, MyApp.Containers.SIMPLE, result = 54) } }, 400L)

    }

    private fun startVideo() {

        Handler().postDelayed({ this.runOnUiThread { go(SELECT_MEDIA, MyApp.Containers.SIMPLE, result = 54, params = hashMapOf("video" to true)) } }, 400L)


    }

    private fun bindUser(user: User?) {
        if (profileAvatar != null) {
            Picasso.get().load(user?.avatar_url).fit().placeholder(R.drawable.ic_launcher).transform(CropCircleTransformation()).into(profileAvatar)
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 54 && resultCode == Activity.RESULT_OK && data != null) {
            val media = data.getStringExtra("media")
            val isVideo = data.getBooleanExtra("video", false)
            if (media != null) {
                go(MyApp.Screens.NEW_POST, MyApp.Containers.SIMPLE, params = hashMapOf("media" to media, "video" to isVideo))
            }
        }
    }

    private fun setupTabIcons() {
        tabs.getTabAt(0)?.icon = resources.getDrawable(R.drawable.menu_feed)
        tabs.getTabAt(1)?.icon = resources.getDrawable(R.drawable.menu_search)
        var tabMessages = LayoutInflater.from(this).inflate(R.layout.tab_messages, null, false)
        tabMessages.layoutParams = TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        tabMessages.setPadding(0, 0, 0, 0)
        counter = tabMessages.findViewById(R.id.tvNotificationsCount)
        tabs.getTabAt(2)?.customView = tabMessages
        var tabProfile = LayoutInflater.from(this).inflate(R.layout.tab_profile, null, false)
        profileAvatar = tabProfile.findViewById(R.id.profileAvatar)
        tabProfile.layoutParams = TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        tabProfile.setPadding(0, 0, 0, 0)
        tabs.getTabAt(3)?.customView = tabProfile
        bindUser(AccountManager.value)
    }

    override val contentViewId = R.layout.activity_tabbed

}