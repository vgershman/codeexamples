package app.tomato.mainpeople.ui.container

import android.app.Activity
import android.content.Intent
import android.support.design.widget.BottomSheetBehavior
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import app.tomato.basic.ui.basic.BasicFragmentActivity
import app.tomato.mainpeople.data.GPayUtil

/**
 * Created by user on 21.08.17.
 */

abstract class BottomSheetActivity : BasicFragmentActivity() {


    var afterWebCallback = {}

    var bottomMenuShown = false

    override fun init() {
        backerView().setOnTouchListener { _, _ -> onBackerTouch(); true }
        BottomSheetBehavior.from(bottomView()).setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN || newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    backerView().visibility = View.GONE
                    bottomMenuShown = false
                }
            }

        })
    }

    protected open fun onBackerTouch() {
        backerView().visibility = View.GONE
        BottomSheetBehavior.from(bottomView()).state = BottomSheetBehavior.STATE_HIDDEN
    }


    protected abstract fun bottomView(): RecyclerView

    protected abstract fun backerView(): View

    fun bottomMenu(adapter: BottomAdapter) {
        bottomView().layoutManager = LinearLayoutManager(this)
        bottomView().adapter = adapter
        backerView().visibility = View.VISIBLE
        bottomMenuShown = true
        BottomSheetBehavior.from(bottomView()).state = BottomSheetBehavior.STATE_EXPANDED
    }

    fun hideBottomMenu() {
        bottomMenuShown = false
        BottomSheetBehavior.from(bottomView()).state = BottomSheetBehavior.STATE_HIDDEN
        backerView().visibility = View.GONE
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == webRequestCode && resultCode == Activity.RESULT_OK) {
            if (afterWebCallback != null) {
                afterWebCallback()
            }
        }
        if (requestCode == GPayUtil.GPAY_REQUEST && resultCode == Activity.RESULT_OK) {
            if (afterWebCallback != null) {
                afterWebCallback()
            }
        }
    }

    var webRequestCode: Int = 0


    override fun onBackPressed() {
        if (bottomMenuShown) {
            hideBottomMenu()
        } else {
            super.onBackPressed()
        }
    }
}


abstract class BottomAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>()