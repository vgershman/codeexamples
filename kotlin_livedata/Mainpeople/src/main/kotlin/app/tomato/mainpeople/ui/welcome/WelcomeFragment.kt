package app.tomato.mainpeople.ui.welcome

import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.fragment_welcome.*

/**
 * Created by user on 14.08.17.
 */
class WelcomeFragment : BasicFragment() {
    override fun initViews() {
        btnLogin.setOnClickListener { go(MyApp.Screens.LOGIN) }
        btnRegister.setOnClickListener { go(MyApp.Screens.REGISTRATION) }
    }

    override fun onFragmentBackPressed(): Boolean {
        activity?.finish()
        return false
    }

    override val containerView = R.layout.fragment_welcome

}