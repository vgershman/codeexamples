package app.tomato.mainpeople.ui.profile

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.data.*
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.fragment_list.*

/**
 * Created by user on 14.08.17.
 */
class FundAboutFragment : BasicFragment() {

    override fun initViews() {
        lv.layoutManager = LinearLayoutManager(context)
        lv.adapter = FundAboutAdapter((parentFragment as FundProfileFragment).fund!!, { openUrl(it) }, { openUser(it) })
        loadData()
    }

    private fun loadData() {
        Rest.rest().listFollowings((parentFragment as FundProfileFragment).fund?.id!!).enqueue(object : ToastCallback<Followings>(context) {
            override fun onSuccess(response: Followings) {

                (lv.adapter as FundAboutAdapter).setData(response.following.map { it.relation_user!! })

            }
        })
    }

    private fun openUser(user: User?) {
        go(MyApp.Screens.USER, MyApp.Containers.SIMPLE, params = hashMapOf("user" to user))
    }

    private fun openUrl(it: String?) {
        go(MyApp.Screens.WEB, MyApp.Containers.SIMPLE, params = hashMapOf("url" to it))
    }

    override val containerView = R.layout.fragment_list


}