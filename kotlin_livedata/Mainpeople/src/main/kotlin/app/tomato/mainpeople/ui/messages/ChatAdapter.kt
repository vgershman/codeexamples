package app.tomato.mainpeople.ui.messages

import android.support.v7.widget.RecyclerView
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.tomato.mainpeople.MentionHelper
import app.tomato.mainpeople.data.AccountManager
import app.tomato.mainpeople.data.Message
import app.tomato.mainpeople.data.User
import com.mainpeople.android.R
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation

/**
 * Created by user on 18.08.17.
 */
class ChatAdapter(var userClick: (user: User) -> Unit, var hashTagClick: (tag: String) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {


    private var data = ArrayList<Message>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 0) {
            return VHMy(LayoutInflater.from(parent?.context).inflate(R.layout.item_message_my, parent, false))
        }
        if (viewType == 1) {
            return VHUser(LayoutInflater.from(parent?.context).inflate(R.layout.item_message_user, parent, false))
        }
        if (viewType == 2) {
            return VHPadding(LayoutInflater.from(parent?.context).inflate(R.layout.item_message_footer, parent, false))
        }
        return VHMy(TextView(parent?.context))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VHMy) {
            holder.bind(data[position])
        }
        if (holder is VHUser) {
            holder.bind(data[position])
        }
    }

    override fun getItemCount() = data.size + 1

    override fun getItemViewType(position: Int): Int {
        if (position == data.size) return 2
        if (data[position].author?.id == AccountManager.value?.id) return 0
        if (data[position].author?.id != AccountManager.value?.id) return 1
        return -1
    }

    inner class VHMy(innerView: View) : RecyclerView.ViewHolder(innerView) {


        var tvTime = innerView.findViewById<TextView>(R.id.tvTime)
        var tvMessage = innerView.findViewById<TextView>(R.id.tvContent)


        fun bind(message: Message) {
            tvTime.text = message.getCreatedNice()
            tvMessage.text = message.content
            tvMessage.setLinkTextColor(itemView.context.resources.getColor(R.color.blue))
            MentionHelper.Creator.create(itemView.context.resources.getColor(R.color.blue), object : MentionHelper.OnHashTagClickListener {
                override fun onHashTagClicked(hashTag: String) {
                    hashTagClick(hashTag.substring(1))
                }

            }).handle(tvMessage)

            Linkify.addLinks(tvMessage, Linkify.WEB_URLS)

        }

    }

    inner class VHPadding(innerView: View) : RecyclerView.ViewHolder(innerView)

    inner class VHUser(innerView: View) : RecyclerView.ViewHolder(innerView) {


        var tvTime = innerView.findViewById<TextView>(R.id.tvTime)
        var tvMessage = innerView.findViewById<TextView>(R.id.tvContent)
        var ivAvatar = innerView.findViewById<ImageView>(R.id.ivAvatar)


        fun bind(message: Message) {
            Picasso.get().load(message.author?.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
            ivAvatar.setOnClickListener { userClick(message.author!!) }
            tvTime.text = message.getCreatedNice()
            tvMessage.text = message.content
            tvMessage.setLinkTextColor(itemView.context.resources.getColor(R.color.blue))
            MentionHelper.Creator.create(itemView.context.resources.getColor(R.color.blue), object : MentionHelper.OnHashTagClickListener {
                override fun onHashTagClicked(hashTag: String) {
                    hashTagClick(hashTag.substring(1))
                }

            }).handle(tvMessage)
            Linkify.addLinks(tvMessage, Linkify.WEB_URLS)
        }

    }

    fun setData(response: List<Message>) {
        data.clear()
        data.addAll(response)
        notifyDataSetChanged()
    }
}