package app.tomato.mainpeople.ui.settings

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.Toast
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.AccountManager
import app.tomato.mainpeople.data.Rest
import com.bumptech.glide.Glide
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.fragment_feedback.*
import kotlinx.android.synthetic.main.item_feedback_image.view.*
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File

/**
 * Created by user on 01.09.17.
 */
class FeedbackFragment : BasicFragment() {

    private var images = ArrayList<String>()

    override fun initViews() {
        btnClose.setOnClickListener { onBackPressed() }
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        btnSend.setOnClickListener { validate() }
        btnAddImage.setOnClickListener { addImage() }
    }

    private fun addImage() {
        go(MyApp.Screens.SELECT_MEDIA, MyApp.Containers.SIMPLE, result = 54, from = this)
    }

    private fun validate() {
        val feedback = etFeedback.text.toString()
        if (feedback.length < 3) {
            Toast.makeText(context, getString(R.string.error_feedback_short), Toast.LENGTH_SHORT).show()
            return
        }
        sendFeedback(feedback)
    }

    private fun sendFeedback(feedback: String) {
        hideKeyboard(etFeedback, context)
        var textBody = RequestBody.create(MediaType.parse("text/plain"), feedback)
        var body: RequestBody? = null
        if (images.size > 0) {
            body = RequestBody.create(MediaType.parse("image/jpeg"), File(images[0]))
        }
        Rest.rest().feedback(textBody, body).enqueue(object : ToastCallback<LinkedTreeMap<String, Any?>>(context) {
            override fun onSuccess(response: LinkedTreeMap<String, Any?>) {
                onBackPressed()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 54 && resultCode == Activity.RESULT_OK && data != null) {
            val media = data.getStringExtra("media")
            if (media != null) {
                images.clear()
                images.add(media)
                bindImages()
            }
        }
    }

    private fun bindImages() {
        imageContainer.removeAllViews()
        images.forEach { addImageView(it) }
    }

    private fun addImageView(media: String) {
        var view = LayoutInflater.from(context).inflate(R.layout.item_feedback_image, imageContainer, false)
        Glide.with(context).load(media).into(view.ivFeedback)
        view.btnDelete.setOnClickListener { images.remove(media);bindImages() }
        imageContainer.addView(view)
    }

    override val containerView = R.layout.fragment_feedback

}