package app.tomato.mainpeople.ui.main

import android.support.v7.widget.RecyclerView
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.tomato.mainpeople.MentionHelper
import app.tomato.mainpeople.data.Comment
import app.tomato.mainpeople.data.StoriesManager
import app.tomato.mainpeople.data.Story
import app.tomato.mainpeople.data.User
import com.mainpeople.android.R
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_comment.view.*
import kotlinx.android.synthetic.main.story_desc.view.*

/**
 * Created by user on 16.08.17.
 */
class CommentsAdapter(var story: Story, var userClick: (user: User) -> Unit, var likesClick: () -> Unit, var hashTagClick: (tag: String) -> Unit, var mentionClick: (id: Int?) -> Unit, var nameClicked: (user: User?) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val data = ArrayList<Comment>()

    fun updateStory(newStory: Story) {
        story = newStory
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 0) {
            return VHeader(LayoutInflater.from(parent.context).inflate(R.layout.story_desc, parent, false))
        }
        if (viewType == 1) {
            return VH(LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false))
        }
        if (viewType == 2) {
            return VHPadding(LayoutInflater.from(parent.context).inflate(R.layout.item_comment_footer, parent, false))
        }
        return VHPadding(TextView(parent.context))
    }

    override fun getItemCount() = data.size + 2


    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return 0
        }
        if (position == itemCount - 1) {
            return 2
        }
        return 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VH) {
            holder.bind(data[position - 1], position == data.size)
        }
        if (holder is VHeader) {
            holder.bind()
        }
    }

    inner class VHPadding(innerView: View) : RecyclerView.ViewHolder(innerView)

    inner class VHeader(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var tvDesc = innerView.tvDesc
        var tvLikes = innerView.tvCounter
        var tvComments = innerView.tvCommentsCounter
        var btnSupport = innerView.btnSupport

        fun bind() {

            val commentsCount = StoriesManager.get(story.id!!)?.commentsCount ?: story.commentsCount
            val likesCount = StoriesManager.get(story.id!!)?.likesCount ?: story.likesCount

            tvDesc.text = story.title
            tvDesc.setLinkTextColor(itemView.context.resources.getColor(R.color.blue))
            MentionHelper.Creator.create(itemView.context.resources.getColor(R.color.blue), object : MentionHelper.OnHashTagClickListener {
                override fun onHashTagClicked(hashTag: String) {
                    if (hashTag.startsWith("@")) {
                        var id = story.mentions?.get(hashTag)
                        mentionClick(id)
                    } else {
                        hashTagClick(hashTag.substring(1))
                    }
                }

            }).handle(tvDesc)
            Linkify.addLinks(tvDesc, Linkify.WEB_URLS)

            tvLikes.text = likesCount.toString() + " " + itemView.context.getString(R.string.lbl_contributors)
            btnSupport.setOnClickListener { likesClick() }
            tvComments.text = commentsCount.toString()
        }


    }


    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var ivAvatar = innerView.ivAvatar
        var tvAuthor = innerView.tvAuthorName
        var tvInfo = innerView.tvCommentInfo
        var tvComment = innerView.tvComment
        var padding = innerView.pdBottom

        fun bind(comment: Comment, showPadding: Boolean) {
            Picasso.get().load(comment.author?.avatar_url).placeholder(R.drawable.ic_launcher).fit().transform(CropCircleTransformation()).into(ivAvatar)
            tvAuthor.text = comment.author?.getUserFullName()
            tvAuthor.setOnClickListener { nameClicked(comment.author!!) }
            ivAvatar.setOnClickListener { userClick(comment.author!!) }
            tvComment.text = comment.content
            tvComment.setLinkTextColor(itemView.context.resources.getColor(R.color.blue))
            MentionHelper.Creator.create(itemView.context.resources.getColor(R.color.blue), object : MentionHelper.OnHashTagClickListener {
                override fun onHashTagClicked(hashTag: String) {
                    if (hashTag.startsWith("@")) {
                        var id = comment.mentions?.get(hashTag)
                        mentionClick(id)
                    } else {
                        hashTagClick(hashTag.substring(1))
                    }
                }

            }).handle(tvComment)
            Linkify.addLinks(tvComment, Linkify.WEB_URLS)



            tvInfo.text = comment.getCreatedNice()
            if (showPadding) {
                padding.visibility = View.VISIBLE
            } else {
                padding.visibility = View.GONE
            }
        }

    }

    fun setData(comments: List<Comment>) {
        data.clear()
        data.addAll(comments)
        notifyDataSetChanged()
    }
}