package app.tomato.mainpeople.data

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import java.io.File

/**
 * Created by user on 24.08.17.
 */
object ImagesManager {


    fun listGallery(context: Context): ArrayList<String> {
        var result = ArrayList<String>()
        var imageCursor: Cursor? = null
        try {
            val columns = arrayOf(MediaStore.Images.Media.DATA, MediaStore.Images.ImageColumns.ORIENTATION)
            val orderBy = MediaStore.Images.Media.DATE_ADDED + " DESC"
            imageCursor = context.contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy)
            if (imageCursor != null) {
                while (imageCursor.moveToNext()) {
                    result.add(imageCursor.getString(imageCursor.getColumnIndex(MediaStore.Images.Media.DATA)))
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (imageCursor != null && !imageCursor.isClosed) {
                imageCursor.close()
            }
        }
        return result
    }

    fun listVideos(context: Context): ArrayList<String> {
        var result = ArrayList<String>()
        var imageCursor: Cursor? = null
        try {
            val columns = arrayOf(MediaStore.Video.Media.DATA)
            val orderBy = MediaStore.Video.Media.DATE_ADDED + " DESC"
            imageCursor = context.contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy)
            if (imageCursor != null) {
                while (imageCursor.moveToNext()) {
                    result.add(imageCursor.getString(imageCursor.getColumnIndex(MediaStore.Video.Media.DATA)))
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (imageCursor != null && !imageCursor.isClosed) {
                imageCursor.close()
            }
        }
        return result
    }

}