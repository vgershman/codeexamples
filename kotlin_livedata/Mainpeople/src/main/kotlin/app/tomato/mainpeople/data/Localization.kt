package app.tomato.mainpeople.data

import android.arch.lifecycle.LiveData
import android.content.Context
import app.tomato.basic.data.ToastCallback
import java.util.*

/**
 * Created by user on 31.08.17.
 */
object LocalizationManager : LiveData<Localization>() {

    fun init(context: Context) {
        Rest.rest().getLocalization(Locale.getDefault().language.toLowerCase())
                .enqueue(object : ToastCallback<Localization>(context) {
                    override fun onSuccess(response: Localization) {
                        value = response
                    }
                })
    }

    fun getEntry(key: String?) = value?.entries?.get(key)
    fun getError(key: String?) = value?.errors?.get(key)

}