package app.tomato.mainpeople.ui.search

import android.arch.lifecycle.Observer
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import app.tomato.basic.data.ToastCallback
import app.tomato.mainpeople.MyApp
import app.tomato.mainpeople.data.*
import app.tomato.mainpeople.ui.post.SelectFundsAdapter
import com.mainpeople.android.R
import kotlinx.android.synthetic.main.fragment_search_.*

/**
 * Created by user on 14.08.17.
 */
class SearchFundsFragment : BaseSearchFragment() {


    override fun initViews() {
        rv.layoutManager = LinearLayoutManager(context)
        SearchManager.observe(this, Observer { bindData(it) })
    }

    override fun bindData(data: SearchResult?) {
        if (data == null || data.trendings) {
            loadData()
            return
        } else {
            if (data.fund.result.isNotEmpty()) {
                var results = SearchResult(fund = data.fund)
                rv.adapter = searchAdapter
                rv.visibility = View.VISIBLE
                lblError.visibility = View.GONE
                searchAdapter.setData(results)
            } else {
                showNotFound(R.string.funds_not_found)
            }
        }
    }

    private fun loadData() {
        Rest.rest().listFundsGrouped().enqueue(object : ToastCallback<List<GrouppedFund>>(context) {
            override fun onSuccess(response: List<GrouppedFund>) {
                rv.visibility = View.VISIBLE
                rv.adapter = SelectFundsAdapter(true, { bindFund(it) }, { openFund(it) }, context, response)
            }

        })
    }

    private fun openFund(it: Fund) {
        go(MyApp.Screens.FUND, MyApp.Containers.SIMPLE, params = hashMapOf("fund" to it))
    }

    private fun bindFund(it: Fund) {
        go(MyApp.Screens.FUND, MyApp.Containers.SIMPLE, params = hashMapOf("fund" to it))
    }

    override val containerView = R.layout.fragment_search_

}