package app.tomato.mainpeople

import android.support.multidex.MultiDexApplication
import app.tomato.mainpeople.ui.container.DarkActivity
import app.tomato.mainpeople.ui.container.SimpleActivity
import app.tomato.mainpeople.ui.container.TabbedActivity
import app.tomato.mainpeople.ui.main.FeedTabsFragment
import app.tomato.mainpeople.ui.main.StoryFragment
import app.tomato.mainpeople.ui.main.StorySupportersFragment
import app.tomato.mainpeople.ui.media.SelectMediaFragment
import app.tomato.mainpeople.ui.media.TrimFragment
import app.tomato.mainpeople.ui.media.TuneFragment
import app.tomato.mainpeople.ui.messages.ChatFragment
import app.tomato.mainpeople.ui.messages.ChatListFragment
import app.tomato.mainpeople.ui.payments.WebFragment
import app.tomato.mainpeople.ui.post.EditPostFragment
import app.tomato.mainpeople.ui.post.NewPostFragment
import app.tomato.mainpeople.ui.post.SelectFundsFragment
import app.tomato.mainpeople.ui.profile.*
import app.tomato.mainpeople.ui.search.*
import app.tomato.mainpeople.ui.settings.*
import app.tomato.mainpeople.ui.welcome.*
import com.github.hiteshsondhi88.libffmpeg.FFmpeg
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig


class MyApp : MultiDexApplication() {


    override fun onCreate() {
        super.onCreate()
        YandexMetrica.activate(applicationContext, YandexMetricaConfig.newConfigBuilder("fb3f5d67-0662-483b-a177-74dfc477bba8")
                .build())
        YandexMetrica.enableActivityAutoTracking(this)
        try {
            FFmpeg.getInstance(applicationContext).loadBinary(object : LoadBinaryResponseHandler() {

                override fun onStart() {}

                override fun onFailure() {}

                override fun onSuccess() {}

                override fun onFinish() {}
            })
        } catch (e: FFmpegNotSupportedException) {
        }

    }

    interface Config {
        companion object {
            val HOST = "https://dev.mainpeople.com/"
            val PROD_HOST = "https://mainpeople.com/"
            val SUPPORT_COST = arrayListOf("1", "5", "10", "100")
            val FILL_AMOUNTS = arrayListOf("10", "20", "50", "100", "1000")
            val PDF_VIEW = "http://drive.google.com/viewerng/viewer?embedded=true&url="

        }
    }

    interface Screens {
        companion object {
            //WELCOME
            val SLIDER = SliderFragment::class.java
            val WELCOME = WelcomeFragment::class.java
            //AUTH
            val LOGIN = LoginFragment::class.java
            val REGISTRATION = RegistrationFragment::class.java
            val PASSWORD = PasswordFragment::class.java
            val FORG0T = ForgotPassword::class.java
            val RESTORE = RestorePassword::class.java
            val INPUT_PHONE = InputPhoneFragment::class.java
            val CONFIRM_PHONE = ConfirmPhoneFragment::class.java
            //STORY
            val FEEDS = FeedTabsFragment::class.java
            val STORY = StoryFragment::class.java
            val SUPPORTERS = StorySupportersFragment::class.java
            //SEARCH
            val NEW_SEARCH = NewSearchFragment::class.java
            val FUNDS_SEARCH = NewSearchFragment::class.java
            val SEARCH = SearchFragment::class.java
            val MODAL_SEARCH = ModalSearchFragment::class.java
            val FUNDS = FundsFragment::class.java
            val POST_SEARCH = SearchPostsFragment::class.java
            //NOTIFICATIONS
            val MESSAGES = ChatListFragment::class.java
            val NOTIFICATIONS = NotificationsFragment::class.java
            val CHAT = ChatFragment::class.java
            //PROFILE
            val ME = MyProfileFragment::class.java
            val USER = UserProfileFragment::class.java
            val FUND = FundProfileFragment::class.java
            val EDIT_ME = EditProfileFragment::class.java

            //MEDIA
            val SELECT_MEDIA = SelectMediaFragment::class.java
            val TUNE = TuneFragment::class.java
            val TRIM = TrimFragment::class.java

            //CREATE_POST
            val NEW_POST = NewPostFragment::class.java
            val EDIT_POST = EditPostFragment::class.java
            //OTHER
            val FOLLOWING = FollowingsFragment::class.java
            val FOllOWERS = FollowersFragment::class.java
            val RANKINGS = RankingsFragment::class.java
            val WEB = WebFragment::class.java
            val SETTINGS = SettingsFragment::class.java
            val CARDS = EditPaymentFragment::class.java
            val SELECT_FUNDS = SelectFundsFragment::class.java
            val FEEDBACK = FeedbackFragment::class.java
            val INVITE = InviteFragment::class.java


        }
    }

    interface Containers {
        companion object {
            val SIMPLE = SimpleActivity::class.java
            val DARK = DarkActivity::class.java
            val TABBED = TabbedActivity::class.java

        }
    }
}

