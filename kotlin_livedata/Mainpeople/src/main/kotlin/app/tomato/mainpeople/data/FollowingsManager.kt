package app.tomato.mainpeople.data

import android.arch.lifecycle.LiveData
import android.content.Context
import app.tomato.basic.data.UnitCallback

/**
 * Created by user on 01.09.17.
 */
object FollowingsManager : LiveData<List<User>>() {

    fun init(context: Context) {
        if (AccountManager.value != null) {
            Rest.rest().listFollowings(AccountManager.value!!.id!!).enqueue(UnitCallback(context, {
                value = it.following.map { it.relation_user!! }
            }))
        }
    }

}