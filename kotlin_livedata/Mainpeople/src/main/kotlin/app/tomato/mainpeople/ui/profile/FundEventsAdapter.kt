package app.tomato.mainpeople.ui.profile

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mainpeople.android.R
import app.tomato.mainpeople.data.Campaign
import app.tomato.mainpeople.data.Fund
import kotlinx.android.synthetic.main.item_campaign.view.*


/**
 * Created by user on 16.08.17.
 */
class FundEventsAdapter(var fund: Fund) : RecyclerView.Adapter<FundEventsAdapter.VH>() {

    override fun getItemCount() = fund.campaigns.size


    override fun onBindViewHolder(holder: VH, position: Int) {
        holder?.bind(fund.campaigns[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_campaign, parent, false))
    }


    inner class VH(innerView: View) : RecyclerView.ViewHolder(innerView) {

        var tvTitle = innerView.tvTitle
        var tvDesc = innerView.tvDesc

        fun bind(campaign: Campaign) {
            tvTitle.text = campaign.title
            tvDesc.text = campaign.title + "\n" + campaign.title
        }
    }


}