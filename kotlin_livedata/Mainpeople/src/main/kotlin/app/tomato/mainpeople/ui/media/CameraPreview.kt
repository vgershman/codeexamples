package app.tomato.mainpeople.ui.media

import android.app.Activity
import android.content.Context
import android.hardware.Camera
import android.util.Log
import android.view.Surface
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View



class CameraPreview(mContext: Context, private val mCamera: Camera, var cameraId: Int) : SurfaceView(mContext), SurfaceHolder.Callback {
    private val mHolder: SurfaceHolder
    private val mSupportedPreviewSizes: List<Camera.Size>? = mCamera.parameters.supportedPreviewSizes
    private var mPreviewSize: Camera.Size? = null

    fun getSurface(): Surface {
        return mHolder.surface
    }

    init {

        for (str in mSupportedPreviewSizes!!)
            Log.e(TAG, str.width.toString() + "/" + str.height)

        mHolder = holder
        mHolder.addCallback(this)
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, w: Int, h: Int) {
        Log.e(TAG, "surfaceChanged => w=$w, h=$h")
        if (mHolder.surface == null) {
            return
        }
        try {
            mCamera.stopPreview()
        } catch (e: Exception) {

        }

        try {
            val parameters = mCamera.parameters
            parameters.setPreviewSize(mPreviewSize!!.width, mPreviewSize!!.height)
            parameters.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
            mCamera.parameters = parameters
            setCameraDisplayOrientation(cameraId, mCamera)
            mCamera.setPreviewDisplay(mHolder)
            mCamera.startPreview()

        } catch (e: Exception) {
        }

    }

    fun setCameraDisplayOrientation(cameraId: Int, camera: Camera) {
        val info = Camera.CameraInfo()
        Camera.getCameraInfo(cameraId, info)
        val rotation = (context as Activity).windowManager.defaultDisplay.rotation
        var degrees = 0
        when (rotation) {
            Surface.ROTATION_0 -> degrees = 0
            Surface.ROTATION_90 -> degrees = 90
            Surface.ROTATION_180 -> degrees = 180
            Surface.ROTATION_270 -> degrees = 270
        }


        var result: Int
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360
            result = (360 - result) % 360  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360
        }

      //  val parameters = camera.parameters
      //  parameters.setRotation(result)
      //  camera.parameters = parameters
        camera.setDisplayOrientation(result)
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = View.resolveSize(suggestedMinimumWidth, widthMeasureSpec)
        val height = View.resolveSize(suggestedMinimumHeight, heightMeasureSpec)

        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height)
        }

        if (mPreviewSize != null) {
            val ratio: Float
            if (mPreviewSize!!.height >= mPreviewSize!!.width)
                ratio = mPreviewSize!!.height.toFloat() / mPreviewSize!!.width.toFloat()
            else
                ratio = mPreviewSize!!.width.toFloat() / mPreviewSize!!.height.toFloat()

            // One of these methods should be used, second method squishes preview slightly
            setMeasuredDimension(width, (width * ratio).toInt())
            //        setMeasuredDimension((int) (width * ratio), height);
        }
    }


    private fun getOptimalPreviewSize(sizes: List<Camera.Size>?, w: Int, h: Int): Camera.Size? {
        val ASPECT_TOLERANCE = 0.3
        val targetRatio = h.toDouble() / w

        if (sizes == null)
            return null

        var optimalSize: Camera.Size? = null
        var minDiff = java.lang.Double.MAX_VALUE

        val targetHeight = h

        for (size in sizes) {
            val ratio = size.height.toDouble() / size.width
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue

            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size
                minDiff = Math.abs(size.height - targetHeight).toDouble()
            }
        }

        if (optimalSize == null) {
            minDiff = java.lang.Double.MAX_VALUE
            for (size in sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size
                    minDiff = Math.abs(size.height - targetHeight).toDouble()
                }
            }
        }

        return optimalSize
    }

    companion object {

        private val TAG = "CameraPreview"
    }
}