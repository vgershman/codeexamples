package app.tomato.mainpeople.ui.profile

import android.arch.lifecycle.Observer
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import app.tomato.basic.ui.basic.BasicFragment
import com.mainpeople.android.R
import app.tomato.mainpeople.data.AccountManager
import kotlinx.android.synthetic.main.fragment_edit_profile.*

/**
 * Created by user on 30.08.17.
 */
class EditProfileFragment : BasicFragment() {


    override fun initViews() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        btnClose.setOnClickListener { onBackPressed() }
        btnSave.setOnClickListener { validate() }
        AccountManager.observe(this, Observer { bind() })
        val adapter = ArrayAdapter.createFromResource(activity, R.array.sex_array, R.layout.item_sex_selection_)
        adapter.setDropDownViewResource(R.layout.item_sex_drop)
        spinnerSex.adapter = adapter
        spinnerSex.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            }
        }
    }

    private fun validate() {
        var name = etName.text.toString()
        if (name.replace(Regex.fromLiteral(" "), "").length < 2) {
            Toast.makeText(context, getString(R.string.error_short_name), Toast.LENGTH_SHORT).show()
            return
        }
        var surname = etLastName.text.toString()
        if (surname.replace(Regex.fromLiteral(" "), "").length < 2) {
            Toast.makeText(context, getString(R.string.error_surname_short), Toast.LENGTH_SHORT).show()
            return
        }
        var email = etEmail.text.toString()
        if (!isValidEmail(email)) {
            Toast.makeText(context, getString(R.string.error_wrong_email), Toast.LENGTH_SHORT).show()
            return
        }
        var phone = etPhone.text.toString()
        if (phone.length > 0) {
            if (!isValidPhone(phone)) {
                Toast.makeText(context, getString(R.string.error_wrong_phone), Toast.LENGTH_SHORT).show()
                return
            }
        }
        AccountManager.update(context, name, surname, Math.abs(1 - spinnerSex.selectedItemPosition))
        activity?.finish()

    }

    fun isValidEmail(target: String): Boolean {
        if (TextUtils.isEmpty(target)) {
            return false
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

    fun isValidPhone(target: String): Boolean {
        return android.util.Patterns.PHONE.matcher(target).matches()
    }

    private fun bind() {
        etName.setText(AccountManager.value?.name)
        etLastName.setText(AccountManager.value?.surname)
        etEmail.setText(AccountManager.value?.email)
        spinnerSex.setSelection(AccountManager.value?.gender!!)
        if(AccountManager.value?.phone!=null){
            etPhone.setText(AccountManager.value?.phone)
            llPhone.visibility = View.VISIBLE
        }
    }

    override val containerView = R.layout.fragment_edit_profile

}