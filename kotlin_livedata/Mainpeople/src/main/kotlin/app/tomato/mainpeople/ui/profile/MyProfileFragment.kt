package app.tomato.mainpeople.ui.profile

import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.basic.ui.basic.TabsAdapter
import com.mainpeople.android.R
import app.tomato.mainpeople.data.AccountManager
import app.tomato.mainpeople.data.User
import app.tomato.mainpeople.ui.main.UserCollectedFeedFragment
import app.tomato.mainpeople.ui.main.UserFeedFragment
import app.tomato.mainpeople.ui.main.UserGridFeedFragment
import kotlinx.android.synthetic.main.fragment_me.*

/**
 * Created by user on 14.08.17.
 */
class MyProfileFragment : BasicFragment(), ProfileFragment {

    override fun getCurrentUser() = AccountManager.value!!


    override fun initViews() {
        vpProfile.adapter = TabsAdapter(childFragmentManager, screens = arrayOf(UserFeedFragment::class.java, UserGridFeedFragment::class.java, UserCollectedFeedFragment::class.java))
        tabs1.setupWithViewPager(vpProfile)
        setupTabIcons()
    }

    private fun setupTabIcons() {
        tabs1.getTabAt(0)?.icon = resources.getDrawable(R.drawable.ic_user_feed)
        tabs1.getTabAt(1)?.icon = resources.getDrawable(R.drawable.ic_user_grid)
        tabs1.getTabAt(2)?.icon = resources.getDrawable(R.drawable.ic_user_sort)
    }


    override val containerView = R.layout.fragment_me

}