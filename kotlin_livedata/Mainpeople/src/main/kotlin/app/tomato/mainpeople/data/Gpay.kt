package app.tomato.mainpeople.data

import android.app.Activity
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.wallet.*
import java.util.*


/**
 * Created by user on 01.06.18.
 */


object GPayUtil {

    private var paymentsClient: PaymentsClient? = null

    val GPAY_REQUEST = 745

    fun setup(activity: Activity) {
        if (paymentsClient == null) {
            paymentsClient = Wallet.getPaymentsClient(activity, Wallet.WalletOptions.Builder()
                    .setEnvironment(WalletConstants.ENVIRONMENT_TEST).build())
        }
    }

    fun isReadyToPay() {
        val request = IsReadyToPayRequest.newBuilder()
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                .build()
        val task = paymentsClient?.isReadyToPay(request)
        task?.addOnCompleteListener { task ->
            try {
                val result = task.getResult(ApiException::class.java)

            } catch (exception: ApiException) {
            }
        }
    }

    private fun createPaymentDataRequest(price: Int): PaymentDataRequest {
        val request = PaymentDataRequest.newBuilder()
                .setTransactionInfo(
                        TransactionInfo.newBuilder()
                                .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
                                .setTotalPrice(price.toString())
                                .setCurrencyCode("USD")
                                .build())
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                .setCardRequirements(
                        CardRequirements.newBuilder()
                                .addAllowedCardNetworks(
                                        Arrays.asList(
                                                WalletConstants.CARD_NETWORK_AMEX,
                                                WalletConstants.CARD_NETWORK_DISCOVER,
                                                WalletConstants.CARD_NETWORK_VISA,
                                                WalletConstants.CARD_NETWORK_MASTERCARD))
                                .build())

        val params = PaymentMethodTokenizationParameters.newBuilder()
                .setPaymentMethodTokenizationType(
                        WalletConstants.PAYMENT_METHOD_TOKENIZATION_TYPE_PAYMENT_GATEWAY)
                .addParameter("gateway", "example")
                .addParameter("gatewayMerchantId", "exampleGatewayMerchantId")
                .build()

        request.setPaymentMethodTokenizationParameters(params)
        return request.build()
    }

    fun pay(price: Int, activity: Activity) {
        createPaymentDataRequest(price).let {
            AutoResolveHelper.resolveTask(paymentsClient?.loadPaymentData(it)!!, activity, GPAY_REQUEST)
        }
    }


}