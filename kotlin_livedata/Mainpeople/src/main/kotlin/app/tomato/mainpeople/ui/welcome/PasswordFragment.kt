package app.tomato.mainpeople.ui.welcome

import android.arch.lifecycle.Observer
import android.widget.Toast
import app.tomato.basic.data.ToastCallback
import app.tomato.basic.help.hideKeyboard
import app.tomato.basic.ui.basic.BasicFragment
import app.tomato.mainpeople.FcmManager
import app.tomato.mainpeople.MyApp
import com.mainpeople.android.R
import app.tomato.mainpeople.data.*
import kotlinx.android.synthetic.main.fragment_password.*

/**
 * Created by user on 29.08.17.
 */
class PasswordFragment : BasicFragment() {


    private var pushToken : String? = null
    private var name: String? = null
    private var surname: String? = null
    private var password: String? = null
    private var sex: Int? = null
    private var email: String? = null
    private var city: String? = null
    private var alreadyStarted = false

    override fun initViews() {
        btnLicense.setOnClickListener { getLicense() }
        btnDone.setOnClickListener { validate() }
        AccountManager.observe(this, Observer { proceed(it) })
        FcmManager.observe(this, Observer { pushToken = it })
    }

    private fun proceed(user: User?) {
        if (user != null && !alreadyStarted) {
            alreadyStarted = true
            LocalizationManager.init(context)
            AuthManager.save(context)
            go(null, MyApp.Containers.TABBED, finish = true)
        }
    }

    private fun validate() {
        hideKeyboard(etPassword, context)
        password = etPassword.text.toString()
        var confirm = etConfirm.text.toString()
        if (password!!.length < 6) {
            Toast.makeText(context, getString(R.string.error_password_short), Toast.LENGTH_SHORT).show()
            return
        }
        if (!confirm.equals(password)) {
            Toast.makeText(context, getString(R.string.error_password_match), Toast.LENGTH_SHORT).show()
            return
        }
        AccountManager.register(context, name!!, surname!!, sex!!, email!!, password!!, city!!, pushToken)
    }


    private fun getLicense() {
        if (city != null) {
            Rest.rest().getLicense(city!!).enqueue(object : ToastCallback<License>(context) {
                override fun onSuccess(response: License) {
                    openLicense(response.url)
                }
            })
        }
    }

    private fun openLicense(url: String?) {
        val finalUrl = MyApp.Config.PDF_VIEW + url
        go(MyApp.Screens.WEB, MyApp.Containers.SIMPLE, params = hashMapOf("url" to finalUrl))
    }

    override fun initProperties() {
        super.initProperties()
        name = arguments!!["name"] as String?
        surname = arguments!!["surname"] as String?
        sex = arguments!!["sex"] as Int?
        email = arguments!!["email"] as String?
        city = arguments!!["city"] as String?
    }

    override val containerView = R.layout.fragment_password
}